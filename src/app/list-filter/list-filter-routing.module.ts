import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListFilterPage } from './list-filter.page';

const routes: Routes = [
  {
    path: '',
    component: ListFilterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListFilterPageRoutingModule {}
