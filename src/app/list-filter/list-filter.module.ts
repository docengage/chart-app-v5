import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListFilterPageRoutingModule } from './list-filter-routing.module';

import { ListFilterPage } from './list-filter.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListFilterPageRoutingModule
  ],
  declarations: [ListFilterPage]
})
export class ListFilterPageModule {}
