import { Component, OnInit, Renderer2, ViewChild } from "@angular/core";
import { IonDatetime, ModalController, NavParams } from "@ionic/angular";
import * as moment from "moment";
import { Handlebar } from "../components/handlebar";
import { AuthService } from "../service/auth.service";
import { MasterDataService } from "../service/master-data";
import { ToastServiceData } from "../service/toast-service";
import { Todos } from "../service/todos";

@Component({
  selector: "app-list-filter",
  templateUrl: "./list-filter.page.html",
  styleUrls: ["./list-filter.page.scss"],
})
export class ListFilterPage implements OnInit {
  statusList: any;
  statusData: any;
  statusSec: boolean = false;
  stSelValue: any;

  enqSouSec = false;

  dateRange: any;
  dateRangeSec: boolean = false;

  isStatus = false;
  showDateFields = false;

  customDate: any;
  fromDate: any;
  toDate: any;

  showModal = false;
  presentation: any;
  formatedDate: any;
  fieldName: any;

  @ViewChild("dateRangeCc", { static: false }) dateRangeCc: any;
  @ViewChild("statusCc", { static: false }) statusCc: any;
  @ViewChild("sourceCc", { static: false }) sourceCc: any;

  @ViewChild(IonDatetime) datetime: IonDatetime;
  pageType: any;

  constructor(
    public authService: AuthService,
    public masterData: MasterDataService,
    private modalCtrl: ModalController,
    public todos: Todos,
    public navParams: NavParams,
    public renderer: Renderer2,
    public handlebar: Handlebar,
    private toastServ: ToastServiceData,
  ) {
  }

  ngOnInit() {}

  ionViewDidEnter() {
    this.statusData = this.navParams.get("statusData");
    this.dateRange = this.navParams.get("dateRange");
    this.pageType = this.navParams.get("pageType");
    this.fromDate = this.navParams.get("fromDate")
      ? this.navParams.get("fromDate")
      : undefined;
    this.toDate = this.navParams.get("toDate")
      ? this.navParams.get("toDate")
      : undefined;
    this.stSelValue = [];
    this.isStatus = this.statusData && this.statusData !== "all" ? true : false;
    this.showDateFields = this.dateRange === "customDate" ? true : false;
    this.getStatusList();
  }

  getStatusList() {
    this.statusList = [];
    this.statusList.data = this.stSelValue;
    let data;
    if (this.pageType === 'Schedule') {
      data = [
        "CheckedIn",
        "InProgress",
        "Completed",
        "Scheduled",
        "NoShows",
        "Pending"
      ];
    } else {
      data = [
        "Completed",
        "Scheduled"
      ]
    }
    
    for (let i = 0; i < data.length; i++) {
      const item = {
        statusVal: data[i],
        isChecked: false,
      };
      this.statusList.push(item);
    }

    if (this.pageType === 'Schedule') {
      this.masterData.storeDataInLocalDB(this.statusList, "ScheduleStatus");
    } else {
      this.masterData.storeDataInLocalDB(this.statusList, "ActivityStatus");
    }
    this.setData();
  }

  setData() {
    if (this.statusData !== undefined) {
      this.stSelValue = this.statusData.split(",");
      for (let i = 0; i < this.stSelValue.length; i++) {
        this.statusChecked(this.stSelValue[i]);
      }
    }
  }

  /* EXPANDING CARD CONTENT - START  */

  expandDate() {
    this.dateRangeSec = !this.dateRangeSec;
    if (this.dateRangeSec) {
      if (this.dateRangeCc) {
        this.renderer.setStyle(
          this.dateRangeCc.nativeElement,
          "max-height",
          "1000px"
        );
        this.renderer.setStyle(
          this.dateRangeCc.nativeElement,
          "background",
          "#eee"
        );
      }
    } else {
      if (this.dateRangeCc) {
        this.renderer.setStyle(
          this.dateRangeCc.nativeElement,
          "max-height",
          "0px"
        );
        this.renderer.setStyle(
          this.dateRangeCc.nativeElement,
          "background",
          "#fff"
        );
      }
    }
  }
  expandStatus() {
    this.statusSec = !this.statusSec;
    if (this.statusSec) {
      if (this.statusCc) {
        this.renderer.setStyle(
          this.statusCc.nativeElement,
          "max-height",
          "1000px"
        );
        this.renderer.setStyle(
          this.statusCc.nativeElement,
          "background",
          "#eee"
        );
      }
    } else {
      if (this.statusCc) {
        this.renderer.setStyle(
          this.statusCc.nativeElement,
          "max-height",
          "0px"
        );
        this.renderer.setStyle(
          this.statusCc.nativeElement,
          "background",
          "#fff"
        );
      }
    }
  }
  expandSource() {
    this.enqSouSec = !this.enqSouSec;
    if (this.enqSouSec) {
      if (this.sourceCc) {
        this.renderer.setStyle(
          this.sourceCc.nativeElement,
          "max-height",
          "1000px"
        );
        this.renderer.setStyle(
          this.sourceCc.nativeElement,
          "background",
          "#eee"
        );
      }
    } else {
      if (this.sourceCc) {
        this.renderer.setStyle(
          this.sourceCc.nativeElement,
          "max-height",
          "0px"
        );
        this.renderer.setStyle(
          this.sourceCc.nativeElement,
          "background",
          "#fff"
        );
      }
    }
  }

  /* EXPANDING CARD CONTENT - END  */

  /* SELECTING STATUS DATA  - START  */

  statusSel(value) {
    if (this.stSelValue.length === 0 || this.stSelValue[0] === "all") {
      this.stSelValue = [];
      this.stSelValue.push(value);
    } else {
      const index: number = this.stSelValue.indexOf(value);

      if (index > -1) {
        this.stSelValue.splice(index, 1);
      } else {
        this.stSelValue.push(value);
      }
    }
    this.isStatus =
      this.stSelValue.length > 0 && this.stSelValue[0] !== "all" ? true : false;
    this.statusChecked(value);
  }

  /* SELECTING STATUS DATA  - END  */

  statusChecked(value) {
    for (let i = 0; i < this.statusList.length; i++) {
      if (this.statusList[i].statusVal == value) {
        this.statusList[i].isChecked = !this.statusList[i].isChecked;
        break;
      }
    }
  }

  setDateModal(value, type, fieldName) {
    this.formatedDate = value
      ? moment(value, "YYYY-MM-DD").format()
      : moment().format();
    this[fieldName] = value ? value : moment().format("YYYY-MM-DD");
    this.presentation = type;
    this.fieldName = fieldName;
    this.showModal = true;
  }

  dateChange(value) {
    this[this.fieldName] = value
      ? moment(value, "YYYY-MM-DD").format("YYYY-MM-DD")
      : moment().format("YYYY-MM-DD");
    this.dateCancel();
  }

  dateCancel() {
    if (this.datetime) {
      this.showModal = false;
    }
  }

  dateValidation() {
    let errorMsg = '';
    if (this.dateRange === 'customDate') {
      if (this.fromDate) {
      } else {
        errorMsg = 'Please Enter From date.<br>';
      }
      
      if (this.toDate) {
      } else {
        errorMsg = errorMsg + 'Please Enter To date.<br>';
      }

      if (this.fromDate && this.toDate && (this.fromDate > this.toDate)) {
        errorMsg = errorMsg + 'From date should be less then To data.<br>';
      }
    }

    if (errorMsg) {
      this.toastServ.presentToast(errorMsg);
      return false;
    } else {
      return true;
    }
  }

  /* APPLYING THE SELECTED FILTER - START */

  save() {
    this.statusData = this.stSelValue.toString();
    if (this.handlebar.isEmpty(this.statusData)) {
      this.statusData = "all";
    }

    const isDateValid = this.dateValidation();
    if (isDateValid) {
      const data = {
        dateRange: this.dateRange,
        statusData: this.statusData,
        fromDate: this.fromDate,
        toDate: this.toDate,
      };
      this.dismiss(true, data);
    }
  }

  /* APPLYING THE SELECTED FILTER - END */

  dismiss(isUpdated, data?: any) {
    this.modalCtrl.dismiss(data, isUpdated);
  }
}
