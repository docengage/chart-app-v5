import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TaskItemDetailsPageRoutingModule } from './task-item-details-routing.module';

import { TaskItemDetailsPage } from './task-item-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TaskItemDetailsPageRoutingModule
  ],
  declarations: [TaskItemDetailsPage]
})
export class TaskItemDetailsPageModule {}
