import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { AuthService } from '../service/auth.service';
import { Handlebar } from '../components/handlebar';

@Component({
  selector: 'app-task-item-details',
  templateUrl: './task-item-details.page.html',
  styleUrls: ['./task-item-details.page.scss'],
})
export class TaskItemDetailsPage {

  icons: string[];
   apptId: String;
  item: any;
  patientInfo: any;

  noMoreRecords: string;
  constructor( public navParams: NavParams,
               private modalCtrl: ModalController,
               public authService: AuthService,
               public handlebar: Handlebar) {
    this.apptId = this.navParams.get('apptId');
  }
  ionViewDidEnter() {
    this.authService.isTokenValid().then((returnVal) => {
      this.getScheduleItems();
    });
  }

  getScheduleItems() {
    this.authService.getMyTaskItem(this.apptId).then((data) => {
        this.item = data.appointments[0];
        this.patientInfo = this.item.participants[0];
    });
  }

  updateStatus(status, plannedCareItemId) {
    this.authService.updateApptStatus(status, plannedCareItemId, 'event').then(() => {
      this.dismiss(true);
    });
  }
  dismiss(isUpdate) {
    this.modalCtrl.dismiss(isUpdate);
  }

}
