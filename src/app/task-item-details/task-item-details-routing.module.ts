import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TaskItemDetailsPage } from './task-item-details.page';

const routes: Routes = [
  {
    path: '',
    component: TaskItemDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TaskItemDetailsPageRoutingModule {}
