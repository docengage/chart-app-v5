import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../service/auth.service';
import { LocalStorageService } from '../service/local-storage.service';
import { Handlebar } from '../components/handlebar';
import { AlertController, LoadingController } from '@ionic/angular';
import { ToastServiceData } from '../service/toast-service';
import { Todos } from '../service/todos';
import { MasterDataService } from '../service/master-data';

@Component({
  selector: 'app-select-organization',
  templateUrl: './select-organization.page.html',
  styleUrls: ['./select-organization.page.scss'],
})
export class SelectOrganizationPage implements OnInit {
  loading: any;
  loginData = { loginId: '', password: '', organizationId : 0 };
  data: any;
  errorMessage: string;
  submitted: boolean = false;
  passwordType: string = 'password';
  showPass: boolean = false;
  isErrorExist: boolean = false;
  organizationList: any ;
  organizations: any;
  loginId: any;
  password :any ;
  constructor(
    private router: Router,
    private actvroute: ActivatedRoute,
    private authService: AuthService,
    private localstorage: LocalStorageService,
    private handlebar: Handlebar,
    private toastServ: ToastServiceData,
    private todos: Todos,
    private master: MasterDataService,
    private loadingCtrl: LoadingController
  ) { }

  ngOnInit() {
    this.loginData = { loginId: '', password: '', organizationId: 0, };
    this.actvroute.queryParams.subscribe(params => {
      if (params.loginId && params.password) {
        this.loginData.loginId = params.loginId;
        this.loginData.password = params.password;
        this.organizations = JSON.parse(params.organizationList);
      }
    });
  }

  async login() {

    if (this.handlebar.isNotEmpty(this.loginData.organizationId) ) {
    this.loading = await this.loadingCtrl.create({
      message: 'Authenticating...'
  });
    this.localstorage.setUserId(this.loginData.loginId);
    this.loading.present().then(() => {
        this.authService.login(this.loginData,'orgPage').then((result :any) => {
            this.isErrorExist = false;
            this.errorMessage = '';
            
            this.localstorage.setPassword(this.loginData.password);
            this.localstorage.setOrgId(this.loginData.organizationId);
            this.submitted = true;
            if (result && result[0] && result[0] === 'error') {
                this.loading.dismiss();
                this.toastServ.presentToast('Invalid Credentials');
            } else {
                this.todos.recreateDB();
                this.data = result;
                this.localstorage.setToken(this.data.access_token);
                setTimeout(() => {
                 this.loading.dismiss();
                 this.authService.getLoggedInUserInfo().then((data) => {
                        this.master.storeDataInLocalDB(data, 'loggedInProvider');
                        this.localstorage.setLoggedInProvider(data.uuid);
                        this.saveDeviceInformation(data.uuid);
                        this.authService.getProviderPermissions(data.uuid).then((accessData) => {
                            this.localstorage.setAccessControl(accessData);
                            const accessControl = accessData;
                            const data0 = {
                                accessControl
                            };
                            this.master.getAllMasterData();
                            if (data.uuid) {
                             this.router.navigate(['home']);
                            }
                       });
                });
            }, 100);
            }
        }, (err) => {
            let errMsg = err.json().description;
            if (!errMsg) {
                errMsg = 'Not able to login! Please check your internet connection or connection URL.';
            }
            this.isErrorExist = true;
            this.errorMessage = errMsg;

            this.loading.dismiss();
        });
    });
    } else {
        this.submitted = true;
        this.isErrorExist = true;
        this.toastServ.presentToast('Please select an organization');
    }
}
  saveDeviceInformation(uuid) {
    this.localstorage.getDeviceToken().then((deviceToken) => {
      if (deviceToken && uuid) {
        this.authService.saveDeviceInfo(deviceToken, uuid);
      }
    });
  }
  forceLogout() {
    this.localstorage.getUserId().then((userId) => {
      this.loginData.loginId = userId;
      if (this.handlebar.isEmpty(this.loginData.loginId)) {
        this.toastServ.presentToast('Please Provide login Id to do Force Signout!');
      } else {
        this.authService.forceLogout(this.loginData.loginId, this.loginData.organizationId).then(() => {
          this.isErrorExist = false;
          this.errorMessage = '';
          this.router.navigate(['login']).then(() => {
            window.location.reload();
          });
        });
      }
    });
  }

  dismiss(){
      this.router.navigate(['/home']);
  }

}
