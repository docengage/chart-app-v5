import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyChartsPage } from './my-charts.page';

describe('MyChartsPage', () => {
  let component: MyChartsPage;
  let fixture: ComponentFixture<MyChartsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyChartsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MyChartsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
