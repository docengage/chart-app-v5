import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyChartsPage } from './my-charts.page';

const routes: Routes = [
  {
    path: '',
    component: MyChartsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyChartsPageRoutingModule {}
