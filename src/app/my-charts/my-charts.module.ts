import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyChartsPageRoutingModule } from './my-charts-routing.module';

import { MyChartsPage } from './my-charts.page';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyChartsPageRoutingModule,
    PipesModule
  ],
  declarations: [MyChartsPage]
})
export class MyChartsPageModule {}
