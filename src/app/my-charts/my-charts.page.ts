import { AfterViewChecked, Component } from '@angular/core';
import { ModalController, AlertController, LoadingController, Platform, NavController } from '@ionic/angular';

import * as moment from 'moment';
import {Handlebar} from '../components/handlebar';
import { CallNumberService } from '../service/phone-call-service';
import { AuthService } from '../service/auth.service';
import { MasterDataService } from '../service/master-data';
import { ToastServiceData } from '../service/toast-service';
import { IonRouterOutlet } from '@ionic/angular';
import { PatientTimeLinePage } from '../patient-time-line/patient-time-line.page';
import { Router, NavigationExtras } from '@angular/router';
import { Todos } from '../service/todos';
import { TimeLineDataPushService } from '../service/timeline-auth-service';
import { LocalStorageService } from '../service/local-storage.service';
import { JoinVcPage } from '../videoCall/join-vc/join-vc.page';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { ListFilterPage } from '../list-filter/list-filter.page';

declare var window;
let options = {
  "no_driving_mode":true,
  "no_invite":true,
  "no_meeting_end_message":true,
  "no_titlebar":false,
  "no_bottom_toolbar":false,
  "no_dial_in_via_phone":true,
  "no_dial_out_to_phone":true,
  "no_disconnect_audio":true,
  "no_share":true,
  "no_audio":true,
  "no_video":true,
  "no_meeting_error_message":true
  };
@Component({

  selector: 'my-charts',
  templateUrl: './my-charts.page.html',
  styleUrls: ['./my-charts.page.scss'],
  providers: [
    CallNumberService
  ]
})
export class MyChartsPage implements AfterViewChecked {
   icons: string[];
  items: Array<any>;
  totalItems: String;
  totalPages: number = 0;
  currentPage = 1;

  apptSec = 'today';
  fromDate: string;
  toDate: string;
  noRecordFound: string;
  queryText: string;
  isShowSearchBar = false;
  searchVal: string;
  timer: any;
  storageId: any;
  accessControl: any;
  statusValues: any;

  constructor(private authService: AuthService, 
              private routerOutlet: IonRouterOutlet,
              private router: Router,
              private timelineAuthServ: TimeLineDataPushService,
              private localstorage: LocalStorageService,
              private toastServ: ToastServiceData,
              private handlebar: Handlebar,
              private masterDataServ: MasterDataService, private modalCtrl: ModalController,
              private alertCtrl: AlertController,
              private callNumberService: CallNumberService,
              private loadingCtrl: LoadingController,
              private androidPermissions: AndroidPermissions,
              private platform: Platform,
              private nav: NavController
              ) { 
                window.chartPage = this;
  }

  ngAfterViewChecked() {
    if(document.getElementById('tab-button-myCharts')) {
      document.getElementById('tab-button-myCharts').classList.add('tab-selected');
    }
    if(document.getElementById('tab-button-schedule')) {
      document.getElementById('tab-button-schedule').classList.remove('tab-selected');
    }
    if(document.getElementById('tab-button-allPatient')) {
      document.getElementById('tab-button-allPatient').classList.remove('tab-selected');
    }
    if(document.getElementById('tab-button-leaveList')) {
      document.getElementById('tab-button-leaveList').classList.remove('tab-selected');
    }
  }

  ionViewDidEnter() {
    this.apptSec = 'today';
    this.items = [];
    this.totalItems = '0';
    this.totalPages = 0;
    this.currentPage = 1;
    this.storageId = '';

    this.authService.isTokenValid().then(() => {
      this.localstorage.getAccessControl().then((accessObj) => {
        if (accessObj) {
          this.accessControl = accessObj;
          if (this.accessControl && this.accessControl.permissions.Appointments5674 && this.accessControl.permissions.Appointmentsview) {
            // this.getTodayApptList();
            this.getFilterData();
          } else {
            this.nav.navigateForward(['/home']);
          }
        } else {
          this.nav.navigateForward(['/login']);
        }
      });
      
    });
  }
  searchData(){
      clearTimeout(this.timer);
      if (this.searchVal != this.queryText) {

        const temp = this;
        this.timer = setTimeout(function() {
          temp.searchVal = temp.queryText;
          temp.updateSchedule();
        }, 400);
      }
  }
  isAllowVideoCall(item) {
    if (this.accessControl && this.accessControl.permissions && this.accessControl.permissions.Telehealth5674) {
      if (item.apptType === 'Video' && this.handlebar.isNotEmpty(item.videoCallSid)) {
        return true;
      }
      return false;
    }
    return false;
  }

  getFilterData(){
    this.localstorage.getScheduleFilter().then((filterdata) => {
      if (filterdata){
        this.statusValues = filterdata.statusValues;
        this.apptSec = (filterdata.apptSec === 'all' || this.handlebar.isEmpty(filterdata.apptSec)) ? 'today' : filterdata.apptSec;
        if (this.apptSec === 'customDate') {
          this.fromDate = filterdata.fromDate;
          this.toDate = filterdata.toDate;
        }
        this.updateSchedule();
      } else {
        this.getTodayApptList();
      }
      
    });
  }

  async filter() {
    let data0 = {};
    
    if (this.apptSec === 'customDate') {
      data0 = {
        pageType: 'Schedule',
        statusData: this.statusValues,
        dateRange: this.apptSec,
        fromDate: this.fromDate,
        toDate: this.toDate
      };
    } else {
      data0 = {
        pageType: 'Schedule',
        statusData: this.statusValues,
        dateRange: this.apptSec,
      };
    }
    const modal = await this.modalCtrl.create({
      component: ListFilterPage,
      componentProps: data0
    });
    
    await modal.present();
    modal.onDidDismiss().then((data) => {
      const isUpdate = data.role;
      if (isUpdate) {
        this.statusValues = data.data.statusData;
        this.apptSec = data.data.dateRange;
        let filterdata = {};

        if (this.apptSec === 'customDate') {
          this.fromDate = data.data.fromDate;
          this.toDate = data.data.toDate;
          filterdata = {
            statusValues: data.data.statusData,
            apptSec: data.data.dateRange,
            fromDate: this.fromDate,
            toDate: this.toDate
          };
        } else {
          filterdata = {
            statusValues: data.data.statusData,
            apptSec: data.data.dateRange,
          };
        }
        
        this.localstorage.setScheduleFilter(filterdata);
        this.updateSchedule();
      }
    });
  }

  updateSchedule() {
    this.resetList();
    if (this.apptSec === 'recent') {
      this.getRecentApptList();
    } else if (this.apptSec === 'today') {
      this.getTodayApptList();
    } else if (this.apptSec === 'upcoming') {
      this.getUpcomingApptList();
    } else {
      this.getCustomApptList();
    }
  }
  getTodayApptList(){
    this.fromDate =  moment().format('YYYY-MM-DD');
    this.toDate =  moment().format('YYYY-MM-DD');
    this.storageId = 'todayChartData';
    this.getAppointmentList(true, 'todayChartData');
  }
  getRecentApptList(){
    this.fromDate =  moment().subtract(7, 'day').format('YYYY-MM-DD');
    this.toDate =  moment().subtract(1, 'day').format('YYYY-MM-DD');
    this.storageId = 'recentChartData';
    this.getAppointmentList(true, 'recentChartData');
  }

  getUpcomingApptList() {
    this.fromDate =   moment().add(1, 'day').format('YYYY-MM-DD');
    this.toDate = moment().add(6, 'day').format('YYYY-MM-DD');
    this.storageId = 'futureChartdata';
    this.getAppointmentList(true, 'futureChartdata');
  }

  getCustomApptList() {
    this.storageId = 'customChartdata';
    this.getAppointmentList(true, 'customChartdata');
  }

  async startVideoCall(item) {
    // const data = {
    //   name: item.participants[0].name,
    //   roomId: item.videoCallSid
    // };
    // const startVideoCallModal = await this.modalCtrl.create({
    //     component: JoinVcPage,
    //     componentProps: data,
    //     id: 'joinVcModal'
    // });
    // await startVideoCallModal.present();

    if (item) {
      if (item.videoVendor === 'enableX' && this.handlebar.isNotEmpty(item.videoCallSid)) {
        // const navigationExtras: NavigationExtras = {
        //   queryParams: {
        //     name: item.participants[0].name,
        //     roomId: item.videoCallSid
        //   }
        // };
        // this.router.navigate(['/join-vc'], navigationExtras);
        const data = {
          name: item.participants[1].name,
          roomId: item.videoCallSid,
          vendorName: item.videoVendor
        };
        const startVideoCallModal = await this.modalCtrl.create({
          component: JoinVcPage,
          componentProps: data,
          id: 'joinVcModal'
        });
        await startVideoCallModal.present();

        return true;
      }
      else if (item.videoVendor === 'dyte' && this.handlebar.isNotEmpty(item.videoCallSid)) {
        if (this.platform.is('android')) {
          const permissions = [this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.RECORD_AUDIO]
          this.androidPermissions.requestPermissions(permissions).then(
            (result) => {
              if (!result.hasPermission) {
                this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
                  (result) => {
                    if (!result.hasPermission) {
                      this.toastServ.presentToast('You camera permission denied, unable to start video consultation.');
                    } else {
                      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.RECORD_AUDIO).then(
                        (result) => {
                          if (!result.hasPermission) {
                            this.toastServ.presentToast('You microphone permission denied, unable to start video consultation.');
                            return false;
                          } else {
                            this.startDyteMeeting(item);
                          }
                        }
                      )
                    }
                  }
                )
                return false;
              } else {
                this.startDyteMeeting(item);
              }

            });
        } else {
          this.startDyteMeeting(item);
        }

      } else {
        this.toastServ.presentToast('Invalid Vendor details, Please contact your admin.');
        return false;
      }
    } else {
      this.toastServ.presentToast('Something went wrong...');
      return false;
    }
  }

  async startDyteMeeting(item) {
    const providerName = item.participants[1].name;
    const meetingNode = JSON.parse(item.meetingNode);
    let meetingUrl = meetingNode.join_url + `&providerName=${providerName}`;
    (this.platform.is('android')) ? meetingUrl += "&platform=android" : meetingUrl += "&platform=ios";
    const data = {
      name: item.participants[1].name,
      roomId: item.videoCallSid,
      vendorName: item.videoVendor,
      meetingUrl: meetingUrl
    };
    const startVideoCallModal = await this.modalCtrl.create({
      component: JoinVcPage,
      componentProps: data,
      id: 'joinVcModal'
    });
    await startVideoCallModal.present();
    return true;
  }

  resetList () {
    this.items = [];
    this.totalPages = 0;
    this.currentPage = 1;
  }

  getAppointmentList(isEmpty, storageId){
    if (this.statusValues) {
    } else {
      this.statusValues = 'all';
    }
    if (isEmpty){
      this.resetList();
    }
    this.authService.getMyChartsListService(this.fromDate, this.toDate, this.queryText, this.currentPage, this.statusValues, storageId).then((data) => {
      
      if (data && data['appointments']){
          if (data['appointments']){
            for (let i = 0; i < data['appointments'].length; i++){
              this.items.push(data['appointments'][i]);
            }
          }
          if(data['appointments'].length > 0){
            this.totalPages = this.currentPage+1;
          }else{
            this.totalPages = 0;
          }
      }else{
        this.totalPages = 0;
      }
      let recordName="Schedule";
      this.noRecordFound = this.toastServ.displayNoRecordsFound(this.items,recordName);

    });
  }
  itemSelected(uhid, status, plannedCareItemId: string){
    if (status == 'Pending' || status == 'Scheduled'){
      this.updateStatus('CheckedIn', uhid, plannedCareItemId, '');
    }else if (status != 'NoShows'){
      this.startCharting(uhid, status, plannedCareItemId);
    }
  }
  // Start Charting page Redirect function from the root navigation.

  async startCharting(uhid, status, plannedCareItemId: string, loading?){
     
    this.localstorage.getAccessControl().then((accessObj :any) => {
      if (accessObj.permissions.Charting5674){
        this.navigateToStartChartPage(uhid, status, plannedCareItemId, loading);
      }else{
        if (loading) {
          loading.dismiss();
        }
        
        this.toastServ.presentToast('You Dont Have Permission To Do Charting');
      }
    });

  }
  async navigateToStartChartPage(uhid, status, plannedCareItemId, loading?){
      const data: NavigationExtras = {
        queryParams: {
          uhid: uhid,
          status: status,
          plannedCareItemId: plannedCareItemId
        }
      };

      this.router.navigate(['/patient-charting'], data).then(()=>{
        if (loading) {
          loading.dismiss();
        }
      });
    }
  
  backToChartingPage(){
    this.updateSchedule();
  }

  checkIsApptActive(status, uhid, plannedCareItemId, item) {
    this.authService.checkIsApptActive(plannedCareItemId).then((data) => {
      if (data === 'Success' ) {
        this.updateStatus(status, uhid, plannedCareItemId, '');
      } else {
        this.toastServ.presentToast('Appointment is not Active. Please Contact Admin.');
      }
    });
  }

  viewPatientTimeLine(uhid) {
    this.timelineAuthServ.viewPatientTimeLine(uhid);
  }
  async updateStatus(status, uhid, plannedCareItemId, type){
    if(status=== 'CheckedIn'){
    var loading = await this.loadingCtrl.create({
      message: 'Loading Charting Page...'
    });
    await loading.present();
    this.updateApptStatus(status, uhid, plannedCareItemId, type, loading);
    } else if(status === 'NoShows') {
        const alert = await this.alertCtrl.create({
          message: 'Are you sure you want to mark the appointment as NoShow ?',
          buttons:[
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
              }
            },
            {
              text: 'Ok',
              handler: () => {
                this.updateApptStatus(status, uhid, plannedCareItemId,type);
              }
            }
          ]
        });
        await alert.present();
    } else {
      this.updateApptStatus(status, uhid, plannedCareItemId,type);
    }
  }

  updateApptStatus(status, uhid, plannedCareItemId, type, loading?) {
    if (status === 'CheckedIn') {
      this.authService.updateApptsAsCheckedIn(plannedCareItemId).then(() => {
        this.getApptandStartChart(status, type, uhid, plannedCareItemId, loading);
      });
    } else {
      this.authService.updateApptStatus(status, plannedCareItemId, 'appointment').then(() => {
        this.getApptandStartChart(status, type, uhid, plannedCareItemId, loading);
      });
    }
  }

  getApptandStartChart(status, type, uhid, plannedCareItemId, loading) {
    if (status === 'CheckedIn' || status === 'Completed' && type !== 'Checkout') {
      this.startCharting(uhid, status, plannedCareItemId, loading);
    }
    this.getAppointmentList(true, this.storageId);
  }

  doInfinite(event) {
    var clientHeight = event.target.clientHeight;
    var scrollUpto = event.target.scrollHeight - event.target.scrollTop;
    if (clientHeight === scrollUpto) {
      setTimeout(() => {
        setTimeout(() => {
          this.currentPage = Number(this.currentPage) + 1;
          if (this.currentPage <= this.totalPages){
            this.getAppointmentList(false, this.storageId);
        }
        event.target.complete();
        }, 500);
      });
    }
  }

  checkPatientHasImage(imagePath) {
    return this.handlebar.checkImagePath(imagePath);
  }

  doRefresh(event) {
    this.isShowSearchBar = true;
    this.updateSchedule();
    setTimeout(() => {
          event.target.complete();
          this.toastServ.presentToast('Charts List Refreshed');
        }, 1000);
    }


    checkClinicalData(uuid, plannedCareItemId):Promise<any> {
      return this.authService.getTodaysChartInfo(uuid, plannedCareItemId).then((chartData) => {
        const encounterId = chartData.encounter["encounterId"];
        if (chartData.sqlVital || (chartData.patient && chartData.patient.allergies)
          || chartData.lastprogressNote || chartData.radiologydata || chartData.pathologydata || (chartData.encounter && chartData.encounter["diagnosis"])
          || (chartData.encounter && chartData.encounter["suggestedPlan"])) {
          return true;
        } else {
          this.authService.getPatientTimeLineData(uuid, 1, 'files', 2).then((data0) => {
            if (data0 && data0.items) {
              return true;
            } else {
              this.authService.getAssociateDynamicForms("Care Record", plannedCareItemId, encounterId).then((data) => {
                if (data) {
                  return true;
                } else {
                  return false;
                }
              });
            }
          });
        }
      });
    }

  async checkoutAppt(status, uuid, plannedCareItemId) {
    this.checkClinicalData(uuid, plannedCareItemId).then(async (isDataAvilable) => {
      if (isDataAvilable) {
        let alert = await this.alertCtrl.create({
          message: 'Do you want to Check Out ?',
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
              }
            },
            {
              text: 'Check Out',
              handler: () => {
                this.authService.checkoutEvent(plannedCareItemId).then(() => {
                  this.getAppointmentList(true, this.storageId);               
                });
              }
            }
          ]
        });
        await alert.present();
      } else {
        this.toastServ.presentToast("You must enter at least one clinical.");
      }
    });
    
  }

  callPatientMobile(mobileNo,countryCode,localPatientId){
    this.callNumberService.callToNumber(mobileNo,countryCode,localPatientId);
  }

 

}
