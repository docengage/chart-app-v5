import { Component, OnInit } from '@angular/core';
import { CallNumberService } from '../service/phone-call-service';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-support',
  templateUrl: './support.page.html',
  styleUrls: ['./support.page.scss'],
})
export class SupportPage implements OnInit {

  constructor(private callNumberService : CallNumberService,
    private authService: AuthService) { 
  }
  callPatientMobile(mobileNo){
    this.callNumberService.callToNumberDialer(mobileNo);
  }
  ngOnInit() {
  }

}
