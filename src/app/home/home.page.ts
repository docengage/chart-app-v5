import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import * as moment from 'moment';
import { LocalStorageService } from '../service/local-storage.service';
import { NavController, AlertController, LoadingController, ModalController, Platform } from '@ionic/angular';
import { CallNumberService } from '../service/phone-call-service';
import { AddLeavePage } from '../add-leave/add-leave.page';
import { Handlebar } from '../components/handlebar';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  providerInfo: any;
  accessControl: any;
  appPages: any;
  fromDate: any;
  toDate: any;
  todayChart: any;
  todayTask: any;
  isIos: boolean;
  organizationData: any;
  isHoliday = false;

  constructor(
    public authService: AuthService,
    public localStorge: LocalStorageService,
    private nav: NavController,
    private alertCtrl: AlertController,
    private callNumberService: CallNumberService,
    private loadingCtrl: LoadingController,
    private modalCtrl: ModalController,
    private handlebar: Handlebar,
    private platform: Platform
  ) { }

  ngOnInit() {
    if (this.platform.is('ios')) {
      this.isIos = true;
    } else {
      this.isIos = false;
    }

    this.loadAllData();
  }
  
  ionViewDidEnter() {
    this.loadAllData();
    this.getOrgMasterData();
  }

  getOrgMasterData() {
    this.authService.getOrganizationInfo().then((orgData) => {
      this.organizationData = orgData;
    });
  }

  loadAllData() {
    this.fromDate =   moment().format('YYYY-MM-DD');
    this.toDate =  moment().format('YYYY-MM-DD');
    this.providerInfo = [];
    this.authService.isTokenValid().then((returnVal) => {
      this.authService.getProviderInfo().then((data) => {
        this.providerInfo = data;
        this.checkHoliday();
        this.providerInfo.greetings = 'Good ' + this.getGreetingTime(moment());
        this.localStorge.getAccessControl().then((accessObj: any) => {
          if (accessObj) {
            this.accessControl = accessObj.permissions;
            this.setAppPages();
            this.chartData();
            this.taskData();
          } else {
            this.authService.getLoggedInUserInfo().then((data)=>{
              this.authService.getProviderPermissions(data.uuid).then((accessData) => {
                if (accessData) {
                  this.accessControl = accessData.permissions;
                  this.setAppPages();
                  this.chartData();
                  this.taskData();
                }
              });
            });
          }
        });
      });
    });
  }

  checkHoliday() {
    this.authService.getLeaveInfo(this.providerInfo.providerId, 1).then((data) => {
      if (data) {
        const todayDate = moment().format('DD MMM YYYY HH:mm');
        for (let i = 0; i < data.length; i = i + 1) {
          if ((moment(data[i].leavefrom, 'DD MMM YYYY hh:mm A').format('DD MMM YYYY HH:mm') <= todayDate 
          && moment(data[i].leaveto, 'DD MMM YYYY hh:mm A').format('DD MMM YYYY HH:mm') >= todayDate) 
          && data[i].status === 'Confirmed') {
            this.isHoliday = true;
            break;
          } else {
            this.isHoliday = false;
          }
        }
      }
    });
  }

  callPatientMobile(mobileNo){
    this.callNumberService.callToNumberDialer(mobileNo);
  }

  chartData(){
    if(this.accessControl){
    if (this.accessControl.Appointments5674 && this.accessControl.Appointmentsview) {
      this.authService.getMyChartsCount(this.fromDate, this.toDate,'todayChartCount').then((data) => {
        if(data){
          this.todayChart = data;
        } else {
          this.todayChart = 0;
        }
        this.appPages[0].dataLength = this.todayChart;
      });
   }
  }
  }
  taskData(){
    if(this.accessControl){
    if (this.accessControl.Activity5674 && this.accessControl.Activityview) {
      this.authService.getMyTaskListService(this.fromDate, this.toDate, '', 1, 'all').then((data) => {
        if (data && data.totalItems) {
          this.todayTask = data.totalItems;
        } else {
          this.todayTask = 0;
        }
        this.appPages[1].dataLength = this.todayTask;
      });
   }
  }
  }
  setAppPages(){
    this.appPages = [];
    if (this.accessControl) {
      if (this.accessControl.Appointments5674 && this.accessControl.Appointmentsview) {
        this.appPages.push({ title: 'Schedule', pageName: 'MyChartsPage', 
        component: '/tabs/myCharts', icon: 'calendar'});
      }
      if (this.accessControl.Activity5674 && this.accessControl.Activityview) {
        this.appPages.push({ title: 'Activity', pageName: 'SchedulePage',
        component: '/tabs/schedule', icon: 'file-tray-full'});
      }
      if (this.accessControl.Patient5674 && this.accessControl.Patientview) {
        this.appPages.push({ title: 'Patients', pageName: 'AllpatientPage', 
        component: '/tabs/allPatient', icon: 'people', dataLength: 0 });
      }
      if (this.accessControl.Leave5674 && this.accessControl.Leaveview) {
        this.appPages.push({ title: 'Leaves', pageName: 'LeaveListPage', 
        component: '/tabs/leaveList', icon: 'airplane', dataLength: 0 });
      }
    }
  }
  getGreetingTime(m) {
    let g = null; // return g
    if (!m || !m.isValid()) { return; }

    const split_afternoon = 12; // 24hr time to split the afternoon
    const split_evening = 17; // 24hr time to split the evening
    const currentHour = parseFloat(m.format('HH'));

    if (currentHour >= split_afternoon && currentHour <= split_evening) {
      g = 'Afternoon';
    } else if (currentHour >= split_evening) {
      g = 'Evening';
    } else {
      g = 'Morning';
    }
    return g;
  }

  async openLeavePage(){
    let modal = await this.modalCtrl.create({
      component: AddLeavePage
    });
    await modal.present();
  }

  async logOutAlert(){
  let alert = await this.alertCtrl.create({
    message: 'Do you want to logout?',
    mode: 'ios',
    cssClass: 'alertLabel',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
        }
      },
      {
        text: 'Log Out',
        cssClass: 'logoutBtnIos',
        handler: () => {
          this.logOut();
        }
      }
    ]
  });
  await alert.present();
}

  async logOut() {
    let loading = await this.loadingCtrl.create({
      message: 'Logging out...'
    });
    await loading.present();
    this.authService.logout().then((returnValue) => {
      loading.dismiss();
      // Give the menu time to close before changing to logged out
      if (returnValue) {
        this.localStorge.clearStorage();
        this.nav.navigateForward('/login');
      }
    });
  }
  checkPatientHasImage(imagePath) {
    return this.handlebar.checkImagePath(imagePath);
  }
}
