import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuController, IonSlides, NavController } from '@ionic/angular';
import { LocalStorageService } from '../service/local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.page.html',
  styleUrls: ['./tutorial.page.scss'],
})
export class TutorialPage implements OnInit {
  showSkip = true;

  constructor(
    private router: Router,
    public menu: MenuController,
    public storage: LocalStorageService,
    private nav: NavController

  ) { }

  ngOnInit() {
  }
  startApp() {
    this.storage.setHasSeenTutorial('true');
    this.storage.getToken().then((tokenValue) => {
      if (!tokenValue) {
        this.router.navigate(['/login']);
      } else {

        this.storage.getAccessControl().then((accessObj: any) => {
          let accessControl = accessObj.permissions;

          let data = {
            accessControl: accessControl
          };
          this.nav.navigateForward('/home', accessControl);
        });
      }
    });
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

  ionViewDidLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }
}
