import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddactivityPageRoutingModule } from './addactivity-routing.module';

import { AddactivityPage } from './addactivity.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddactivityPageRoutingModule
  ],
  declarations: [AddactivityPage]
})
export class AddactivityPageModule {}
