import { Component, OnInit, ViewChild } from "@angular/core";
import * as moment from "moment";
import { Location } from "@angular/common";
import { LocalStorageService } from "../service/local-storage.service";
import { Handlebar } from "../components/handlebar";
import { ToastServiceData } from "../service/toast-service";
import { AuthService } from "../service/auth.service";
import { IonDatetime, ModalController } from "@ionic/angular";
import { RequestedServicesPage } from "../requested-services/requested-services.page";
import { SelectPatientPage } from '../select-patient/select-patient.page';
import { SelectProviderPage } from "../select-provider/select-provider.page";

@Component({
  selector: "app-addactivity",
  templateUrl: "./addactivity.page.html",
  styleUrls: ["./addactivity.page.scss"],
})
export class AddactivityPage implements OnInit {

  addForm: any = {
    title: "",
    note: "",
    activityName: "Call",
    startDate: '',
    startTime: '',
    endDate: '',
    endTime:'',
    serviceid: "",
    serviceName: "",
    startDateTime: "",
    duration: "",
    providerId: "",
    reqService: "",
    careCenterId: 0,
  };

  providerName: any;
  providers: any;
  minDateTime = new Date().toISOString();
  isValidStartDate = true;
  isValidStartTime = true;
  isValidEndDate = true;
  isValidEndTime = true;
  accessControl: string;
  reminder: any;
  notify: any;
  isServiceRequired = false;
  formatedDate: any;
  presentation: any;
  fieldName: any;
  showModal = false;
  selectedCareCenter: any = [];

  patientInfo: any;

  @ViewChild(IonDatetime) datetime: IonDatetime;
  careCenters: any;
  provData: any;

  constructor(private location: Location,
    private localstorage: LocalStorageService,
    private handlebar: Handlebar,
    private toastServ: ToastServiceData,
    private authService: AuthService,
    private modalCntrl: ModalController,) {}

  ngOnInit() {
    this.localstorage.getAccessControl().then((accessObj) => {
      this.accessControl = accessObj;
    });
    this.getAllCareCenter();
    this.getAllProvByOrgId();
  }

  activityTypeOnChange(value) {
    this.isServiceRequired = value === 'BlockForService' ? true : false;
  }

  setDateModal(value, type, fieldName) {
    this.formatedDate = value
      ? moment(value).format()
      : moment().format();
    this.addForm[fieldName] = value ? value : moment().format("YYYY-MM-DD");
    this.presentation = type;
    this.fieldName = fieldName;
    this.showModal = true;
  }

  dateChange(value) {
    this.addForm[this.fieldName] = value ? moment(value).format() : moment().format();
    this.dateCancel();
  }

  dateCancel() {
    if (this.datetime) {
      this.showModal = false;
    }
  }

  getAllProvByOrgId() {
    this.authService.getAllProvByOrgId().then((data) => {
      this.provData = JSON.parse(JSON.stringify(data));
    });
  }

  getAllCareCenter() {
    this.authService.getAllCareCenter().then((data) => {
      this.careCenters = data;
    });
  }

  onChangeCareCenter() {
    this.addForm.careCenterId = this.selectedCareCenter.care_center_id;
    this.addForm.providerId = '';
    this.providerName = '';
  }

  async openServiceList(){
    const data = {
      careCenterId : this.addForm.careCenterId
    };
    const serviceModal = await this.modalCntrl.create({
      component: RequestedServicesPage,
      componentProps: data
    });
    await serviceModal.present();
    serviceModal.onDidDismiss().then((data0) => {
      if (data0 && data0.role){
        this.addForm.serviceName = data0.data.service_name;
        this.addForm.serviceid = data0.data.health_service_template_id;
      }
    });
  }

  async navigateToSelectModel(type) {
    const data0 = {
      ccid: this.addForm.careCenterId,
      type: type
    };
    const modal = await this.modalCntrl.create({
      component: SelectProviderPage,
      componentProps: data0
    });
    modal.onDidDismiss().then((data) => {
      const isUpdate = data.role; // Here's your selected user!
      if (isUpdate) {
        if (type === 'Staff') {
          this.addForm.providerId = data.data.selectedId;
          this.providerName = data.data.selectedValue;
        }
      }
    });
    return await modal.present();
  }

  async openPatientList(){
    const data = {
      careCenterId : this.addForm.careCenterId
    };
    const patientModal = await this.modalCntrl.create({
      component: SelectPatientPage,
      componentProps: data
    });
    await patientModal.present();
    patientModal.onDidDismiss().then((data0) => {
      if (data0 && data0.role){
        this.patientInfo = data0.data;
      }
    });
  }

  saveActivity() {
    const title = this.addForm.title;
    const careCenterId = this.addForm.careCenterId;
    const activityName = this.addForm.activityName;

    const patientUhid = (this.patientInfo && this.patientInfo.patientUhid) ? this.patientInfo.patientUhid: '';

    const note = this.addForm.note;
    let endDateTime = "";
    let startDateTime = "";
    const allProviderId = "";

    const providerId = this.addForm.providerId;

    const serviceid = this.addForm.serviceid;
    const startDate = this.addForm.startDate;
    const startTime = this.addForm.startTime;
    const endDate = this.addForm.endDate;
    const endTime = this.addForm.endTime;

    let global = "";
    if(this.handlebar.isEmpty(title)){
      var error="Enter a Event Title.\n";
      global=global+error;
  }
  if(this.handlebar.isEmpty(careCenterId)|| careCenterId=="0" || careCenterId==0){
      var error="Select a  Care Center .\n";
      global=global+error;
  }

  if(this.handlebar.isEmpty(providerId)){
    var error="Select a Provider.\n";
      global=global+error;
  }

  if(activityName === "BlockForService"){
    if(this.handlebar.isEmpty(serviceid)){
      var error="Select a Service.\n";
            global=global+error;
      }
  }

  var isStartDateValid = this.handlebar.checkdate(moment(startDate).format("DD-MM-YYYY"));
  if(isStartDateValid){
    var validStartTime = (moment(startTime).format("hh:mm A")).match(/^(0?[1-9]|1[012])(:[0-5]\d) [APap][mM]$/);
    if (!validStartTime) {
      var error="Enter Valid Start Time.\n";
      global=global+error;
    } else {
      startDateTime = moment(startDate).format("DD-MM-YYYY")+" "+moment(startTime).format("hh:mm A");
    }
  }else{
    var error="Enter Valid Start Date.\n";
      global=global+error;
  }

  var isEndDateValid = this.handlebar.checkdate(moment(endDate).format("DD-MM-YYYY"));
  if(isEndDateValid){
    var validTime = moment(endTime).format("hh:mm A").match(/^(0?[1-9]|1[012])(:[0-5]\d) [APap][mM]$/);
    if (!validTime) {
      var error="Enter Valid End Time.\n";
        global=global+error;
    } else {
      endDateTime = moment(endDate).format("DD-MM-YYYY")+" "+moment(endTime).format("hh:mm A");
    }
  }else{
    var error="Enter Valid End date.\n";
      global=global+error;
  }

  if(startDateTime && endDateTime){
    if(moment(startDateTime,'DD-MM-YYYY hh:mm A') >= moment(endDateTime,'DD-MM-YYYY hh:mm A')){
      var error="End Date Should be greater than Start Date.\n";
        global=global+error;
    }
  }

  if (global) {
    this.toastServ.presentToast(global);
    return false;
  } else {
    const eventData = {
      title     		 : title,
      providerId       : providerId,
      careCenterId     : careCenterId,
      startDateTime    : moment(startDateTime,'DD-MM-YYYY hh:mm A').format("DD-MM-YYYY HH:mm"),
      endDateTime      : moment(endDateTime,'DD-MM-YYYY hh:mm A').format("DD-MM-YYYY HH:mm"),
      activityName     : activityName,
      patientUhid		 : patientUhid,
      note		     : note,
      serviceid	     : serviceid,
      NotifyProvider   : this.notify ? this.notify.includes('Provider') : false,
      NotifyPatient    : this.notify ? this.notify.includes('Patient') : false,
      RemindProvider   : this.reminder ? this.reminder.includes('Provider') : false,
      RemindPatient    : this.reminder ? this.reminder.includes('Patient') : false
    };
    
    this.authService.saveActivity(eventData).then((data) => {
      if (
        data &&
        (data.status === 400 || data.status === 401 || data.status === 500)
      ) {
        const data0 = JSON.parse(data._body);
        this.toastServ.presentToast(data0.description);
      } else {
        this.dismiss();
        this.toastServ.presentToast("Activity added Successfully.");
      }
    });
  }
}

  dismiss() {
    this.location.back();
  }
}
