import { Component, ViewChild, OnChanges, OnInit,  } from '@angular/core';

import { Platform, NavController, AlertController, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Subscription } from 'rxjs';
import { LocalStorageService } from './service/local-storage.service';
import { ToastServiceData } from './service/toast-service';
import { AuthService } from './service/auth.service';
import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
import {Network} from '@ionic-native/network/ngx';
import { Router, RouterEvent } from '@angular/router';
import { AllPatientPage } from './all-patient/all-patient.page';
import { Location } from '@angular/common';

declare var cordova;
export interface PageInterface {
  title: string;
  pageName: string;
  component: any;
  tabComponent?: any;
  index?: number;
  icon: string;
  logsOut?: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  // @ViewChild(NavController , {static : false}) nav: NavController;
  rootPage: any;
  accessControl: any;
  connected: Subscription;
  disconnected: Subscription;

// Tab Nivagation page Interface added here...
  appPages: PageInterface[] = [];
  selectedIndex: number;
  selectedUrl: any;
  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
              private menuCtrl: MenuController,
              private localStorge: LocalStorageService,
              private nav: NavController,
              public router: Router,
              public push: Push,  public alertCtrl: AlertController, private authService: AuthService,
              private network: Network, public toastService: ToastServiceData,
              private location: Location)
              {
                this.localStorge.getHasSeenTutorial().then((hasSeenTutorial) => {
                  if (hasSeenTutorial) {
                    this.rootPage = '/login';
                    this.initializeApp();
                  } else {
                    this.nav.navigateForward(['/tutorial']);
                  }
                });

                this.backButton();
              }
  initializeApp() {
    this.authService.checkIsLoggedIn().then((returnVal) => {
      if (returnVal) {
        this.rootPage = '/home';
      } else {
        this.nav.navigateBack(this.rootPage);
      }
    });
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.networkCheck();
      this.initPushNotification();
      if (cordova && cordova.plugins && cordova.plugins.iosrtc) {
        cordova.plugins.iosrtc.registerGlobals();
        cordova.plugins.iosrtc.debug.enable('*', true);
        (navigator as any).__defineGetter__('product', function(){ return 'ReactNative' });
      }
    });
  }

  backButton() {
      this.platform.backButton.subscribeWithPriority(0, async () => {
        if ((this.router.url === '/login') || (this.router.url === '/home')) {
          const alert = await this.alertCtrl.create({
            header: 'Close App',
            message: 'Do you really want to exit ?',
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel'
              },
              {
                text: 'Exit',
                handler: () => {
                  navigator['app'].exitApp();
              }
            }
          ]
          });
          await alert.present();
        } else {
          await this.location.back();
        }
      });
  }

  networkCheck() {
    this.network.onConnect().subscribe(() => {
      this.localStorge.getIsOnline().then((isOnline) => {
        if (!isOnline) {
          this.localStorge.setIsOnline(true);
          let networkType = this.network.type;
          if (networkType && networkType != null && networkType !== 'null') {
            networkType = ' via ' + networkType;
          } else {
            networkType = '';
          }
          this.authService.pushOfflineStoredData();
          this.toastService.presentToast('All good! Your Internet connection is back ' + networkType + '...', false, 10000);
        }
      });
    });
    this.network.onDisconnect().subscribe(() => {
      this.localStorge.getIsOnline().then((isOnline) => {
        if (isOnline) {
          this.localStorge.setIsOnline(false);
          this.toastService.presentToast('It looks like you\'ve gone offline. Check your Internet connection.', false, 1000);
        }
      });
    });
  }
  initPushNotification() {
    if (!this.platform.is('cordova')) {
      console.warn('Push notifications not initialized. Cordova is not available - Run in physical device');
      return;
    }
    const options: PushOptions = {
      android: {
        senderID: '303909800001'
      },
      ios: {
        alert: 'true',
        badge: false,
        sound: 'true'
      },
      windows: {}
    };
    const pushObject: PushObject = this.push.init(options);

    pushObject.on('registration').subscribe((data: any) => {
      this.localStorge.setDeviceToken(data.registrationId);
    });

    pushObject.on('notification').subscribe(async (data: any) => {
      console.log('message -> ' + data.message);
      // if user using app and push notification comes
      if (data.additionalData.foreground) {
        // if application open, show popup
        const confirmAlert = await this.alertCtrl.create({
          header: 'Appt Notification',
          message: data.message,
          buttons: [{
            text: 'Ignore',
            role: 'cancel'
          }, {
            text: 'View',
            handler: () => {
              // TODO: Your logic here
              this.nav.navigateForward('/home',  data.message );
            }
          }]
        });
        await confirmAlert.present();
      } else {
        // if user NOT using app and push notification comes
        // TODO: Your logic on click of push notification directly
        this.nav.navigateForward('/home', data.message );
        console.log('Push notification clicked');
      }
    });

    pushObject.on('error').subscribe(error => console.error('Error with Push plugin' + error));
  }
  // Accroding to index element page component and parameters willl be rendered from here.

}