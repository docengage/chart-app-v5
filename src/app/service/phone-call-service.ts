import { Injectable } from '@angular/core';

import { CallNumber } from '@ionic-native/call-number/ngx';

import {Handlebar} from '../components/handlebar';
import { ToastServiceData } from './toast-service';
import { TimeLineDataPushService } from './timeline-auth-service';
import { AuthService } from '../service/auth.service';
import { LocalStorageService } from '../service/local-storage.service';

@Injectable()
export class CallNumberService {
    
  constructor(public callNumber:CallNumber,private handlebar:Handlebar, public timeLineService: TimeLineDataPushService,
    private toastServ: ToastServiceData,private authService: AuthService,private localstorage: LocalStorageService) { }

    callToNumber(mobileNo,countryCode,localPatientId){
        if(this.handlebar.isEmpty(mobileNo)){
            this.toastServ.presentToast("Mobile Number is Empty!");
        }else{
            this.authService.getOrgPreferences().then((pref) => {
                this.localstorage.setOrgPreferences(pref);
                if(this.handlebar.isEmpty(pref.ivrVendor)){
                    this.callToNumberDialer(mobileNo);
                }else{
                    this.toastServ.presentToast("Call Connecting Through IVR!");
                    this.timeLineService.clickToCallPatient(mobileNo,countryCode,localPatientId);
                }
            });
        }
    }

    callToNumberDialer(mobileNo){
        if(this.handlebar.isEmpty(mobileNo)){
            this.toastServ.presentToast("Mobile Number is Empty!");
        }else{
            this.toastServ.presentToast("Call Connecting Through Dialer!");
            this.callNumber.callNumber(mobileNo, true)
            .then(() => console.log('Launched dialer!'))
            .catch(() => console.log('Error launching dialer'));
        }
    }
    }
