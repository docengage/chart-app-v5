import { Injectable } from '@angular/core';

import { ToastController} from '@ionic/angular';
import { Toast, ToastOptions } from '@ionic-native/toast/ngx';



@Injectable()
export class ToastServiceData {
  deniedMsg = 'Permission Denied! Please Contact Your Admin.';
  private toastInstance: Toast;
  toastInst : any;
  constructor(private toastCtrl: ToastController,
             ) { }

  displayNoRecordsFound(items, recordName) {
    if (items === undefined || items.length === 0) {
        return 'No '+recordName+' Found!';
    } else {
        return '';
    }
  }
  // presentToast(msg:string,dismissOnPageChange?:boolean,duration?:number){
  //   if(msg==="Denied"){
  //     msg = this.deniedMsg;
  //   }
  //   let toast = this.toastCtrl.create({
  //     message: msg,
  //     duration: duration? duration :3000,
  //     position: 'bottom',
  //     showCloseButton: true,
  //     closeButtonText :'close',
  //     dismissOnPageChange : dismissOnPageChange?dismissOnPageChange : true
  //   });

  //   toast.onDidDismiss(() => {
  //   });

  //   toast.present();
  // }



  async presentToast(msg: string, dismissOnPageChange?: boolean, duration?: number) {

    if (this.toastInstance) {
      return;
    }

    this.toastInst = await this.toastCtrl.create({
      message: msg,
      duration: duration ? duration : 3000,
      position: 'bottom',
      // dismissOnPageChange : dismissOnPageChange ? dismissOnPageChange : true
      buttons: [{
        text: 'Close',
        role: 'cancel',
      }]
    });

    this.toastInst.dismiss(() => {
      this.toastInstance = null;
    });

    this.toastInst.present();
  }

}
