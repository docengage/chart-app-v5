import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import PouchFind from 'pouchdb-find';
import { Handlebar } from 'src/app/components/handlebar';
PouchDB.plugin(PouchFind);

@Injectable()
export class Todos {

  data: any;
  db: any;
  remote: any;

  constructor(
    private handlebar: Handlebar) {

      this.recreateDB();

    // this.remote = 'http://localhost:5984/docLeads';
   /** let options = {
      live: true,
      retry: true,
      continuous: true
    }; */
  //  this.db.sync(this.remote, options);

  }
  recreateDB() {
  //  this.destroyDB();
    this.db = new PouchDB('docLeads');
  }
  getTodos() {


      if (this.data && this.data.length > 0) {
        return Promise.resolve(this.data);
      }

      return new Promise(resolve => {

        this.db.allDocs({

          include_docs: true

        }).then((result) => {

          this.data = [];
          result.rows.map((row) => {
            this.data.push(row.doc);
          });

          resolve(this.data);

          this.db.changes({live: true, since: 'now', include_docs: true}).on('change', (change) => {
            this.handleChange(change);
          });

        }).catch((error) => {

          console.log(error);

        });

      });

  }

  getToDosData(getVariable) {
    if (this.handlebar.isNotEmpty(getVariable)) {
      return this.db.find({
        selector: {
          type: getVariable
        }
      }).then((res) => {
        const todos = res.docs;
        let items: any;
        let returnObj = {};
        if (getVariable === 'leads' || getVariable === 'patients' || getVariable === 'LeadActivity') {
          items = [];
          for (let i = 0; i < todos.length; i++) {
            items.push(this.handlebar.jsonString(todos[i]['message']));
          }
          returnObj[getVariable] = items;
        } else {
          items = this.handlebar.jsonString(todos[0]['message']);
          returnObj = items;
        }
        return returnObj;
      }).catch((err) => {
          return false;
      });
    } else {
      return null;
    }

  }
  getOfflineSaveData(getVariable) {

    return this.db.find({
      selector: {
        type: getVariable,
        isSync: false
      }
    }).then((res) => {
      const todos = res.docs;
      let items: any;
      items = [];
      for (let i = 0; i < todos.length; i++) {
        items.push(this.handlebar.jsonString(todos[i]));
      }
      const returnObj = {};
      returnObj[getVariable] = items;
      return returnObj;
    }).catch((err) => {
        return false;
    });
  }

  createTodo(todo) {
    this.db.put(todo).catch((err) => {
      console.log(err);
    });
  }

  postToLocalDb(data, type) {
      const messageObj = this.handlebar.jsonString(data);
      messageObj['_id'] = data._id;

      const newDoc = {
          _id: type,
          message: messageObj,
          type: type,
          isSync: false
      };
      this.createTodo(newDoc);
  }
  updateTodo(todo) {

    this.db.put(todo).catch((err) => {
        console.log(err);
      });
  }

  deleteTodo(todo) {
    this.db.remove(todo).catch((err) => {
      console.log(err);
    });

  }

  deleteAndAddTodoById(id, newDoc) {
    return this.db.find({
      selector: {
        _id: id
      }
    }).then((res) => {
      const todos = res.docs;
      let isSyncEle = false;
      for (let i = 0; i < todos.length; i++) {
        if (todos[i].isSync === true) {
          this.deleteTodo(todos[i]);
        } else {
          isSyncEle = true;
        }
      }
      if (!isSyncEle) {
        return this.createTodo(newDoc);
      } else {
        return false;
      }
    }).catch((err) => {
        return false;
    });
  }
  handleChange(change) {

    let changedDoc = null;
    let changedIndex = null;

    this.data.forEach((doc, index) => {

      if (doc._id === change.id) {
        changedDoc = doc;
        changedIndex = index;
      }

    });

    // A document was deleted
    if (change.deleted) {
      this.data.splice(changedIndex, 1);
    }
    else {

      // A document was updated
      if (changedDoc) {
        this.data[changedIndex] = change.doc;
      }

      // A document was added
      else {
        this.data.push(change.doc);
      }

    }

  }
  destroyDB() {
    this.data = null;
    new PouchDB('docLeads').destroy().then(function() {
      // database destroyed

    }).catch(function(err) {
      // error occurred
    });
  }

}
