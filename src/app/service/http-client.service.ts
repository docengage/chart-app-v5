import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import { LocalStorageService } from './local-storage.service';


@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  // tslint:disable-next-line: deprecation
  constructor( private http: Http, private localstorage: LocalStorageService) { }


  // tslint:disable-next-line: deprecation
  createAuthorizationHeader(headers: Headers): Promise<any> {

    return this.localstorage.getToken().then((tokenValue) => {
      if (tokenValue) {
        headers.append('X-Auth-Token', tokenValue);
      }
    });
  }

  get(url): Promise<any> {
    // tslint:disable-next-line: deprecation
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.createAuthorizationHeader(headers).then(() => {
      return this.http.get(url, {
        headers
      });
    });
  }

  post(url, data) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.createAuthorizationHeader(headers).then(() =>{
      return this.http.post(url, data, {
        headers
      });
    });
  }

  put(url, data) {
   const headers = new Headers();
   headers.append('Content-Type', 'application/json');
   return this.createAuthorizationHeader(headers).then(() =>{
     return this.http.put(url, data, {
       headers
     });
   });
  }
  patch(url,data) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.createAuthorizationHeader(headers).then(() =>{
      return this.http.patch(url, data, {
        headers
      });
    });
  }
}
