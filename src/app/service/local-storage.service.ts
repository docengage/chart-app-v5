import { Injectable } from '@angular/core';
import {Storage} from '@ionic/storage';
import { stageEnv } from '../../environments/environment.stage';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor( private storage: Storage ) { }

      // store the email address
      setToken(token) {
        this.storage.set('token', token);
    }
   getToken(): Promise<string> {
        return this.storage.get('token').then((value) => {
            return value;
        });
    }
    setUserId(userId) {
        this.storage.set('userId', userId);
    }
    getUserId(): Promise<string> {
        return this.storage.get('userId').then((value) => {
            return value;
        });
    }
    setOrgId(userId) {
        this.storage.set('orgId', userId);
    }
    getOrgId(): Promise<string> {
        return this.storage.get('orgId').then((value) => {
            return value;
        });
    }
    setPassword(password) {
        this.storage.set('password', password);
    }
    getPassword(): Promise<string> {
        return this.storage.get('password').then((value) => {
            return value;
        });
    }
    setTracks(tracks) {
        this.storage.set('tracks', tracks);
    }
    getTracks(): Promise<string> {
        return this.storage.get('tracks').then((value) => {
            return value;
        });
    }
    setChartingTracks(tracks) {
        this.storage.set('chartingtracks', tracks);
    }
    getChartingTracks(): Promise<string> {
        return this.storage.get('chartingtracks').then((value) => {
            return value;
        });
    }
    setLeadTracks(tracks) {
        this.storage.set('leadtracks', tracks);
    }
    getLeadTracks(): Promise<string> {
        return this.storage.get('leadtracks').then((value) => {
            return value;
        });
    }
    setIsOnline(value) {
        this.storage.set('isOnline', value);
    }
    setCustomURL(customURL) {
        this.storage.set('customURL', customURL);
    }
    getCustomURL(): Promise<string> {
        return this.storage.get('customURL').then((value) => {
            return value;
        });
    }
    getIsOnline(): Promise<boolean> {
        return this.storage.get('isOnline').then((value) => {
            if (value == null) {
                return true;
            } else {
                return value;
            }
        });
    }
    setLoggedInProvider(uuid) {
        this.storage.set('loggedInProviderUUID', uuid);
    }
    getLoggedInProvider(): Promise<string> {
        return this.storage.get('loggedInProviderUUID').then((value) => {
            return value;
        });
    }
    setAccessControl(accessObj) {
        this.storage.set('accessControl', accessObj);
    }
    getAccessControl(): Promise<string> {
        return this.storage.get('accessControl').then((accessObj) => {
            return accessObj;
        });
    }
    setCustomFields(customFields) {
        this.storage.set('customFields', customFields);
    }
    getCustomFields(): Promise<string> {
        return this.storage.get('customFields').then((customFields) => {
            return customFields;
        });
    }
    setHasSeenTutorial(hasSeenTutorial) {
        this.storage.set('hasSeenTutorial', hasSeenTutorial);
    }
    getHasSeenTutorial(): Promise<string> {
        return this.storage.get('hasSeenTutorial').then((hasSeenTutorial) => {
            return hasSeenTutorial;
        });
    }
    setOrgPreferences(orgPreferences) {
        this.storage.set('orgPreferences', orgPreferences);
    }
    getOrgPreferences(): Promise<string> {
        return this.storage.get('orgPreferences').then((orgPreferences) => {
            return orgPreferences;
        });
    }
    setEnvironment(env) {
        this.storage.set('environment', env);
    }
    getEnvironment(): Promise<string> {
        return this.storage.get('environment').then((environment) => {
            return environment;
        });
    }
    getURLByEnv():Promise<string>{
        return this.storage.get('environment').then((env) => {
            if(env==="stageEnv"){
                return stageEnv.BASE_URL;
            }else if(env==="customEnv"){
                return this.getCustomURL().then((customURL) => {
                    return customURL;
                });
            }else{
                return environment.BASE_URL;
            }
        });
    }
    setDeviceToken(deviceToken) {
        this.storage.set('deviceToken', deviceToken);
    }
    getDeviceToken(): Promise<string> {
        return this.storage.get('deviceToken').then((deviceToken) => {
            return deviceToken;
        });
    }
    setNoteSeq(noteCounter) {
        this.storage.set('noteCounter', noteCounter);
    }
    getNoteSeq(): Promise<string> {
        return this.storage.get('noteCounter').then((noteCounter) => {
            return noteCounter;
        });
    }
    setActivitySeq(counter) {
        this.storage.set('activityCounter',  counter);
    }
    getActivitySeq(): Promise<string> {
        return this.storage.get('activityCounter').then((counter) => {
            return counter;
        });
    }
    storeRecords(id, record){
        this.storage.set(id, record);
    }
    getRecord(id): Promise<string> {
        return this.storage.get(id).then((record) => {
            return record;
        });
    }
    setScheduleFilter(filterdata) {
        this.storage.set('scheduleFilterData', filterdata);
    }
    getScheduleFilter(): Promise<any> {
        return this.storage.get('scheduleFilterData').then((value) => {
            return value;
        });
    }
    setActivityFilter(filterdata) {
        this.storage.set('activityFilterData', filterdata);
    }
    getActivityFilter(): Promise<any> {
        return this.storage.get('activityFilterData').then((value) => {
            return value;
        });
    }
    storeIds(url, id){
        const data={url, id}
        return this.storage.get("offlineIds").then((record) => {
            if(record){
                record.push(data);
            }else {
                record = [];
                record.push(data);
            }
            this.storage.set('offlineIds', record);
        });
    }

    removeStoredIDs(index){
        return this.storage.get("offlineIds").then((record) => {
            if(record){
                record.splice(index, 1);
            }
            this.storage.set('offlineIds', record);
        });
    }
    // clear the whole local storage
    clearStorage() {
        this.getEnvironment().then((environment) => {
            this.getHasSeenTutorial().then((hasSeenTutorial) => {
                // this.getDeviceToken().then((deviceToken) => {
                    this.getCustomURL().then((customURL) => {
                        this.storage.clear().then(() => {
                            this.setHasSeenTutorial(hasSeenTutorial);
                            this.setEnvironment(environment);
                            this.setCustomURL(customURL);
                        });
                    });
               // });
            });
        });
    }
}
