import { Injectable,ViewChild, Component } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient } from './HttpClient';
import { Handlebar } from '../components/handlebar';
import { AuthService } from './auth.service';
import { Router, NavigationExtras } from '@angular/router';
import { ToastServiceData } from './toast-service';
import { MasterDataService } from './master-data';
import { LocalStorageService } from './local-storage.service';



@Injectable()
export class TimeLineDataPushService {
  apptItems: any;
  isLoggedIn: boolean = false ;
  constructor(public http: HttpClient,public https: Http,public handlebar:Handlebar,
    private authService : AuthService,
    private router: Router,
    private toastServ: ToastServiceData,
    private masterDataServ: MasterDataService,
    private localstorage : LocalStorageService
    ) {
  }

  pushChartData(uuid:string,data:any,type:string):Promise<any>{
    if(type=='orders'){
      return this.saveLabOrdersData(uuid,data);
    }else if(type=='suggPlan'){
      return this.saveSuggPlanData(uuid,data);
    }else if(type=='diagnosis'){
      return this.saveDiagnosisData(uuid,data);
    }
  }
  saveAllergiesData(uuid:string,data:any):Promise<any>{
    return this.authService.offlinePost('api/v1/save-charting-allergies/'+uuid,data, true, 'saveallergies_' + uuid);

  }
  saveDiagnosisData(uuid:string,data:any):Promise<any>{
    return this.authService.offlinePost('api/v1/save-charting-diagnosis/'+uuid,data, true, 'saveDiagnosisData_' + data.encounterId);
  }

  saveSuggPlanData(uuid:string,data:any):Promise<any>{
    return this.authService.offlinePost('api/v1/save-charting-suggplan/'+uuid,data, true, 'saveSuggPlanData_' + data.encounterId);

  }


  saveClinicalNoteData(uuid:string,data:any):Promise<any>{
    return this.authService.offlinePost('api/v1/save-charting-note/'+uuid,data, true, 'saveClinicalNoteData_' + data.encounterId);

  }

  saveLabOrdersData(uuid:string,data:any):Promise<any>{
    return this.authService.offlinePost('api/v1/save-charting-laborders/'+uuid,data, true, 'saveLabOrdersData_' + data.encounterId);

  }

  savePrescriptionData(uuid:string,data:any):Promise<any>{
    return this.authService.offlinePost('api/v1/save-charting-prescription/'+uuid,data, true, 'savePrescriptionData_' + data.encounterId);

  }

  deleteMedicineLieItem(prescriptionId:string,medicineId:string){
        let data = {
          prescriptionId : prescriptionId
        }
        return this.authService.offlinePost('api/v1/delete-prescription-line-item/'+medicineId,data, true, 'deleteMedicineLieItem_' + prescriptionId);
  }
  clickToCallPatient(mobileNumber:String,countryCode:String,referenceId:String){
    let data = {
      countryCode : countryCode ,
      referenceId : referenceId 
    }
    let url = 'clickToCall/'+ countryCode + '/' + mobileNumber + '/' + referenceId + '/Patient'  ;
    return this.authService.post(url,data);
  }

  pushVitalData(uuid,data):Promise<any>{
    return this.authService.offlinePost('api/v1/vitals/'+uuid,data, true, 'pushVitalData_' + uuid);

  }
  pushWorkListData(plannedCareItemId,data):Promise<any>{
    return this.authService.offlinePost('api/v1/appointment/worklist/'+plannedCareItemId,data, true, 'pushWorkListData_' + plannedCareItemId);

  }
  getAppointmentPlanItem(plannedCareItemId):Promise<any>{
    return this.authService.get('api/v1/appointment/'+plannedCareItemId, 'appointmentPlanItem_'+ plannedCareItemId);

  }

  pushPatientProfilePic(uuid,byteArray):Promise<any>{
        let data = {
          "byteArray" : byteArray
        }
        return  this.authService.post('api/v1/pushPatientImage/'+uuid,data);

  }
  pushDynamicData(uuid,data):Promise<any>{
    return  this.authService.offlinePost('api/v1/push/dynamicform/'+uuid,data, true, 'pushDynamicData_');
  }
  pushDynamicDataAndShare(data):Promise<any>{
    return  this.authService.offlinePost('saveDynamicFormData',data, true, 'pushDynamicDataAndShare_');
  }
  pushPatientDocumentSave(uuid,byteArray,startDate,itemName,urifilename,mimetype):Promise<any>{
    let data = {
      "byteArray" : byteArray,
      "startDate" : startDate,
      "itemName"  : itemName,
      "uuid"      : uuid,
      "urifilename" : urifilename,
      "mimetype" : mimetype
    }
    if(mimetype === 'application/pdf'){
      return  this.authService.post('api/v1/pushPatientDocument/mobile', data);
    } else {
    return  this.authService.post('api/v1/patient/document/upload', data);
  }

  }
  viewPatientTimeLine(uhid) {
    this.localstorage.getAccessControl().then((accessObj :any) => {
      if (accessObj.permissions.Charting5674){
        const data: NavigationExtras = {
          queryParams: {
            id: uhid,
          }
        };
        this.router.navigate(['/patient-time-line'], data);
      } else {
          this.toastServ.presentToast('You Dont Have Permission To Do Charting');
      }
    });
  }

}