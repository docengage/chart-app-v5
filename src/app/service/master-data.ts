import { Injectable } from '@angular/core';
import { LocalStorageService } from './local-storage.service';
import { AuthService } from './auth.service';
import { Handlebar } from 'src/app/components/handlebar';
import { Todos } from './todos';



@Injectable()
export class MasterDataService {
  data: any;
  organizationId: any;
  accessControl: any;
  constructor(private localstorage: LocalStorageService,
              private handlebar: Handlebar,
              public todoService: Todos) { }

  getTimeLineFilters(): Promise<any> {
    return this.localstorage.getAccessControl().then((accessObj) => {
      return this.localstorage.getTracks().then((tracksCache) => {
        this.accessControl = accessObj;

        if (this.handlebar.isEmpty(tracksCache)) {

            const tracks = [];
            if (this.accessControl.permissions.Appointmentsview) {
              tracks.push({value: 'planneditem', label: 'Appointmet', isChecked: true});
            }
            if (this.accessControl.permissions.Activityview) {
              tracks.push({value: 'activity', label: 'Activity', isChecked: true});
            }
            if (this.accessControl.permissions.Vitalview) {
              tracks.push({value: 'vitals', label: 'Vitals', isChecked: true});
            }
            if (this.accessControl.permissions.Careplanview) {
              tracks.push({value: 'careplan', label: 'Careplan', isChecked: true});
            }
            if (this.accessControl.permissions.GeneralNotesview) {
              tracks.push({value: 'progressnote,instruction,clinicalnote,history', label: 'Notes', isChecked: true});
            }
            if (this.accessControl.permissions.Prescriptionview) {
              tracks.push({value: 'medication', label: 'Medication', isChecked: true});
            }
            if (this.accessControl.permissions.Radiologyview ||
              this.accessControl.permissions.Pathologyview) {
              tracks.push({value: 'laborder', label: 'Laborder', isChecked: true});
            }
            if (this.accessControl.permissions.Invoiceview) {
              tracks.push({value: 'invoiced', label: 'Invoice', isChecked: true});
            }
            if (this.accessControl.permissions.Estimationview ) {
              tracks.push({value: 'estimated', label: 'Estimation', isChecked: true});
            }
            if (this.accessControl.permissions.Paymentview) {
              tracks.push({value: 'payments', label: 'Payments', isChecked: true});
            }
            tracks.push({value: 'files', label: 'Documents', isChecked: true});

            if (this.accessControl.permissions.DynamicFormview) {

              tracks.push({value: 'dynamicform-', label: 'Assessment', isChecked: true});
            }

            this.localstorage.setTracks(tracks);
            return tracks;
          } else {
            return tracksCache;
          }
        });

    });

  }


  getChartingConfig(): Promise<any> {
    return this.localstorage.getOrgPreferences().then((pref) => {
    return this.localstorage.getAccessControl().then((accessObj) => {
      return this.localstorage.getChartingTracks().then((tracksCache) => {
        this.accessControl = accessObj;
        if (this.handlebar.isEmpty(tracksCache)) {
          const tracks = [];
          if (this.accessControl.permissions.Vitalcreate) {
            tracks.push({value: 'vitals', label: 'Vitals', isChecked: true});
          }
          if ( pref["allergiesReqInChart"]){
            tracks.push({value: 'allergies', label: 'Allergies', isChecked: true});
          }
          if (this.accessControl.permissions.GeneralNotescreate) {
            tracks.push({value: 'notes', label: 'Notes', isChecked: true});
          }
          if (pref["diagnosisReqInChart"]){
            tracks.push({value: 'diagnosis', label: 'Diagnosis', isChecked: true});
          }
          if (pref["suggPlanReqInChart"]){
            tracks.push({value: 'suggplan', label: 'Sugg.Plan', isChecked: true});
          }
          if (this.accessControl.permissions.Prescriptioncreate) {
            tracks.push({value: 'prescription', label: 'Prescription', isChecked: true});
          }
          if (this.accessControl.permissions.Pathologycreate) {
            tracks.push({value: 'pathology', label: 'Pathology', isChecked: false});
          }
          if (this.accessControl.permissions.Radiologycreate) {
            tracks.push({value: 'radiology', label: 'Radiology', isChecked: false});
          }
          tracks.push({value: 'files', label: 'Document', isChecked: false});
          tracks.push({value: 'dynamicform-', label: 'Assessment', isChecked: false});
          tracks.push({value: 'certificate', label: 'Certificate', isChecked: false});
          this.localstorage.setChartingTracks(tracks);
          return tracks;
        } else {
          return tracksCache;
        }
    });
    });
    });
  }

  mergeWorkListDefWithData(appitems:any){
    let workListDef = new Array();
    if(appitems && appitems.worklistDefination){
      let planWorkList = new Array();

      try{
        workListDef =  JSON.parse(appitems.worklistDefination.value);
      }catch(e){}

      try{
        planWorkList = appitems.worklist = JSON.parse(appitems.worklist.value);
      }catch(e){}
      for(let k=0;k<workListDef.length;k++){
        for(let it=0;it<planWorkList.length;it++){
          if (workListDef[k].task_seq === planWorkList[it].task_seq){
            workListDef[k].appointment_id = planWorkList[it].appointment_id;
            workListDef[k].planned_care_item_id = planWorkList[it].planned_care_item_id;
            workListDef[k].session_name = planWorkList[it].session_name;
            workListDef[k].status = planWorkList[it].status;

          }
        }
      }
      }
      appitems.worklistDefination = workListDef;
      return appitems;
    }

    
  getLeadTimeLineFilters(): Promise<any> {
    return this.localstorage.getAccessControl().then((accessObj) => {
      return this.localstorage.getLeadTracks().then((tracksCache) => {
        this.accessControl = accessObj;

        if (this.handlebar.isEmpty(tracksCache)) {

            const tracks = [];
            if (this.accessControl.permissions.Activityview) {
              tracks.push({value: 'activity', label: 'Activity', isChecked: true});
            }
            if (this.accessControl.permissions.GeneralNotesview) {
              tracks.push({value: 'progressnote,instruction,clinicalnote,history', label: 'Notes', isChecked: true});
            }
            tracks.push({value: 'files', label: 'Documents', isChecked: true});
            tracks.push({value: 'email', label: 'Emails', isChecked: true});
            tracks.push({value: 'sms', label: 'SMS', isChecked: true});

            this.localstorage.setLeadTracks(tracks);
            return tracks;
          } else {
            return tracksCache;
          }
        });

    });

  }
  pushObjectToElement(elements, data) {
    if (data) {
      for (let i = 0; i < Object.keys(data).length; i++) {
        const key: string = Object.keys(data)[i];
        if (data[key]) {
          for (let j = 0; j < data[key].length; j++) {
            elements.push(data[key][j]);
          }
        }
      }
    }
    return elements;
  }

    checkPermission(type, actionName) {
      return new Promise((resolve) => {
        this.localstorage.getAccessControl().then((accessObj: any) => {
          this.accessControl = accessObj.permissions;
          if (this.accessControl[type + '5674']) {
            resolve(this.accessControl[type + actionName]);
          } else {
            resolve(false);
          }
        });
      });

    }
    deDoubleValidation(numberValue, fieldName, checkNotEmpty) {
      let errorMessage = '';
      const regEx = /^\-?([0-9]+(\.[0-9]+)?|Infinity)$/;
      if (checkNotEmpty && numberValue === '') {
            errorMessage = errorMessage + fieldName + ' can\'t be empty.<br>';
        } else if (numberValue !== '') {
        if (regEx.test(numberValue) === false) {
            errorMessage = errorMessage + fieldName + ' should be a number.<br>';
        } else {
          if (numberValue !== '' && Math.ceil(numberValue) < 1) {
                errorMessage = errorMessage + fieldName + ' should be positive a value.<br>';
            }
        }
        }

      return errorMessage;
    }
    getLoggedInProviderInfo() {

      this.todoService.getToDosData('loggedInProvider').then((data0) => {
          if (data0) {
              this.organizationId = data0.organizationId;
          }
      });
  }

    getAllMasterData() {
      this.getLoggedInProviderInfo();
    }


    storeDataInLocalDB(data, type) {

      const newDoc = {
        _id: type,
        message: data,
        type,
        isSync: true
      };
      this.todoService.deleteAndAddTodoById(type, newDoc);
    }
    deIntegerValidation(numberValue, fieldName, checkNotEmpty) {
      let errorMessage = '';
      if (numberValue != '') {
        let number = /^-?[0-9]+$/;
        const regex = RegExp(number);
        if (regex.test(numberValue) === false) {
            const error = fieldName + ' should be number.<br>';
            errorMessage = errorMessage + error;
        } else if (checkNotEmpty) {
         if (numberValue.trim() === '') {
            const error = fieldName + ' is Required.<br>';
            errorMessage = errorMessage + error;
         }
        } else if (numberValue !== '' && Math.ceil(numberValue) < 1) {
            const error = fieldName + ' should be positive a value.<br>';
            errorMessage = errorMessage + error;
        }
      }
      return errorMessage;
    }
    hours_am_pm(time) {
      let hours: any = Number(time.match(/^(\d+)/)[1]);
      let min: any =  Number(time.match(/:(\d+)/)[1]);
      if (min < 10) { min = '0' + min; }
      if (hours < 12) {
          return hours + ':' + min + ' AM';
      } else  if (hours == 12) {
        return hours + ':' + min + ' PM';
      } else  if (hours == 24) {
        return  '00:' + min + ' AM';
      } else {
          hours = hours - 12;
          hours = (hours < 10) ? '0' + hours : hours;
          return hours + ':' + min + ' PM';
      }
  }

  bmiPrediction (height,height_scale,weight,weight_scale,bmiMinRange,bmiMaxRange) {
    if (!height || height == "" || !weight || weight=="") {
      return "";
    }
    let bmi;
    let range="";
    if (height != "" && weight != "") {
        if (height_scale == "cm") {
            height = parseFloat(height) / 100;
        } else if (height_scale == "inch") {
            height = parseFloat(height) / 39.370;
        }else if (height_scale == "ft") {
            height = parseFloat(height) / 3.2808;
        }

        if (weight_scale == "Lbs") {
            weight = parseFloat(weight) / 2.2;
        }
        bmi = parseFloat(weight) / (parseFloat(height) * parseFloat(height));
    }
    bmi = parseFloat(bmi).toFixed(2);
    if(!bmiMinRange){
      bmiMinRange = 18.5;
    }
    if(!bmiMaxRange){
      bmiMaxRange = 24.9;
    }

    range = this.checkRange(bmi,bmiMinRange,bmiMaxRange);
    let bmiData = {
      "bmi" : bmi,
      "prediction" : range
    }
    return bmiData;
}
getTempMinRange(temperature_scale,temperatureMinRange){
  if (temperature_scale == "degF") {

    if(!temperatureMinRange){
      temperatureMinRange = 97;
    }

  } else {
      if (temperature_scale == "Cel") {

        if(!temperatureMinRange){
          temperatureMinRange = 36;
        }
      }
  }
  return temperatureMinRange;
}
getTempMaxRange(temperature_scale,temperatureMaxRange){
  if (temperature_scale == "degF") {

    if(!temperatureMaxRange){
      temperatureMaxRange = 99;
    }

  } else {
      if (temperature_scale == "Cel") {

        if(!temperatureMaxRange){
          temperatureMaxRange = 37;
        }
      }
  }
  return temperatureMaxRange;
}
getSystolicMinRange(minRange){
  if(!minRange){
    minRange = 120;
  }
  return minRange;
}
getSystolicMaxRange(maxRange){
  if(!maxRange){
    maxRange = 120;
  }
  return maxRange;
}
getDiastolicMinRange(minRange){
  if(!minRange){
    minRange = 80;
  }
  return minRange;
}
getDiastolicMaxRange(maxRange){
  if(!maxRange){
    maxRange = 80;
  }
  return maxRange;
}
tempPrediction (temp,temperature_scale,temperatureMinRange,temperatureMaxRange) {
    if (!temp || temp == "") {
        return "";
    }else{
      let tempErrorMsg = "";
      tempErrorMsg =this.deDoubleValidation(temp,"Temperature",false);


      if (tempErrorMsg) {
        alert(tempErrorMsg);
      } else {
        temperatureMinRange = this.getTempMinRange(temperature_scale,temperatureMinRange);
        temperatureMaxRange = this.getTempMaxRange(temperature_scale,temperatureMaxRange);
      }

      return this.checkRange(temp,temperatureMinRange,temperatureMaxRange);
    }
}
checkRange(actualVal,minRange,maxRange){ 
  let range ="";
  if (parseFloat(actualVal) > maxRange) {
    range = 'High';
  } else if (parseFloat(actualVal) < minRange) {
    range = 'Low';
  } else if (parseFloat(actualVal) >= minRange && parseFloat(actualVal) <= maxRange) {
    range = 'Normal';
  }
  return range;
}
pulsePrediction(pulseRate,patientage,minRange,maxRange) {
    if (!pulseRate || pulseRate == "") {
        return "";
    }else{
    let errorMsg = "";
    errorMsg = errorMsg + this.deIntegerValidation(pulseRate,"Pulse Rate",false);
    if (errorMsg) {
      alert(errorMsg);
    }
    let number = /^\-{0,1}(?:[0-9]+){0,1}(?:\.[0-9]+){0,1}$/;
    let regex = RegExp(number);

    if (regex.test(pulseRate) == false) {
      alert("pulseRate should be number.");
    } else {

        if (patientage >= 7 && patientage <= 9) {
          if(!minRange){
            minRange = 70;
          }
          if(!maxRange){
            maxRange = 110;
          }
        } else if (patientage >= 5 && patientage <= 6) {
          if(!minRange){
            minRange = 75;
          }
          if(!maxRange){
            maxRange = 115;
          }
        } else if (patientage >= 3 && patientage <= 4) {
          if(!minRange){
            minRange = 80;
          }
          if(!maxRange){
            maxRange = 120;
          }
        } else if (patientage >= 1 && patientage <= 2) {
          if(!minRange){
            minRange = 70;
          }
          if(!maxRange){
            maxRange = 130;
          }
        } else if (patientage > 0 && patientage < 1) {
          if(!minRange){
            minRange = 70;
          }
          if(!maxRange){
            maxRange = 190;
          }
        } else {
          if(!minRange){
            minRange = 60;
          }
          if(!maxRange){
            maxRange = 100;
          }
        }

    }

    return this.checkRange(pulseRate,minRange,maxRange);
  }
}
valuePrediction(valueType,value,minRange,maxRange){
  if (!value || value == "" ) {
    return "";
  }
  let errorMsg = "";
  errorMsg = errorMsg + this.deIntegerValidation(value,valueType,false);
  if (errorMsg) {
    alert(errorMsg);
  }

  let number = /^\-{0,1}(?:[0-9]+){0,1}(?:\.[0-9]+){0,1}$/;
  let regex = RegExp(number);
  if (regex.test(value) == false) {
    alert(valueType+" should be number.");
    return "";
  } else {
    return this.checkRange(value,minRange,maxRange);
  }
}
heartPrediction (heartRate,patientage,minrange,maxrange) {

    if (!heartRate || heartRate == "" || patientage == "") {
        return "";
    }
    let errorMsg = "";
    errorMsg = errorMsg + this.deIntegerValidation(heartRate,"Respiration Rate",false);
    if (errorMsg) {
      alert(errorMsg);
    }

    let number = /^\-{0,1}(?:[0-9]+){0,1}(?:\.[0-9]+){0,1}$/;
    let regex = RegExp(number);
    if (regex.test(heartRate) == false) {
      alert("Respirationrate should be number.");
      return "";
    } else {

        if (patientage <= 3 && patientage != 0) {
          if(!maxrange){
            maxrange = 40
          }
          if(!minrange){
            minrange = 25;
          }
        } else if (patientage > 3 && patientage <= 6) {
          if(!maxrange){
            maxrange = 30
          }
          if(!minrange){
            minrange = 20;
          }
        } else if (patientage >= 6 && patientage <= 10) {
          if(!maxrange){
            maxrange = 25
          }
          if(!minrange){
            minrange = 18;
          }
        } else {
          if(!maxrange){
            maxrange = 20
          }
          if(!minrange){
            minrange = 12;
          }
        }

        return this.checkRange(heartRate,minrange,maxrange);
    }


}
  systolicPrediction(systolic,minRange,maxRange) {
    if (!systolic || systolic == "") {
      return "";
    }
      let errorMsg = "";
      errorMsg = errorMsg + this.deIntegerValidation(systolic,"Systolic Pressure",false);
      if (errorMsg) {
        alert(errorMsg);
      }

      let number = /^\-{0,1}(?:[0-9]+){0,1}(?:\.[0-9]+){0,1}$/;
      let regex = RegExp(number);

      if (regex.test(systolic) == false) {
        alert("Systolic Pressure should be number.");
        return "";
      } else {
        maxRange = this.getSystolicMaxRange(maxRange);
        minRange = this.getSystolicMinRange(minRange);
      }
      return this.checkRange(systolic,minRange,maxRange);
  }

  diastolicPrediction (diastolic,minrange,maxrange) {
    if (!diastolic || diastolic == "") {
      return "";
    }
      let errorMsg = "";
      errorMsg = errorMsg + this.deIntegerValidation(diastolic,"Diastolic Pressure",false);
      if (errorMsg) {
        alert(errorMsg);
      }

      let number = /^\-{0,1}(?:[0-9]+){0,1}(?:\.[0-9]+){0,1}$/;
      let regex = RegExp(number);

      if (regex.test(diastolic) == false) {
        alert("Diastolic Pressure should be number.");
        return "";
      } else {
        minrange = this.getDiastolicMinRange(minrange);
        maxrange = this.getDiastolicMaxRange(maxrange);

      }

      return this.checkRange(diastolic,minrange,maxrange);
  }
  spo2Interpretation(spo2,minRange,maxRange) {
    if (!spo2 || spo2 == "") {
      return "";
    }
    if(!minRange){
      minRange = 90;
    }
    if(!maxRange){
      maxRange = 95;
    }


    return this.checkRange(spo2,minRange,maxRange);

  }
}
