import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { LocalStorageService } from './local-storage.service';
import { HttpClientService } from './http-client.service';
import { Router } from '@angular/router';
import { ToastServiceData } from './toast-service';
import { Todos } from './todos';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Handlebar } from 'src/app/components/handlebar';
import { MasterDataService } from './master-data';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLoggedIn = false;
  pageType: any ;
  constructor(private https: Http, private localstorage: LocalStorageService,
              public app: Router,
              private toastServ: ToastServiceData,
              private handlebar:Handlebar,
              private todos: Todos,
              public todosServ: MasterDataService,
              private localStorage: LocalStorageService,
              private http: HttpClientService) { }

  login(credentials,pageType) {
    return new Promise((resolve, reject) => {
      this.pageType = pageType;
      if (this.handlebar.isNotEmpty(credentials.loginId) && this.handlebar.isNotEmpty(credentials.password) && credentials.organizationId !== undefined) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.localstorage.getURLByEnv().then((baseURL) => {
        this.https.post(baseURL + 'mobileLoginAction', JSON.stringify(credentials), { headers })
          .subscribe(res => {
            try {
              resolve(res.json());
            } catch (err) {
              const obj = { 0: 'error', 1: 'Please Check Your Credentials and Try Login Again.' };
              resolve(obj);
            }
          }, (err) => {
            if (this.pageType === 'orgPage') {
              this.app.navigate(['/select-organization']);
            } else {
              this.app.navigate(['/login']);
            }
            reject(err);
          });
        });
      } else {
        const obj = { 0: 'error', 1: 'Please Check Your Credentials and Try Login Again.' };
        resolve(obj);
      }
    });
  }
  doReLogin(): Promise<any> {
    const loginData = { loginId: '', password: '', organizationId : 0};
    return this.localstorage.getUserId().then((userId) => {
      loginData.loginId = userId;
      return this.localstorage.getPassword().then((password) => {
        loginData.password = password;
        return this.localstorage.getOrgId().then((organizationId) => {
          loginData.organizationId = organizationId ? Number(organizationId) : 0;
          return this.login(loginData,'').then((result) => {
            const data: any = result;
            this.localstorage.setToken(data.access_token);
          });
        });
      });
    });
  }
  isTokenValid(): Promise<any> {
    return this.localstorage.getLoggedInProvider().then((uuid) => {
      return this.localstorage.getIsOnline().then((isOnline) => {
        if (isOnline) {
          return this.localstorage.getURLByEnv().then((baseURL) => {
            if (baseURL && uuid) {
              this.http.get(baseURL + 'api/v1/checkIsTokenValid/' + uuid).then((obj) => {
                obj.map(res => res.json())
                  .catch((error: any) => {
                    return this.doReLogin().then(() => {
                      return true;
                    });
                  })
                  .subscribe(data => {
                    return true;
                  }, (error) => {
                    return this.doReLogin().then((data) => {
                      return true;
                    }, (err) => {
                      this.toastServ.presentToast("UnAuthorised Login!");
                      this.app.navigate(['/login']);
                    });
                  });
              });
            } else {
              return false;
            }
          });
        } else {
          return false;
        }
      });
    });
  }
  post(url, data) {
    return new Promise((resolve, reject) => {
      this.localstorage.getIsOnline().then((isOnline) => {
        if (isOnline) {
          resolve(this.postData(url, data));
        } else {
          this.toastServ.presentToast('You are saving in offline Mode. Check your Internet connection.', false, 10000);
          reject();
        }
      });
   });
  }
  postData(url, data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.checkIsLoggedIn().then((isLoggedIn) => {
        if (isLoggedIn) {
          this.localstorage.getURLByEnv().then((baseURL) => {
            this.http.post(baseURL + url, data).then((obj) => {
              obj.map(res => res.json())
              .subscribe(data => {
                resolve(data);
              }, (error) => {
                resolve(error);
              });
            });
          });
        } else {
          reject();
        }
      });
    });
  }
  putData(url,data):Promise<any> {
    return new Promise((resolve, reject) => {
      this.checkIsLoggedIn().then((isLoggedIn) => {
        if(isLoggedIn){
            this.localstorage.getURLByEnv().then((baseURL) => {
              this.http.put(baseURL + url, data).then((obj) => {
                obj.map(res => res.json())
              .subscribe(data => {
                resolve(data);
              }, (error) => {
                resolve(error);
              });
            });
          });
        } else {
          reject();
        }
      });
    });
  }

  offlinePost(url, data, isPushFromDb, type): Promise<any> {
    return new Promise((resolve, reject) => {
      this.localstorage.getIsOnline().then((isOnline) => {
        if (isOnline) {
          resolve(this.postData(url, data));
        } else {
          if (isPushFromDb) {
            this.toastServ.presentToast('It looks like you\'ve gone offline. Check your Internet connection.', false, 10000);
            this.localStorage.storeIds(url, type);
            resolve(this.todos.postToLocalDb(data, type));
          }
        }
      });
   });

  }


  offlinePut(url,data,isPushFromDb,type):Promise<any> {
    return new Promise((resolve, reject) => {
      this.localstorage.getIsOnline().then((isOnline) => {
        if(isOnline){
          resolve(this.putData(url,data));
        } else {
          if(!isPushFromDb) {
            this.toastServ.presentToast("It looks like you've gone offline. Check your Internet connection.",false,10000);
            resolve(this.todos.postToLocalDb(data,type));
          }
        }
      });
   });
  }
  get(url, returnObj): Promise<any> {
    return this.localstorage.getIsOnline().then((isOnline) => {
      if (isOnline) {
        return this.getData(url, 0).then((returnData) => {
          this.todosServ.storeDataInLocalDB(returnData, returnObj);
          return returnData;
        });
      } else {
        this.toastServ.presentToast("It looks like you've gone offline. Check your Internet connection.",false,10000);
        return this.todos.getToDosData(returnObj).then((returnLocalData) => {
          if(returnLocalData){
            return returnLocalData;
          } else {
            return this.localStorage.getRecord(returnObj).then((data) => {
              return data;
            });
          }
        });
      }
    });
  }
  checkIsLoggedIn(): Promise<any> {
    return this.localstorage.getToken().then((tokenValue) => {
      if (tokenValue) {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
      return this.isLoggedIn;
    });

  }
  getData(url, attempt) {
    return new Promise((resolve, reject) => {

      this.checkIsLoggedIn().then((isLoggedIn) => {
        if (isLoggedIn) {
         this.localstorage.getURLByEnv().then((baseURL) => {
           if (baseURL) {
                  this.http.get(baseURL + url).then((obj) => {
                    obj.map(res =>  res )
                      .catch(() => {
                        // this.app.navigate(['/login']);
                        resolve('false');
                      })
                      .subscribe(data => {
                        data ? (data._body === 'Success' ? resolve('Success')
                                : this.handlebar.isNotEmpty(data._body) ? resolve(data.json()) : resolve('false'))
                                : resolve('false');
                        // resolve(data.json());
                      }, (error) => {
                        // this.app.navigate(['/login']);
                        resolve(error);
                      });
                  });
                } else {
                  resolve('false');
                }
             });
            } else {
              resolve('false');
            }
          });
    });
  }
  logout():Promise<any> {
    let data = {};
    return this.isTokenValid().then((returnVal) => {
      return this.localstorage.getLoggedInProvider().then((providerUUID) => {
        return this.localstorage.getIsOnline().then((isOnline) => {
          if(isOnline){
            this.deactivateDeviceInfo(providerUUID);
            this.todos.destroyDB();
            return this.post('mobileLogout',data);
          } else {
            this.toastServ.presentToast("You Can't Logout in offline Mode. Check your Internet connection.",false,10000);
            return false;
          }
        });
      });
    });

  }
  pushOfflineStoredData(){
      this.localstorage.getRecord('offlineIds').then((data) => {
      for(let i=0; i<data.length; i++){
      const id = data[i]['id'];
      this.todos.getOfflineSaveData(id).then((storedData)=>{
        storedData = storedData[id][0];
        if(!storedData.isSync){
        return new Promise((resolve, reject) => {
          this.localstorage.getIsOnline().then((isOnline) => {
            if (isOnline) {
              resolve(this.postData(data[i]['url'], storedData.message));
              storedData.isSync = true;
              this.todos.createTodo(storedData);
              this.localStorage.removeStoredIDs(i);
            }
          });
        });
      }
      });
    }
  });

  }

  forceLogout(loginId, orgId): Promise<any> {
    const data = {
      loginId,
      orgId
    };
    return this.localstorage.getLoggedInProvider().then((providerUUID) => {
      return this.localstorage.getIsOnline().then((isOnline) => {
        if (isOnline) {
          if (providerUUID) {
            this.deactivateDeviceInfo(providerUUID);
          }
          return this.forceLogoutPost('mobileForceLogout', data);
        } else {
          this.toastServ.presentToast('You Can\'t Logout in offline Mode. Check your Internet connection.', false, 10000);
          return false;
        }
      });
    });

  }
  forceLogoutPost(url, data) {
    return new Promise((resolve, reject) => {
     this.localstorage.getURLByEnv().then((baseURL) => {
        this.https.post(baseURL + url, data).subscribe(data => {
            resolve(data);
          }, (error) => {
            resolve(error);
          });
     });

    });

  }
  
  getChartsListService(fromDate,toDate):Promise<any> {
    return  this.getMyChartsListService(fromDate,toDate,"$",1,'all', '');
  }
  getMyChartsListService(fromDate,toDate,searchValue,pageno, statusValues, storageId):Promise<any> {
    if(this.handlebar.isEmpty(searchValue)) {
      searchValue = "$";
    }
    searchValue = "&patientSearch=" + searchValue;
    let offset = (Number(pageno)-1) * 15;
    return this.get('api/v1/appointments?myAppt=true&from='+ fromDate+'&to='+toDate+searchValue+'&statusFilter='+statusValues+"&offset=" + offset, storageId);
  }
  getAppointmentById(apptId):Promise<any> {
    return this.get('api/v1/appointments?appointmentId=' + apptId, '');
  }
  getMyChartsCount(fromDate,toDate, storageId):Promise<any> {
    return this.get('api/v1/appointments/meta/count?from='+ fromDate+'&to='+toDate, storageId);
  }
  getLoggedInUserInfo():Promise<any> {
    return  this.get('api/v1/whoami', 'whoami');
  }
  getProviderInfo():Promise<any> {
    return this.localstorage.getLoggedInProvider().then((uuid) => {
      return  this.get('api/v1/providers/' + uuid, 'loggedInProvider');
    });
  }
  getLeaveInfo(providerId, pageno):Promise<any> {
    return  this.get('getLeaveList?providerId=' + providerId +'&status=all&searchBy=$&globalpageno=' + pageno, 'getLeaveList');
  }
  getPrintSettingsInfo():Promise<any> {
    return this.getProviderInfo().then((prov) => {
      return  this.get('api/v1/org/'+prov["organizationId"] + '/print/settings ', 'printSettings');
    });
  }

  checkIsApptActive(apptId):Promise<any> {
    return  this.get('validate/checkin/allowed/' + apptId, '');
  }

  getVideoCommunicationConfig():Promise<any> {
    return  this.get('video/config', 'videoConfig');
  }
  getMyPatientsList():Promise<any> {
    return this.get('api/v1/patients', 'allPatients');
  }
  getTaskListService(fromDate,toDate):Promise<any> {
    return  this.getMyTaskListService(fromDate,toDate,"",1, 'all');
  }
  getMyTaskListService(fromDate,toDate,searchValue,pageno, statusValues):Promise<any> {
    if(this.handlebar.isEmpty(searchValue)) {
      searchValue = "$";
    }
    searchValue = "&patientSearch=" + searchValue;
    let offset = (Number(pageno)-1) * 15;
    return this.get('api/v1/mytask?onlyEvent=true&from='+fromDate+'&to='+toDate+searchValue+'&statusFilter='+statusValues+"&offset=" + offset, 'taskList');
  }
  getMyTaskItem(apptId):Promise<any> {
    return this.get('api/v1/mytask?onlyEvent=true&appointmentId=' + apptId, 'taskItem_' + apptId);
  }
  getPatientTimeLineData(uhid,pageno,includeFilters, limit?: any):Promise<any> {
    let offset = (Number(pageno)-1) * 15;
    let limitCond = '';
    if (limit){
      limitCond = '&limit='+limit;
    }
    return  this.get('api/v1/timeline/overview/items/'+uhid+"?type="+includeFilters+"&offset=" + offset + limitCond, 'PatientTimelinefId_'+uhid);
  }
  getCertData(uhid,pageno):Promise<any> {
    return  this.get('getAllCertificate?pgno='+pageno+'&uhid='+uhid, 'PatientCertificate');
  }
  getCertTmpltData(certTmplt, uhid):Promise<any> {
    return  this.get('certificate/template/parse?certTmplt='+certTmplt+'&uhid='+uhid, 'PatientCertTmplt');
  }
  getPatientCert(uhid,certificateId):Promise<any> {
    return  this.get("getPatientCertificateById/" + certificateId + "/" + uhid, 'PatientCertificateById');
  }

  getLastTenEnctr(uhid):Promise<any> {
    return  this.get('get/lasttenencounters/' + uhid, 'LastTenEnctr');
  }

  getAllProvByOrgId():Promise<any> {
    return  this.get('providers/orgId', 'AllProviders');
  }

  getAllCareCntr():Promise<any> {
    return  this.get('all/carecenter', 'AllCareCntr');
  }

  getCustomerProfileInfo(uhid):Promise<any> {
    return  this.get('customer/profileInfo/'+uhid, 'CustomerProfileInfo');
  }

  getProviderModel():Promise<any> {
    return  this.get('provider', 'ProviderModel');
  }

  getOrganisationModel():Promise<any> {
    return  this.get('getOrganisation', 'OrganisationModel');
  }

  getPatientTimeLineItemDetails(referenceId,referenceType):Promise<any> {
    return this.get('api/v1/timeline/items/details/'+referenceId+'/' + referenceType, 'ptLineItemDetails');
  }
  getPatientInfo(uuid):Promise<any> {
    return this.get('api/v1/patients?uuid='+uuid + '&limit=1&offset=0', 'patientInfo');
  }
  getPatientInfoLight(uuid):Promise<any> {
    return this.get('api/v1/patients/light/' + uuid, 'PatientLocalRefId_'+uuid);
  }
  getAssociateDynamicForms(moduleName,plannedCareItemId,encounterId):Promise<any> {
    return this.get('api/v1/dynamicforms/'+moduleName+'/'+plannedCareItemId+"/" + encounterId, 'associateDynamicForms');
  }

  getAllDynamicTemplateByGroup():Promise<any> {
    return this.get('getAllDynamicTemplateByGroup', 'allDynamicTemplateByGroup');
  }
  pushLogDetails(roomId, data):Promise<any> {
    return this.post('api/v1/room/logs/' + roomId, data);
  }
  getPdfUrl(uhid, encounterId, plannedCareItemId):Promise<any> {
    return this.get('chartingPdf/url/' + uhid + '/' + encounterId + '/' + plannedCareItemId, '');
  }
  expandUrl(url): Promise<any> {
    return this.get('expand/'+ encodeURIComponent(url), 'expandUrl');
  }
  getDynamicFormData(formDefId,formId):Promise<any> {
    return this.get('api/v1/dynamicform/'+formDefId+"/" + formId, 'dynamicFormData');
  }

  getServices(careCenterId): Promise<any>{
    return this.get('serviceListWithCategoryWise/' + careCenterId , 'serviceList_' + careCenterId);
  }

  getServiceBySearch(careCenterId, searchTerm, pgno, serviceType): Promise<any> {
    if (searchTerm === '') {
      searchTerm = 'null';
    }
    return this.get("serviceListBySearch/" + careCenterId + "?searchTerm=" + searchTerm + "&pgno=" + pgno + '&serviceType=' + serviceType, "serviceSearchList");
  }

  getServiceTypeFromServices(pageNo): Promise<any> {
    return this.get("serviceTypeFromServices?pageNo="+pageNo, "serviceTypeList");
  }

  getServiceListByCareCenterId(careCenterId): Promise<any> {
    var type = "mobile"
    return this.get("serviceListByCriteria/all/" + careCenterId + "?type=" + type, "serviceList");
  }
  
  searchPatientsList(searchValue,pageno):Promise<any> {
    if(this.handlebar.isEmpty(searchValue)) {
      searchValue = "$";
    }
    let offset = (Number(pageno)-1) * 15;
    return  this.get('api/v1/patients?patientSearch='+searchValue+"&offset=" + offset, 'searchPatientsList');
    
  }
  updateApptStatus(status,plannedCareItemId,eventType):Promise<any> {
    let data = {
      status : status,
      type : eventType
    };
    return  this.offlinePost('api/v1/change/task-status/' + plannedCareItemId, data, true, 'updateApptStatus');
  }
  
  checkoutEvent(planeCareId): Promise<any> {
    return this.get("checkoutEvent/" + planeCareId, "CheckOut");
  }

  updateApptsAsCheckedIn(planeCareId): Promise<any> {
    return this.get("updateApptsAsCheckedIn/" + planeCareId, "CheckOut");
  }

  saveLeaveInfo(data):Promise<any> {
    return this.post('leaves', data);
  }
  shareVisitSummary(uhid, encounterId, plannedCareItemId): Promise<any> {
    return this.post('share/VisitSummary/' + uhid + '/' + encounterId + '/' + plannedCareItemId, '');
  }
  saveCertInfo(data, certId):Promise<any> {
    let urlStr;
    if (certId === 0 ){
      urlStr = 'savePatientCertificate';
    } else {
      urlStr = 'updatePatientCertificate/'+ certId;
    }
    return this.post(urlStr, data);
  }
  sendTimelineDataByMail(data, uhid):Promise<any> {
    let urlStr = 'sendTimelineDataByMail/'+uhid;
    return this.post(urlStr, data);
  }
  saveActivity(data):Promise<any> {
    return this.post('schedule/event', data);
  }
  getAllergiesMasterData(searchValue):Promise<any> {
    if(this.handlebar.isEmpty(searchValue)) {
      searchValue = "$";
    }
    return this.get('api/v1/master/allergies?searchValue=' + searchValue, 'allergiesMasterData');
  }
  getDiagnosisMasterData(searchValue, pageno):Promise<any> {
    if(this.handlebar.isEmpty(searchValue)) {
      searchValue = "all";
    }
    if(this.handlebar.isEmpty(pageno)){
      pageno = 1;
    }
    return this.get("api/v1/master/diagnosis?searchValue=" + searchValue +"&pageno="+pageno , 'diagnosisMasterData');
  }
  getSuggestedPlansMasterData(searchValue,pageno):Promise<any> {
    if(this.handlebar.isEmpty(searchValue)) {
      searchValue = "$";
    }
    var limit;
    if(this.handlebar.isEmpty(pageno)) {
      limit = 1 * 15;
    } else {
      limit = Number(pageno) * 15;
    }
    return this.get('api/v1/master/suggestedplans?searchValue=' + searchValue+'&limit='+limit, 'suggestedPlansMasterData');
  }
  getMedicationMasterData(productname,careCenterId,isShowAvailStock):Promise<any> {
    return this.get('api/v1/master/medication/'+productname+"/"+careCenterId+"/" + isShowAvailStock, 'medicationMasterData');
  }
  getMedicationMasterDataLight(productname,careCenterId,isShowAvailStock):Promise<any> {
    return this.get('api/v1/medication/light/' + productname, 'medicationMasterDataLight');
  }
  getPrescriptionTemplates(templateName,category):Promise<any> {
    return this.get('getOnClickOfAddFromTemplate?presctemplateName=' + templateName+'&category=' +category, 'data');
  }
  getAllPreconditions():Promise<any> {
    return this.get('settings/getAllPreconditions', 'data');
  }
  getPrescriptionMedication(templateId):Promise<any> {
    return this.get('getPrecriptionTemplateDetails?templateId='+templateId, 'data');
  }
  getLabOrdersMasterData(ordersType,pageno,searchValue):Promise<any> {
    if(this.handlebar.isEmpty(searchValue)) {
      searchValue = "$";
    }
    var limit;
    if(this.handlebar.isEmpty(pageno)) {
      limit = 1 * 15;
    } else {
      limit = Number(pageno) * 15;
    }
    return this.get('api/v1/master/components/'+ordersType+"?deviceType=mobile&searchValue=" + searchValue+'&limit='+limit, ordersType + '_masterData');
  }
  getNoteTemplateMasterData(categoryName,searchValue):Promise<any> {
    if(this.handlebar.isEmpty(searchValue)) {
      searchValue = "$";
    }
    return this.get('api/v1/master/notetemplates/'+categoryName+"?searchValue=" + searchValue, 'noteTemplateMasterData');
  }
  getCareCentersMasterData():Promise<any> {
    return this.localstorage.getLoggedInProvider().then((uuid) => {
          return  this.get('api/v1/master/care_center/' + uuid, 'careCentersMasterData');
    });
  }
  getCareCentersMasterDataById(careCenterId):Promise<any> {
      return  this.get('api/v1/master/care_center/byid/' + careCenterId,  'careCentersMasterDataById');
  }
  pushParicipant(roomId, data): Promise<any> {
    return this.post('api/participant/' + roomId, data);
  }
  
  getTodaysChartInfo(uuid:string,plannedId:string):Promise<any> {
    return this.get('api/v1/todays/encounter/details/'+uuid+'/' + plannedId, 'chartInfo_'+uuid);
  }
  
  getProviderPermissions(providerUUId):Promise<any> {
    return this.get('api/v1/mypermission/' + providerUUId, 'accessControl');
  }
  getVitalConfigurations():Promise<any> {
    return this.get('vital/configurations', 'vitalConfig');
  }
  getOrganizationInfo(): Promise<any> {
    return  this.get('getOrganisation ', 'organization');
  }
  
  getOrgPreferences():Promise<any> {
      return this.localstorage.getOrgPreferences().then((pref) => {
        if(pref){
          return pref;
        } else {
          return this.getProviderInfo().then((prov) => {
            return this.get('api/v1/org/'+prov["organizationId"] + '/preferences', 'orgPreferences').then((prefData) => {
              this.localstorage.setOrgPreferences(prefData["preferences"]);
              return prefData["preferences"];
            });
          })
          
        }
      });
  }

  getAllCareCenter(): Promise<any> {
    return this.get('api/v1/care_centers', 'careCenters');
  }

  getEnquiryTypes(): Promise<any> {
    return this.get("api/v1/master/enquiryType", "enquiryTypes");
  }

  getActivities(): Promise<any> {
    return this.get("api/v1/master/activities", "activities");
  }

  getEnquirySubTypes(type): Promise<any> {
    return this.get("getAllEnquirySubTypeByType/" + type, type + "SubType");
  }

  getSources(): Promise<any> {
    return this.get("getPtReferneces", "enquirySources");
  }

  getAllStaff(): Promise<any> {
    return this.get("getallorgprovidestaff", "allStaff");
  }

  getSourcesBySubType(enquiryType, enquirySubType): Promise<any> {
    return this.get('getAllEnquirySourceBySubType/' + enquiryType + '/' + enquirySubType, "enquirySources");
  }

  getAllConsultants(): Promise<any> {
    return this.get("getallorgconsultants", "consultants");
  }

  getExProviders(): Promise<any> {
    return this.get("allexternalprovidernames", "exProviders");
  }

  getNotifyTeams(): Promise<any> {
    return this.get('notifyProviders/all', "teams");
  }

  getScheduleProvidersBySpeciality(ccid): Promise<any> {
    return this.get('getScheduleProvidersBySpeciality/' + ccid + '/false/all', "providers");
  }

  getCalendarSettings(): Promise<any> {
    return this.get('get/calendar/settings', "calendarSettings");
  }

  savePatientInfo(data): Promise<any> {
    return this.post('api/v1/patients', data);
  }

  saveApptInfo(data): Promise<any> {
    return this.post('quick/book/appointment', data);
  }

  provPicSave(data) {
    return this.post('profilePicAsBiteArray', data);
  }
  saveDeviceInfo(deviceToken,uuid):Promise<any> {
    let data = {
      deviceToken : deviceToken,
      providerUUID : uuid,
      deviceType: 'ChartApp'
    };
    return  this.post('saveDevice',data);
    
  }
  deactivateDeviceInfo(uuid):Promise<any> {
    return this.localstorage.getDeviceToken().then((deviceToken) => {
      let data = {
        deviceToken : deviceToken,
        providerUUID : uuid
      };
      return  this.post('deactivateDevice', data);
      
    });
  }
  callPatient(countryCode, mobile, patientId): Promise<any> {
    const data = {};
    return this.post('clickToCall/' + countryCode + '/' + mobile + '/' + patientId + '/Patient', data);
  }
  getVendorCallDetails(): Promise<any> {
    return this.get('communication/vendor/CALL', 'vendorCall');
  }

  getAllEMRCustomFields(): Promise<any> {
    return this.get('customFieldConfig/emr', 'EMRCustomFields');
  }

  checkForUnique(value, columnName): Promise<any> {
    return this.get("checkForUniqueData/"+value+"/"+columnName, 'uniqueField');
  }
}
