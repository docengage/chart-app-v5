import { Injectable } from '@angular/core';
import { LoadingController, ActionSheetController } from '@ionic/angular';
import { ToastServiceData } from '../service/toast-service';
import { Http } from '@angular/http';
import { Camera } from '@ionic-native/camera/ngx';
import { TimeLineDataPushService } from './timeline-auth-service';

@Injectable()
export class CameraService {
  base64Image: any;
  loading: any;
  imagePath: string;
  constructor(public http: Http,
              public camera: Camera,
              public loadingCtrl: LoadingController,
              public actionSheetCtrl: ActionSheetController,
              private timeLineService: TimeLineDataPushService
    ,         public toastServ: ToastServiceData
) { }


    takePicture(uhid): Promise<any> {
        return new Promise((resolve) => {
            this.camera.getPicture({
                destinationType: this.camera.DestinationType.DATA_URL,
                sourceType: this.camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: this.camera.EncodingType.JPEG,
                targetWidth: 100,
                targetHeight: 100,
                saveToPhotoAlbum: false

            }).then((imageData) => {
                this.base64Image = 'data:image/jpeg;base64,' + imageData;
                this.savePatientImage(uhid).then((returnObj) => {
                    resolve(returnObj);

                });
            });
        });
      }
      accessGallery(uhid) {
        return new Promise((resolve) => {
            this.camera.getPicture({
                destinationType: this.camera.DestinationType.DATA_URL,
                sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
                allowEdit: true,
                encodingType: this.camera.EncodingType.JPEG,
                targetWidth: 100,
                targetHeight: 100,
                saveToPhotoAlbum: false
            }).then((imageData) => {
                this.base64Image = 'data:image/jpeg;base64,' + imageData;
                this.savePatientImage(uhid).then((returnObj) => {
                    resolve(returnObj);

                });
            });
        });
      }
      showLoader() {
        this.loading = this.loadingCtrl.create({
            message: 'Uploading...'
        });
      }
      savePatientImage(uhid) {
        return new Promise<void>((resolve) => {
        this.showLoader();

        this.loading.present().then(() => {

                this.timeLineService.pushPatientProfilePic(uhid, this.base64Image).then((data) => {

                    this.loading.dismiss();
                    if (data) {
                        this.uploadMessageToast('Patient Image Uploaded.');
                        resolve(data.url);
                    } else {
                        resolve();
                    }

                });
            });
        });

      }
      uploadMessageToast(msg) {
        this.toastServ.presentToast(msg);
    }

}
