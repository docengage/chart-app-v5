import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { LocalStorageService } from '../service/local-storage.service';
import { Handlebar } from '../components/handlebar';
import { AuthService } from '../service/auth.service';
import * as moment from 'moment';
import { Location } from '@angular/common';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-about-provider',
  templateUrl: './about-provider.page.html',
  styleUrls: ['./about-provider.page.scss'],
})
export class AboutProviderPage implements OnInit{

  providerInfo: any;
  myDocument: string;
  accessControl: any;
  constructor(public navCtrl: NavController,
              public localstorage: LocalStorageService,
              public handlebar: Handlebar,
              public authService: AuthService,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController,
              private location: Location,
              private camera: Camera) {

  }

  ngOnInit() {
    this.getProvInfo();
    this.localstorage.getAccessControl().then((accessObj: any) => {
      this.accessControl = accessObj.permissions;
    });
  }

  getProvInfo() {
    this.authService.getProviderInfo().then((data) => {
      this.providerInfo = data;
      this.providerInfo.greetings = 'Good' + this.getGreetingTime(moment());
    });
  }

  getGreetingTime(m) {
    let g = null; // return g
    if (!m || !m.isValid()) { return; }

    const split_afternoon = 12; // 24hr time to split the afternoon
    const split_evening = 17; // 24hr time to split the evening
    const currentHour = parseFloat(m.format('HH'));

    if (currentHour >= split_afternoon && currentHour <= split_evening) {
      g = 'Afternoon';
    } else if (currentHour >= split_evening) {
      g = 'Evening';
    } else {
      g = 'Morning';
    }
    return g;
  }
  checkPatientHasImage(imagePath){
    return this.handlebar.checkImagePath(imagePath);
  }

  async logOutAlert() {
    const alert = await this.alertCtrl.create({
      message: 'Do you want to logout?',
      mode: 'ios',
      cssClass: 'alertLabel',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Log Out',
          cssClass: 'logoutBtnIos',
          handler: () => {
            this.logOut();
          }
        }
      ]
    });
    await alert.present();
  }

  changeProfilePic() {
    if (this.accessControl.Provider5674 && this.accessControl.Provideredit) {
      const options: CameraOptions = {
        destinationType: this.camera.DestinationType.DATA_URL,
        sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
        allowEdit: true,
        encodingType: this.camera.EncodingType.JPEG,
        saveToPhotoAlbum: false
      };

      this.camera.getPicture(options).then((imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        this.myDocument =  'data:image/jpeg;base64,' + imageData;

        const data = {
          byteArray: this.myDocument,
          uuid: this.providerInfo.uuid,
          mimetype: 'image/jpeg'
        };

        this.authService.provPicSave(data).then((returnValue) => {
          this.getProvInfo();
        });

      }, (err) => {
        // Handle error
        console.log('getpicture: ' + JSON.stringify(err));
      });
    }
  }

  async dismiss() {
    await this.location.back();
  }

  async logOut() {
    let loading = await this.loadingCtrl.create({
      message: 'Logging out...'
    });
    await loading.present();
    this.authService.logout().then((returnValue) => {
      loading.dismiss();
      // Give the menu time to close before changing to logged out
      if (returnValue) {
        this.localstorage.clearStorage();
        this.navCtrl.navigateForward('/login');
      }
    });
  }
}
