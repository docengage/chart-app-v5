import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutProviderPage } from './about-provider.page';

const routes: Routes = [
  {
    path: '',
    component: AboutProviderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AboutProviderPageRoutingModule {}
