import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AboutProviderPage } from './about-provider.page';

describe('AboutProviderPage', () => {
  let component: AboutProviderPage;
  let fixture: ComponentFixture<AboutProviderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutProviderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AboutProviderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
