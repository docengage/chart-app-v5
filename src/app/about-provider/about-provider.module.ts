import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AboutProviderPageRoutingModule } from './about-provider-routing.module';

import { AboutProviderPage } from './about-provider.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AboutProviderPageRoutingModule,
    ComponentsModule
  ],
  declarations: [AboutProviderPage]
})
export class AboutProviderPageModule {}
