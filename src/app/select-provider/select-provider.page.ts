import { Component, OnInit } from "@angular/core";
import { ModalController, NavParams } from "@ionic/angular";
import { Handlebar } from "../components/handlebar";
import { AuthService } from "../service/auth.service";
import { MasterDataService } from "../service/master-data";

@Component({
  selector: "app-select-provider",
  templateUrl: "./select-provider.page.html",
  styleUrls: ["./select-provider.page.scss"],
})
export class SelectProviderPage implements OnInit {
  selectedId: string;
  selectedName: string;
  searchTerm: string;
  masterFilters: any = [];
  masterTeamFilters: any = [];
  apptFilters: any;
  searchVal: string;
  timer: any;
  loading: any;
  providers: any;
  teams: any;
  selectedValue: string;
  showRecords = false;
  currentPageClass = this;

  triggerAlphaScrollChange = 0;
  ccid = 0;
  type = "";

  constructor(
    public masterData: MasterDataService,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public authService: AuthService,
    private handlebar: Handlebar
  ) {}

  ngOnInit() {
    this.selectedId = this.navParams.get("selectedId");
    if (!this.selectedId) {
      this.selectedId = "0";
    }
    this.ccid = this.navParams.get("ccid");
    this.type = this.navParams.get("type");
    if (this.handlebar.isEmpty(this.ccid)) {
      this.ccid = 0;
    }
    this.authService.isTokenValid().then((returnVal) => {
      this.getProvidersList();
      if (this.type === "Assign To") {
        this.getTeamList();
      }
    });
  }

  getTeamList() {
    this.authService.getNotifyTeams().then((data) => {
      this.teams = data;
      //   this.masterTeamFilters = data;
      this.masterData.storeDataInLocalDB(this.teams, "teams");
    });
  }

  getProvidersList() {
    this.authService
      .getScheduleProvidersBySpeciality(this.ccid)
      .then((data) => {
        this.providers = data;
        this.masterFilters = data;
        if (this.ccid === 0) {
          this.masterData.storeDataInLocalDB(this.providers, "providers");
        }
      });
  }

  onSelectItem(item, isTeam) {
    if (item) {
      let data1 = {
        selectedValue: item.fullname,
        selectedId: item.provider_id,
        selectedEmpId: item.employee_id,
      };
      this.dismiss(true, data1);
    } else {
      this.dismiss(false);
    }
  }

  onSearchInput() {
    if (this.handlebar.isNotEmpty(this.searchTerm)) {
      clearTimeout(this.timer);
      if (this.searchVal !== this.searchTerm) {
        const temp = this;
        const temData = this.providers;
        temp.searchVal = temp.searchTerm;
        temp.showRecords = true;
        temp.providers = [];
        const masterffilter = [];
        temData.filter((item) => {
          const providers: any = [];
          let hasEle = false;
          for (let i = 0; i < item.providers.length; i++) {
            masterffilter.push(item);
            if (
              item.providers[i].fullname
                .toLowerCase()
                .indexOf(temp.searchTerm.toLowerCase()) > -1
            ) {
              providers.push(item.providers[i]);
              hasEle = true;
            }
          }
          if (hasEle) {
            item.providers = providers;
            temp.providers.push(item);
          }
          temp.masterFilters = masterffilter;
        });
      }
    } else {
      this.getProvidersList();
    }
  }

  dismiss(isUpdated, data?: any) {
    this.modalCtrl.dismiss(data, isUpdated);
  }
}
