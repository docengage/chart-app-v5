import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectProviderPageRoutingModule } from './select-provider-routing.module';

import { SelectProviderPage } from './select-provider.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectProviderPageRoutingModule
  ],
  declarations: [SelectProviderPage]
})
export class SelectProviderPageModule {}
