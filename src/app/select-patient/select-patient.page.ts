import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController, NavController, NavParams } from '@ionic/angular';
import { AuthService } from '../service/auth.service';
import { LocalStorageService } from '../service/local-storage.service';
import { ToastServiceData } from '../service/toast-service';
import { AddPatientPage } from '../add-patient/add-patient.page';

@Component({
  selector: 'app-select-patient',
  templateUrl: './select-patient.page.html',
  styleUrls: ['./select-patient.page.scss'],
})
export class SelectPatientPage implements OnInit {
  items: any[];
  totalPages: number;
  currentPage: number;
  noRecordFound: any;
  queryText: any;
  timer: any;
  searchVal: any;
  pageType: any;
  accessControl: any;

  constructor(
    private authService: AuthService, 
    public navParam: NavParams,
    private toastServ: ToastServiceData,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private nav: NavController,
    public localStorge: LocalStorageService,
  ) { }

  ngOnInit() {
    this.pageType = this.navParam.get("pageType");
    this.getMyPatientsList(true);
    this.getAccessControl();
  }

  getAccessControl() {
    this.localStorge.getAccessControl().then((accessObj: any) => {
      if (accessObj) {
        this.accessControl = accessObj.permissions;
      }
    });
  }

  doRefresh(event) {
    this.getMyPatientsList(true);
    setTimeout(() => {
      event.target.complete();
      this.toastServ.presentToast('Patients List Refreshed');
    }, 1000);
  }

  getMyPatientsList(isEmpty) {
    if (isEmpty) {
      this.items = [];
      this.totalPages = 0;
      this.currentPage = 1;
    }
    this.authService.searchPatientsList(this.queryText, this.currentPage).then((data) => {
      if (data && data.patients) {
        
        for (let i = 0; i < data.patients.length; i++) {
          this.items.push(data.patients[i]);
        }
        this.totalPages = data.patients.length;
      } else {
        let recordName = "Patients"
        this.noRecordFound = this.toastServ.displayNoRecordsFound(this.items, recordName);
      }
    });
  }

  searchPatients() {
    clearTimeout(this.timer);
    if (this.searchVal != this.queryText) {
      let temp = this;
      this.timer = setTimeout(function () {
        temp.searchVal = temp.queryText;
        temp.getMyPatientsList(true);
      }, 400);
    }
  }

  async patientSelected(data) {
    const loader = await this.loadingCtrl.create({
      message: 'Loading...'
    });
    await loader.present();
    this.dismiss(true, data);
    loader.dismiss();
  }

  dismiss(isUpdated, data) {
    this.modalCtrl.dismiss(data, isUpdated);
  }

  async openAddPatientPage() {
    const data = {
      pageType: 'quickSchedule'
    };

    const patientModal = await this.modalCtrl.create({
      component: AddPatientPage,
      componentProps: data
    });
    await patientModal.present();
    patientModal.onDidDismiss().then((data0) => {
      if (data0 && data0.role && data0.role !== 'backdrop'){
        this.getMyPatientsList(true);
      }
    });
  }

  doInfinite(event) {
    var clientHeight = event.target.clientHeight;
    var scrollUpto = event.target.scrollHeight - event.target.scrollTop;
    if (clientHeight === scrollUpto) {
      this.currentPage = Number(this.currentPage) + 1;
      setTimeout(() => {
        setTimeout(() => {
          if (this.totalPages >= 15) {
            this.getMyPatientsList(false);
          }
          event.target.complete();
        }, 500);
      });
    } 
  }  

}
