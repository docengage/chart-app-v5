import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TimeLineItemDetailsPageRoutingModule } from './time-line-item-details-routing.module';

import { TimeLineItemDetailsPage } from './time-line-item-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TimeLineItemDetailsPageRoutingModule
  ],
  declarations: [TimeLineItemDetailsPage]
})
export class TimeLineItemDetailsPageModule {}
