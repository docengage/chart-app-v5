import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimeLineItemDetailsPage } from './time-line-item-details.page';

const routes: Routes = [
  {
    path: '',
    component: TimeLineItemDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TimeLineItemDetailsPageRoutingModule {}
