import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { AuthService } from '../service/auth.service';
import { Handlebar } from '../components/handlebar';
import { ToastServiceData } from '../service/toast-service';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';
import { TableColumnsPage } from '../table-columns/table-columns.page';

@Component({
  selector: 'app-time-line-item-details',
  templateUrl: './time-line-item-details.page.html',
  styleUrls: ['./time-line-item-details.page.scss'],
})
export class TimeLineItemDetailsPage {

  icons: string[];
  referenceType: string;
  referenceId: String;
 patientName: String;
 item: any[];
 pdfLink:any;
 content: any;

 noMoreRecords: string;

 constructor( public navParams: NavParams,
              public authService: AuthService,
              public handlebar: Handlebar,
              private file: File,
              private fileOpener: FileOpener,
              public toastServ: ToastServiceData,
              public modalCtrl: ModalController) {
   this.patientName = this.navParams.get('patientName');
   this.referenceType = this.navParams.get('referenceType');
   this.referenceId = this.navParams.get('referenceId');
 }
 ionViewDidEnter() {
   this.authService.isTokenValid().then((returnVal)=>{
     this.getPlannedItems();
   });
 }
 openFile(url,mimeType,base64,filename){
     const writeDirectory = this.file.externalDataDirectory;
     this.file.writeFile(writeDirectory, filename, this.convertBaseb64ToBlob(base64, 'data:'+mimeType+';base64'), {replace: true})
       .then(() => {
           this.fileOpener.open(writeDirectory + filename, mimeType)
               .catch(() => {
                   this.toastServ.presentToast('Error opening file');
               });
       })
       .catch(() => {
         this.toastServ.presentToast('Error writing file');
   });

}
convertBaseb64ToBlob(b64Data, contentType): Blob {
 contentType = contentType || '';
 const sliceSize = 512;
 b64Data = b64Data.replace(/^[^,]+,/, '');
 b64Data = b64Data.replace(/\s/g, '');
 const byteCharacters = window.atob(b64Data);
 const byteArrays = [];
 for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
 }
return new Blob(byteArrays, {type: contentType});
}
  async openColumns(secCounter,fieldCounter,rowCounter){
 let row = this.content[secCounter].sectionContent[fieldCounter].rowContentValuesList[rowCounter];
 let fieldLabel = this.content[secCounter].sectionContent[fieldCounter].field_label;
 let sectionName = this.content[secCounter].sectionName;
 let colElem = this.content[secCounter].sectionContent[fieldCounter].columnValues;
 let data = {
   "rowElem" : row,
   "sectionName" : sectionName,
   "fieldLabel" : fieldLabel,
   "colElem":colElem,
   "secCounter":secCounter,
   "fieldCounter":fieldCounter,
   "rowCounter":rowCounter,
 }

 let modal = await this.modalCtrl.create({
   component: TableColumnsPage,
   componentProps: data
  });
 await modal.present();
 modal.onDidDismiss().then((data) => {
   if(data){
     let secCounter = Number(data.data["secCounter"]);
     let fieldCounter = Number(data.data["fieldCounter"]);
     let rowCounter = Number(data.data["rowCounter"]);
     this.content[secCounter].sectionContent[fieldCounter].rowContentValuesList[rowCounter] = data["rowElem"];
   }
 });

}
 getPlannedItems(){
   this.item = [];
   let referenceIds = this.referenceId.split(",");
   for(let i=0;i<referenceIds.length;i++){
     this.authService.getPatientTimeLineItemDetails(referenceIds[i],this.referenceType).then((data)=>{

         if(this.referenceType!=="vitals"){
           data.itemDetails = data.itemDetails;
         }
         if(this.referenceType==="invoiced"){
          data.itemDetails.lineItems = JSON.parse(data.itemDetails.invoiceLineitemData);

         }
         else if(this.referenceType==="careplan"){
           for(let k=0;k<data.itemDetails.carePlanItems.length;k++){
             if(data.itemDetails.carePlanItems[k].clinical_notes != null){
               data.itemDetails.carePlanItems[k].clinical_notes_list = JSON.parse(data.itemDetails.carePlanItems[k].clinical_notes);
             }
           }

         }
         else if(this.referenceType==="progressnote" || this.referenceType==="clinicalnote"
          || this.referenceType==="instruction"  || this.referenceType==="history" ){
           if(data.itemDetails.suggested_plan != null){
               data.itemDetails.suggestedplans = JSON.parse(data.itemDetails.suggested_plan);
           }

         }
         else if(this.referenceType==="estimated"){
           if(data.itemDetails.estimationLineitemData != null){
             data.itemDetails.lineItems = JSON.parse(data.itemDetails.estimationLineitemData.value);
           }
         }
         else if(this.referenceType==="laborder"){
           if(data.itemDetails.orderLineitemData != null){
             data.itemDetails.lineItems = JSON.parse(data.itemDetails.orderLineitemData.value);
           }
         }else if(this.referenceType==="DynamicForm"){
           if(data.itemDetails.FormData != null){
             data.itemDetails.FormDataContent = JSON.parse(data.itemDetails.FormData);
             let content: any = JSON.parse(data.itemDetails.FormDataContent.content);
             if(data.itemDetails.FormDataContent){
               data.itemDetails.form_name = data.itemDetails.FormDataContent.form_name;
               data.itemDetails.disclaimerContent = data.itemDetails.FormDataContent.disclaimerContent;
               data.itemDetails.isDisclaimer = data.itemDetails.FormDataContent.isDisclaimer;
               data.itemDetails.signature_image_url = data.itemDetails.FormDataContent.signature_image_url;
               data.itemDetails.is_signature_required = data.itemDetails.FormDataContent.is_signature_required;

             }
             data.itemDetails.content = content;
             this.content = content;
           }
         }
         if(this.referenceType==="vitals" || this.referenceType==="medication"){
           this.item.push(data.itemDetails);
         }else{
           this.item = data.itemDetails;
         }
     });
   }
 }

 goBack(){
   this.modalCtrl.dismiss();
 }
}
