import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { LocalStorageService } from '../service/local-storage.service';
import { LoadingController, ModalController, NavController } from '@ionic/angular';
import { NavigationExtras, Router } from '@angular/router';
import { ToastServiceData } from '../service/toast-service';
import { Todos } from '../service/todos';
import { MasterDataService } from '../service/master-data';
import { Handlebar } from '../components/handlebar';
import { AppSettingsPage } from '../app-settings/app-settings.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  loading: any;
  loginData = { loginId: '', password: '', organizationId : 0 };
  loginCredentialOrg ={ loginId: '', password: '', organizationList : {}};
  data: any;
  errorMessage: string;
  submitted = false;
  passwordType = 'password';
  showPass = false;
  isErrorExist = false;
  organizationList: any ;

  constructor(private authService: AuthService,
              private localstorage: LocalStorageService,
              private loadingCtrl: LoadingController,
              private router: Router,
              private toastServ: ToastServiceData,
              private modalCtrl: ModalController,
              private todos: Todos,
              private master: MasterDataService,
              private handlebar: Handlebar) {
    this.ionViewDidLoad();
  }

  ionViewDidLoad() {
    // this.authService.isTokenValid().then((returnVal) => {
    //   if ( returnVal ) {
    //     this.router.navigate(['tabs/myCharts']);
    //   }
    // });
    this.localstorage.clearStorage();
   }
  login() {
    if (this.handlebar.isNotEmpty(this.loginData.loginId) && this.handlebar.isNotEmpty(this.loginData.password)) {
      this.authService.login(this.loginData,'home').then((result) => {
        this.router.navigate(['home']);
      }, (err) => {
        let errMsg = err.json().description;
        if (!errMsg) {
          errMsg = 'Not able to login! Please check your internet connection or connection URL.';
        }
      });
    }else{
      this.submitted = true;
      this.errorMessage = "Please Enter User credentials.";
      this.isErrorExist = true;
    }
  }

  async doLogin() {
    if (this.handlebar.isNotEmpty(this.loginData.loginId) && this.handlebar.isNotEmpty(this.loginData.password)) {
    this.showLoader();
    this.loading = await this.loadingCtrl.create({
      message: 'Authenticating...'
  });

    this.loading.present().then(() => {
        this.authService.login(this.loginData,'home').then((result :any) => {
            this.isErrorExist = false;
            this.errorMessage = '';
            this.localstorage.setUserId(this.loginData.loginId);
            this.localstorage.setPassword(this.loginData.password);
            this.localstorage.setOrgId(this.loginData.organizationId);
            this.submitted = true;
            if (result && result[0] && result[0] === 'error') {
                this.loading.dismiss();
                this.toastServ.presentToast('Invalid Credentials');
            } else if (result.length > 0) {
              this.loading.dismiss();
              this.loginCredentialOrg.loginId = this.loginData.loginId; 
              this.loginCredentialOrg.password = this.loginData.password; 
              this.loginCredentialOrg.organizationList = JSON.stringify(result); 
              const data0: NavigationExtras = {
                queryParams: this.loginCredentialOrg 
              };
              this.router.navigate(['/select-organization'], data0);
            } else {
                this.todos.recreateDB();
                this.data = result;
                this.localstorage.setToken(this.data.access_token);
                setTimeout(() => {
                 this.loading.dismiss();
                 this.authService.getLoggedInUserInfo().then((data) => {
                        this.master.storeDataInLocalDB(data, 'loggedInProvider');
                        this.localstorage.setLoggedInProvider(data.uuid);
                        this.saveDeviceInformation(data.uuid);
                        this.authService.getProviderPermissions(data.uuid).then((accessData) => {
                            this.localstorage.setAccessControl(accessData);
                            const accessControl = accessData;
                            const data0 = {
                                accessControl
                            };
                           // this.events.publish('accessControl:changed', accessControl);
                            this.master.getAllMasterData();
                            if (data.uuid) {
                            this.router.navigate(['home']);
                            }
                       });
                });
            }, 100);
            }
        }, (err) => {
            let errMsg = err.json().description;
            if (!errMsg) {
                errMsg = 'Not able to login! Please check your internet connection or connection URL.';
            }
            this.isErrorExist = true;
            this.errorMessage = errMsg;

            this.loading.dismiss();
        });
    });
    } else {
        this.submitted = true;
        this.isErrorExist = true;
        this.errorMessage = "Please Enter User credentials.";
    }
}
saveDeviceInformation(uuid) {
    this.localstorage.getDeviceToken().then((deviceToken) => {
        if (deviceToken && uuid) {
            this.authService.saveDeviceInfo(deviceToken, uuid);
        }
    });
}

forceLogout() {
    if (this.handlebar.isEmpty(this.loginData.loginId)) {
        alert('Please Provide login Id to do Forece Signout!');
      //  this.toastServ.presentToast('Please Provide login Id to do Forece Signout!');
    } else {
      this.authService.forceLogout(this.loginData.loginId, 0).then(() => {
        this.isErrorExist = false;
        this.errorMessage = '';
      });
    }

}


showPassword() {
    this.showPass = !this.showPass;

    if (this.showPass) {
      this.passwordType = 'text';
    } else {
      this.passwordType = 'password';
    }
}

async showLoader() {
    this.loading = await this.loadingCtrl.create({
        message: 'Authenticating...'
    });
}

openSettings() {
    this.localstorage.getEnvironment().then(async (env) => {
        const data0 = {
            env
        };
        const modal = await this.modalCtrl.create({
          component: AppSettingsPage,
          componentProps: data0
        });
        await modal.present();

        // modal.onWillDismiss(() => {
        // });
    });
  }
}
