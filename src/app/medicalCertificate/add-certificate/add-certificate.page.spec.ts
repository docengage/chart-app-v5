import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddCertificatePage } from './add-certificate.page';

describe('AddCertificatePage', () => {
  let component: AddCertificatePage;
  let fixture: ComponentFixture<AddCertificatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCertificatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddCertificatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
