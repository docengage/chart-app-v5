import { Component, OnInit, ViewChild } from "@angular/core";
import {
  IonDatetime,
  LoadingController,
  ModalController,
  NavParams,
} from "@ionic/angular";
import * as moment from "moment";
import { Handlebar } from "src/app/components/handlebar";
import { AuthService } from "src/app/service/auth.service";
import { LocalStorageService } from "src/app/service/local-storage.service";
import { ToastServiceData } from "src/app/service/toast-service";
import { CertificateTemplateComponent } from "../certificate-template/certificate-template.component";

@Component({
  selector: "app-add-certificate",
  templateUrl: "./add-certificate.page.html",
  styleUrls: ["./add-certificate.page.scss"],
})
export class AddCertificatePage implements OnInit {
  patientInfo: any;
  uhid: any;
  certData: any;
  careCenterId: any;
  encounterId: any = "";
  certificateId: any;
  allCert: any;
  certificate: string = "";
  certName: any;
  providerInfo: any[];
  accessControl: any;
  enctrData: any;
  providerId: any;
  isValidDate = true;
  isValidTime = true;
  prescriptionTime: any;
  prescriptionDate: any;
  formatedTime: any;
  formatedDate: any;
  provData: any;
  careCntrData: any;
  pageType: any;
  showDiv: boolean = true;
  displayName: string;
  provName: string;
  pageLoad: any;

  @ViewChild(IonDatetime) datetime: IonDatetime;
  showModal = false;
  presentation: any;

  constructor(
    public navParam: NavParams,
    private modalCtrl: ModalController,
    private authService: AuthService,
    private toastServ: ToastServiceData,
    private localstorage: LocalStorageService,
    public handlebar: Handlebar,
    private loadingController: LoadingController
  ) {}

  ngOnInit() {
    this.patientInfo = this.navParam.get("patientInfo");
    this.uhid = this.patientInfo.patientUhid;
    this.certificateId = this.navParam.get("certId");
    this.pageType = this.navParam.get("pageType");

    this.localstorage.getAccessControl().then((accessObj: any) => {
      if (accessObj) {
        this.accessControl = accessObj.permissions;
      }
    });

    this.getPatientCert();
    this.getProviderInfo();
    this.getAllProvByOrgId();
    this.getAllCareCntr();
  }

  dismiss(isUpdated) {
    this.modalCtrl.dismiss(isUpdated);
  }

  getAllProvByOrgId() {
    this.authService.getAllProvByOrgId().then((data) => {
      this.provData = JSON.parse(JSON.stringify(data));
    });
  }

  getAllCareCntr() {
    this.authService.getAllCareCntr().then((data) => {
      this.careCntrData = JSON.parse(JSON.stringify(data));
    });
  }

  getPatientCert() {
    this.authService
      .getPatientCert(this.uhid, this.certificateId)
      .then((data) => {
        this.certData = data[0];
        if (this.handlebar.isNotEmpty(this.certData)) {
          this.certificate = this.handlebar.stripHtmlData(
            this.certData.certificate
          );
          this.certName = this.certData.certificate_name;
          this.encounterId = this.certData.encounter_id;
        } else {
          this.certificate = "";
          this.certName = "";
        }
        this.getLastTenEnctr(this.uhid);
        this.allCert = data[2];
      });
  }

  getProviderInfo() {
    this.providerInfo = [];
    this.authService.getProviderInfo().then((data) => {
      this.providerInfo = data;
    });
  }

  getLastTenEnctr(uhid) {
    this.authService.getLastTenEnctr(uhid).then((data) => {
      this.enctrData = data;
      if (this.certificateId === 0 && this.enctrData) {
        this.encounterId = this.enctrData[0].encounter_id;
      }
      this.changeEncounter();
    });
  }

  changeEncounter() {
    if (
      this.handlebar.isNotEmpty(this.encounterId) &&
      this.handlebar.isNotEmpty(this.enctrData)
    ) {
      let onSelectedEnctr;
      for (var i = 0; i < this.enctrData.length; i += 1) {
        if (this.enctrData[i].encounter_id == this.encounterId) {
          onSelectedEnctr = this.enctrData[i];
        }
      }
      this.setResetData(false, onSelectedEnctr);
      this.showDiv = true;
    } else {
      this.setResetData(true, "");
      this.showDiv = false;
    }
  }

  setResetData(isResetData, selectedEnctr) {
    if (isResetData) {
      this.careCenterId = "";
      this.providerId = "";
      this.displayName = "";
      this.provName = "";
      this.formatedDate = moment().format();
      this.prescriptionDate = moment().format("DD-MM-YYYY");
      this.prescriptionTime = moment().format("hh:mm A");
    } else if (!isResetData && selectedEnctr) {
      this.careCenterId = selectedEnctr.carecenterid;
      this.providerId = selectedEnctr.providerid;
      this.displayName = selectedEnctr.display_name;
      this.provName = selectedEnctr.providername;
      this.formatedDate = moment(selectedEnctr.encounterdate).format();
      this.prescriptionDate = moment(selectedEnctr.encounterdate).format(
        "DD-MM-YYYY"
      );
      this.prescriptionTime = moment(selectedEnctr.encounterdate).format(
        "hh:mm A"
      );
    }
  }

  checkdate(txtDate) {
    var currVal = txtDate;
    if (!currVal) return false;

    const rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    const dtArray = currVal.match(rxDatePattern);

    if (dtArray == null) return false;

    //Checks for mm/dd/yyyy format.
    const dtMonth = dtArray[3];
    const dtDay = dtArray[1];
    const dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12) return false;
    else if (dtDay < 1 || dtDay > 31) return false;
    else if (
      (dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) &&
      dtDay == 31
    )
      return false;
    else if (dtMonth == 2) {
      const isleap =
        dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0);
      if (dtDay > 29 || (dtDay == 29 && !isleap)) return false;
    }
    return true;
  }

  setDateModal(value, type) {
    this.formatedDate = value ? value : moment().format();
    this.presentation = type;
    this.showModal = true;
  }

  dateChange(value) {
    if (this.handlebar.isEmpty(value) || value === undefined) {
      value = moment().format();
    }
    this.formatedDate = value;
    this.prescriptionDate = moment(value).format("DD-MM-YYYY");
    this.prescriptionTime = moment(value).format("hh:mm A");

    this.dateCancel();
  }

  dateCancel() {
    if (this.datetime) {
      this.showModal = false;
      this.datetime.cancel(true);
    }
  }

  async presentLoading() {
    this.pageLoad = await this.loadingController.create({
      message: "Please wait...",
      duration: 2000,
    });
    await this.pageLoad.present();
  }

  saveCert() {
    this.presentLoading().then(() => {
      this.pageLoad.message = "validating Data...";
      const isValid = this.doValidation();
      let fullDate = this.prescriptionDate + "" + this.prescriptionTime;
      if (
        this.handlebar.isEmpty(this.encounterId) &&
        this.encounterId.length === 0
      ) {
        fullDate = moment(fullDate, "DD-MM-YYYY hh:mm A").format(
          "DD-MM-YYYY HH:mm"
        );
      } else {
        fullDate = moment(fullDate, "DD MMM YY hh:mm A").format(
          "DD-MM-YYYY hh:mm A"
        );
      }

      if (this.handlebar.isNotEmpty(isValid)) {
        this.toastServ.presentToast(isValid);
      } else {
        this.pageLoad.message = "Saving Data...";
        const data0 = {
          encounterId: this.encounterId,
          certificate: this.certificate,
          providerId: this.providerId,
          careCenterId: this.careCenterId,
          date: fullDate,
          uhid: this.uhid,
          certificateName: this.certName,
        };
        this.authService
          .saveCertInfo(data0, this.certificateId)
          .then((data) => {
            if (
              data &&
              (data.status === 400 ||
                data.status === 401 ||
                data.status === 500)
            ) {
              const data0 = JSON.parse(data._body);
              this.toastServ.presentToast(data0.description);
            } else if (data) {
              this.dismiss(true);
              this.toastServ.presentToast(
                "Medical Certificate added successfully."
              );
              this.pageLoad.dismiss();
            }
          });
      }
    });
  }
  doValidation() {
    let global = "";
    this.isValidTime = true;
    if (this.handlebar.isEmpty(this.certificate)) {
      global = "Type Some Note.<br>";
    }

    if (this.handlebar.isEmpty(this.providerId)) {
      global = global + "Provider Name required<br>";
    }

    if (this.handlebar.isEmpty(this.careCenterId)) {
      global = global + "Care Center required<br>";
    }

    if (
      this.handlebar.isEmpty(this.encounterId) &&
      this.encounterId.length === 0
    ) {
      if (!this.accessControl.Appointmentscreate) {
        const error =
          "Since the user no permission to create on the spot appointment required for walking use case, the user has to select an existing visit to create note.<br>";
        global = global + error;
      }
      var isDateValid = this.checkdate(
        moment(this.prescriptionDate, "DD-MM-YYYY").format("DD-MM-YYYY")
      );
      if (isDateValid) {
        let presTime = moment(this.prescriptionTime, "hh:mm A").format(
          "hh:mm A"
        );

        let validTime =
          presTime && presTime.match(/^(0?[1-9]|1[012])(:[0-5]\d) [APap][mM]$/);
        if (!validTime) {
          this.isValidTime = false;
          global = global + "Entered time is not valid.<br>";
        }
      } else {
        global = global + "Entered date is not valid.<br>";
      }
    }
    return global;
  }

  async navigateToSelectModel() {
    let data0 = {
      allCert: this.allCert,
      uhid: this.uhid,
    };
    let modal = await this.modalCtrl.create({
      component: CertificateTemplateComponent,
      componentProps: data0,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: "chartModelStyle",
    });
    await modal.present();

    var chartPage = document.getElementsByTagName("app-patient-charting");
    chartPage[0].classList.add("disableClick");

    modal.onDidDismiss().then((data) => {
      chartPage[0].classList.remove("disableClick");
      if (data.data) {
        this.replaceItem(data.data);
      }
    });
  }
  replaceItem(selectedValue: any) {
    this.certName = selectedValue.certName;
    this.certificate = this.handlebar.stripHtmlData(selectedValue.certificate);
  }
}
