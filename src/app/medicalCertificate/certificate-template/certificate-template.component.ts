import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { Handlebar } from 'src/app/components/handlebar';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-certificate-template',
  templateUrl: './certificate-template.component.html',
  styleUrls: ['./certificate-template.component.scss'],
})
export class CertificateTemplateComponent implements OnInit {
  allCert: any;
  contentLoading = false;
  selectedValue: any;
  certificate: any;
  uhid: any;
  allData: any;
  newCertTmplt: any;
  newCertData: any;

  constructor(public navParam: NavParams,
    private modalCtrl: ModalController,
    private authService: AuthService,
    public handlebar: Handlebar
  ) { }

  ngOnInit() {
    this.contentLoading = true;
    this.allCert = this.navParam.get('allCert');
    this.uhid = this.navParam.get('uhid');
    this.allCert.forEach(ele => {
      if (ele.certificate) {
        ele.cert = this.handlebar.stripHtml(ele.certificate, 40);
      }
    });
    this.contentLoading = false;
  }

  onItemClick(item) {
    if (this.uhid !== "all") {
      this.displayTemplate(item);
    }
  }

  async displayTemplate(item) {
    this.authService.getCertTmpltData(item.certificate, this.uhid).then((data) => {
      if (data === false){
      } else {
        this.selectedValue = item;
        this.selectedValue.cert = this.handlebar.stripHtmlData(data);
      }
      this.dismiss();
    });

  }

  dismiss() {
    if (this.selectedValue) {
      const returnData = {
        certName: this.selectedValue.certificate_name,
        certificate: this.selectedValue.cert
      };
      this.modalCtrl.dismiss(returnData);
    } else {
      this.modalCtrl.dismiss();
    }
  }

}
