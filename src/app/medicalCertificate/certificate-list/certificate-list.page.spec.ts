import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CertificateListPage } from './certificate-list.page';

describe('CertificateListPage', () => {
  let component: CertificateListPage;
  let fixture: ComponentFixture<CertificateListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertificateListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CertificateListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
