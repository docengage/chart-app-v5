import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CertificateListPageRoutingModule } from './certificate-list-routing.module';

import { CertificateListPage } from './certificate-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CertificateListPageRoutingModule
  ],
  declarations: [CertificateListPage]
})
export class CertificateListPageModule {}
