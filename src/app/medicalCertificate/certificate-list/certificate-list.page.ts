import { Component, OnInit } from '@angular/core';
import { Printer, PrintOptions } from '@ionic-native/printer/ngx';
import { ModalController, NavParams } from '@ionic/angular';
import * as moment from 'moment';
import { Handlebar } from 'src/app/components/handlebar';
import { AuthService } from 'src/app/service/auth.service';
import { LocalStorageService } from 'src/app/service/local-storage.service';

import { ToastServiceData } from 'src/app/service/toast-service';
import { AddCertificatePage } from '../add-certificate/add-certificate.page';

@Component({
  selector: 'app-certificate-list',
  templateUrl: './certificate-list.page.html',
  styleUrls: ['./certificate-list.page.scss'],
})
export class CertificateListPage implements OnInit {
  uuid: any;
  patientInfo: any;
  accessControl: string;
  pageno: string;
  uhid: string;
  certData: any;
  currentDate: any;
  certPrintData: any;

  constructor(public navParam: NavParams,
    private modalCtrl: ModalController,
    private authService: AuthService,
    private toastServ: ToastServiceData,
    private printer: Printer,
    private localstorage: LocalStorageService,
    public handlebar: Handlebar
  ) { }

  ngOnInit() {

    this.patientInfo = this.navParam.get('patientInfo');
    this.uhid = this.patientInfo.patientUhid;
    this.currentDate = moment().format('DD/MM/YYYY');
    this.localstorage.getAccessControl().then((accessObj) => {
      this.accessControl = accessObj;
    });

    this.getCertData();
  }

  getCertData() {
    this.authService.getCertData(this.uhid, 1).then((data) => {
      this.certData = data;
      this.certData.forEach(ele => {
        if (ele.certificate) {
          ele.certName = this.handlebar.stripHtml(ele.certificate, 30);
        }
      });
    });
  }

  shareCertificate(item) {
    const data0 = {
      htmlContext: item.certificate,
      htmlSubject: "Medical Certificate"
    }
    this.authService.sendTimelineDataByMail(data0, this.uhid).then((data) => {
      if (data && (data.status === 400 || data.status === 401 || data.status === 500)) {
        const data0 = JSON.parse(data._body);
        this.toastServ.presentToast(data0.description);
      } else if (data) {
        this.toastServ.presentToast('Mail Send Succesfully.');
      }
    });
  }

  printCert(item) {

    this.certPrintData = item.certificate;
    let element = document.getElementById('getElemnetVal').innerHTML;

    let options: PrintOptions = {
      name: 'MyDocument',
      printer: 'printer007',
      duplex: true,
      orientation: 'landscape'
    };

    this.printer.print(element, options).then((value: any) => {
      console.log('value:', value);
    }, (error) => {
      console.log('error:', error);
    });
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  async addCertificate() {
    let data0 = {
      patientInfo: this.patientInfo,
      certId: 0,
      pageType: 'Issue'
    }
    const modal = await this.modalCtrl.create({
      component: AddCertificatePage,
      componentProps: data0
    });
    await modal.present();

    modal.onWillDismiss().then((data) => {
      if (data && data.data === true) {
        this.getCertData();
      }
    });
  }

  async editCertificate(item) {
    if (this.accessControl && this.accessControl["permissions"] && this.accessControl["permissions"].MedicalCertificate5674 && this.accessControl["permissions"].MedicalCertificatecreate) {
      let data0 = {
        patientInfo: this.patientInfo,
        certId: item.patient_certificate_id,
        pageType: 'Edit'
      }
      const modal = await this.modalCtrl.create({
        component: AddCertificatePage,
        componentProps: data0
      });
      await modal.present();

      modal.onWillDismiss().then((data) => {
        if (data && data.data === true) {
          this.getCertData();
        }
      });
    } else {
      this.toastServ.presentToast("You don't permission to edit Certificate.");
    }

  }

}
