import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CertificateListPage } from './certificate-list.page';

const routes: Routes = [
  {
    path: '',
    component: CertificateListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CertificateListPageRoutingModule {}
