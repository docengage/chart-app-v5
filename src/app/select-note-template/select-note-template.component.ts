import { Component, OnInit } from '@angular/core';
import { ModalController, LoadingController } from '@ionic/angular';
import { AuthService } from '../service/auth.service';
import { Handlebar } from '../components/handlebar';

@Component({
  selector: 'app-select-note-template',
  templateUrl: './select-note-template.component.html',
  styleUrls: ['./select-note-template.component.scss'],
})
export class SelectNoteTemplateComponent implements OnInit{


  selectedValue : string;
  searchTerm : string;
  noteTemplates: any = [];
  searchVal : string;
  timer : any;
  loading: any;
  contentLoading = false;
  currentPageClass = this;
 
  triggerAlphaScrollChange: number = 0;



  constructor(
    public viewCtrl: ModalController,
    public authService : AuthService,
    public loadingCtrl : LoadingController,
    private handlebar:Handlebar
  ) {
  }
  onItemClick(item) {
    this.triggerAlphaScrollChange++;
    this.selectedValue = item.template;
    this.dismiss();
  }

  ngOnInit() {
    this.contentLoading = true;
    this.searchData('$');
  }

  searchData(itemname){
    if (this.handlebar.isEmpty(itemname)) {
      itemname = "$";
    }

    this.authService.getNoteTemplateMasterData("Clinical Note",itemname).then((data) => {
      this.noteTemplates = data.templates;
      this.contentLoading = false;
    });
  }

  onSearchInput() {
    if (this.searchTerm) {
      if (this.searchVal != this.searchTerm) {
        clearTimeout(this.timer);
        let temp = this;
        this.timer = setTimeout(function() {
          temp.searchVal = temp.searchTerm;
          temp.searchData(temp.searchTerm);
        }, 400);
      }
    } else {
      this.searchData('$');
    }
  }

  dismiss() {
      if (this.selectedValue) {
        const returnData = {
          selectedValue : this.selectedValue
        };
        this.viewCtrl.dismiss( returnData);
      } else {
        this.viewCtrl.dismiss();
      }
  }
}
