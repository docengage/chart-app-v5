import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RequestedServicesPage } from './requested-services.page';

const routes: Routes = [
  {
    path: '',
    component: RequestedServicesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequestedServicesPageRoutingModule {}
