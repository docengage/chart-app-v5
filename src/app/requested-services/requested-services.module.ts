import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RequestedServicesPageRoutingModule } from './requested-services-routing.module';

import { RequestedServicesPage } from './requested-services.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RequestedServicesPageRoutingModule
  ],
  declarations: [RequestedServicesPage]
})
export class RequestedServicesPageModule {}
