import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController, NavParams } from '@ionic/angular';
import { Handlebar } from '../components/handlebar';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-requested-services',
  templateUrl: './requested-services.page.html',
  styleUrls: ['./requested-services.page.scss'],
})
export class RequestedServicesPage implements OnInit {

  searchTerm: string;
  masterFilters: any = [];
  timer: any;
  loading: any;
  serviceList: any;
  selectedValue: string;
  selectedFilters: any = [];
  isSearch = false;
  totalPages: number;
  currentPage = 1;
  selectedValues: any;
  triggerAlphaScrollChange = 0;
  limit = 15;
  ccid = '0';
  type = '';
  SelectedList = [];
  increment = 0;
  isMultiple = true;
  serviceSearchList: any;
  serviceTypePagNo = 1;
  isMoreServiceType =  true;
  clickedType: string;
  serviceTypeList: any;
  serviceType: any;
  searchService: any;
  
  constructor(
    private navParams: NavParams,
    private modalCntrl: ModalController,
    private loadingCtrl: LoadingController,
    private authService: AuthService,
    private handlebar: Handlebar,
  ) { 
    this.ccid = this.navParams.get('careCenterId');
    if (this.handlebar.isEmpty(this.ccid)) {
      this.ccid = '0';
    }
    this.isMultiple = true;
    this.serviceType = '';
    this.increment = this.selectedFilters.length;
    this.totalPages = 0;
    this.currentPage = 1;
  }

  ngOnInit() {
    this.authService.isTokenValid().then(() => {
      if (this.isMultiple){
        this.getServiceTypeFromServices(); 
      } else {
        this.getReqServiceList();
      }
    });
  }

  async serviceSelected(data) {
      const loader = await this.loadingCtrl.create({
        message: 'Loading...'
      });
      await loader.present();
      this.dismiss(true, data);
      loader.dismiss();
  }

  changeServiceByType(type, searchTerm) {
    if (type === this.clickedType) {
      this.clickedType = '';
    } else {
      if (this.handlebar.isEmpty(this.ccid)) {
        this.ccid = '0';
      }
      this.clickedType = type;
      this.serviceTypeList = '';
      if (this.masterFilters && !this.isMultiple){
        this.masterFilters.forEach(ele => {
          if (ele.serviceType === type ) {
            this.serviceTypeList = ele.services;
          }
        });
      } else {
        if (this.handlebar.isEmpty(searchTerm)) {
          this.searchService = '';
        }
        this.authService.getServiceBySearch(this.ccid, this.searchService, 1, type).then((data) => {
          if (data) {
            this.serviceTypeList = data;
          }
        });
      }
    }
  }

  searchServiceData(type, searchTerm) {
    if (this.handlebar.isEmpty(searchTerm)) {
      this.searchService = '';
    }
    this.authService.getServiceBySearch(this.ccid, this.searchService, 1, type).then((data) => {
      if (data) {
        this.serviceTypeList = data;
      }
    });
  }

  getServiceTypeFromServices(){
    this.serviceTypePagNo = 1;
    this.authService.getServiceTypeFromServices(this.serviceTypePagNo).then((data) => {
      this.serviceList = data;
      if (this.serviceList.length === 0){
        this.isMoreServiceType = false;
      } else {
        this.isMoreServiceType = true;
      }
      for (let i = 0; i < this.increment; i=i+1) {
        this.SelectedList[i] = this.selectedFilters[i].health_service_template_id;
      }
    });
  }
  getReqServiceList() {
    if (this.handlebar.isEmpty(this.ccid)) {
      this.ccid = '0';
    }
    this.authService.getServiceListByCareCenterId(this.ccid).then((data) => {
      this.serviceList = data;
      this.masterFilters = data;
      for (let i = 0; i < this.increment; i=i+1) {
        this.SelectedList[i] = this.selectedFilters[i].health_service_template_id;
      }

    });
  }
  unselectItem(id) {
    this.selectedFilters = this.selectedFilters.filter(item => (item.health_service_template_id !== id));
    for (let i = 0; i < this.SelectedList.length; i=i+1) {
      if (this.SelectedList[i] === id) {
        this.SelectedList.splice(i, id);
      }
    }
  }

  doInfinite(infiniteScroll) {
    this.currentPage = Number(this.currentPage) + 1;
    setTimeout(() => {
      setTimeout(() => {
        if (this.currentPage <= this.totalPages) {
          this.authService.getServiceBySearch(this.ccid, this.searchTerm, this.currentPage, 'all').then((data) => {
            this.serviceSearchList = this.serviceSearchList.concat(data);
          });
        }
        infiniteScroll.target.complete();
      }, 500);
    });
  }
  
  loadServiceType(infiniteScroll) {
    this.serviceTypePagNo = Number(this.serviceTypePagNo) + 1;
    setTimeout(() => {
      setTimeout(() => {
          this.authService.getServiceTypeFromServices(this.serviceTypePagNo).then((data) => {
            if (data && data.length === 0){
              this.isMoreServiceType = false;
            } else {
              this.serviceList = this.serviceList.concat(data);
              this.isMoreServiceType = true;
            }
          });
        infiniteScroll.target.complete();
      }, 500);
    });
  }

  onSearchInput() {
    this.isSearch = true;
    this.currentPage = 1;
    if (this.handlebar.isNotEmpty(this.searchTerm)) {
      this.authService.getServiceBySearch(this.ccid, this.searchTerm, this.currentPage, 'all').then((data) => {
        if (data.length === 0) {
          this.serviceSearchList = '';
        } else {
          this.serviceSearchList = data;
          const totl = (this.serviceSearchList[0].full_count) / 15;
          this.totalPages = totl;
          this.isMoreServiceType = false;
        }
      });
    } else {
      this.isSearch = false;
      this.limit = 15;
      this.currentPage = 1;
      if (this.isMultiple) {
        this.getServiceTypeFromServices();
      } else {
        this.getReqServiceList();
      }
    }
  }
  
  dismiss(isUpdated, data?: any) {
    this.modalCntrl.dismiss(data, isUpdated);
  }
}
