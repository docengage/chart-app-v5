import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpModule } from '@angular/http';

import { IonicModule, IonicRouteStrategy, NavParams } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import {NgPipesModule} from 'ngx-pipes';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ComponentsModule } from './components/components.module';
import { AuthService } from './service/auth.service';
import { HttpClientService } from './service/http-client.service';
import { HttpClient } from './service/HttpClient';
import { Handlebar } from './components/handlebar';
import { Todos } from './service/todos';
import { ToastServiceData } from './service/toast-service';
import { MasterDataService } from './service/master-data';
import { ColorgeneratorComponent } from './components/colorgenerator/colorgenerator.component';
import { IonicStorageModule } from '@ionic/storage';
import { Push } from '@ionic-native/push/ngx';
import { Network } from '@ionic-native/network/ngx';

// import { Zoom } from '@ionic-native/zoom/ngx';
import { PipesModule } from './pipes/pipes.module';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { CameraService } from './service/camera-service';
import { Camera } from '@ionic-native/camera/ngx';
import { TimeLineDataPushService } from './service/timeline-auth-service';

import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { CallNumberService } from './service/phone-call-service';
import { PatientChartingPage } from './patient-charting/patient-charting.page';
import { SelectNoteTemplateComponent } from './select-note-template/select-note-template.component';
import { LocalStorageService } from './service/local-storage.service';
import { Printer } from '@ionic-native/printer/ngx';
import { DynamicFormCommon } from './components/dynamicform-common';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment.prod';
import { FilePath } from '@ionic-native/file-path/ngx';
import { CertificateTemplateComponent } from './medicalCertificate/certificate-template/certificate-template.component';
import { CallPopoverComponent } from './components/call-popover/call-popover.component';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { AppSettingsPageModule } from './app-settings/app-settings.module';
import { TimelineFilterPageModule } from './timeline-filter/timeline-filter.module';
import { TimeLineItemDetailsPageModule } from './time-line-item-details/time-line-item-details.module';
import { TableColumnsPageModule } from './table-columns/table-columns.module';
import { TaskItemDetailsPageModule } from './task-item-details/task-item-details.module';
import { ClinicalNotePageModule } from './chartPage/clinical-note/clinical-note.module';
import { AddVitalPartialPageModule } from './chartPage/add-vital-partial/add-vital-partial.module';
import { UnitSelectPageModule } from './chartPage/unit-select/unit-select.module';
import { AddWorkListPageModule } from './chartPage/add-work-list/add-work-list.module';
import { SelectListPageModule } from './chartPage/select-list/select-list.module';
import { PrescriptionListPageModule } from './chartPage/prescription-list/prescription-list.module';
import { AddDocumentPageModule } from './chartPage/add-document/add-document.module';
import { MultiFormPageModule } from './chartPage/multi-form/multi-form.module';
import { DynamicFormSectionPageModule } from './chartPage/dynamic-form-section/dynamic-form-section.module';
import { ChartPageConfigPageModule } from './chartPage/chart-page-config/chart-page-config.module';
import { JoinVcPageModule } from './videoCall/join-vc/join-vc.module';
import { VideoCallPageModule } from './videoCall/video-call/video-call.module';
import { AddLeavePageModule } from './add-leave/add-leave.module';
import { CertificateListPageModule } from './medicalCertificate/certificate-list/certificate-list.module';
import { AddCertificatePageModule } from './medicalCertificate/add-certificate/add-certificate.module';
import { VideoChatPageModule } from './videoCall/video-chat/video-chat.module';
import { AddPrescriptionPageModule } from './chartPage/add-prescription/add-prescription.module';
import { DynamicFormPageModule } from './chartPage/dynamic-form/dynamic-form.module';
import { SelectMedicinePageModule } from './chartPage/select-medicine/select-medicine.module';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { Chooser } from '@ionic-native/chooser/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { PrescriptionTemplatePageModule } from './chartPage/prescription-template/prescription-template.module';

@NgModule({
    declarations: [
        AppComponent,
        SelectNoteTemplateComponent,
        CertificateTemplateComponent,
        CallPopoverComponent,
    ],
    imports: [
        BrowserModule, FormsModule, IonicModule.forRoot(), AppRoutingModule,
        AngularFireModule.initializeApp(environment.firebaseConfig),
        IonicStorageModule.forRoot(), HttpModule, ComponentsModule, NgPipesModule, PipesModule,
        AppSettingsPageModule,
        TimelineFilterPageModule,
        TimeLineItemDetailsPageModule,
        TableColumnsPageModule,
        TaskItemDetailsPageModule,
        ClinicalNotePageModule,
        AddVitalPartialPageModule,
        UnitSelectPageModule,
        AddWorkListPageModule,
        SelectListPageModule,
        PrescriptionListPageModule,
        AddDocumentPageModule,
        MultiFormPageModule,
        DynamicFormSectionPageModule,
        ChartPageConfigPageModule,
        JoinVcPageModule,
        VideoCallPageModule,
        AddLeavePageModule,
        CertificateListPageModule,
        AddCertificatePageModule,
        VideoChatPageModule,
        AddPrescriptionPageModule,
        DynamicFormPageModule,
        SelectMedicinePageModule,
        PrescriptionTemplatePageModule,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        AuthService,
        IonicStorageModule,
        HttpClientService,
        HttpClient,
        Handlebar,
        Todos,
        LocalStorageService,
        ToastServiceData,
        MasterDataService,
        ColorgeneratorComponent,
        Push,
        Network,
        // Zoom,
        CallNumber,
        CameraService,
        TimeLineDataPushService,
        Camera,
        FileChooser,
        FileOpener,
        File,
        FilePath,
        NavParams,
        CallNumberService,
        CallNumberService,
        Printer,
        DynamicFormCommon,
        AngularFireStorage,
        InAppBrowser,
        FileTransfer,
        DocumentViewer,
        PhotoViewer,
        Chooser,
        AndroidPermissions
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
