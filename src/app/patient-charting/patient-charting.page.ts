import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController, NavController, PopoverController, Platform, GestureController, LoadingController } from '@ionic/angular';
import { AuthService } from '../service/auth.service';
import { Handlebar } from '../components/handlebar';
import * as moment from 'moment';
import { LocalStorageService } from '../service/local-storage.service';
import { ClinicalNotePage } from '../chartPage/clinical-note/clinical-note.page';
import { MasterDataService } from '../service/master-data';
import { TimeLineDataPushService } from '../service/timeline-auth-service';
import { AddVitalPartialPage } from '../chartPage/add-vital-partial/add-vital-partial.page';
import { AddWorkListPage } from '../chartPage/add-work-list/add-work-list.page';
import { SelectListPage } from '../chartPage/select-list/select-list.page';
import { AddDocumentPage } from '../chartPage/add-document/add-document.page';
import { DynamicFormPage } from '../chartPage/dynamic-form/dynamic-form.page';
import { MultiFormPage } from '../chartPage/multi-form/multi-form.page';
import { ActivatedRoute } from '@angular/router';
import { ChartPageConfigPage } from '../chartPage/chart-page-config/chart-page-config.page';
import { AddPrescriptionPage } from '../chartPage/add-prescription/add-prescription.page';
import { PrescriptionTemplatePage } from '../chartPage/prescription-template/prescription-template.page';
import { ToastServiceData } from '../service/toast-service';
import { Printer, PrintOptions } from '@ionic-native/printer/ngx';
import { AddCertificatePage } from '../medicalCertificate/add-certificate/add-certificate.page';
import { CallPopoverComponent } from '../components/call-popover/call-popover.component';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallNumberService } from '../service/phone-call-service';
import { Location } from '@angular/common';

declare var window;

@Component({
  selector: 'app-patient-charting',
  templateUrl: './patient-charting.page.html',
  styleUrls: ['./patient-charting.page.scss'],
})
export class PatientChartingPage implements OnInit {

  icons: string[];
  items: Array<{}>;
  totalItems: String;
  uhid: string;
  uuid: string;
  chartingSec: string = "clinical";
  patientInfo: any;
  dynamicForms: any;
  careCenterMasterList: any;
  vitalData: any;
  vitalRecordDate: string;
  allegries: string;
  diagnosis: string;
  suggTreatment: any;
  suggTreatmentLabels: string = "";

  pathologyOrders: any = [];
  pathologyOrdersLabels: string = "";
  pathologydata: Array<{}>;
  investigation_id_pathology: string;
  radiologyOrders: any = []; radiologyOrdersLabels: string = "";
  investigation_id_radiology: string;

  plannedCareItemId: string;
  careCenter: {};
  admissionRecord: {};
  encounterId: number = 0;
  includeFilters: any = [];
  medications: Array<{}>;
  radiologydata: Array<{}>;
  progressNote: Array<{}>;
  sqlVital: Array<{}>;
  accessControl: {};
  prescription: {};
  status: string;
  workList: string;
  isWorklistExist: boolean = false;
  msg: string = "";
  allowAllergiesReqInChart: boolean = false;
  allowSuggPlanReqInChart: boolean = false;
  allowDiagnosisReqInChart: boolean = false;
  formId: any;
  toggleData: any;
  startDateTime: any;
  printSettings: any;
  careCenterData: any;
  isMCI: any;
  certData: any;
  certPrintData: any;
  emailShortStr: any;
  pdfCount: number = 0;
  appointmentId: any;
  appointmentData: any;
  organizationData: any;
  docData: any;
  showPdfBtn: boolean = false;
  fileLoader: any;
  checkedTelephoneNo: string;
  constructor(
    private authService: AuthService,
    private modalCtrl: ModalController,
    private localstorage: LocalStorageService,
    private handlebar: Handlebar,
    private alertCtrl: AlertController,
    private masterDataService: MasterDataService,
    private timelineServ: TimeLineDataPushService,
    private actvroute: ActivatedRoute,
    private navCtrl: NavController,
    private toastServ: ToastServiceData,
    private printer: Printer,
    public popoverCtrlr: PopoverController,
    private file: File,
    private fileOpener: FileOpener,
    private documentViewer: DocumentViewer,
    private platform: Platform,
    private gestureCtrl: GestureController,
    private transfer: FileTransfer,
    private loadingCtrl: LoadingController,
    private location: Location,
    private callNumberService: CallNumberService,
    private inAppBrowser: InAppBrowser
  ) { window.pChart = this; }
  ngOnInit(): void {
    this.toggleData = {
      showVitalData: false,
      showAllergies: false,
      showNotes: false,
      diagnosysShow: false,
      suggPlan: false,
      pathologyShow: false,
      radiologyShow: false,
      prescriptionShow: false,
      certShow: false,
      docShow: false
    }
    this.actvroute.queryParams.subscribe(params => {
      if (params) {
        this.uhid = params.id;
        this.uhid = params.uhid;
        this.status = params.status;
        this.plannedCareItemId = params.plannedCareItemId;
        this.getDocumentData(this.uhid);
      }
    });
    this.authService.getOrganisationModel().then(data => {
      this.organizationData = data;
    });
    this.authService.isTokenValid().then((returnVal) => {
      this.localstorage.getAccessControl().then((accessObj) => {
        this.accessControl = accessObj;
      });
      this.getPatientInfo();

      this.authService.getOrgPreferences().then((pref) => {
        this.localstorage.setOrgPreferences(pref);
        this.masterDataService.getChartingConfig().then((listFilters) => {
          listFilters.forEach(ele => {
            if (ele.isChecked) {
              this.includeFilters.push(ele.value);
            }
          });
        });
        this.allowAllergiesReqInChart = pref["allergiesReqInChart"];
        this.allowDiagnosisReqInChart = pref["diagnosisReqInChart"];
        this.allowSuggPlanReqInChart = pref["suggPlanReqInChart"];
        this.authService.getTodaysChartInfo(this.uhid, this.plannedCareItemId).then((data) => {
          const chartData = data;
          this.careCenter = chartData.careCenter;
          this.admissionRecord = chartData.admissionRecord;
          const encounter = chartData.encounter;
          if (encounter) {
            this.encounterId = encounter["encounterId"];
          }
          this.getAssociateDynamicForms();

          this.pathologydata = chartData.pathologydata;
          this.setPathologyOrders();
          this.radiologydata = chartData.radiologydata;
          this.setRadiologyOrders();
          this.progressNote = chartData.lastprogressNote;
          this.sqlVital = chartData.sqlVital;
          if (chartData.patient && chartData.patient.allergies && chartData.patient.allergies !== 'null') {
            this.allegries = chartData.patient.allergies;
            this.allegries = this.allegries.replace(/,/gi, ', ');
          }
          this.medications = chartData.lastprescription != null ? chartData.lastprescription.mh : "";
          this.prescription = chartData.lastprescription != null ? chartData.lastprescription.prescription : "";
          this.setVitalDetails();
          this.setSuggestedPlans(encounter);
          if (this.prescription && this.prescription["care_center_id"]) {
            this.getCareCentersMasterDataById(this.prescription["care_center_id"]);
          }
          this.authService.getOrgPreferences().then((pref) => {
            this.isMCI = pref["isMCIFormat"];
          });
        });
      });
      this.loadMasterData();
      this.getWorkList();
      this.getPrintSettingsInfo();
    });
  }

  getPrintSettingsInfo() {
    this.authService.getPrintSettingsInfo().then((data) => {
      this.printSettings = data["printSettings"];
    });
  }
  getCareCentersMasterDataById(careCenterId) {
    this.authService.getCareCentersMasterDataById(careCenterId).then((data) => {
      this.careCenterData = data["careCenter"];
    });

  }

  getDocumentData(uhid){
    this.authService.getPatientTimeLineData(uhid, 1, 'files', 2).then((data)=>{
      if (data && data.items){
        this.docData = data.items;
      }
    });
  }
  
  checkPatientHasImage(imagePath) {
    return this.handlebar.checkImagePath(imagePath);
  }

  async setConfig() {
    const modal = await this.modalCtrl.create({
      component: ChartPageConfigPage,
      componentProps: this.includeFilters
    });
    await modal.present();
    modal.onWillDismiss().then((data) => {
      if (data && data.role && data.role !== 'backdrop') {
        this.includeFilters = data.data;
      }
    });
  }


  async openDocument() {
    const data = {
      "uuid": this.uhid
    }
    const modal = await this.modalCtrl.create({
      component: AddDocumentPage,
      componentProps: data,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: "chartModelStyle"
    });
    modal.present();

    var chartPage = document.getElementsByTagName("app-patient-charting");
    chartPage[0].classList.add("disableClick");

    modal.onWillDismiss().then((data0) => {
      if (data0 && data0.role) {
        this.getDocumentData(this.uhid);
      }
      chartPage[0].classList.remove("disableClick");
    });
  }


  openForm(formId, formDefId, formDef, pageType) {
    if (this.handlebar.isEmpty(formId)) {
      formId = 0;
    }
    let secCount = 0;
    if (formDef && formDef.value) {
      const formDefVal = JSON.parse(formDef.value);
      if (formDefVal.content) {
        secCount = formDefVal.content.length;
      }
    }
    const data = {
      "uuid": this.uhid,
      "formId": formId,
      "formDefId": formDefId,
      "encounterId": this.encounterId,
      "plannedCareItemId": this.plannedCareItemId,
      "pageType":pageType
    }
    if (secCount > 2) {
      this.openMultiSectionForm(data);
    } else {
      this.openSmallForm(data);
    }
  }


  async openSmallForm(data) {
    const modal = await this.modalCtrl.create({
      component: DynamicFormPage,
      componentProps: data,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: "chartModelStyle"
    });
    await modal.present();

    var chartPage = document.getElementsByTagName("app-patient-charting");
    chartPage[0].classList.add("disableClick");

    modal.onWillDismiss().then((data0) => {
      chartPage[0].classList.remove("disableClick");
      if (data0.role){
        this.getAssociateDynamicForms();
      }
    });
  }

  async openMultiSectionForm(data: any) {
    const modal = await this.modalCtrl.create({
      component: MultiFormPage,
      componentProps: data,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: "chartModelStyle"
    });

    await modal.present();
    var chartPage = document.getElementsByTagName("app-patient-charting");
    chartPage[0].classList.add("disableClick");

    modal.onDidDismiss().then((data) => {
      chartPage[0].classList.remove("disableClick");
      if (data && data.role) {
        const form = data.data[1];
        this.formId = form.dynamicFormId;
        this.getAssociateDynamicForms();
      }
    });
  }

  getAppointmentData() {
    this.authService.getAppointmentById(this.appointmentId).then(data0 => {
      this.appointmentData = data0.appointments[0];
    });
  }
  getWorkList() {
    this.getWorkListFromTLServ(this.plannedCareItemId).then((data) => {
      this.isWorklistExist = false;
      this.appointmentId = data.appointmentId;
      let label = "";
      if (data) {
        this.getAppointmentData();
        if (data.worklistDefination != null) {
          for (let i = 0; i < data.worklistDefination.length; i++) {
            this.isWorklistExist = true;
            if (data.worklistDefination[i].status === "Completed") {
              if (label) {
                label += ", ";
              }
              label += data.worklistDefination[i].task;
            }
          }
        }
      }
      this.workList = label;
    });
  }



  setVitalDetails() {
    this.vitalData = [];
    if (this.sqlVital) {
      this.vitalRecordDate = this.sqlVital["record_time"];
      if (this.sqlVital["height"]) {
        const heightData = {
          "name": "height",
          "value": this.sqlVital["height"].split(",")[0],
          "unit": this.sqlVital["height"].split(",")[1]
        }
        this.vitalData.push(heightData);
      }
      if (this.sqlVital["weight"]) {
        const weightData = {
          "name": "weight",
          "value": this.sqlVital["weight"].split(",")[0],
          "unit": this.sqlVital["weight"].split(",")[1]
        }
        this.vitalData.push(weightData);
      }

      if (this.sqlVital["temperature"]) {
        const temperatureData = {
          "name": "temperature",
          "value": this.sqlVital["temperature"].split(",")[0],
          "unit": this.sqlVital["temperature"].split(",")[1]
        }
        this.vitalData.push(temperatureData);
      }

      if (this.sqlVital["systolic"]) {
        const systolicData = {
          "name": "systolic",
          "value": this.sqlVital["systolic"].split(",")[0],
          "unit": this.sqlVital["systolic"].split(",")[1]
        }
        this.vitalData.push(systolicData);
      }

      if (this.sqlVital["diastolic"]) {
        const diastolicData = {
          "name": "diastolic",
          "value": this.sqlVital["diastolic"].split(",")[0],
          "unit": this.sqlVital["diastolic"].split(",")[1]
        }
        this.vitalData.push(diastolicData);
      }

      if (this.sqlVital["pulseRate"]) {
        const pulseRateData = {
          "name": "pulseRate",
          "value": this.sqlVital["pulseRate"].split(",")[0],
          "unit": this.sqlVital["pulseRate"].split(",")[1]
        }
        this.vitalData.push(pulseRateData);
      }
      if (this.sqlVital["heartRate"]) {
        const heartRateData = {
          "name": "heartRate",
          "value": this.sqlVital["heartRate"].split(",")[0],
          "unit": this.sqlVital["heartRate"].split(",")[1]
        }
        this.vitalData.push(heartRateData);
      }

      if (this.sqlVital["spo2"]) {
        const spo2Data = {
          "name": "spo2",
          "value": this.sqlVital["spo2"].split(",")[0],
          "unit": this.sqlVital["spo2"].split(",")[1]
        }
        this.vitalData.push(spo2Data);
      }

    }
  }
  getPatientInfo() {
    this.authService.getPatientInfoLight(this.uhid).then((data) => {
      this.patientInfo = data;
      this.getCertData();
      if (this.patientInfo.email) {
        this.emailShortStr = this.stringShort(this.patientInfo.email, 15);
      }

      this.getAccessControl();
    });

  }

  getAccessControl()  {
    this.localstorage.getAccessControl().then((accessObj) => {
      this.accessControl = accessObj;
      this.allowToSeeContactInfo();
    });
  }

  allowToSeeContactInfo() {
    if (this.accessControl['permissions'].allowToSeeContactInfo) {
      if (this.patientInfo.telephone) {
        this.checkedTelephoneNo = this.patientInfo.telephone;
      }
    } else {
      if (this.patientInfo.telephone) {
        this.checkedTelephoneNo = "XXXXXXX"+ this.patientInfo.telephone.substring(this.patientInfo.telephone.length - 3);
      }
      if (this.patientInfo.alternatePhone) {
        this.patientInfo.alternatePhone = "XXXXXXX"+ this.patientInfo.alternatePhone.substring(this.patientInfo.alternatePhone.length - 3);
      }
    }
  }

  getAssociateDynamicForms() {
    this.authService.getAssociateDynamicForms("Care Record", this.plannedCareItemId, this.encounterId).then((data) => {
      this.dynamicForms = data;
      if (this.dynamicForms){
        this.dynamicForms.forEach(ele => {
          if (ele) {
            ele.showDynamic = false;
            if (this.handlebar.isNotEmpty(ele.dynamic_form_id) && ele.dynamic_form_id !== 0){
              this.authService.getDynamicFormData(ele.dynamic_form_def_id, ele.dynamic_form_id).then((data) => {
                let dynamicFormData = JSON.parse(data);
                 dynamicFormData.dynamicFormContent = JSON.parse(dynamicFormData.content);
                ele.dynamicFormData = dynamicFormData;
              });
            }
          }
        });
      }
    });
  }

  setSuggestedPlans(encounter) {
    this.suggTreatmentLabels = "";
    if (encounter) {
      if (encounter["diagnosis"] && encounter["diagnosis"] !== 'null') {
        this.diagnosis = encounter["diagnosis"];
        this.diagnosis = this.diagnosis.replace(/,/gi, ', ');
      }
      if (encounter["suggestedPlan"] && encounter["suggestedPlan"] !== "" && encounter["suggestedPlan"].length > 0) {
        this.suggTreatment = JSON.parse(encounter["suggestedPlan"]);
        for (let i = 0; i < this.suggTreatment.length; i++) {
          if (this.suggTreatmentLabels) {
            this.suggTreatmentLabels += ",";
          }
          this.suggTreatmentLabels += this.suggTreatment[i].value;
        }
      }
    }
  }
  setPathologyOrders() {
    this.pathologyOrdersLabels = "";
    this.pathologyOrders = [];
    if (this.pathologydata) {
      this.investigation_id_pathology = this.pathologydata["investigation_id"];
      if (this.pathologydata["order_lineitem_data"]) {
        const pathologyOrders = JSON.parse(this.pathologydata["order_lineitem_data"].value);
        for (let i = 0; i < pathologyOrders.length; i++) {
          if (this.pathologyOrdersLabels) {
            this.pathologyOrdersLabels += ", ";
          }
          this.pathologyOrdersLabels += pathologyOrders[i].component_name;
          this.pathologyOrders.push({
            lab_sub_category_id: 0,
            order_id: 0,
            lab_test_item_id: pathologyOrders[i].lab_test_item_id,
            lab_component_item_id: pathologyOrders[i].lab_component_item_id,
            isEditable: true,
            isChecked: true,
            label: pathologyOrders[i].component_name
          });
        }
      }
    }
  }
  setRadiologyOrders() {
    this.radiologyOrdersLabels = "";
    this.radiologyOrders = [];
    if (this.radiologydata) {
      this.investigation_id_radiology = this.radiologydata["investigation_id"];
      if (this.radiologydata["order_lineitem_data"]) {
        const radiologyOrders = JSON.parse(this.radiologydata["order_lineitem_data"].value);
        for (let i = 0; i < radiologyOrders.length; i++) {
          if (this.radiologyOrdersLabels) {
            this.radiologyOrdersLabels += ", ";
          }
          this.radiologyOrdersLabels += radiologyOrders[i].component_name;

          this.radiologyOrders.push({
            lab_sub_category_id: 0,
            order_id: 0,
            lab_test_item_id: radiologyOrders[i].lab_test_item_id,
            lab_component_item_id: radiologyOrders[i].lab_component_item_id,
            isEditable: true,
            isChecked: true,
            label: radiologyOrders[i].component_name
          });
        }
      }

    }
  }
  loadMasterData() {
    this.authService.getCareCentersMasterData().then((data) => {
      this.careCenterMasterList = data.careCenters;
    });
  }

  async navigateToVitalAddPage() {
    let age = "";
    if (this.patientInfo) {
      age = this.patientInfo.age;
    }
    const data = {
      uuid: this.uhid,
      age: age
    }

    const modal = await this.modalCtrl.create({
      component: AddVitalPartialPage,
      componentProps: data,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: "chartModelStyle"
    });

    await modal.present();

    var chartPage = document.getElementsByTagName("app-patient-charting");
    chartPage[0].classList.add("disableClick");

    modal.onDidDismiss().then((data) => {
      chartPage[0].classList.remove("disableClick");
      const isUpdated = data.role;
      if (isUpdated) {
        this.vitalRecordDate = moment() + "";
        this.vitalData = data.data;
      }
    });
  }

  async navigateToWorkListAddPage() {
    const data0 = {
      plannedCareItemId: this.plannedCareItemId
    }
    const modal = await this.modalCtrl.create({
      component: AddWorkListPage,
      componentProps: data0,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: "chartModelStyle"
    });
    await modal.present();
    var chartPage = document.getElementsByTagName("app-patient-charting");
    chartPage[0].classList.add("disableClick");
    modal.onDidDismiss().then((data) => {
      chartPage[0].classList.remove("disableClick");
      const isUpdated = data.role
      if (isUpdated) {
        let label = "";
        for (let i = 0; i < data.data.tasklist.length; i++) {
          if (data.data.tasklist[i].status === "Completed") {
            if (label) {
              label += ", ";
            }
            label += data.data.tasklist[i].task;
          }
        }
        this.workList = label;
      }
    });
  }
  async openNote() {
    const data0 = {
      "ClinicalNote": this.progressNote,
      "plannedCareItemId": this.plannedCareItemId,
      "encounterId": this.encounterId,
      "uuid": this.uhid
    };

    const modal = await this.modalCtrl.create({
      component: ClinicalNotePage,
      componentProps: data0,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: "noteModelStyle"
    });
    await modal.present();
    var chartPage = document.getElementsByTagName("app-patient-charting");
    chartPage[0].classList.add("disableClick");
    modal.onDidDismiss().then((data) => {
      chartPage[0].classList.remove("disableClick");
      if (data && data.data) {
        if (this.progressNote && this.progressNote["clinical_notes"]) {
          this.progressNote["clinical_notes"] = data.data.note;

        } else {
          const note = [];
          note["clinical_notes"] = data.data.note;
          this.progressNote = note;
        }
        const returnJson = data.data.returnJson;
        this.encounterId = returnJson[0];
        this.progressNote["progress_note_id"] = returnJson[1];

      }
    });
  }



  selectValues(key) {
    let selectedValues: any;
    let selectedId: string;
    if (key === "Allergies") {
      selectedValues = this.allegries;
    } else if (key === "Diagnosis") {
      selectedValues = this.diagnosis;
    } else if (key === "Sugg. Plans") {
      selectedValues = this.suggTreatment;
    } else if (key === "Pathology") {
      selectedValues = this.pathologyOrders;
      selectedId = this.investigation_id_pathology;
    } else if (key === "Radiology") {
      selectedValues = this.radiologyOrders;
      selectedId = this.investigation_id_radiology;
    }

    const data = {
      "key": key,
      "values": selectedValues,
      "plannedCareItemId": this.plannedCareItemId,
      "encounterId": this.encounterId,
      "uuid": this.uhid,
      "selectedId": selectedId

    };
    this.navigateToSelectModel(data, key);

  }
  async navigateToSelectModel(data0?: any, key?: string) {

    const modal = await this.modalCtrl.create({
      component: SelectListPage,
      componentProps: data0,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: "chartModelStyle"
    });
    await modal.present();

    var chartPage = document.getElementsByTagName("app-patient-charting");
    chartPage[0].classList.add("disableClick");

    modal.onDidDismiss().then((data) => {
      chartPage[0].classList.remove("disableClick");
      const isUpdated = data.role;
      if (isUpdated && data.data) {
        const returnObj = data.data.returnObj;
        if (key === "Allergies") {
          this.allegries = data.data.values.toString();
          this.allegries = this.allegries.replace(/,/gi, ', ');
        } else if (key === "Diagnosis") {
          this.diagnosis = data.data.values.toString();
          this.diagnosis = this.diagnosis.replace(/,/gi, ', ');
        } else if (key === "Sugg. Plans") {
          this.suggTreatment = data.data.values;
          this.suggTreatmentLabels = data.data.labels.toString();
        } else if (key === "Pathology") {
          this.pathologyOrders = data.data.values;
          this.pathologyOrdersLabels = data.data.labels.toString();
          this.pathologyOrdersLabels = this.pathologyOrdersLabels.replace(/,/gi, ', ');
          this.investigation_id_pathology = returnObj[1];
        } else if (key === "Radiology") {
          this.radiologyOrders = data.data.values;
          this.radiologyOrdersLabels = data.data.labels.toString();
          this.radiologyOrdersLabels = this.radiologyOrdersLabels.replace(/,/gi, ', ');
          this.investigation_id_radiology = returnObj[1];
        }
        if (key !== "Allergies") {
          this.encounterId = returnObj[0];
        }
      }
    });

  }

  async addPrescription() {
    let data = {
      "uuid": this.uhid,
      "patientName": this.patientInfo.name,
      "encounterId": this.encounterId,
      "plannedCareItemId": this.plannedCareItemId,
      "chartingPrescriptionId": this.prescription ? this.prescription["prescription_id"] : 0
    }
    let modal = await this.modalCtrl.create({
      component: AddPrescriptionPage,
      componentProps: data,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: "chartModelStyle"
    });
    await modal.present();

    var chartPage = document.getElementsByTagName("app-patient-charting");
    chartPage[0].classList.add("disableClick");

    modal.onWillDismiss().then((data) => {
      chartPage[0].classList.remove("disableClick");
      const isUpdate = data.role;
      if (isUpdate) {
        if (!this.medications) {
          this.medications = data.data[1];
        } else {
          let medications = data.data[1];
          for (let i = 0; i < medications.length; i = i + 1) {
            this.medications.push(medications[i]);
          }
        }
        if (!this.prescription) {
          this.prescription = {};
        }
        this.prescription["prescription_id"] = data.data[0];
        this.encounterId = data.data[2];
      }
    });
  }

  async deletePrescription(medicineId) {
    let alert = await this.alertCtrl.create({
      header: 'Confirm Delete',
      message: 'Do you want to delete this medicine ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'Delete',
          handler: () => {
            this.masterDataService.checkPermission("Prescription", "delete").then((isPermission) => {
              if (isPermission) {

                this.timelineServ.deleteMedicineLieItem(this.prescription["prescription_id"], medicineId).then(() => {

                  for (let i = this.medications.length - 1; i >= 0; i = i - 1) {
                    if (this.medications[i]["medication"].medication_id == medicineId) {
                      this.medications.splice(i, 1);
                    }
                  }
                });
              } else {
                this.toastServ.presentToast("You don't have permission to delete.");
              }
            });
          }
        }
      ]
    });
    await alert.present();
  }

  print() {
    const element = document.getElementById('getElementValue').innerHTML;

    const options: PrintOptions = {
      name: 'MyDocument',
      duplex: true,
      orientation: 'landscape'

    };

    this.printer.print(element, options);
  }
  getCertData() {
    this.authService.getCertData(this.patientInfo.patientUhid, 1).then((data) => {
      this.certData = data;
      this.certData.forEach(ele => {
        if (ele.certificate) {
          ele.certName = this.handlebar.stripHtml(ele.certificate, 30);
        }
      });
    });
  }

  shareCertificate(item) {
    const data0 = {
      htmlContext: item.certificate,
      htmlSubject: "Medical Certificate"
    }
    this.authService.sendTimelineDataByMail(data0, this.uhid).then((data) => {
      if (data && (data.status === 400 || data.status === 401 || data.status === 500)) {
        const data0 = JSON.parse(data._body);
        this.toastServ.presentToast(data0.description);
      } else if (data) {
        this.toastServ.presentToast('Mail Send Succesfully.');
      }
    });
  }

  printCert(item) {
    this.certPrintData = item.certificate;

    let options: PrintOptions = {
      name: 'MyDocument',
      duplex: true,
      orientation: 'landscape'
    };

    this.printer.print(this.certPrintData, options).then((value: any) => {
      console.log('value:', value);
    }, (error) => {
      console.log('error:', error);
    });
  }

  async addCertificate() {
    let data0 = {
      patientInfo: this.patientInfo,
      certId: 0,
      pageType: 'Issue',
    }
    const modal = await this.modalCtrl.create({
      component: AddCertificatePage,
      componentProps: data0,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: "chartModelStyle"
    });
    await modal.present();

    var chartPage = document.getElementsByTagName("app-patient-charting");
    chartPage[0].classList.add("disableClick");

    modal.onWillDismiss().then((data) => {
      chartPage[0].classList.remove("disableClick");
      if (data && data.data === true) {
        this.getCertData();
      }
    });
  }

  async editCertificate(item) {
    if (this.accessControl && this.accessControl["permissions"] && this.accessControl["permissions"].MedicalCertificate5674 && this.accessControl["permissions"].MedicalCertificatecreate) {
      let data0 = {
        patientInfo: this.patientInfo,
        certId: item.patient_certificate_id,
        pageType: 'Edit'
      }
      const modal = await this.modalCtrl.create({
        component: AddCertificatePage,
        componentProps: data0,
        showBackdrop: true,
        backdropDismiss: true,
        cssClass: "chartModelStyle"
      });
      await modal.present();

      var chartPage = document.getElementsByTagName("app-patient-charting");
      chartPage[0].classList.add("disableClick");

      modal.onWillDismiss().then((data) => {
        chartPage[0].classList.remove("disableClick");
        if (data && data.data === true) {
          this.getCertData();
        }
      });
    } else {
      this.toastServ.presentToast("You don't permission to edit Certificate.");
    }

  }
  getAppointmentById(ev:any){
    this.authService.getAppointmentById(this.appointmentId).then(data0 => {
      this.isAllowVideoCall(data0.appointments[0],ev);
    });
  }
  isAllowVideoCall(item,ev) {
    if (this.accessControl && this.accessControl["permissions"] && this.accessControl["permissions"].Telehealth5674) {
      if (item.apptType === 'Video' && this.handlebar.isNotEmpty(item.videoCallSid)) {
        this.openPopover(ev,item);
        this.showPdfBtn = true;
        return true;
      }
      this.callPatient();
      this.showPdfBtn = false;
      return false;
    }
    this.callPatient();
    this.showPdfBtn = false;
    return false;
  }

  callPatient() {
      if (this.patientInfo && this.patientInfo.telephone){
        this.callNumberService.callToNumberDialer(this.patientInfo.telephone);
        (this.patientInfo.telephone);
        // this.callPatientMobile(this.patientInfo.telephone, '91',this.patientInfo.localPatientId)
      } else {
        this.toastServ.presentToast('Please add mobile number.');
      }
  }
 
  async openPopover(ev: any, item) {
    let mobileNo;
      if (this.patientInfo.telephone) {
        mobileNo = this.patientInfo.telephone
      }
      const data = {
        "mobileNo": mobileNo,
        appointmentId: this.appointmentId,
        appointmentData: item,
        patientInfo: this.patientInfo
      }
      const popover = await this.popoverCtrlr.create({
        component: CallPopoverComponent,
        componentProps: data,
        event: ev,
        cssClass: 'popoverStyle',
      });
      popover.present(); 
  }

  stringShort(passedString, no) {
    if (passedString != undefined) {
      if (passedString.length > no) {
        passedString = passedString.substring(0, no);
        passedString = passedString + "..";
      } else {
        passedString = passedString;
      }
    }
    return passedString;

  }
  getWorkListFromTLServ(plannedCareItemId): Promise<any> {
    return this.timelineServ.getAppointmentPlanItem(plannedCareItemId).then((data) => {
      if (data && data.appointments) {
        let appitems = data.appointments[0];
        this.startDateTime = moment(appitems.startDateTime).format("DD/MM/YYYY, hh:mm a");
        return this.masterDataService.mergeWorkListDefWithData(appitems);
      } else {
        return null;
      }
    });
  }
  checkWorkListStatus(): Promise<any> {
    let pendingItems: number = 0;
    return this.getWorkListFromTLServ(this.plannedCareItemId).then((data) => {
      if (data) {
        if (data.worklistDefination != null) {
          for (let j = 0; j < data.worklistDefination.length; j++) {
            if (data.worklistDefination[j].status !== "Completed") {
              pendingItems++;
            }

          }
        }
        return pendingItems;
      } else {
        return null;
      }
    });
  }
  
  async addPrescriptionTemplate() {
    let data = {
      "uuid": this.uhid,
      "encounterId": this.encounterId,
      "plannedCareItemId": this.plannedCareItemId,
      "chartingPrescriptionId": this.prescription ? this.prescription["prescription_id"] : 0
    }
    let modal = await this.modalCtrl.create({
      component: PrescriptionTemplatePage,
      componentProps: data,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: "chartModelStyle"
    });
    await modal.present();

    var chartPage = document.getElementsByTagName("app-patient-charting");
    chartPage[0].classList.add("disableClick");

    
    modal.onWillDismiss().then((data) => {
      chartPage[0].classList.remove("disableClick");
      const isUpdate = data.role;
      if (isUpdate) {
        if (!this.medications) {
          this.medications = data.data[1];
        } else {
          let medications = data.data[1];
          for (let i = 0; i < medications.length; i = i + 1) {
            this.medications.push(medications[i]);
          }
        }
        if (!this.prescription) {
          this.prescription = {};
        }
        this.prescription["prescription_id"] = data.data[0];
        this.encounterId = data.data[2];
      }
    });
  }


  viewVisitSummary() {
    if (this.progressNote || (this.medications && this.medications[0]) 
    || this.pathologyOrdersLabels || this.radiologyOrdersLabels) {
      this.pdfCount = 0;
      this.pdfAlert();      
    } else {
      this.toastServ.presentToast("You must enter at least one clinical.");
    }
  }

  async pdfAlert() {
    this.pdfCount = this.pdfCount + 1;
    if (this.encounterId === 0) {
      this.toastServ.presentToast('You must enter at least one clinical');
      return false;
    }
    if (this.pdfCount > 3) {
      const countAlert = await this.alertCtrl.create({
        message: 'Maximum attempts(3) reached... please try after sometime',
        buttons: [{
          text: 'OK',
          handler: () => {
            countAlert.dismiss();
          }
        }]
      });
      countAlert.present();
      return false;
    }
    const loader = await this.loadingCtrl.create({
      message: 'Generating Pdf...',
      mode: 'ios'
    });
    await loader.present();
    this.authService.getPdfUrl(this.patientInfo.patientUhid, this.encounterId, this.plannedCareItemId).then(async data => {
      loader.dismiss();      
      if (this.handlebar.isEmpty(data)){
        const alert = await this.alertCtrl.create({
          message: 'Failed to generate PDF for Visit Summary...(atempts ' + this.pdfCount + ')',
          buttons: [
            {
              text: 'Try Again',
              handler: () => {
                alert.dismiss();
                this.pdfAlert();
              }
            },
            {
              text: 'Close',
              role: 'cancel',
              handler: () => {
              }
            }
          ]
        });
        await alert.present();
      } else {
        const alert = await this.alertCtrl.create({
          message: 'PDF is generated for Consultation Summary',
          buttons: [
            {
              text: 'Share PDF',
              handler: () => {
                this.sharePDF();
              }
            },
            {
              text: 'Preview PDF',
              handler: () => {
                //  this.previewPdf(url);
                const ios = this.platform.is('ios');
                if (ios) {
                  this.authService.expandUrl(data.url).then(async expandedUrl => {
                    this.openFile(expandedUrl, 'application/pdf', data.base64, 'myfile');
                  });          
                } else {
                  this.openFile('', 'application/pdf', data.base64, 'myfile');
                }
              }
            }
          ]
        });
        await alert.present();
      }
    }).catch(err => {
      console.log(err);
      loader.dismiss();
    });
  }

  sharePDF() {
    this.authService.shareVisitSummary(this.patientInfo.patientUhid, this.encounterId, this.plannedCareItemId).then(data => {
      if (data === 'Success') {
        this.toastServ.presentToast('PDF shared succesfully. Patient will recieve Email and SMS.');
      } else {
        this.toastServ.presentToast('Something went wrong... Please try after some time.');
      }
    });
  }
  openIosFile(url, filename, mimeType) {
    if (mimeType.includes('image')) {
      try {
        this.platform.ready().then((data) => {
          const target = '_blank';
          this.inAppBrowser.create(url, target, 'location=no').show();
          this.fileLoader.dismiss();
          return true;
        }).catch((err) => {
          this.fileLoader.dismiss();
        });
      } catch (error) {
        this.fileLoader.dismiss();
      }
    }

    const path = this.file.dataDirectory + filename;
    const fileTransfer: FileTransferObject = this.transfer.create();

    fileTransfer.download(url, path).then((entry) => {
      const finalUrl = entry.toURL();
      const options: DocumentViewerOptions = {
        title: filename
      };
      this.documentViewer.viewDocument(finalUrl, mimeType, options);
      this.fileLoader.dismiss();
    }).catch((error) => {
      this.fileLoader.dismiss();
      this.toastServ.presentToast('Error view file');
    });
  }
  
  async openFile(url, mimeType, base64, filename) {  
    const loader = await this.loadingCtrl.create({
      message: 'opening...'
    });
    await loader.present();
    this.fileLoader = loader;
    const ios = this.platform.is('ios');
    if (ios) {
      this.openIosFile(url, filename, mimeType);
    } else {
      const writeDirectory = this.file.externalDataDirectory;
      this.file.writeFile(writeDirectory, filename, this.convertBaseb64ToBlob(base64, 'data:' + mimeType + ';base64'), {replace: true})
        .then(() => {
              loader.dismiss();
              this.fileOpener.open(writeDirectory + filename, mimeType)
                  .catch((err) => {
                      loader.dismiss();
                      alert(JSON.parse(err));
                      this.toastServ.presentToast('Error opening file');
                  });
          })
          .catch((err) => {
            loader.dismiss();
            alert(JSON.parse(err));
            this.toastServ.presentToast('Error writing file');
      });
    }
  }

  convertBaseb64ToBlob(b64Data, contentType): Blob {
    contentType = contentType || '';
    const sliceSize = 512;
    b64Data = b64Data.replace(/^[^,]+,/, '');
    b64Data = b64Data.replace(/\s/g, '');
    const byteCharacters = window.atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    return new Blob(byteArrays, { type: contentType });
  }


  dragGesture() {
    const vcNode = document.getElementById('videoCallModal');
    const joinVCMOdal = document.getElementsByTagName('app-patient-charting')[0];
    const vcDrag = this.gestureCtrl.create({
      el: vcNode,
      gestureName: 'vcDrag',
      onStart: ev => {
        vcNode.style.opacity = 0.8 + '';
      },
      onMove: ev => {
        vcNode.style.transform = `translate(${ev.deltaX}px, ${ev.deltaY}px)`;
      },
      onEnd: ev => {
        const positionData = this.isInBound(ev.deltaX, ev.deltaY, joinVCMOdal, vcNode);
        vcNode.style.transform = `translate(${positionData.x}px, ${positionData.y}px)`;
        vcNode.style.opacity = 1 + '';
      }
    });
    vcDrag.enable();
  }
  isInBound(x, y, modalPage, drageedDiv) {
    console.log('x ' + x , 'y ' + y);
    const pageBoundaries = modalPage.getBoundingClientRect();
    console.log('left ' + pageBoundaries.left, ' right ' + pageBoundaries.right, ' top ' + pageBoundaries.top, ' bottom ' 
    + pageBoundaries.bottom);
    let data = {x: '0', y: '0'};
    if (x < pageBoundaries.left || x >= pageBoundaries.right) {
      data.x = 0 + '';
    } else {
      data.x = x;
    }
    if (y < pageBoundaries.top || y >= pageBoundaries.bottom) {
      data.y = 0 + '';
    } else {
      data.y = y;
    }
    console.log(data);
    console.log('-----------------------------------------------------');
    return data;
  }

  checkClinicalData() {
    if ((this.vitalData && this.vitalData[0]) || this.allegries || this.progressNote 
    || this.diagnosis || this.suggTreatmentLabels || (this.medications && this.medications[0]) 
    || (this.certData && this.certData[0]) || this.pathologyOrdersLabels || this.radiologyOrdersLabels
    || (this.docData && this.docData[0]) || (this.dynamicForms && this.includeFilters.indexOf('dynamicform-') > -1)) {
      return true;
    } else {
      return false;
    }
  }

  checkoutAppt() {
    const isDataAvilable =  this.checkClinicalData();
    if (isDataAvilable) {
      this.checkWorkListStatus().then(async (pendingItems) => {
        this.msg = "Do you want to Check Out ?";
        if (pendingItems > 0) {
          this.msg = "You have " + pendingItems + " pending work item(s). " + this.msg;
        }
        const alert = await this.alertCtrl.create({
          message: this.msg,
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
  
              }
            },
            {
              text: 'Check Out',
              handler: () => {
                this.authService.checkoutEvent(this.plannedCareItemId).then(() => {
                  this.dismiss(true);
                });
              }
            }
          ]
        });
        await alert.present();
      });
    } else {
      this.toastServ.presentToast("You must enter at least one clinical.");
    }
  }
  dismiss(data?: boolean) {
    // using the injected ViewController this page
    // can "dismiss" itself and pass back data
    window.chartPage.backToChartingPage();
    this.navCtrl.navigateBack(['tabs/myCharts']);
  }
}
