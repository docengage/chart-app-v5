import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PatientChartingPageRoutingModule } from './patient-charting-routing.module';

import { PatientChartingPage } from './patient-charting.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PatientChartingPageRoutingModule,
    ComponentsModule
  ],
  declarations: [PatientChartingPage]
})
export class PatientChartingPageModule {}
