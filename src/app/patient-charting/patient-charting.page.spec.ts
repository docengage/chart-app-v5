import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PatientChartingPage } from './patient-charting.page';

describe('PatientChartingPage', () => {
  let component: PatientChartingPage;
  let fixture: ComponentFixture<PatientChartingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientChartingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PatientChartingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
