import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PatientChartingPage } from './patient-charting.page';

const routes: Routes = [
  {
    path: '',
    component: PatientChartingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PatientChartingPageRoutingModule {}
