import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { DataService } from '../service/data.service';

@Injectable({
  providedIn: 'root'
})
export class DataResolverService implements Resolve<any>{

  constructor(private dataService: DataService) { }

  resolve(route: import('@angular/router').ActivatedRouteSnapshot) {

    const id = route.paramMap.get('id');
    return this.dataService.getData(id);

  }

}
