import { Component } from '@angular/core';
import { MyChartsPage } from '../my-charts/my-charts.page';
import { SchedulePage } from '../schedule/schedule.page';
import { AllPatientPage } from '../all-patient/all-patient.page';
import { AboutProviderPage } from '../about-provider/about-provider.page';

import { LocalStorageService } from '../service/local-storage.service';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';


export interface TabInterface {
  title: string;
  root: any;
  icon: string;
}
@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = MyChartsPage;
  tab2Root: any = SchedulePage;
  tab3Root: any = AllPatientPage;
  myIndex: number;
  tabs: TabInterface[] = [];

  constructor(public router: Router,
              private localstorage: LocalStorageService,
              private authService: AuthService
              // public events: Events
) {
    this.tabs = [];
    this.authService.isTokenValid().then((returnVal) => {
      this.OnLoadCheckAuth();
      this.localstorage.getAccessControl().then((accessObj) => {
        this.setTabs(accessObj);
      });
    });
  }
  setTabs(accessControl) {
    let isAccess = false;
    if (accessControl) {
      accessControl = accessControl['permissions'];
      if (accessControl.Appointments5674 && accessControl.Appointmentsview) {
        isAccess = true;
        this.tabs.push({ title: 'Schedule', root: 'myCharts', icon: 'calendar' });
      }
      if (accessControl.Activity5674 && accessControl.Activityview) {
        isAccess = true;
        this.tabs.push({ title: 'Activity', root: 'schedule', icon: 'file-tray-full' });
      }
      if (accessControl.Patient5674 && accessControl.Patientview) {
        isAccess = true;
        this.tabs.push({ title: 'Patients', root: 'allPatient', icon: 'people' });
      }
      if (accessControl.Leave5674 && accessControl.Leaveview) {
        this.tabs.push({ title: 'Leaves', root: 'leaveList', icon: 'airplane' });
      }
    } else {
      isAccess = true;
      this.tabs.push({ title: 'Schedule', root: 'myCharts', icon: 'calendar' });
      this.tabs.push({ title: 'Tasks', root: 'schedule', icon: 'checkbox' });
      this.tabs.push({ title: 'Patients', root: 'allPatient', icon: 'people' });
      this.tabs.push({ title: 'Leaves', root: 'leaveList', icon: 'airplane' });
    }
    // { title: 'Support', pageName: 'SupportPage', component: '/support', icon: 'chatbubbles' },

  }
  OnLoadCheckAuth() {
    this.localstorage.getToken().then((tokenValue) => {
      if (!tokenValue) {
        this.authService.isTokenValid();
      }
    });

  }
}
