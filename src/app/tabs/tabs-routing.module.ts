import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'myCharts',
        children: [
          {
            path: '',
            loadChildren: () => import('../my-charts/my-charts.module').then( m => m.MyChartsPageModule)
          }
        ]
      },
      {
        path: 'schedule',
        children: [
          {
            path: '',
            loadChildren: () => import('../schedule/schedule.module').then( m => m.SchedulePageModule)
          }
        ]
      },
      {
        path: 'allPatient',
        children: [
          {
            path: '',
            loadChildren: () => import('../all-patient/all-patient.module').then( m => m.AllPatientPageModule)
          }
        ]
      },
      {
        path: 'leaveList',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../leave-list/leave-list.module').then(m => m.LeaveListPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/myCharts',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/myCharts',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
