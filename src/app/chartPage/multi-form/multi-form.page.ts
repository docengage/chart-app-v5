import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { AuthService } from 'src/app/service/auth.service';
import { DynamicFormCommon } from 'src/app/components/dynamicform-common';
import { Handlebar } from 'src/app/components/handlebar';
import { ToastServiceData } from 'src/app/service/toast-service';
import { TimeLineDataPushService } from 'src/app/service/timeline-auth-service';
import { TableColumnsPage } from 'src/app/table-columns/table-columns.page';
import * as moment from 'moment';

@Component({
  selector: 'app-multi-form',
  templateUrl: './multi-form.page.html',
  styleUrls: ['./multi-form.page.scss'],
})
export class MultiFormPage implements OnInit {


  uhid: string;
  formDefId: string;
  formId: any;
  dynamicFormData: any;
  formName: string;
  content: any;
  counter: Number;
  encounterId: string;
  plannedCareItemId:string;
  isDisclaimer:string="false";
  isDisclaimerCheck:boolean=false;
  disclaimerContent:string;
  sectionCounter:number = 0;
  cardContent:boolean = false;
  showAddForm = false;
  emptyfield = '';
  patientInfo: any;
  itemIndex: any;
  secIndex: any;
  startDate: string;
  showModal = false;

  constructor(
    public navParams: NavParams,
    private authService: AuthService,
    private modalCtrl: ModalController,
    public dynamicFormCommon: DynamicFormCommon,
    private handlebar: Handlebar,
    public toastServ: ToastServiceData,
    public timeLineAuthService: TimeLineDataPushService
    ) {

    this.uhid = this.navParams.data.uuid;
    this.formDefId = this.navParams.data.formDefId;
    this.formId = this.navParams.data.formId;
    this.encounterId = this.navParams.data.encounterId;
    this.plannedCareItemId = this.navParams.data.plannedCareItemId;
  }
  
  ngOnInit(): void {
    this.getDynamicFormContent();
    this.getPatientInfo();
  }

  getDynamicFormContent(){
    this.authService.getDynamicFormData(this.formDefId,this.formId).then((data)=>{
      this.dynamicFormData = JSON.parse(data);
      if(this.dynamicFormData){
        this.formName = this.dynamicFormData.form_name;
        this.disclaimerContent = this.dynamicFormData.disclaimerContent;
        this.isDisclaimer = this.dynamicFormData.isDisclaimer;
        this.content = JSON.parse(this.dynamicFormData.content);
        this.counter = this.dynamicFormData.counter;
        this.content.isDisclaimer = this.isDisclaimer;
        this.content.disclaimerContent = this.disclaimerContent;
        if(Number(this.formId) > 0){
          this.isDisclaimerCheck = true;
          this.showAddForm = true;
        } else {
          this.showAddForm = false;

        }
        if (this.content.length > 0) {
          for(var i = 0, I = this.content.length; i < I; i++ ) {
            this.content[i].showContent = false;
          }
        }
      }
    });
  }
  setDateModal(value, itemIndex, secIndex) {
    this.itemIndex = itemIndex;
    this.secIndex = secIndex;
    if (this.handlebar.isEmpty(value) || value === undefined) {
      this.startDate = value = moment().format();
      this.content[itemIndex].sectionContent[secIndex].value = moment(value).format('DD-MM-YYYY');
    } else {
      this.startDate = moment(value, 'DD-MM-YYYY').format();
      this.content[itemIndex].sectionContent[secIndex].value = moment(value, 'DD-MM-YYYY').format('DD-MM-YYYY');
    }

    this.showModal = true;
  }

  dateChange(value, itemIndex, secIndex) {
    this.itemIndex = itemIndex;
    this.secIndex = secIndex;
    if (this.handlebar.isEmpty(value) || value === undefined) {
      this.startDate = value = moment().format();
      this.content[itemIndex].sectionContent[secIndex].value = moment(value).format('DD-MM-YYYY');
    } else {
      this.startDate = moment(value).format();
      this.content[itemIndex].sectionContent[secIndex].value = moment(value).format('DD-MM-YYYY');
    }
    this.showModal = false;
  }
  
  openForm(counter){
    if (counter === this.sectionCounter){
      this.content[counter].showContent = !this.content[counter].showContent;
    } else {
      this.content[this.sectionCounter].showContent = false;
      this.content[counter].showContent = true;
      this.sectionCounter = counter;
    }
  }
  addAsNewForm(){
    this.formId = 0;
    this.getDynamicFormContent();
  }
  getPatientInfo() {
    this.authService.getPatientInfoLight(this.uhid).then((data) => {
      this.patientInfo = data;
    });

  }
  saveAndShareForm() {
    this.emptyfield = '';
    const isSave: boolean = this.doValidation();
    const signatureurl = '';
    if (isSave) {
      const data = {
        encounterId: this.encounterId,
        content: this.content,
        formDefId: this.formDefId,
        formId: this.formId,
        carePlanItemId: this.plannedCareItemId,
        isDisclaimer: this.isDisclaimerCheck,
        signatureurl: signatureurl,
        uhid: this.patientInfo.patientUhid,
        externalSourceEmail: this.patientInfo.email,
        externalSourceMobile: this.patientInfo.telephone,
        isMobileApp: true
    }
      this.timeLineAuthService.pushDynamicDataAndShare(data).then((data0) => {
        if (data0 && (data0.status === 400 || data0.status === 401 || data0.status === 500)) {
          const responseData = JSON.parse(data0._body);
          this.toastServ.presentToast(responseData.description);
        } else {
          this.toastServ.presentToast('The document is share and saved successfully');
          this.dismiss(true, data);
        }
      });
    } else if (this.handlebar.isNotEmpty(this.emptyfield)) {
      this.toastServ.presentToast('Please fill mandatory field ' + this.emptyfield);
    }
  }
  saveForm(){
    if(this.isDisclaimer==="true"){
      if(!this.isDisclaimerCheck){
        this.toastServ.presentToast("Please Check Disclaimer!");
      }else{
        this.continueSaveForm();
      }
    }else{
      this.continueSaveForm();
    }
  }
  continueSaveForm() {
    this.emptyfield = '';
    const isSave: boolean = this.doValidation();

    if (isSave) {
      const data = {
        encounterId: this.encounterId,
        content : this.content,
        formDefId : this.formDefId,
        formId : this.formId,
        carePlanItemId: this.plannedCareItemId,
        isDisclaimer : this.isDisclaimerCheck

      };
      this.timeLineAuthService.pushDynamicData(this.uhid, data).then((data0) => {
        this.dismiss(true, data0);
      });
    } else if (this.handlebar.isNotEmpty(this.emptyfield)) {
      this.toastServ.presentToast('Please fill mandatory field ' + this.emptyfield);
    }
  }

  enableDependentField(secCounter,counter,fieldName,fieldType){

    this.content = this.dynamicFormCommon.enableDependentField(secCounter,counter,fieldName,fieldType,this.content);

  }

  doValidation(){
    let isSave :boolean = true;
    let i: number = Number(this.sectionCounter);
    for(let j=0;j<this.content[i].sectionContent.length; j = j+1){
    if(this.content[i].sectionContent[j].required==="true" &&  this.content[i].sectionContent[j].visible==="Show"){
      if(this.content[i].sectionContent[j].field_type.toLowerCase() === "checkbox"){
        let isContainVal: boolean = false;
        for(let k=0;k<this.content[i].sectionContent[j].field_type_values.length; k = k+1){
          if(this.content[i].sectionContent[j].field_type_values[k].isChecked){
            isContainVal = true;
            break;
          }
        }
        if (!isContainVal) {
          this.content[i].sectionContent[j].valid = false;
          isSave = false;
        } else {
          this.content[i].sectionContent[j].valid = true;
        }
      } else {
        if (this.handlebar.isEmpty(this.content[i].sectionContent[j].value)) {
          this.content[i].sectionContent[j].valid = false;
          isSave = false;
          if (this.handlebar.isEmpty(this.emptyfield)) {
            this.emptyfield = this.content[i].sectionContent[j].field_label;
          } else {
            this.emptyfield += ', ' + this.content[i].sectionContent[j].field_label;
          }
        } else {
          this.content[i].sectionContent[j].valid = true;
        }
      }
    }
    }

    return isSave;
  }

  async openColumns(secCounter,fieldCounter,rowCounter){
    let row = this.content[secCounter].sectionContent[fieldCounter].rowContentValuesList[rowCounter];
    let fieldLabel = this.content[secCounter].sectionContent[fieldCounter].field_label;
    let sectionName = this.content[secCounter].sectionName;
    let colElem = this.content[secCounter].sectionContent[fieldCounter].columnValues;
    let data0 = {
      "uuid" : this.uhid,
      "rowElem" : row,
      "sectionName" : sectionName,
      "fieldLabel" : fieldLabel,
      "colElem":colElem,
      "secCounter":secCounter,
      "fieldCounter":fieldCounter,
      "rowCounter":rowCounter,
    }

    let modal = await this.modalCtrl.create({
      component: TableColumnsPage,
      componentProps: data0
    });
    await modal.present();
    modal.onDidDismiss().then((data) => {
      if(data){
        let secCounter = Number(data.data["secCounter"]);
        let fieldCounter = Number(data.data["fieldCounter"]);
        let rowCounter = Number(data.data["rowCounter"]);
        this.content[secCounter].sectionContent[fieldCounter].rowContentValuesList[rowCounter] = data.data["rowElem"];
      }
    });

  }

  dismiss(isupdated,data?: any) {
    this.modalCtrl.dismiss(data,isupdated);
  }
}
