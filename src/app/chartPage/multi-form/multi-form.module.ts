import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MultiFormPageRoutingModule } from './multi-form-routing.module';

import { MultiFormPage } from './multi-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MultiFormPageRoutingModule
  ],
  declarations: [MultiFormPage]
})
export class MultiFormPageModule {}
