import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MultiFormPage } from './multi-form.page';

describe('MultiFormPage', () => {
  let component: MultiFormPage;
  let fixture: ComponentFixture<MultiFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiFormPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MultiFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
