import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddWorkListPage } from './add-work-list.page';

const routes: Routes = [
  {
    path: '',
    component: AddWorkListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddWorkListPageRoutingModule {}
