import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddWorkListPageRoutingModule } from './add-work-list-routing.module';

import { AddWorkListPage } from './add-work-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddWorkListPageRoutingModule
  ],
  declarations: [AddWorkListPage]
})
export class AddWorkListPageModule {}
