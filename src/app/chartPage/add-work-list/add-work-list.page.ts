import { Component, OnInit } from '@angular/core';
import { TimeLineDataPushService } from 'src/app/service/timeline-auth-service';
import { NavParams, AlertController, ModalController } from '@ionic/angular';
import { AuthService } from 'src/app/service/auth.service';
import { MasterDataService } from 'src/app/service/master-data';
import * as moment from "moment";

@Component({
  selector: 'app-add-work-list',
  templateUrl: './add-work-list.page.html',
  styleUrls: ['./add-work-list.page.scss'],
})
export class AddWorkListPage implements OnInit {

  plannedCareItemId: string;
  workList: any = [];

  oldWorkList: any = [];
  serviceName: string;
  noRecordFound: string;
  constructor( private navParams: NavParams,
               private timeLineAuthService: TimeLineDataPushService,
               private authService: AuthService,
               private alertCtrl: AlertController,
               private modalCtrl: ModalController,
               private timelineServ: TimeLineDataPushService,
               private masterDataService: MasterDataService) {
    this.plannedCareItemId = this.navParams.get('plannedCareItemId');
  }

  getWorkListFromTLServ(plannedCareItemId):Promise<any>{
    return this.timelineServ.getAppointmentPlanItem(plannedCareItemId).then((data) => {
      if (data && data.appointments){
        let appitems = data.appointments[0];
        return this.masterDataService.mergeWorkListDefWithData(appitems);
      }else{
        return null;
      }
    });
   }

  ngOnInit() {
    this.authService.isTokenValid().then((returnVal) => {

      this.getWorkListFromTLServ(this.plannedCareItemId).then((data) => {
        let isExist = false;
        if (data) {
          this.serviceName = data.serviceName;
          if (data.worklistDefination != null) {
            for (let i = 0; i < data.worklistDefination.length; i++) {
              isExist = true;
              if (data.worklist != null) {
                if (data.worklist.length > 0) {
                  isExist = true;
                }
                for (let j = 0; j < data.worklist.length; j++) {
                  if (data.worklistDefination[i].task_seq === data.worklist[j].task_seq) {
                    data.worklistDefination[i].appointment_id = data.worklist[j].appointment_id;
                    data.worklistDefination[i].planned_care_item_id = data.worklist[j].planned_care_item_id;
                    data.worklistDefination[i].session_name = data.worklist[j].session_name;
                    data.worklistDefination[i].status = data.worklist[j].status;
                    let isChecked = false;
                    if (data.worklist[j].status === 'Completed') {
                      isChecked = true;
                      data.worklistDefination[i].time1 = this.masterDataService.hours_am_pm(data.worklist[j].time);
                      data.worklistDefination[i].time = data.worklist[j].time;
                    }
                    data.worklistDefination[i].isChecked = isChecked;
                    data.worklistDefination[i].taskCompleted = isChecked;
                    break;
                  }
                }
              }

              this.workList.push(data.worklistDefination[i]);
              this.oldWorkList.push(data.worklistDefination[i]);
            }
          }
        }
        if (!isExist) {
          this.noRecordFound = 'No Worklist Found!';
        } else {
          this.noRecordFound = '';
        }
      });
    });

  }
  async confirmationToUnCheck(index, taskCompleted) {
    if (taskCompleted) {
      const alert = await this.alertCtrl.create({
        message: 'Task already done! Want to Revert ?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              for (let i = 0; i < this.workList.length; i++) {
                if (this.workList[i].task_seq === index) {
                  this.workList[i].isChecked = true;
                }
              }
            }
          },
          {
            text: 'Done',
            handler: () => {
            }
          }
        ]
      });
      await alert.present();
    }
  }
  saveData() {
    for (let i = 0; i < this.workList.length; i++) {
      if (this.workList[i].isChecked) {

        const statusPen = this.oldWorkList[i].status;
        this.workList[i].status = 'Completed';
        if (statusPen === 'Pending') {
          this.workList[i].time = moment().format('HH:mm');
        }
      } else {
        this.workList[i].status = 'Pending';
      }
    }
    if (this.workList.length > 0) {
      const data = {
        tasklist : this.workList
      };
      this.timeLineAuthService.pushWorkListData(this.plannedCareItemId, data).then(() => {
        this.dismiss(true, data);
      });

    } else {
      this.dismiss(false);
    }
  }


  dismiss(isupdated, data?: any) {
    // using the injected ViewController this page
    // can "dismiss" itself and pass back data
    this.modalCtrl.dismiss(data, isupdated);
  }

}
