import { Component, OnInit } from '@angular/core';
import { TimeLineDataPushService } from '../../service/timeline-auth-service';
import { AuthService } from '../../service/auth.service';
import { NavParams, ModalController, PickerController, AlertController } from '@ionic/angular';
import { PickerOptions } from '@ionic/core';
import { MasterDataService } from '../../service/master-data';
import { Handlebar } from '../../components/handlebar';
import { UnitSelectPage } from '../unit-select/unit-select.page';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-add-vital-partial',
  templateUrl: './add-vital-partial.page.html',
  styleUrls: ['./add-vital-partial.page.scss'],
})
export class AddVitalPartialPage {

  height: string = "";
  weight: string = "";
  weightSel: string = "";
  temperature: string = "";
  temperatureSel: string = "";
  bodyFat: string;
  bodyFatMinRange: number;
  bodyFatMaxRange: number;
  bodyFatUnit: string = "%";
  caloriesBurned: string;
  caloriesBurnedUnit: string = "kcal";
  caloriesBurnedMinRange: string;
  caloriesBurnedMaxRange: string;
  expiratoryTime: string;
  expiratoryTimeUnit: string = "s";
  inspiratoryTime: string;
  inspiratoryTimeUnit: string = "s";
  stepCount: string;
  weightUnit: string = "kg";
  heightUnit: string = "cm";
  temperatureUnit: string = "degF";
  temperatureMinRange: number;
  temperatureMaxRange: number;
  systolicUnit: string = "mmHg";
  systolicMinRange: number;
  systolicMaxRange: number;
  diastolicUnit: string = "mmHg";
  diastolicMinRange: number;
  diastolicMaxRange: number;
  pulseRateUnit: string = "bpm";
  pulseRateMinRange: number;
  pulseRateMaxRange: number;
  heartRateUnit: string = "bpm";
  heartRateMinRange: number;
  heartRateMaxRange: number;
  spo2Unit: string = "%";
  bmiUnit: string = "kg/m2";
  bmiMinRange: number;
  bmiMaxRange: number;
  bloodGlucoseUnit: string = "mg/dL";
  bloodGlucoseMinRange: string;
  bloodGlucoseMaxRange: string;
  rbcCountUnit: string = "cells/liter";
  rbcCountMinRange: string;
  rbcCountMaxRange: string;
  cholesterolUnit: string = "mg/dL";
  cholesterolMinRange: string;
  cholesterolMaxRange: string;
  headCircumferenceUnit: string = "cm";
  headCircumferenceMaxRange: string;
  headCircumferenceMinRange: string;
  measureGroupLabel: string = "";
  measureGroup: string = "";
  systolic: string;
  diastolic: string;
  pulseRate: string;
  heartRate: string;
  spo2: string;
  spo2MinRange: string;
  spo2MaxRange: string;
  uuid: string;
  bmi: string;
  simpleColumns: any;
  weightColumns: any;
  heightColumns: any;
  tempColumns: any;
  systColumns: any;
  dilColumns: any;
  pulColumns: any;
  resColumns: any;
  spoColumns: any;
  fatColumns: any;
  heartColumns: any;
  choleColumns: any;
  headcolumns: any;
  itimeColumns: any;
  bgcolumns: any;
  rbccolms: any;
  stepCountColumns: any;
  estColumns: any;
  caloriesColumns: any;
  painLevelColumns: any;
  bmi_interpretation: string;
  spo2_interpretation: string;
  patientAge: number;
  numberPickerObject: any;
  timePickerCallback: any;
  painLevel: string;
  painLevelMinRange: string;
  painLevelMaxRange: string;
  painLevelSel: string;
  bloodGlucose: string;
  bloodGlucoseSel: string;
  rbcCount: string;
  rbcCountSel: string;
  cholesterol: string;
  cholesterolSel: String;
  headCircumference: string;
  headCircumferenceSel: string;
  temperature_interpretation: string;
  pulserate_interpretation: string;
  systolic_interpretation: string;
  heartRate_interpretation: string;
  diastolic_interpretation: string;

  painlevel_interpretation: string;
  bloodGlucose_interpretation: string;
  rbcCount_interpretation: string;
  cholesterol_interpretation: string;
  headCircumference_interpretation: string;
  bodyFat_interpretation: string;
  caloriesBurned_interpretation: string;
  expiratoryTime_interpretation: string;
  expiratoryTimeMinRange: string;
  expiratoryTimeMaxRange: string;
  inspiratoryTime_interpretation: string;
  inspiratoryTimeMinRange: string;
  inspiratoryTimeMaxRange: string;
  stepCount_interpretation: string;
  stepCountMinRange: string;
  stepCountMaxRange: string;
  vitalConfig: any;
  constructor(public navParams: NavParams,
    public timeLineAuthService: TimeLineDataPushService,
    public authService: AuthService,
    public modalCtrl: ModalController,
    public masterData: MasterDataService,
    public picketCtrl: PickerController,
    private alertController: AlertController,
    private handlebar: Handlebar) {
    this.uuid = this.navParams.get('uuid');
    const patientAge = this.navParams.get('age');
    this.getAge(patientAge);
    this.getVitalConfigurations();

  }
  getVitalConfigurations() {
    this.authService.getVitalConfigurations().then((data) => {
      this.vitalConfig = data;
      let decimalOptions = this.getDecimalVal();

      for (let i = 0; i < this.vitalConfig.length; i++) {
        if (this.vitalConfig[i].variable_name === 'weight') {
          if (this.vitalConfig[i].default_unit === 'lb'){
            this.weightUnit = 'lbs';
          } else {
            this.weightUnit = this.vitalConfig[i].default_unit;
          }
          this.weightMeasurementArray(this.vitalConfig[i].start_range, this.vitalConfig[i].end_range, decimalOptions);

        } else if (this.vitalConfig[i].variable_name === 'height') {
          this.heightUnit = this.vitalConfig[i].default_unit;
          this.heightMeasurementArray(this.vitalConfig[i].start_range, this.vitalConfig[i].end_range, decimalOptions);
        } else if (this.vitalConfig[i].variable_name === 'bmi') {
          this.bmiUnit = this.vitalConfig[i].default_unit;
          this.bmiMinRange = this.vitalConfig[i].min_range;
          this.bmiMaxRange = this.vitalConfig[i].max_range;
        } else if (this.vitalConfig[i].variable_name === 'temperature') {
          this.temperatureUnit = this.vitalConfig[i].default_unit;
          this.temperatureMinRange = this.vitalConfig[i].min_range;
          this.temperatureMaxRange = this.vitalConfig[i].max_range;
          this.tempMeasurementArray(this.vitalConfig[i].start_range, this.vitalConfig[i].end_range, decimalOptions);
        } else if (this.vitalConfig[i].variable_name === 'systolic') {
          this.systolicUnit = this.vitalConfig[i].default_unit;
          this.systolicMinRange = this.vitalConfig[i].min_range;
          this.systolicMaxRange = this.vitalConfig[i].max_range;
          this.systolicMeasurementArray(this.vitalConfig[i].start_range, this.vitalConfig[i].end_range, decimalOptions);
        } else if (this.vitalConfig[i].variable_name === 'diastolic') {
          this.diastolicUnit = this.vitalConfig[i].default_unit;
          this.diastolicMinRange = this.vitalConfig[i].min_range;
          this.diastolicMaxRange = this.vitalConfig[i].max_range;
          this.diastolicMeasurementArray(this.vitalConfig[i].start_range, this.vitalConfig[i].end_range, decimalOptions);
        } else if (this.vitalConfig[i].variable_name === 'pulseRate') {
          this.pulseRateUnit = this.vitalConfig[i].default_unit;
          this.pulseRateMinRange = this.vitalConfig[i].min_range;
          this.pulseRateMaxRange = this.vitalConfig[i].max_range;
          this.pulseRateMeasurementArray(this.vitalConfig[i].start_range, this.vitalConfig[i].end_range, decimalOptions);
        } else if (this.vitalConfig[i].variable_name === 'heartRate') {
          this.heartRateUnit = this.vitalConfig[i].default_unit;
          this.heartRateMinRange = this.vitalConfig[i].min_range;
          this.heartRateMaxRange = this.vitalConfig[i].max_range;
          this.heartRateMeasurementArray(this.vitalConfig[i].start_range, this.vitalConfig[i].end_range, decimalOptions);
        } else if (this.vitalConfig[i].variable_name === 'spo2') {
          this.spo2Unit = this.vitalConfig[i].default_unit;
          this.spo2MinRange = this.vitalConfig[i].min_range;
          this.spo2MaxRange = this.vitalConfig[i].max_range;
          this.spo2MeasurementArray(this.vitalConfig[i].start_range, this.vitalConfig[i].end_range, decimalOptions);
        } else if (this.vitalConfig[i].variable_name === 'bloodGlucose') {
          this.bloodGlucoseUnit = this.vitalConfig[i].default_unit;
          this.bloodGlucoseMinRange = this.vitalConfig[i].min_range;
          this.bloodGlucoseMaxRange = this.vitalConfig[i].max_range;
          this.bloddGlucoseMeasurementArray(this.vitalConfig[i].start_range, this.vitalConfig[i].end_range, decimalOptions);
        } else if (this.vitalConfig[i].variable_name === 'rbcCount') {
          this.rbcCountUnit = this.vitalConfig[i].default_unit;
          this.rbcCountMinRange = this.vitalConfig[i].min_range;
          this.rbcCountMaxRange = this.vitalConfig[i].max_range;
          this.rbcCountMeasurementArray(this.vitalConfig[i].start_range, this.vitalConfig[i].end_range, decimalOptions);
        } else if (this.vitalConfig[i].variable_name === 'cholesterol') {
          this.cholesterolUnit = this.vitalConfig[i].default_unit;
          this.cholesterolMinRange = this.vitalConfig[i].min_range;
          this.cholesterolMaxRange = this.vitalConfig[i].max_range;
          this.cholesterolMeasurementArray(this.vitalConfig[i].start_range, this.vitalConfig[i].end_range, decimalOptions);
        } else if (this.vitalConfig[i].variable_name === 'headCircumference') {
          this.headCircumferenceUnit = this.vitalConfig[i].default_unit;
          this.headCircumferenceMinRange = this.vitalConfig[i].min_range;
          this.headCircumferenceMaxRange = this.vitalConfig[i].max_range;
          this.headMeasurementArray(this.vitalConfig[i].start_range, this.vitalConfig[i].end_range, decimalOptions);
        } else if (this.vitalConfig[i].variable_name === 'painLevel') {
          this.painLevelMinRange = this.vitalConfig[i].min_range;
          this.painLevelMaxRange = this.vitalConfig[i].max_range;
          this.painLevelMeasurementArray(this.vitalConfig[i].start_range, this.vitalConfig[i].end_range, decimalOptions);
        } else if (this.vitalConfig[i].variable_name === 'bodyFat') {
          this.bodyFatUnit = this.vitalConfig[i].default_unit;
          this.bodyFatMinRange = this.vitalConfig[i].min_range;
          this.bodyFatMaxRange = this.vitalConfig[i].max_range;
          this.fatMeasurementArray(this.vitalConfig[i].start_range, this.vitalConfig[i].end_range, decimalOptions);
        } else if (this.vitalConfig[i].variable_name === 'caloriesBurned') {
          this.caloriesBurnedUnit = this.vitalConfig[i].default_unit;
          this.caloriesBurnedMinRange = this.vitalConfig[i].min_range;
          this.caloriesBurnedMaxRange = this.vitalConfig[i].max_range;
          this.caloriesMeasurementArray(this.vitalConfig[i].start_range, this.vitalConfig[i].end_range, decimalOptions);
        } else if (this.vitalConfig[i].variable_name === 'expiratoryTime') {
          this.expiratoryTimeUnit = this.vitalConfig[i].default_unit;
          this.expiratoryTimeMinRange = this.vitalConfig[i].min_range;
          this.expiratoryTimeMaxRange = this.vitalConfig[i].max_range;
          this.etimeMeasurementArray(this.vitalConfig[i].start_range, this.vitalConfig[i].end_range, decimalOptions);
        } else if (this.vitalConfig[i].variable_name === 'inspiratoryTime') {
          this.inspiratoryTimeUnit = this.vitalConfig[i].default_unit;
          this.inspiratoryTimeMinRange = this.vitalConfig[i].min_range;
          this.inspiratoryTimeMaxRange = this.vitalConfig[i].max_range;
          this.itimeMeasurementArray(this.vitalConfig[i].start_range, this.vitalConfig[i].end_range, decimalOptions);
        } else if (this.vitalConfig[i].variable_name === 'stepCount') {
          this.stepCountMinRange = this.vitalConfig[i].min_range;
          this.stepCountMaxRange = this.vitalConfig[i].max_range;
          this.stepcountMeasurementArray(this.vitalConfig[i].start_range, this.vitalConfig[i].end_range, decimalOptions);
        }
      }
      this.measureGroupLabel = this.weightUnit + ", " + this.heightUnit + ", " + this.temperatureUnit + ", " + this.bloodGlucoseUnit + ", " + this.cholesterolUnit;
    });
  }
  weightMeasurementArray(startRange, endRange, decimalOptions) {
    if (!endRange && endRange !== 0) {
      endRange = 150;
    }
    if (!startRange && startRange !== 0) {
      startRange = 1.0;
    }
    this.weightColumns = this.getMeasureArray(startRange, endRange, decimalOptions);
  }
  heightMeasurementArray(startRange, endRange, decimalOptions) {
    if (!endRange) {
      endRange = 250;
    }
    if (!startRange) {
      startRange = 30;
    }
    this.heightColumns = this.getMeasureArray(startRange, endRange, decimalOptions);
  }
  systolicMeasurementArray(startRange, endRange, decimalOptions) {
    if (!endRange) {
      endRange = 210;
    }
    if (!startRange) {
      startRange = 110;
    }
    this.systColumns = this.getMeasureArray(startRange, endRange, decimalOptions);
  }
  diastolicMeasurementArray(startRange, endRange, decimalOptions) {
    if (!endRange) {
      endRange = 120;
    }
    if (!startRange) {
      startRange = 70;
    }
    this.dilColumns = this.getMeasureArray(startRange, endRange, decimalOptions);
  }
  pulseRateMeasurementArray(startRange, endRange, decimalOptions) {
    if (!endRange) {
      endRange = 110;
    }
    if (!startRange) {
      startRange = 50;
    }
    this.pulColumns = this.getMeasureArray(startRange, endRange, decimalOptions);
  }
  heartRateMeasurementArray(startRange, endRange, decimalOptions) {
    if (!endRange) {
      endRange = 30;
    }
    if (!startRange) {
      startRange = 10;
    }
    this.resColumns = this.getMeasureArray(startRange, endRange, decimalOptions);
  }
  cholesterolMeasurementArray(startRange, endRange, decimalOptions) {
    if (!endRange) {
      endRange = 150;
    }
    if (!startRange) {
      startRange = 30;
    }
    this.choleColumns = this.getMeasureArray(startRange, endRange, decimalOptions);

  }
  bloddGlucoseMeasurementArray(startRange, endRange, decimalOptions) {
    if (!endRange) {
      endRange = 150;
    }
    if (!startRange) {
      startRange = 30;
    }
    this.bgcolumns = this.getMeasureArray(startRange, endRange, decimalOptions);

  }
  rbcCountMeasurementArray(startRange, endRange, decimalOptions) {
    if (!endRange) {
      endRange = 150;
    }
    if (!startRange) {
      startRange = 30;
    }
    this.rbccolms = this.getMeasureArray(startRange, endRange, decimalOptions);

  }
  tempMeasurementArray(startRange, endRange, decimalOptions) {
    if (!endRange) {
      endRange = 150;
    }
    if (!startRange) {
      startRange = 30;
    }
    this.tempColumns = this.getMeasureArray(startRange, endRange, decimalOptions);

  }
  spo2MeasurementArray(startRange, endRange, decimalOptions) {
    if (!endRange) {
      endRange = 110;
    }
    if (!startRange) {
      startRange = 80;
    }
    this.spoColumns = this.getMeasureArray(startRange, endRange, decimalOptions);

  }
  painLevelMeasurementArray(startRange, endRange, decimalOptions) {
    if (!endRange) {
      endRange = 20;
    }
    if (!startRange) {
      startRange = 1;
    }
    this.painLevelColumns = this.getMeasureArray(startRange, endRange, decimalOptions);

  }
  fatMeasurementArray(startRange, endRange, decimalOptions) {
    if (!endRange) {
      endRange = 250;
    }
    if (!startRange) {
      startRange = 1;
    }
    this.fatColumns = this.getMeasureArray(startRange, endRange, decimalOptions);

  }
  caloriesMeasurementArray(startRange, endRange, decimalOptions) {
    if (!endRange) {
      endRange = 250;
    }
    if (!startRange) {
      startRange = 30;
    }
    this.caloriesColumns = this.getMeasureArray(startRange, endRange, decimalOptions);

  }
  headMeasurementArray(startRange, endRange, decimalOptions) {
    if (!endRange) {
      endRange = 150;
    }
    if (!startRange) {
      startRange = 30;
    }
    this.simpleColumns = this.getMeasureArray(startRange, endRange, decimalOptions);

  }

  etimeMeasurementArray(startRange, endRange, decimalOptions) {
    if (!endRange) {
      endRange = 250;
    }
    if (!startRange) {
      startRange = 30;
    }
    this.estColumns = this.getMeasureArray(startRange, endRange, decimalOptions);

  }

  itimeMeasurementArray(startRange, endRange, decimalOptions) {
    if (!endRange) {
      endRange = 250;
    }
    if (!startRange) {
      startRange = 30;
    }
    this.itimeColumns = this.getMeasureArray(startRange, endRange, decimalOptions);

  }

  stepcountMeasurementArray(startRange, endRange, decimalOptions) {
    if (!endRange) {
      endRange = 250;
    }
    if (!startRange) {
      startRange = 30;
    }
    this.stepCountColumns = this.getMeasureArray(startRange, endRange, decimalOptions);

  }

  getMeasureArray(startRange, endRange, decimalOptions) {
    let startRangeVal = parseInt(startRange);
    let endRangeVal = parseInt(endRange);
    let optionsVal: any = [];
    for (let i = startRangeVal; i <= endRangeVal; i++) {
      let obj: any = {
        "text": i + "",
        "value": i + ""
      };
      optionsVal.push(obj);
    }
    let measureArry;
    if (startRange !== startRangeVal || endRange !== endRangeVal) {
      measureArry = [
        {
          name: 'col1',
          options: optionsVal
        }, {
          name: 'col2',
          options: decimalOptions
        }
      ];
    } else {
      measureArry = [
        {
          name: 'col1',
          options: optionsVal
        }
      ];
    }
    return measureArry;
  }
  getDecimalVal() {
    let decimalOptions: any = [];
    for (let i = 0; i < 10; i++) {
      let obj: any = {
        "text": "." + i,
        "value": "." + i
      };
      decimalOptions.push(obj);
    }
    return decimalOptions;
  }
  number: number;
  numberSettings: any = {
    theme: 'material',
    onInit: function (event, inst) {
      // Your custom event handler goes here
    },
    onMarkupReady: function (event, inst) {
      // Your custom event handler goes here
    },
    onBeforeShow: function (event, inst) {
      // Your custom event handler goes here
    },
    onPosition: function (event, inst) {
      // Your custom event handler goes here
    },
    onShow: function (event, inst) {
      // Your custom event handler goes here
    },
    onSet: function (event, inst) {
      // Your custom event handler goes here
    },
    onItemTap: function (event, inst) {
      // Your custom event handler goes here
    },
    onDestroy: function (event, inst) {
      // Your custom event handler goes here
    },
    onClose: function (event, inst) {
      // Your custom event handler goes here
    },
    onChange: function (event, inst) {
      // Your custom event handler goes here
    },
    onCancel: function (event, inst) {
      // Your custom event handler goes here
    },
    onBeforeClose: function (event, inst) {
      // Your custom event handler goes here
    },
    onClear: function (event, inst) {
      // Your custom event handler goes here
    }
  };

  getAge(patientAge) {
    if (patientAge != null && patientAge != undefined) {
      let ageElements = patientAge.split(" ");
      if (ageElements.length > 1) {
        if (ageElements[1].toLowerCase() === "years") {
          this.patientAge = ageElements[0];
        } else {
          this.patientAge = 2;
        }
      } else {
        this.patientAge = 0;
      }
    }
  }


  async getInputValue(dspNm, shrtNm, Units, type, maxRange, minRange) {
    const displayName = dspNm ? dspNm : shrtNm;
    // const range = (maxRange && minRange) ? " between " + minRange + " - " + maxRange : "";
    const header = displayName + " (" + Units + ")";
    const alert = await this.alertController.create({
      header: header,
      cssClass: 'inputAlertData',
      animated: true,
      inputs: [
        {
          name: 'selectedVal',
          type: 'number',
          id: 'inputAlert',
          min: minRange,
          max: maxRange
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Ok',
          role: 'done',
        }
      ]
    });
    document.getElementById('inputAlert').classList.add('inputAlertData');

    await alert.present();
    alert.onDidDismiss().then(async data => {
      if (data.role === 'done') {
        const value = data.data.values.selectedVal;
        this.sendValue(type, value);
      }
    });
  }

  saveVitalData() {
    let data = [];
    if (this.height) {
      let ele = this.height;
      let height = "";
      for (let i = 0; i < ele.length; i++) {
        height += ele[i];
      }
      if (this.heightUnit === 'm') {
        this.heightUnit = 'met';
      } else if (this.heightUnit === 'in') {
        this.heightUnit = 'inch';
      }

      let heightData = {
        "name": "height",
        "value": height,
        "unit": this.heightUnit
      }
      data.push(heightData);
    }
    if (this.weight) {
      let ele = this.weight;
      let weight = "";
      for (let i = 0; i < ele.length; i++) {
        weight += ele[i];
      }
      let weightData = {
        "name": "weight",
        "value": weight,
        "unit": this.weightUnit
      }
      data.push(weightData);
    }

    if (this.temperature) {
      let ele = this.temperature;
      let temperature = "";
      for (let i = 0; i < ele.length; i++) {
        temperature += ele[i];
      }
      let temperatureData = {
        "name": "temperature",
        "value": temperature,
        "unit": this.temperatureUnit,
        "interpretation": this.temperature_interpretation
      }
      data.push(temperatureData);
    }

    if (this.systolic) {
      let systolicData = {
        "name": "systolic",
        "value": this.systolic,
        "unit": this.systolicUnit,
        "interpretation": this.systolic_interpretation
      }
      data.push(systolicData);
    }

    if (this.diastolic) {
      let diastolicData = {
        "name": "diastolic",
        "value": this.diastolic,
        "unit": this.diastolicUnit,
        "interpretation": this.diastolic_interpretation
      }
      data.push(diastolicData);
    }

    if (this.pulseRate) {
      let pulseRateData = {
        "name": "pulseRate",
        "value": this.pulseRate,
        "unit": this.pulseRateUnit,
        "interpretation": this.pulserate_interpretation
      }
      data.push(pulseRateData);
    }
    if (this.heartRate) {
      let heartRateData = {
        "name": "heartRate",
        "value": this.heartRate,
        "unit": this.heartRateUnit,
        "interpretation": this.heartRate_interpretation
      }
      data.push(heartRateData);
    }

    if (this.spo2) {
      let spo2Data = {
        "name": "spo2",
        "value": this.spo2,
        "unit": this.spo2Unit,
        "interpretation": this.spo2_interpretation
      }
      data.push(spo2Data);
    }
    if (this.bmi) {
      let bmiData = {
        "name": "bmi",
        "value": this.bmi,
        "unit": this.bmiUnit,
        "interpretation": this.bmi_interpretation
      }
      data.push(bmiData);
    }

    if (this.painLevel) {
      let painLevelData = {
        "name": "painLevel",
        "value": this.painLevel,
        "interpretation": this.painlevel_interpretation
      }
      data.push(painLevelData);
    }

    if (this.bloodGlucose) {
      let bloodGlucoseData = {
        "name": "bloodGlucose",
        "value": this.bloodGlucose,
        "unit": this.bloodGlucoseUnit,
        "interpretation": this.bloodGlucose_interpretation
      }
      data.push(bloodGlucoseData);
    }

    if (this.rbcCount) {
      let rbcCountData = {
        "name": "rbcCount",
        "value": this.rbcCount,
        "unit": this.rbcCountUnit,
        "interpretation": this.rbcCount_interpretation
      }
      data.push(rbcCountData);
    }

    if (this.cholesterol) {
      let cholesterolData = {
        "name": "cholesterol",
        "value": this.cholesterol,
        "unit": this.cholesterolUnit,
        "interpretation": this.cholesterol_interpretation
      }
      data.push(cholesterolData);
    }

    if (this.headCircumference) {
      let headCircumferenceData = {
        "name": "headCircumference",
        "value": this.headCircumference,
        "unit": this.headCircumferenceUnit,
        "interpretation": this.headCircumference_interpretation
      }
      data.push(headCircumferenceData);
    }


    if (this.bodyFat) {
      let bodyFatData = {
        "name": "bodyFat",
        "value": this.bodyFat,
        "unit": this.bodyFatUnit,
        "interpretation": this.bodyFat_interpretation
      }
      data.push(bodyFatData);
    }


    if (this.caloriesBurned) {
      let caloriesBurnedData = {
        "name": "caloriesBurned",
        "value": this.caloriesBurned,
        "unit": this.caloriesBurnedUnit,
        "interpretation": this.caloriesBurned_interpretation
      }
      data.push(caloriesBurnedData);
    }


    if (this.expiratoryTime) {
      let expiratoryTimeData = {
        "name": "expiratoryTime",
        "value": this.expiratoryTime,
        "unit": this.expiratoryTimeUnit,
        "interpretation": this.expiratoryTime_interpretation
      }
      data.push(expiratoryTimeData);
    }


    if (this.inspiratoryTime) {
      let inspiratoryTimeData = {
        "name": "inspiratoryTime",
        "value": this.inspiratoryTime,
        "unit": this.inspiratoryTimeUnit,
        "interpretation": this.inspiratoryTime_interpretation
      }
      data.push(inspiratoryTimeData);
    }

    if (this.stepCount) {
      let stepCountData = {
        "name": "stepCount",
        "value": this.stepCount,
        "interpretation": this.stepCount_interpretation
      }
      data.push(stepCountData);
    }

    if (data.length > 0) {
      this.timeLineAuthService.pushVitalData(this.uuid, data).then(() => {

        this.dismiss(true, data);
      });

    } else {
      this.dismiss(false);
    }
  }

  heightValueChange(event) {
    if (event) {
      let checkErrorMsg = "";
      let height = event;
      checkErrorMsg = checkErrorMsg + this.masterData.deDoubleValidation(height, "Height", false);
      if (checkErrorMsg) {
        this.height = "0";
        alert(checkErrorMsg);
      } else {
        this.height = height;
        this.bmiValueChange(height, this.weight);
      }
    }
  }
  weightValueModify(event) {
    if (event) {
      let checkErrorMsg = "";

      let weight = this.getDecimalDropDownVal(event);
      checkErrorMsg = checkErrorMsg + this.masterData.deDoubleValidation(event, "Weight", false);
      if (checkErrorMsg) {
        this.weight = "0";
        this.weightSel = "0";
        alert(checkErrorMsg);
      } else {
        this.weightSel = weight;
        this.weight = event;
        this.bmiValueChange(this.height, event);
      }
    }
  }
  weightValueChange(event) {
    if (event) {
      let checkErrorMsg = "";
      let weight = this.getNumberFieldVal(event);
      checkErrorMsg = checkErrorMsg + this.masterData.deDoubleValidation(weight, "Weight", false);
      if (checkErrorMsg) {
        this.weight = "0";
        this.weightSel = "0";
        alert(checkErrorMsg);
      } else {
        this.weightSel = event;
        this.weight = weight;
        this.bmiValueChange(this.height, weight);
      }
    }
  }

  bmiValueChange(height, weight) {
    let ele = weight.split(" ");
    weight = "";
    for (let i = 0; i < ele.length; i++) {
      weight += ele[i];
    }
    let data = this.masterData.bmiPrediction(height, this.heightUnit, weight, this.weightUnit, this.bmiMinRange, this.bmiMaxRange);
    this.bmi = data["bmi"];
    this.bmi_interpretation = data["prediction"];
  }
  weightScaleChange() {
    if (this.weight != "0" && this.handlebar.isNotEmpty(this.weight)) {
      let value = 0.0;
      if (this.weightUnit == "lbs") {
        value = parseFloat(this.weight) * 2.2046;
      } else if (this.weightUnit == "kg") {
        value = parseFloat(this.weight) / 2.2046;
      }
      this.weight = parseFloat(value + "").toFixed(2);
    }
    this.bmiValueChange(this.height, this.weight);
  }
  heightScaleChange(phscale) {
    if (this.handlebar.isNotEmpty(this.height)) {
      let hscale = this.heightUnit;
      let height = parseFloat(this.height);
      if (hscale === "cm") {
        if (phscale == "inch") {
          height = height * (2.54);
        } else if (phscale == "met") {
          height = height * 100;
        } else if (phscale == "Ft") {
          height = height * 30.48;
        }
      } else if (hscale == "met") {
        if (phscale == "cm") {
          height = height / 100;
        } else if (phscale == "inch") {
          height = height / 39.370;
        } else if (phscale == "Ft") {
          height = height * 3.280839895;
        }
      } else if (hscale == "inch") {
        if (phscale == "met") {
          height = height * 39.37;
        } else if (phscale == "cm") {
          height = height * .39370;
        } else if (phscale == "Ft") {
          height = height * 12;
        }
      } else if (hscale == "Ft") {
        if (phscale == "met") {
          height = height * 0.3048;
        } else if (phscale == "cm") {
          height = height * 0.0328084;
        } else if (phscale == "inch") {
          height = height / 12;
        }
      }
      this.height = height.toFixed(2);
    }
    this.bmiValueChange(this.height, this.weight);
  }

  tempValueModify(event) {
    let temperature = this.getDecimalDropDownVal(event);
    this.temperature = event;
    this.temperatureSel = temperature;
    let data = this.masterData.tempPrediction(event, this.temperatureUnit, this.temperatureMinRange, this.temperatureMaxRange);
    this.temperature_interpretation = data;
  }



  tempValueChange(event) {
    let temperature = this.getNumberFieldVal(event);
    this.temperature = temperature;
    this.temperatureSel = event;
    let data = this.masterData.tempPrediction(temperature, this.temperatureUnit, this.temperatureMinRange, this.temperatureMaxRange);
    this.temperature_interpretation = data;
  }

  tempScaleChange() {
    if (this.temperature != "0" && this.handlebar.isNotEmpty(this.temperature)) {
      let value = 0.0;
      if (this.temperatureUnit === "Cel") {
        value = ((parseFloat(this.temperature) - 32) * 5) / 9;
      } else if (this.temperatureUnit === "degF") {
        value = ((parseFloat(this.temperature) * 9) / 5) + 32;
      }
      this.temperature = parseFloat(value + "").toFixed(2);
      this.tempValueChange(this.temperature);
    }
  }

  painLevelChange(event) {
    let painLevel = this.getNumberFieldVal(event);
    this.painLevel = painLevel;
    this.painLevelSel = event;

    this.painLevelPrediction(this.painLevel);
  }
  painLevelModify(event) {
    let painLevel = this.getDecimalDropDownVal(event);

    this.painLevel = event;
    this.painLevelSel = painLevel;
    this.painLevelPrediction(this.painLevel);
  }

  painLevelPrediction(painLevel) {
    if ((this.painLevelMinRange && Number(this.painLevelMinRange) > 0) || (this.painLevelMaxRange && Number(this.painLevelMaxRange) > 0)) {
      let data = this.masterData.valuePrediction("PainLevel", painLevel, this.painLevelMinRange, this.painLevelMaxRange);
      this.painlevel_interpretation = data;
    }
  }
  bloodGlucoseChange(event) {
    let bloodGlucose = this.getNumberFieldVal(event);
    this.bloodGlucose = bloodGlucose;
    this.bloodGlucoseSel = event;

    this.gloucosePrediction(this.bloodGlucose);
  }
  bloodGlucoseModify(event) {
    let bloodGlucose = this.getDecimalDropDownVal(event);

    this.bloodGlucose = event;
    this.bloodGlucoseSel = bloodGlucose;
    this.gloucosePrediction(this.bloodGlucose);

  }


  gloucosePrediction(bloodGlucose) {
    if ((this.bloodGlucoseMinRange && Number(this.bloodGlucoseMinRange) > 0) || (this.bloodGlucoseMaxRange && Number(this.bloodGlucoseMaxRange) > 0)) {
      let data = this.masterData.valuePrediction("BloodGlucose", bloodGlucose, this.bloodGlucoseMinRange, this.bloodGlucoseMaxRange);
      this.bloodGlucose_interpretation = data;
    }
  }

  rbcCountChange(event) {
    let rbcCount = this.getNumberFieldVal(event);
    this.rbcCount = rbcCount;
    this.rbcCountSel = event;
    this.rbcCountPrediction(this.rbcCount);

  }
  rbcCountModify(event) {
    let rbcCount = this.getDecimalDropDownVal(event);

    this.rbcCount = event;
    this.rbcCountSel = rbcCount;
    this.rbcCountPrediction(this.rbcCount);

  }
  rbcCountPrediction(rbcCount) {
    if ((this.rbcCountMinRange && Number(this.rbcCountMinRange) > 0) || (this.rbcCountMaxRange && Number(this.rbcCountMaxRange) > 0)) {
      let data = this.masterData.valuePrediction("RbcCount", rbcCount, this.rbcCountMinRange, this.rbcCountMaxRange);
      this.rbcCount_interpretation = data;
    }
  }

  cholesterolChange(event) {
    let cholesterol = this.getNumberFieldVal(event);
    this.cholesterol = cholesterol;
    this.cholesterolSel = event;
    this.cholesterolPrediction(this.cholesterol);

  }
  cholesterolModify(event) {
    let cholesterol = this.getDecimalDropDownVal(event);

    this.cholesterol = event;
    this.cholesterolSel = cholesterol;
    this.cholesterolPrediction(this.cholesterol);

  }
  cholesterolPrediction(cholesterol) {
    if ((this.cholesterolMinRange && Number(this.cholesterolMinRange) > 0) || (this.cholesterolMaxRange && Number(this.cholesterolMaxRange) > 0)) {
      let data = this.masterData.valuePrediction("Cholesterol", cholesterol, this.cholesterolMinRange, this.cholesterolMaxRange);
      this.cholesterol_interpretation = data;
    }
  }

  headCircumferenceChange(event) {
    let headCircumference = this.getNumberFieldVal(event);
    this.headCircumference = headCircumference;
    this.headCircumferenceSel = event;
    this.headCircumferencePrediction(this.headCircumference);

  }
  headCircumferenceModify(event) {
    let headCircumference = this.getDecimalDropDownVal(event);

    this.headCircumference = event;
    this.headCircumferenceSel = headCircumference;
    this.headCircumferencePrediction(this.headCircumference);
  }
  headCircumferencePrediction(headCircumference) {
    if ((this.headCircumferenceMinRange && Number(this.headCircumferenceMinRange) > 0) || (this.headCircumferenceMaxRange && Number(this.headCircumferenceMaxRange) > 0)) {
      let data = this.masterData.valuePrediction("HeadCircumference", headCircumference, this.headCircumferenceMinRange, this.headCircumferenceMaxRange);
      this.headCircumference_interpretation = data;
    }
  }
  bodyFatChange(event) {
    this.bodyFat = event;
    if ((this.bodyFatMinRange && Number(this.bodyFatMinRange) > 0) || (this.bodyFatMaxRange && Number(this.bodyFatMaxRange) > 0)) {
      let data = this.masterData.valuePrediction("BodyFat", this.bodyFat, this.bodyFatMinRange, this.bodyFatMaxRange);
      this.bodyFat_interpretation = data;
    }
  }
  caloriesBurnedChange(event) {
    this.caloriesBurned = event;
    if ((this.caloriesBurnedMinRange && Number(this.caloriesBurnedMinRange) > 0) || (this.caloriesBurnedMaxRange && Number(this.caloriesBurnedMaxRange) > 0)) {
      let data = this.masterData.valuePrediction("CaloriesBurned", this.caloriesBurned, this.caloriesBurnedMinRange, this.caloriesBurnedMaxRange);
      this.caloriesBurned_interpretation = data;
    }
  }
  expiratoryTimeChange(event) {
    this.expiratoryTime = event;
    if ((this.expiratoryTimeMinRange && Number(this.expiratoryTimeMinRange) > 0) || (this.expiratoryTimeMaxRange && Number(this.expiratoryTimeMaxRange) > 0)) {
      let data = this.masterData.valuePrediction("ExpiratoryTime", this.expiratoryTime, this.expiratoryTimeMinRange, this.expiratoryTimeMaxRange);
      this.expiratoryTime_interpretation = data;
    }
  }
  inspiratoryTimeChange(event) {
    this.inspiratoryTime = event;
    if ((this.inspiratoryTimeMinRange && Number(this.inspiratoryTimeMinRange) > 0) || (this.inspiratoryTimeMaxRange && Number(this.inspiratoryTimeMaxRange) > 0)) {
      let data = this.masterData.valuePrediction("InspiratoryTime", this.inspiratoryTime, this.inspiratoryTimeMinRange, this.inspiratoryTimeMaxRange);
      this.inspiratoryTime_interpretation = data;
    }
  }
  stepCountChange(event) {
    this.stepCount = event;
    if ((this.stepCountMinRange && Number(this.stepCountMinRange) > 0) || (this.stepCountMaxRange && Number(this.stepCountMaxRange) > 0)) {
      let data = this.masterData.valuePrediction("StepCount", this.stepCount, this.stepCountMinRange, this.stepCountMaxRange);
      this.stepCount_interpretation = data;
    }
  }
  getDecimalDropDownVal(event) {
    let ele = event.split(".");
    let element = "";
    for (let i = 0; i < ele.length; i++) {
      if (i > 0) {
        element += " ." + ele[i];
      } else {
        element += ele[i];
      }
    }
    return element;
  }
  getNumberFieldVal(event) {
    let ele = event.split(" ");
    let element = "";
    for (let i = 0; i < ele.length; i++) {
      element += ele[i];
    }
    return element;
  }
  pulseValueChange(event) {
    this.pulseRate = event;
    let data = this.masterData.pulsePrediction(this.pulseRate, this.pulseRateUnit, this.pulseRateMinRange, this.pulseRateMaxRange);
    this.pulserate_interpretation = data;
  }
  systolicValueChange(event) {
    this.systolic = event;
    let data = this.masterData.systolicPrediction(this.systolic, this.systolicMinRange, this.systolicMaxRange);
    this.systolic_interpretation = data;
  }
  diastolicValueChange(event) {
    this.diastolic = event;
    let data = this.masterData.diastolicPrediction(this.diastolic, this.diastolicMinRange, this.diastolicMaxRange);
    this.diastolic_interpretation = data;
  }
  heartRateValueChange(event) {
    this.heartRate = event;
    let data = this.masterData.heartPrediction(this.heartRate, this.patientAge, this.heartRateMinRange, this.heartRateMaxRange);
    this.heartRate_interpretation = data;
  }

  spo2ValueChange(event) {
    this.spo2 = event;
    let data = this.masterData.spo2Interpretation(this.spo2, this.spo2MinRange, this.spo2MaxRange);
    this.spo2_interpretation = data;

  }
  dismiss(isupdated, data?: any) {
    // using the injected ViewController this page
    // can "dismiss" itself and pass back data
    this.modalCtrl.dismiss(data, isupdated);
  }

  async navigateToUnitPage() {
    let data0 = {
      "weightUnit": this.weightUnit,
      "heightUnit": this.heightUnit,
      "tempUnit": this.temperatureUnit,
      "glucoseUnit": this.bloodGlucoseUnit,
      "cholesterolUnit": this.cholesterolUnit,
      "measureGroupLabel": this.measureGroupLabel,
      "vitalConfig": this.vitalConfig
    }
    let modal = await this.modalCtrl.create({
      component: UnitSelectPage,
      componentProps: data0,
      showBackdrop: true,
       backdropDismiss: true,
       cssClass: "chartModelStyle"
    });
    await modal.present();

    var chartPage = document.getElementsByTagName("app-patient-charting");
    chartPage[0].classList.add("disableClick");

    modal.onDidDismiss().then((data) => {
      chartPage[0].classList.remove("disableClick");
      if (data) {
        this.weightUnit = data.data["weightUnit"];
        let oldUnit = this.heightUnit;
        this.heightUnit = data.data["heightUnit"];
        this.headCircumferenceUnit = data.data["heightUnit"];
        this.temperatureUnit = data.data["tempUnit"];
        this.measureGroupLabel = data.data["measureGroupLabel"];
        this.bloodGlucoseUnit = data.data["glucoseUnit"];
        this.cholesterolUnit = data.data["cholesterolUnit"];
        this.weightScaleChange();
        this.heightScaleChange(oldUnit);
        this.tempScaleChange();
      }
    });

  }
  async picker(type) {
    const columnData = [] = this.selectPickerData(type);
    let opts: PickerOptions = {
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Done',
          role: 'done'
        }
      ],
      columns: columnData
    }
    let picker = await this.picketCtrl.create(opts);
    picker.present();
    picker.onDidDismiss().then(async data => {
      if (data.role === 'done') {
        let col = await picker.getColumn(columnData[0].name);
        const value = col.options[col.selectedIndex].value;
        this.sendValue(type, value);
      }
    });
  }
  sendValue(type, value) {
    if (type === 'weight') {
      this.weightValueChange(value);
    } else if (type === 'height') {
      this.heightValueChange(value);
    } else if (type === 'temperature') {
      this.tempValueChange(value);
    } else if (type === 'systolic') {
      this.systolicValueChange(value);
    } else if (type === 'diastolic') {
      this.diastolicValueChange(value);
    } else if (type === 'pulseRate') {
      this.pulseValueChange(value);
    } else if (type === 'heartRate') {
      this.heartRateValueChange(value);
    } else if (type === 'spo2') {
      this.spo2ValueChange(value);
    } else if (type === 'bloodGlucose') {
      this.bloodGlucoseChange(value);
    } else if (type === 'rbcCount') {
      this.rbcCountChange(value);
    } else if (type === 'cholesterol') {
      this.cholesterolChange(value);
    } else if (type === 'headCircumference') {
      this.headCircumferenceChange(value);
    } else if (type === 'painLevel') {
      this.painLevelChange(value);
    } else if (type === 'bodyFat') {
      this.bodyFatChange(value);
    } else if (type === 'caloriesBurned') {
      this.caloriesBurnedChange(value);
    } else if (type === 'expiratoryTime') {
      this.expiratoryTimeChange(value);
    } else if (type === 'inspiratoryTime') {
      this.inspiratoryTimeChange(value);
    } else if (type === 'stepCount') {
      this.stepCountChange(value);
    }
  }
  selectPickerData(type) {
    if (type === 'weight') {
      return this.weightColumns;
    } else if (type === 'height') {
      return this.heightColumns;
    } else if (type === 'temperature') {
      return this.tempColumns;
    } else if (type === 'systolic') {
      return this.systColumns;
    } else if (type === 'diastolic') {
      return this.dilColumns;
    } else if (type === 'pulseRate') {
      return this.pulColumns;
    } else if (type === 'heartRate') {
      return this.resColumns
    } else if (type === 'spo2') {
      return this.spoColumns;
    } else if (type === 'bloodGlucose') {
      return this.bgcolumns;
    } else if (type === 'rbccolms') {
      return this.rbccolms
    } else if (type === 'cholesterol') {
      return this.choleColumns;
    } else if (type === 'headCircumference') {
      return this.simpleColumns;
    } else if (type === 'painLevel') {
      return this.painLevelColumns;
    } else if (type === 'bodyFat') {
      return this.fatColumns;
    } else if (type === 'caloriesBurned') {
      return this.caloriesColumns
    } else if (type === 'expiratoryTime') {
      return this.estColumns
    } else if (type === 'inspiratoryTime') {
      return this.itimeColumns
    } else if (type === 'stepCount') {
      return this.stepCountColumns;
    }
  }
}
