import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddVitalPartialPage } from './add-vital-partial.page';

describe('AddVitalPartialPage', () => {
  let component: AddVitalPartialPage;
  let fixture: ComponentFixture<AddVitalPartialPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVitalPartialPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddVitalPartialPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
