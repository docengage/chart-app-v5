import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddVitalPartialPageRoutingModule } from './add-vital-partial-routing.module';

import { AddVitalPartialPage } from './add-vital-partial.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddVitalPartialPageRoutingModule
  ],
  declarations: [AddVitalPartialPage]
})
export class AddVitalPartialPageModule {}
