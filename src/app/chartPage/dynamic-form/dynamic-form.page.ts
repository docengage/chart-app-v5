import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, ModalController, NavParams, IonDatetime } from '@ionic/angular';
import { AuthService } from 'src/app/service/auth.service';
import { TimeLineDataPushService } from 'src/app/service/timeline-auth-service';
import { ToastServiceData } from 'src/app/service/toast-service';
import { Handlebar } from 'src/app/components/handlebar';
import { DynamicFormCommon } from 'src/app/components/dynamicform-common';
import { TableColumnsPage } from 'src/app/table-columns/table-columns.page';
import * as moment from 'moment';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.page.html',
  styleUrls: ['./dynamic-form.page.scss'],
})
export class DynamicFormPage implements OnInit {

  uhid: string;
  formDefId: string;
  formId: any;
  dynamicFormData: any;
  formName: string;
  content: any;
  counter: Number;
  encounterId: string;
  plannedCareItemId: string;
  isDisclaimer: string = "false";
  isDisclaimerCheck: boolean = false;
  disclaimerContent: string;
  is_signature_required: string = "false";
  is_signature_requiredCheck: boolean = false;
  signature_image_url: string;
  signature = '';
  isDrawing = false;
  showAddForm = false;
  patientInfo: any;
  disableButton = false;
  pageType: any;
  emptyfield = '';
  itemIndex: any;
  secIndex: any;
  startDate = moment().format();

  @ViewChild(IonDatetime) datetime: IonDatetime;
  showModal = false;

  constructor(public navCtrl: NavController, public modalCtrl: ModalController,
    public navParams: NavParams, private authService: AuthService,
    public timeLineAuthService: TimeLineDataPushService, public toastServ: ToastServiceData,
    private handlebar: Handlebar, private dynamicFormCommon: DynamicFormCommon
  ) {

    this.uhid = this.navParams.data.uuid;
    this.formDefId = this.navParams.data.formDefId;
    this.formId = this.navParams.data.formId;
    this.encounterId = this.navParams.data.encounterId;
    this.plannedCareItemId = this.navParams.data.plannedCareItemId;
    this.pageType = this.navParams.data.pageType;

  }
  ngOnInit(): void {
    this.getDynamicFormContent();
    this.getPatientInfo();
  }

  setDateModal(value, itemIndex, secIndex) {
    this.itemIndex = itemIndex;
    this.secIndex = secIndex;
    if (this.handlebar.isEmpty(value) || value === undefined) {
      this.startDate = value = moment().format();
      this.content[itemIndex].sectionContent[secIndex].value = moment(value).format('DD-MM-YYYY');
    } else {
      this.startDate = moment(value, 'DD-MM-YYYY').format();
      this.content[itemIndex].sectionContent[secIndex].value = moment(value, 'DD-MM-YYYY').format('DD-MM-YYYY');
    }

    this.showModal = true;
  }

  dateChange(value, itemIndex, secIndex) {
    this.itemIndex = itemIndex;
    this.secIndex = secIndex;
    
    if (this.handlebar.isEmpty(value) || value === undefined) {
      this.startDate = value = moment().format();
    } else {
      this.startDate = moment(value).format();
    }

    this.content[itemIndex].sectionContent[secIndex].value = moment(value).format('DD-MM-YYYY');

    this.showModal = false;
  }

  getDynamicFormContent() {
    this.authService.getDynamicFormData(this.formDefId, this.formId).then((data) => {
      this.dynamicFormData = JSON.parse(data);
      if (this.dynamicFormData) {
        this.formName = this.dynamicFormData.form_name;
        this.disclaimerContent = this.dynamicFormData.disclaimerContent;
        this.signature_image_url = this.dynamicFormData.signature_image_url;
        this.isDisclaimer = this.dynamicFormData.isDisclaimer;
        this.is_signature_required = this.dynamicFormData.is_signature_required;
        this.content = JSON.parse(this.dynamicFormData.content);
        this.content.isDisclaimer = this.isDisclaimer;
        this.content.disclaimerContent = this.disclaimerContent;
        this.content.is_signature_required = this.is_signature_required;
        this.content.signature_image_url = this.signature_image_url;
        this.counter = this.dynamicFormData.counter;
        if (Number(this.formId) > 0) {
          this.isDisclaimerCheck = true;
          this.is_signature_requiredCheck = true;
          if(this.pageType === 'edit'){
            this.showAddForm = false;
          } else {
            this.showAddForm = true;
          }
        } else {
          this.showAddForm = false;
        }
      }
    });
  }

  addAsNewForm() {
    this.formId = 0;
    this.getDynamicFormContent();
  }
  getPatientInfo() {
    this.authService.getPatientInfoLight(this.uhid).then((data) => {
      this.patientInfo = data;
      if (this.patientInfo && this.handlebar.isEmpty(this.patientInfo.email) && this.handlebar.isEmpty(this.patientInfo.telephone)){
        this.disableButton = true;
      } else {
        this.disableButton = false;
      }
    });

  }
  saveAndShareForm() {
    this.emptyfield = '';
    let isSave: boolean = this.doValidation();
    let signatureurl: string = "";
    if (this.is_signature_requiredCheck) {
      signatureurl = this.signature_image_url;
    }
    if(isSave){
      let data = {
        encounterId: this.encounterId,
        content: this.content,
        formDefId: this.formDefId,
        formId: this.formId,
        carePlanItemId: this.plannedCareItemId,
        isDisclaimer: this.isDisclaimerCheck,
        signatureurl: signatureurl,
        uhid: this.patientInfo.patientUhid,
        externalSourceEmail: this.patientInfo.email,
        externalSourceMobile: this.patientInfo.telephone,
        isMobileApp: true
    }
      this.timeLineAuthService.pushDynamicDataAndShare(data).then((data0) => {
        if (data0 && (data0.status === 400 || data0.status === 401 || data0.status === 500)) {
          const responseData = JSON.parse(data0._body);
          this.toastServ.presentToast(responseData.description);
        } else {
          this.toastServ.presentToast('The document is share and saved successfully');
          this.dismiss(true, data);
        }
      });
    } else if (this.handlebar.isNotEmpty(this.emptyfield)) {
      this.toastServ.presentToast("Please fill mandatory field " + this.emptyfield);
    }
    
  }
  saveForm() {
    let isContinue: boolean = true;
    let errorMsg: string = "";
    if (this.isDisclaimer === "true" && !this.isDisclaimerCheck) {
      isContinue = false;
      errorMsg = "Please Check Disclaimer";
    }

    if (isContinue) {
      this.continueSaveForm();
    } else {
      this.toastServ.presentToast(errorMsg);
    }

  }
  continueSaveForm() {
    this.emptyfield = '';
    let isSave: boolean = this.doValidation();
    let signatureurl: string = "";
    if (this.is_signature_requiredCheck) {
      signatureurl = this.signature_image_url;
    }

    if (isSave) {
      let data = {
        encounterId: this.encounterId,
        content: this.content,
        formDefId: this.formDefId,
        formId: this.formId,
        carePlanItemId: this.plannedCareItemId,
        isDisclaimer: this.isDisclaimerCheck,
        signatureurl: signatureurl
      };

      this.timeLineAuthService.pushDynamicData(this.uhid, data).then((data0) => {
        if (data0 && (data0.status === 400 || data0.status === 401 || data0.status === 500)) {
          const responseData = JSON.parse(data0._body);
          this.toastServ.presentToast(responseData.description);
        } else {
          this.toastServ.presentToast('The document is saved successfully');
          this.dismiss(true, data);
        }
      });
    } else if (this.handlebar.isNotEmpty(this.emptyfield)) {
      this.toastServ.presentToast("Please fill mandatory field "+this.emptyfield);
    }


  }
  doValidation() {
    let isSave: boolean = true;
    for (let i = 0; i < this.content.length; i++) {
      for (let j = 0; j < this.content[i].sectionContent.length; j++) {

        if (this.content[i].sectionContent[j].required === "true" && this.content[i].sectionContent[j].visible === "Show") {
          if (this.content[i].sectionContent[j].field_type.toLowerCase() === "checkbox") {
            let isContainVal: boolean = false;
            for (let k = 0; k < this.content[i].sectionContent[j].field_type_values.length; k++) {
              if (this.content[i].sectionContent[j].field_type_values[k].isChecked) {
                isContainVal = true;
                break;
              }
            }
            if (!isContainVal) {
              this.content[i].sectionContent[j].valid = false;
              isSave = false;
            } else {
              this.content[i].sectionContent[j].valid = true;
            }
          } else {
            if (this.handlebar.isEmpty(this.content[i].sectionContent[j].value)) {
              this.content[i].sectionContent[j].valid = false;
              isSave = false;
              if (this.handlebar.isEmpty(this.emptyfield)){
                this.emptyfield = this.content[i].sectionContent[j].field_label;
              } else {
                this.emptyfield += ', '+this.content[i].sectionContent[j].field_label;
              }
              
            } else {
              this.content[i].sectionContent[j].valid = true;
            }
          }
        }
      }
    }
    return isSave;
  }

  enableDependentField(secCounter, counter, fieldName, fieldType) {

    this.content = this.dynamicFormCommon.enableDependentField(secCounter, counter, fieldName, fieldType, this.content);

  }

  async openColumns(secCounter, fieldCounter, rowCounter) {
    let row = this.content[secCounter].sectionContent[fieldCounter].rowContentValuesList[rowCounter];
    let fieldLabel = this.content[secCounter].sectionContent[fieldCounter].field_label;
    let sectionName = this.content[secCounter].sectionName;
    let colElem = this.content[secCounter].sectionContent[fieldCounter].columnValues;
    let data0 = {
      "uuid": this.uhid,
      "rowElem": row,
      "sectionName": sectionName,
      "fieldLabel": fieldLabel,
      "colElem": colElem,
      "secCounter": secCounter,
      "fieldCounter": fieldCounter,
      "rowCounter": rowCounter,
    }

    let modal = await this.modalCtrl.create({
      component: TableColumnsPage,
      componentProps: data0
    });
    await modal.present();
    modal.onDidDismiss().then((data) => {
      if (data) {
        let secCounter = Number(data.data["secCounter"]);
        let fieldCounter = Number(data.data["fieldCounter"]);
        let rowCounter = Number(data.data["rowCounter"]);
        this.content[secCounter].sectionContent[fieldCounter].rowContentValuesList[rowCounter] = data.data["rowElem"];
      }
    });

  }

  drawComplete() {
    this.isDrawing = false;
  }

  drawStart() {
    this.isDrawing = true;
  }

  dismiss(isupdated, data?: any) {
    this.modalCtrl.dismiss(data, isupdated);
  }
}
