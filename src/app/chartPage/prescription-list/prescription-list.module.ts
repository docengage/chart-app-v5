import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrescriptionListPageRoutingModule } from './prescription-list-routing.module';

import { PrescriptionListPage } from './prescription-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PrescriptionListPageRoutingModule
  ],
  declarations: [PrescriptionListPage]
})
export class PrescriptionListPageModule {}
