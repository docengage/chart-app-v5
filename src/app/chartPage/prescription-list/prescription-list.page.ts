import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController, Platform, AlertController } from '@ionic/angular';
import { TimeLineDataPushService } from 'src/app/service/timeline-auth-service';
import { AuthService } from 'src/app/service/auth.service';
import { ToastServiceData } from 'src/app/service/toast-service';
import { MasterDataService } from 'src/app/service/master-data';
import { AddPrescriptionPage } from '../add-prescription/add-prescription.page';
import { Printer, PrintOptions } from '@ionic-native/printer/ngx';

@Component({
  selector: 'app-prescription-list',
  templateUrl: './prescription-list.page.html',
  styleUrls: ['./prescription-list.page.scss'],
})
export class PrescriptionListPage implements OnInit {

  medications : any[];
  prescription : {};
  plannedCareItemId : number;
  encounterId : number;
  uuid : string;
  deleteArray : any[];
  isIOSPlt:boolean = false;
  printSettings : any;
  patientInfo : any;
  careCenterData : any;
  isMCI:boolean=false;
  constructor(public navParam : NavParams,
              private modalCtrl: ModalController,
              private timelineAuthService : TimeLineDataPushService,
              private authService : AuthService,
              private plt: Platform,
              private toastServ: ToastServiceData,
              private alertCtrl: AlertController,
              private printer: Printer,
              private masterDataServ: MasterDataService) {}

  ngOnInit(): void {
    if (this.plt.is('ios')) {
      this.isIOSPlt = true;
    }
    this.medications = this.navParam.get('medications');
    this.prescription = this.navParam.get('prescription');
    this.plannedCareItemId = this.navParam.get('plannedCareItemId');
    this.encounterId = this.navParam.get('encounterId');
    this.uuid = this.navParam.get('uuid');
    this.patientInfo = this.navParam.get('patientInfo');
    this.getPrintSettingsInfo();
    if (this.prescription["care_center_id"]) {
      this.getCareCentersMasterDataById(this.prescription["care_center_id"]);
    }
    this.authService.getOrgPreferences().then((pref)=>{
      this.isMCI = pref["isMCIFormat"];
    });
  }


  getPrintSettingsInfo(){
    this.authService.getPrintSettingsInfo().then((data)=>{
      this.printSettings = data["printSettings"];
    });

  }
  getCareCentersMasterDataById(careCenterId){
    this.authService.getCareCentersMasterDataById(careCenterId).then((data)=>{
      this.careCenterData = data["careCenter"];
    });

  }
  

  dismiss() {
    let data = {
      "encounterId" : this.encounterId,
      "medications": this.medications,
      "prescription": this.prescription
    }

    this.modalCtrl.dismiss(data);
  }


  print(){
    let element = document.getElementById('getElemnetVal').innerHTML;
    // this.printer.isAvailable().then((onsuccess: any) => {

    let options: PrintOptions = {
        name: 'MyDocument',
        printer: 'printer007',
        duplex: true,
        orientation: 'landscape'
        //landscape: true,
        // grayscale: true
    };

    this.printer.print(element,options).then((value: any) => {
            console.log('value:', value);
        }, (error) => {
            console.log('error:', error);
        });

  // }, (err) => {
  //       alert(err);
  //       console.log('err:', "Printer Not Available");
  //       console.log('UUId',this.uuid);
  // });

}


  async deletePrescription(medicineId){
    let alert = await this.alertCtrl.create({
      header: 'Confirm Delete',
      message: 'Do you want to delete this medicine ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'Delete',
          handler: () => {
            this.masterDataServ.checkPermission("Prescription","delete").then((isPermission)=>{
              if(isPermission){

                this.timelineAuthService.deleteMedicineLieItem(this.prescription["prescription_id"],medicineId).then(()=>{

                    for(let i = this.medications.length - 1; i >= 0; i--){
                      if(this.medications[i].medication.medication_id == medicineId){
                          this.medications.splice(i,1);
                      }
                    }
                });
              }else{
                this.toastServ.presentToast("Denied");
              }
            });
          }
        }
      ]
    });
    await alert.present();

  }


  async addPrescription(){
    let data = {
      "uuid" : this.uuid,
      "encounterId" : this.encounterId,
      "plannedCareItemId": this.plannedCareItemId,
      "chartingPrescriptionId": this.prescription ? this.prescription["prescription_id"]:0
    }
    let modal = await this.modalCtrl.create({
      component: AddPrescriptionPage,
      componentProps: data
    });
    await modal.present();

    modal.onWillDismiss().then((data) => {
      const isUpdate = data.role;
      if(isUpdate){
        if(!this.medications){
          this.medications = data.data[1];
        }else{
          let medications = data.data[1];
          for(let i=0;i<medications.length;i++){
            this.medications.push(medications[i]);
          }
        }
        if(!this.prescription){
          this.prescription = {};
        }
        this.prescription["prescription_id"] = data.data[0];
        this.encounterId = data.data[2];

      }

    });
  }
}
