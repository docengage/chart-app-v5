import { Component, OnInit } from '@angular/core';
import { MasterDataService } from 'src/app/service/master-data';
import { ModalController, NavParams } from '@ionic/angular';
import { LocalStorageService } from 'src/app/service/local-storage.service';

@Component({
  selector: 'app-chart-page-config',
  templateUrl: './chart-page-config.page.html',
  styleUrls: ['./chart-page-config.page.scss'],
})
export class ChartPageConfigPage implements OnInit {

  apptFilters: Array<{value: string, isChecked: boolean, label: string}> = [];
  includedTrackNames: any;

  constructor(
    public masterData: MasterDataService,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private localstorage: LocalStorageService
    ) { }

  ngOnInit() {
    this.includedTrackNames = this.navParams.data;
    this.masterData.getChartingConfig().then((listFilters) => {
      listFilters.forEach(ele => {
        this.apptFilters.push({
          value: ele.value,
          isChecked: ele.isChecked,
          label: ele.label
        });
      });
    });
  }

  reset(){
    this.localstorage.setChartingTracks([]);
    this.apptFilters = [];
    this.masterData.getChartingConfig().then((listFilters) => {
      listFilters.forEach(ele => {
        this.apptFilters.push({
          value: ele.value,
          isChecked: ele.isChecked,
          label: ele.label
        });
      });
    });
  }

  applyFilters() {
    // Pass back a new array of track names to exclude
    const includedTrackNames = this.apptFilters.filter(c => c.isChecked).map(c => c.value);
    if (this.includedTrackNames.toString() === includedTrackNames.toString()) {
      this.dismiss(false);
    } else {
      this.dismiss(true, includedTrackNames);
    }

    const tracks = [];
    for (let i = 0; i < this.apptFilters.length; i++) {
      if (this.apptFilters[i].isChecked) {
        tracks.push({value: this.apptFilters[i].value, label: this.apptFilters[i].label, isChecked: true});
      } else {
        tracks.push({value: this.apptFilters[i].value, label: this.apptFilters[i].label, isChecked: false});
      }
    }
    this.localstorage.setChartingTracks(tracks);

  }

  dismiss(isupdated, data?: any) {
    // using the injected ViewController this page
    // can "dismiss" itself and pass back data
    this.modalCtrl.dismiss(data, isupdated);
  }

}
