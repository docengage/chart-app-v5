import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChartPageConfigPage } from './chart-page-config.page';

const routes: Routes = [
  {
    path: '',
    component: ChartPageConfigPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChartPageConfigPageRoutingModule {}
