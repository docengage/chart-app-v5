import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChartPageConfigPage } from './chart-page-config.page';

describe('ChartPageConfigPage', () => {
  let component: ChartPageConfigPage;
  let fixture: ComponentFixture<ChartPageConfigPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartPageConfigPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChartPageConfigPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
