import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChartPageConfigPageRoutingModule } from './chart-page-config-routing.module';

import { ChartPageConfigPage } from './chart-page-config.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChartPageConfigPageRoutingModule
  ],
  declarations: [ChartPageConfigPage]
})
export class ChartPageConfigPageModule {}
