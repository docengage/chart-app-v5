import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectMedicinePage } from './select-medicine.page';

const routes: Routes = [
  {
    path: '',
    component: SelectMedicinePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectMedicinePageRoutingModule {}
