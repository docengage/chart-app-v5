import { Component, OnInit } from '@angular/core';
import { MasterDataService } from 'src/app/service/master-data';
import { NavParams, ModalController, LoadingController } from '@ionic/angular';
import { AuthService } from 'src/app/service/auth.service';
import { Handlebar } from 'src/app/components/handlebar';

@Component({
  selector: 'app-select-medicine',
  templateUrl: './select-medicine.page.html',
  styleUrls: ['./select-medicine.page.scss'],
})
export class SelectMedicinePage implements OnInit {

  selectedId : string;
  selectedName : string;
  searchTerm : string;
  masterFilters: any = [];
  categoryFilterData:any = [];
  selectedCat: string;
  apptFilters : any;
  searchVal : string;
  timer : any;
  loading: any;
  isNoMedicine = false;

  currentPageClass = this;
 
  triggerAlphaScrollChange: number = 0;



  constructor(
    public masterData: MasterDataService,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public authService : AuthService,
    public loadingCtrl : LoadingController,
    private handlebar:Handlebar
  ) {
    this.selectedId = this.navParams.data.selectedId;
  }
  onItemClick(item) {
    this.triggerAlphaScrollChange++;
    this.selectedId = item.productId;
    this.dismiss();
  }
  ngOnInit() {
    this.searchDataWithLoader('$');
  }
  // showLoader(){
  //   this.loading = this.loadingCtrl.create({
  //       content: 'loading...'
  //   });
//}
  ionViewDidLoad() {
    //this.showLoader();
    //this.loading.present().then(()=>{
      this.searchDataWithLoader('$');
   // });
  }

  getData(itemname, loading?) {
    if(this.handlebar.isEmpty(itemname)){
      itemname = "$";
    }
    this.authService.getMedicationMasterDataLight(itemname,0,false).then((data) => {
      if (loading) {
        loading.dismiss();
      }

      this.masterFilters = data.medicines;
      const parsedData = this.parseMedListvalues(this.masterFilters);
      this.categoryFilterData = parsedData.selectedCategory;
      this.apptFilters = this.masterFilters;
      this.selectedId = "";
      
      if (this.masterFilters && this.masterFilters.length == 0) {
        this.isNoMedicine = true;
      }
    });
  }

  async searchDataWithLoader(itemname){
    this.isNoMedicine = false;
    let loading = await this.loadingCtrl.create({
      message: 'loading...'
    });
    await loading.present();

    this.getData(itemname, loading);
  }
  
  searchData(itemname){
    this.isNoMedicine = false;
    
    if (this.masterFilters) {
      if (itemname && itemname !== '$') {
       const medicines = this.apptFilters.filter(function(row) {
          return row.productName.trim().toLowerCase().includes(itemname.toLowerCase());
       });
       this.masterFilters = medicines;

      } else {
        this.masterFilters = this.apptFilters;
      }

      if (this.masterFilters && this.masterFilters.length == 0) {
        this.isNoMedicine = true;
      }
    } else {
      this.getData(itemname);
    }
    
  }

  filterByCategory() {
    if (this.handlebar.isEmpty(this.selectedCat)) {
        this.searchData("$");
    }else if (this.selectedCat === 'Empty') {
      this.selectedCat = "";
    }
    let selectedCategory = this.selectedCat;
    const categoryData = this.apptFilters.filter(function(row) {
        return row.category.trim() === selectedCategory.trim();
     });
     this.masterFilters = categoryData;
  }

  selectPatientMedicine(val){
    this.selectedId = val;
    this.dismiss();
  }
  parseMedListvalues(selectedData) {
    let selectedCategory = [];
    selectedData.forEach(element => {
      if (selectedCategory.indexOf(element.category) < 0) {
        if (this.handlebar.isEmpty(element.category)) {
          element.catogory = "Empty";
      }
        selectedCategory.push(element.category);
      }
    });
    let data = {
      selectedCategory: selectedCategory
    }
    return data;
  }

  onSearchInput(){
    if (this.searchTerm){
      if (this.searchVal != this.searchTerm) {
        clearTimeout(this.timer);
        let temp = this;
        this.timer = setTimeout(function() {
          temp.searchVal = temp.searchTerm;
          // temp.apptFilters = [];
          temp.searchData(temp.searchTerm);
        }, 400);
      }
    }else{
      this.searchData('$');
    }
  }

  dismiss() {
    let selectedMedicine = this.masterFilters.filter(
    item => item.productId == this.selectedId);
    if (selectedMedicine){
      let medicine = selectedMedicine[0];
      let returnData = {
        medicine : medicine,
        selectedId : this.selectedId
      }
      this.modalCtrl.dismiss(returnData);
    }else {
      this.modalCtrl.dismiss();
    }
  }

}
