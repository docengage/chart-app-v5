import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectMedicinePage } from './select-medicine.page';

describe('SelectMedicinePage', () => {
  let component: SelectMedicinePage;
  let fixture: ComponentFixture<SelectMedicinePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectMedicinePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectMedicinePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
