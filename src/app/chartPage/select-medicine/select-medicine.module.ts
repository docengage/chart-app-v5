import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectMedicinePageRoutingModule } from './select-medicine-routing.module';

import { SelectMedicinePage } from './select-medicine.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectMedicinePageRoutingModule
  ],
  declarations: [SelectMedicinePage]
})
export class SelectMedicinePageModule {}
