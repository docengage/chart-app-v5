import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, ActionSheetController, LoadingController, NavParams, Platform, IonDatetime } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { TimeLineDataPushService } from 'src/app/service/timeline-auth-service';
import { ToastServiceData } from 'src/app/service/toast-service';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { AngularFireStorage } from '@angular/fire/storage';
import * as moment from 'moment';
import * as firebase from 'firebase';
import { Chooser } from '@ionic-native/chooser/ngx';
import { Handlebar } from 'src/app/components/handlebar';

@Component({
  selector: 'app-add-document',
  templateUrl: './add-document.page.html',
  styleUrls: ['./add-document.page.scss'],
})
export class AddDocumentPage implements OnInit {
  myDocument: any;
  uhid: string;
  loading: any;
  startDate: any;
  itemName: string;
  urifilename: string;
  nativepath: any;
  mimetype: string;
  filename: string;
  showDoneBtn: boolean = false;
  originalPath: any;
  isAndroidPlt: boolean = false;
  dateValue: any = '';

  @ViewChild(IonDatetime) datetime: IonDatetime;
  showModal = true;

  constructor(
    public modalCtrl: ModalController,
    private actionSheetCtrl: ActionSheetController,
    private camera: Camera,
    private timeLineService: TimeLineDataPushService,
    public loadingCtrl: LoadingController,
    public toastServ: ToastServiceData,
    public navParams: NavParams,
    private fileChooser: FileChooser,
    private file: File,
    private fireStorage: AngularFireStorage,
    private filePath: FilePath,
    private plt: Platform,
    private chooser: Chooser,
    public handlebar: Handlebar
  ) {
    this.uhid = this.navParams.data.uuid;
    if (this.plt.is('android')) {
      this.isAndroidPlt = true;
    }
  }

  ngOnInit() {
  }
  dismiss(isupdated, data?: any) {
    this.modalCtrl.dismiss(data, isupdated);
  }

  // captureFile() {
  //   this.fileChooser.open().then(uri => {
  //   (<any>window).FilePath.resolveNativePath(uri, (result) => {
  //     this.nativepath = result;
  //     const filenameSIndex = this.nativepath.lastIndexOf('/');
  //     const path = this.nativepath.substring(0, filenameSIndex);
  //     this.filename = this.nativepath.substring(filenameSIndex + 1, this.nativepath.length);
  //     const fileTypeIndx = this.filename.lastIndexOf('.');
  //     const ftype = this.filename.substring(fileTypeIndx + 1, this.filename.length);
  //     if (ftype === 'pdf') {
  //       this.file.readAsBinaryString(path, this.filename).then(content => {
  //         content = (<any>window).btoa(content);
  //         this.myDocument = 'data:application/pdf;base64,' + JSON.stringify(content);
  //         this.mimetype = 'application/pdf';
  //         this.showDoneBtn = true;
  //       })
  //       .catch(err => {
  //         this.showDoneBtn = false;
  //       });
  //     } else {
  //       this.showDoneBtn = false;
  //       this.toastServ.presentToast('Please select PDF file.');
  //     }
  //     // this.audioplay();
  //   }, (err) => {
  //     this.showDoneBtn = false;
  //   });
  // })
  // .catch(e => {
  //   this.showDoneBtn = false;
  //   console.log(e)});
  // }

  dateChange(value) {
    if (this.handlebar.isEmpty(value) || value === undefined) {
      value = moment().format();
    }
    this.startDate = value;
    this.dateValue = moment(value).format('DD-MM-YYYY');
    this.dateCancel();
  }

  dateCancel() {
    if (this.datetime) {
      this.datetime.cancel(true);
    }
  }

  captureFile() {
    this.fileChooser.open().then(uri => {
      this.originalPath = uri;
      this.filePath.resolveNativePath(uri).then(async (result) => {
        this.nativepath = result;
        const filenameSIndex = this.nativepath.lastIndexOf('/');
        const path = this.nativepath.substring(0, filenameSIndex);
        this.filename = this.nativepath.substring(filenameSIndex + 1, this.nativepath.length);
        const fileTypeIndx = this.filename.lastIndexOf('.');
        const ftype = this.filename.substring(fileTypeIndx + 1, this.filename.length);
        if (ftype === 'pdf') {
          const loader = await this.loadingCtrl.create({
            message: 'Processing...'
          });
          await loader.present();
          this.mimetype = 'application/pdf';
          const myPath = this.nativepath;
          const directoryUrl = myPath.substr(0, myPath.lastIndexOf('/') + 1);
          const name = myPath.substr(myPath.lastIndexOf('/') + 1);
          this.file.resolveDirectoryUrl(directoryUrl).then((dataEntry) => {
            this.file.getFile(dataEntry, name, {}).then((file) => {
              this.uploadFiletoFireStore(file, loader);
            }, err => {
              loader.dismiss();
              console.log(JSON.stringify(err));
            });
          }, error => {
            loader.dismiss();
            console.log(JSON.stringify(error));
          });

        } else {
          this.toastServ.presentToast('Please select PDF file.');
        }
      }, (err) => {
      });
    })
      .catch(e => {
        console.log(e)
      });
  }

  getFileBlob(f, loader) {
    const reader = new FileReader();
    reader.readAsArrayBuffer(f);
    reader.onload = () => {
      const fileBlob = new Blob([new Uint8Array((reader.result as ArrayBuffer))], { type: 'application/pdf' });
      this.uploadFiletoFireStore(fileBlob, loader);
    };
  }

  async getFileBlobIos(f: FileEntry, loader) {
    const path = f.nativeURL.substr(0, f.nativeURL.lastIndexOf('/') + 1);
    const buffer = await this.file.readAsArrayBuffer(path, f.name);
    const fileBlob = new Blob([buffer], { type: 'application/pdf' });
    this.uploadFiletoFireStore(fileBlob, loader);
  }


  async uploadFiletoFireStore(fileBlob, loader) {
    const randomId = Math.random().toString(36).substring(2, 8);
    const imagePath = `patientUploadedFiles/patfile_${this.uhid}_${randomId}`;
    const uploadTask = this.fireStorage.upload(imagePath, fileBlob);
    uploadTask.percentageChanges().subscribe((progress) => {
      /* returns value between 0-100 on contunious upload progress */
      if (loader) {
      loader.message = 'Processing.... ' + progress;
      }
    }, (err) => { alert(JSON.stringify(err)); });
    uploadTask.then(res => {
      if (loader) {
        loader.message = 'Processing...';
      }
      const storageRef = firebase.storage().refFromURL('gs://patientschartapp.appspot.com/' + imagePath);

      storageRef.getDownloadURL().then((url) => {
        console.log(url);
        this.myDocument = url;
        loader.dismiss();
        this.showDoneBtn = true;
      });
    }).catch(err => {
      alert(JSON.stringify(err));
    });
  }

  captureIosFile() {
    this.chooser.getFile('application/pdf').then(async nativeData => {
      if (nativeData.uri) {
        this.nativepath = nativeData.uri;

        const filenameSIndex = this.nativepath.lastIndexOf('/');

        this.filename = this.nativepath.substring(filenameSIndex + 1, this.nativepath.length);
        const fileTypeIndx = this.filename.lastIndexOf('.');

        const ftype = this.filename.substring(fileTypeIndx + 1, this.filename.length);

        if (ftype === 'pdf') {
          const loader = await this.loadingCtrl.create({
            message: 'Processing...'
          });
          await loader.present();

          this.mimetype = 'application/pdf';

          const myPath = this.nativepath;

          const directoryUrl = myPath.substr(0, myPath.lastIndexOf('/') + 1);
          const name = myPath.substr(myPath.lastIndexOf('/') + 1);
          this.file.resolveDirectoryUrl(directoryUrl).then((dataEntry) => {
            this.file.getFile(dataEntry, name, {}).then((file) => {
              this.getFileBlobIos(file, loader);
            }, err => {
              loader.dismiss();
              console.log(JSON.stringify(err));
            });
          }, error => {
            loader.dismiss();
            console.log(JSON.stringify(error));
          });
        } else {
          this.toastServ.presentToast('Please select PDF file.');
        }
      }
    }).catch((error: any) => console.log(error));
  }

  async onFileChange(fileChangeEvent) {
    const file = fileChangeEvent.target.files[0];
    if (file && file.type === 'application/pdf') {
      const loader = await this.loadingCtrl.create({
        message: 'Processing...'
      });
      await loader.present();
      this.mimetype = file.type;
      this.filename = file.name;
      this.getFileBlob(file, loader);
    } else {
      this.toastServ.presentToast('Please select PDF file.');
    }
  }

  async presentActionSheet() {
    const buttonsArray: any = [
      /*{
      text: 'Take Photo',
      role: 'photo',
      handler: () => {
        const options: CameraOptions = {
          quality: 70,
          destinationType: this.camera.DestinationType.DATA_URL,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then((imageData) => {
          // imageData is either a base64 encoded string or a file URI
          // If it's base64 (DATA_URL):
          this.myDocument = 'data:image/jpeg;base64,' + imageData;
          this.mimetype = 'image/jpeg';
          this.showDoneBtn = true;
        }, (err) => {
          this.showDoneBtn = false;
          // Handle error
        });
      }
    }, */
    {
      text: 'Choose Image',
      role: 'gallery',
      handler: () => {
        const options: CameraOptions = {
          destinationType: this.camera.DestinationType.DATA_URL,
          sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
          allowEdit: true,
          encodingType: this.camera.EncodingType.JPEG,
          saveToPhotoAlbum: false
        };
        this.camera.getPicture(options).then((imageData) => {
          // imageData is either a base64 encoded string or a file URI
          // If it's base64:
          this.myDocument = 'data:image/jpeg;base64,' + imageData;
          this.mimetype = 'image/jpeg';
          this.showDoneBtn = true;
        }, (err) => {
          this.showDoneBtn = false;
          // Handle error
        });
      }
    },
    ];
    buttonsArray.push({
      text: 'Choose PDF',
      role: 'gallery',
      handler: () => {
        const ios = this.plt.is('ios');
        if (ios) {
          this.captureIosFile();
        } else {
          // this.captureFile();
          document.getElementById('file').click();
        }
      }
    });
    buttonsArray.push({
      text: 'Cancel',
      role: 'cancel',
      handler: () => { }
    });
    let actionSheet = await this.actionSheetCtrl.create({
      header: 'Upload Patient Documents',
      buttons: buttonsArray
    });
    await actionSheet.present();
  }



  async savePatientDocument() {
    let chartingSaveErr: boolean = false;
    if (!this.myDocument) {
      chartingSaveErr = true;
    }
    if (!this.startDate) {
      chartingSaveErr = true;
    }
    if (!this.itemName) {
      chartingSaveErr = true;
    }
    
    if (chartingSaveErr) {
      this.toastServ.presentToast("Please fill mandatory fields");
    } else {
      this.loading = await this.loadingCtrl.create({
        message: 'Uploading...'
      });
      await this.loading.present();
      this.startDate = moment(this.startDate).format('DD/MM/YYYY');
      this.timeLineService.pushPatientDocumentSave(this.uhid, this.myDocument, this.startDate,
        this.itemName, this.urifilename, this.mimetype).then((data) => {
          this.loading.dismiss();
          if (data) {
            this.uploadMessageToast('Patient Document Uploaded.');
            this.dismiss(true);
          } else {
            this.uploadMessageToast('Patient Document Not Uploaded. Please try again!');
          }
      });
    }
  }
  uploadMessageToast(msg) {
    this.toastServ.presentToast(msg);
  }
}
