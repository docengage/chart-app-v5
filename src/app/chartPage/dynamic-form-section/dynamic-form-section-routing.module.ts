import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DynamicFormSectionPage } from './dynamic-form-section.page';

const routes: Routes = [
  {
    path: '',
    component: DynamicFormSectionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DynamicFormSectionPageRoutingModule {}
