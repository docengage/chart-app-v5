import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DynamicFormSectionPageRoutingModule } from './dynamic-form-section-routing.module';

import { DynamicFormSectionPage } from './dynamic-form-section.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DynamicFormSectionPageRoutingModule
  ],
  declarations: [DynamicFormSectionPage]
})
export class DynamicFormSectionPageModule {}
