import { Component, OnInit, ViewChild } from '@angular/core';
import { NavParams, ModalController, IonDatetime } from '@ionic/angular';
import { TimeLineDataPushService } from 'src/app/service/timeline-auth-service';
import { ToastServiceData } from 'src/app/service/toast-service';
import { Handlebar } from 'src/app/components/handlebar';
import { DynamicFormCommon } from 'src/app/components/dynamicform-common';
import { TableColumnsPage } from 'src/app/table-columns/table-columns.page';
import * as moment from 'moment';

@Component({
  selector: 'app-dynamic-form-section',
  templateUrl: './dynamic-form-section.page.html',
  styleUrls: ['./dynamic-form-section.page.scss'],
})
export class DynamicFormSectionPage {

  uhid: string;
  formDefId: string;
  formId: string;
  dynamicFormData: any;
  formName: string;
  content: any;
  counter: Number;
  encounterId: string;
  plannedCareItemId:string;
  isDisclaimer:string="false";
  isDisclaimerCheck:boolean=false;
  disclaimerContent:string;
  sectionCounter: Number=0;
  itemIndex: any;
  secIndex: any;
  startDate: any;

  @ViewChild(IonDatetime) datetime: IonDatetime;
  showModal = false;

  constructor( public navParams: NavParams,
    public timeLineAuthService: TimeLineDataPushService,public toastServ: ToastServiceData,
    private handlebar:Handlebar,public modalCtrl: ModalController,public dynamicFormCommon:DynamicFormCommon) {

    this.uhid = this.navParams.data.uuid;
    this.formDefId = this.navParams.data.formDefId;
    this.formId = this.navParams.data.formId;
    this.encounterId = this.navParams.data.encounterId;
    this.plannedCareItemId = this.navParams.data.plannedCareItemId;
    this.sectionCounter = this.navParams.data.secSequnceCounter;
    this.content = this.navParams.data.content;
    this.formName = this.navParams.data.formName;
    this.isDisclaimer = this.content.isDisclaimer;
    this.disclaimerContent = this.content.disclaimerContent;
    if(Number(this.formId) > 0){
      this.isDisclaimerCheck = true;
    }
  }

  ionViewDidLoad() {

  }

  setDateModal(value, itemIndex, secIndex) {
    this.itemIndex = itemIndex;
    this.secIndex = secIndex;
    if (this.handlebar.isEmpty(value) || value === undefined) {
      this.startDate = value = moment().format();
      this.content[itemIndex].sectionContent[secIndex].value = moment(value).format('DD-MM-YYYY');
    } else {
      this.startDate = moment(value, 'DD-MM-YYYY').format();
      this.content[itemIndex].sectionContent[secIndex].value = moment(value, 'DD-MM-YYYY').format('DD-MM-YYYY');
    }

    this.showModal = true;
  }

  dateChange(value, itemIndex, secIndex) {
    this.itemIndex = itemIndex;
    this.secIndex = secIndex;
    
    if (this.handlebar.isEmpty(value) || value === undefined) {
      this.startDate = value = moment().format();
    } else {
      this.startDate = moment(value).format();
    }

    this.content[itemIndex].sectionContent[secIndex].value = moment(value).format('DD-MM-YYYY');

    this.showModal = false;
  }

  saveForm(){
    if(this.isDisclaimer==="true"){
      if(!this.isDisclaimerCheck){
        this.toastServ.presentToast("Please Check Disclaimer!");
      }else{
        this.continueSaveForm();
      }
    }else{
      this.continueSaveForm();
    }
  }
  continueSaveForm(){
    let isSave :boolean = this.doValidation();

    if(isSave){
      let data = {
        encounterId: this.encounterId,
        content : this.content,
        formDefId : this.formDefId,
        formId : this.formId,
        carePlanItemId: this.plannedCareItemId,
        isDisclaimer : this.isDisclaimerCheck

      };
      this.timeLineAuthService.pushDynamicData(this.uhid,data).then((data)=>{
        this.dismiss(true,data);
      });
    }


  }

  enableDependentField (secCounter,counter,fieldName,fieldType){

    this.content = this.dynamicFormCommon.enableDependentField(secCounter,counter,fieldName,fieldType,this.content);

  }

  doValidation(){
    let isSave :boolean = true;
    let i: number = Number(this.sectionCounter);
    for(let j=0;j<this.content[i].sectionContent.length;j++){
    if(this.content[i].sectionContent[j].required==="true" &&  this.content[i].sectionContent[j].visible==="Show"){
      if(this.content[i].sectionContent[j].field_type.toLowerCase() === "checkbox"){
        let isContainVal: boolean = false;
        for(let k=0;k<this.content[i].sectionContent[j].field_type_values.length;k++){
          if(this.content[i].sectionContent[j].field_type_values[k].isChecked){
            isContainVal = true;
            break;
          }
        }
        if(!isContainVal){
          this.content[i].sectionContent[j].valid=false;
          isSave = false;
        }else{
          this.content[i].sectionContent[j].valid=true;
        }
      }else{
        if(this.handlebar.isEmpty(this.content[i].sectionContent[j].value)){
          this.content[i].sectionContent[j].valid=false;
          isSave = false;
        }else{
          this.content[i].sectionContent[j].valid=true;
        }
      }
    }
    }

    return isSave;
  }

  async openColumns(secCounter,fieldCounter,rowCounter){
    let row = this.content[secCounter].sectionContent[fieldCounter].rowContentValuesList[rowCounter];
    let fieldLabel = this.content[secCounter].sectionContent[fieldCounter].field_label;
    let sectionName = this.content[secCounter].sectionName;
    let colElem = this.content[secCounter].sectionContent[fieldCounter].columnValues;
    let data0 = {
      "uuid" : this.uhid,
      "rowElem" : row,
      "sectionName" : sectionName,
      "fieldLabel" : fieldLabel,
      "colElem":colElem,
      "secCounter":secCounter,
      "fieldCounter":fieldCounter,
      "rowCounter":rowCounter,
    }

    let modal = await this.modalCtrl.create({
      component: TableColumnsPage,
      componentProps: data0
    });
    await modal.present();
    modal.onDidDismiss().then((data) => {
      if(data){
        let secCounter = Number(data.data["secCounter"]);
        let fieldCounter = Number(data.data["fieldCounter"]);
        let rowCounter = Number(data.data["rowCounter"]);
        this.content[secCounter].sectionContent[fieldCounter].rowContentValuesList[rowCounter] = data.data["rowElem"];
      }
    });

  }


  dismiss(isupdated,data?: any) {
    this.modalCtrl.dismiss(data,isupdated);
  }


}
