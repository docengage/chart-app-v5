import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { Handlebar } from 'src/app/components/handlebar';
import { NavParams, ModalController, LoadingController, AlertController } from '@ionic/angular';
import { TimeLineDataPushService } from 'src/app/service/timeline-auth-service';
import { LocalStorageService } from 'src/app/service/local-storage.service';
import { ToastServiceData } from 'src/app/service/toast-service';
import * as moment from "moment";

@Component({
  selector: 'app-prescription-template',
  templateUrl: './prescription-template.page.html',
  styleUrls: ['./prescription-template.page.scss'],
})
export class PrescriptionTemplatePage implements OnInit {

  categories: any = [];
  masterTemplates: any = [];
  templateMedication: any = [];
  medicineListArray: any = [];


  searchTerm: string = "";
  selectedCat: string = "";
  searchVal: string;
  timer: any;

  chartingPrescriptionId: string;
  plannedCareItemId: string;
  encounterId: string;
  uuid: string;

  currentPageClass = this;
  constructor(
    public modalCtrl: ModalController,
    public navParams: NavParams,
    private handlebar: Handlebar,
    public loadingCtrl: LoadingController,
    public authService: AuthService,
    private timeLineDataPushService: TimeLineDataPushService,
    private localstorage: LocalStorageService,
    public toastServ: ToastServiceData,
    private alertController: AlertController,
  ) {
    this.chartingPrescriptionId = this.navParams.get('chartingPrescriptionId');
    this.plannedCareItemId = this.navParams.get('plannedCareItemId');
    this.encounterId = this.navParams.get('encounterId');
    this.uuid = this.navParams.get('uuid');
  }

  ngOnInit() {
    this.getAllPreconditions();
    this.searchData("", "all");
  }

  filterByCategory() {
    if (this.selectedCat === 'Empty' || this.selectedCat === "clearSelection") {
      this.searchTerm = ""
      this.selectedCat = "all";
      this.searchData(this.searchTerm, this.selectedCat);
    } else if (!this.handlebar.isEmpty(this.selectedCat)) {
      this.searchData(this.searchTerm, this.selectedCat);
    }
  }

  getAllPreconditions() {
    this.authService.getAllPreconditions().then((data) => {
      this.categories = data;
    });
  }

  searchData(searchString, category) {
    if (this.handlebar.isEmpty(searchString)) {
      searchString = "";
    }

    if (this.handlebar.isEmpty(category)) {
      category = "all";
    }

    this.authService.getPrescriptionTemplates(searchString, category).then((data) => {
      this.masterTemplates = data;

    });
  }

  onItemClick(template) {
    if (template !== null) {
      this.authService.getPrescriptionMedication(template.template_id).then((response) => {
        if (response != null) {
          this.templateMedication = JSON.parse(response.presc_details.value);
          if (this.templateMedication.length > 0) {
            this.saveTemplateMedication(this.templateMedication);
          } else {
            this.templateMedication = []
          }
        }
      });
    }
  }

  async saveTemplateMedication(templateMedication) {
    if (templateMedication.length > 0) {
      for (let index = 0; index < templateMedication.length; index++) {

        let medication: any = []
        medication[0] = this.mapMedicationArray(this.templateMedication[index]);
        let data = {
          plannedId: this.plannedCareItemId,
          medicineList: medication,
          chartingPrescriptionId: this.chartingPrescriptionId,
          encounterId: this.encounterId,
        }

        await this.timeLineDataPushService.savePrescriptionData(this.uuid, data).then((result) => {
              this.saveToArray(result, this.templateMedication[index]);
        });
      }

      this.saveMedicineData();
    } else {
        this.toastServ.presentToast("No Medication Available In the Selected Template");
        this.dismiss(false, "") 
    }
  }

  saveToArray(data1: any, prescDetails: any) {
    let medicineId = data1[1];
    let prescriptionId = data1[0];
    let encounterId = data1[2];
    if (prescDetails) {
      let data = {
        medicine_name: prescDetails[0],
        strength: prescDetails[1],
        strength_unit: prescDetails[2],

        dosage_instruction: prescDetails[4],
        product_id: prescDetails[7],
        medication_id: medicineId,
        duration: prescDetails[13],
        duration_unit: prescDetails[14],
        m: prescDetails[15],
        n: prescDetails[16],
        e: prescDetails[17],
        suggestion: prescDetails[18],
        medication_start_date: prescDetails[5],
        prescriptionId: prescriptionId,
        encounterId: encounterId

      }
      let medicine = { "medication": data };
      this.medicineListArray.push(medicine);
    }
  }

  saveMedicineData() {
    if (this.medicineListArray.length > 0) {
      let prescriptionId = this.medicineListArray[0].medication.prescriptionId;
      let encounterId = this.medicineListArray[0].medication.encounterId;

      let dataArray = [];

      dataArray.push(prescriptionId);
      dataArray.push(this.medicineListArray);
      dataArray.push(encounterId);
      this.dismiss(true, dataArray);
    }
  }

  onSearchInput() {
    if (this.handlebar.isNotEmpty(this.searchTerm)) {
      if (this.searchVal != this.searchTerm) {
        clearTimeout(this.timer);
        let temp = this;
        this.timer = setTimeout(function () {
          temp.searchVal = temp.searchTerm;
          temp.searchData(temp.searchTerm, temp.selectedCat);
        }, 400);
      }
    } else {
      this.searchData('', "all");
    }
  }

  mapMedicationArray(prescDetails) {
    let item = prescDetails;
    let StartDate: any = "";
    let startFrom = prescDetails[20];
    if (startFrom == 0 || startFrom === 1) {
      StartDate = new Date();
    } else {
      StartDate = new Date(+new Date() + (86400000*(startFrom-1)));
    }
    let formattedStartDate = moment(StartDate).format('DD-MM-YYYY')
    let medication: any = [item[0], item[1], item[2],
      parseInt(item[7]), item[3], item[4], formattedStartDate, item[6], 'started', item[13], item[14], 0,
      item[15], item[16], item[17], item[18]]
    return medication;
  }

  dismiss(isUpdated, data) {
    if (this.handlebar.isEmpty(data)) {
      this.toastServ.presentToast("No Template Selected");
    } else {
      this.toastServ.presentToast("Products Selected Successfully");
    }

    this.modalCtrl.dismiss(data, isUpdated);
  }
}
