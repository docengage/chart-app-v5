import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrescriptionTemplatePageRoutingModule } from './prescription-template-routing.module';

import { PrescriptionTemplatePage } from './prescription-template.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PrescriptionTemplatePageRoutingModule
  ],
  declarations: [PrescriptionTemplatePage]
})
export class PrescriptionTemplatePageModule {}
