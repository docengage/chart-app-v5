import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrescriptionTemplatePage } from './prescription-template.page';

const routes: Routes = [
  {
    path: '',
    component: PrescriptionTemplatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrescriptionTemplatePageRoutingModule {}
