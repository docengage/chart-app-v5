import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnitSelectPage } from './unit-select.page';

const routes: Routes = [
  {
    path: '',
    component: UnitSelectPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UnitSelectPageRoutingModule {}
