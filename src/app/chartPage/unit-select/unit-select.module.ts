import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UnitSelectPageRoutingModule } from './unit-select-routing.module';

import { UnitSelectPage } from './unit-select.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UnitSelectPageRoutingModule
  ],
  declarations: [UnitSelectPage]
})
export class UnitSelectPageModule {}
