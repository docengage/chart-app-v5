import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UnitSelectPage } from './unit-select.page';

describe('UnitSelectPage', () => {
  let component: UnitSelectPage;
  let fixture: ComponentFixture<UnitSelectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitSelectPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UnitSelectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
