import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-unit-select',
  templateUrl: './unit-select.page.html',
  styleUrls: ['./unit-select.page.scss'],
})
export class UnitSelectPage{

  selectedValues: any;
  title: string;
  
  heightUnitFilters: Array<{value: any, label:string}> = [];
  weightUnitFilters: Array<{value: any, label:string}> = [];
  tempUnitFilters: Array<{value: any,label:string}> = [];
  weightUnitChecked: boolean = false;
  heightUnitChecked: boolean = false;
  tempUnitChecked: boolean = false;
  vitalConfig: any;
  measureGroupLabel: string;
  weightUnit: string;
  heightUnit: string;
  tempUnit: string;
  glucoseUnit: string;
  cholesterolUnit: string;
  showMetricSelection : boolean = false;
  glucoseUnitChecked: boolean = false;
  constructor(
    public navParams: NavParams,
    public modalCtrl: ModalController
  ) {
    // passed in array of track names that should be excluded (unchecked)
    this.selectedValues = this.navParams.data;
    this.weightUnit = this.selectedValues.weightUnit;
    this.heightUnit = this.selectedValues.heightUnit;
    this.tempUnit = this.selectedValues.tempUnit;
    this.glucoseUnit = this.selectedValues.glucoseUnit;
    this.cholesterolUnit = this.selectedValues.cholesterolUnit;
    this.vitalConfig = this.selectedValues.vitalConfig;
    this.measureGroupLabel = this.selectedValues.measureGroupLabel;
    
  }

  onSelectMertics(){
    if(this.heightUnitChecked && this.weightUnitChecked && this.tempUnitChecked 
    && this.glucoseUnitChecked){

      this.showMetricSelection = false;
      this.weightUnitChecked = false;
      this.heightUnitChecked = false;
      this.tempUnitChecked = false;
      this.glucoseUnitChecked = false;
    }
  }
      
  applyFilters() {
   
      if(this.weightUnitChecked || this.heightUnitChecked || this.tempUnitChecked ||
        this.glucoseUnitChecked  || this.cholesterolUnit){
        this.measureGroupLabel = this.weightUnit+", "+this.heightUnit+", "+this.tempUnit+", "+this.glucoseUnit+", "+this.cholesterolUnit;
      }
      let data = {
          "weightUnit" : this.weightUnit,
          "heightUnit" : this.heightUnit,
          "tempUnit" : this.tempUnit,
          "glucoseUnit" : this.glucoseUnit,
          "cholesterolUnit" : this.cholesterolUnit,
          "measureGroupLabel" : this.measureGroupLabel
      };
      this.dismiss(data);
  }

  dismiss(data?: any) {
    // using the injected ViewController this page
    // can "dismiss" itself and pass back data
    this.modalCtrl.dismiss(data);
  }
}
