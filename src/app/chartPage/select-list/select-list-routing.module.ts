import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectListPage } from './select-list.page';

const routes: Routes = [
  {
    path: '',
    component: SelectListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectListPageRoutingModule {}
