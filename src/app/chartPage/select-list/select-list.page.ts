import { Component } from '@angular/core';
import { ModalController, AlertController, NavParams, LoadingController } from '@ionic/angular';
import { Handlebar } from 'src/app/components/handlebar';
import { AuthService } from 'src/app/service/auth.service';
import { MasterDataService } from 'src/app/service/master-data';
import { TimeLineDataPushService } from 'src/app/service/timeline-auth-service';
import { ToastServiceData } from 'src/app/service/toast-service';

@Component({
  selector: 'app-select-list',
  templateUrl: './select-list.page.html',
  styleUrls: ['./select-list.page.scss'],
})
export class SelectListPage {

  apptFilters: any = [];
  masterFilters: any = [];
  selectedFilters: any = [];
  

  labApptFilters: Array<{lab_sub_category_id: number,order_id:number, lab_test_item_id:number,
  lab_component_item_id:number,isEditable:boolean,isChecked: boolean,label:string}> = [];
  
  deleteOrderIds : string[];
  selectedValues: any;
  title: string;
  showRecords : boolean = false;
  searchTerm : string;
  encounterId : number = 0;
  uuid : string;
  selectedId : string;
  plannedCareItemId : string;
  searchVal : string;
  timer : any;
  enableCustomField : boolean= false;
  currentPageClass = this;
  diagnosisList : any = [];
  triggerAlphaScrollChange: number = 0;
  contentLoading = false;
  diagnosisScroll = true;
  pageno: any;
  suggPlanScroll = true;
  totalSuggPlans = 0;
  totalOrder = 0;
  infiniteScroll = false;
  isNoRecord = false;
  constructor(
    public masterData: MasterDataService,
    public navParams: NavParams,
    public viewCtrl: ModalController,
    public authService : AuthService,
    private alertCtrl: AlertController,
    private timeLineServ : TimeLineDataPushService,
    private tost: ToastServiceData,
    public handlebar: Handlebar,
    public loadingCtrl : LoadingController
  ) {
    // passed in array of track names that should be excluded (unchecked)
    
    this.contentLoading = true;
    this.title = this.navParams.get("key");
    if(this.title==="Radiology" || this.title ==="Pathology" || this.title === "Neurology"){
      this.selectedFilters = this.navParams.get("values");
      this.deleteOrderIds = this.navParams.get("deletArray");
    }else{
        this.selectedValues = this.navParams.get("values");
    }
    if(!this.deleteOrderIds){
      this.deleteOrderIds = [];
    }
    if(this.title==="Allergies"){
      this.enableCustomField = true;
      this.setAllergiesFilterValues();
    }else if(this.title==="Diagnosis"){
      this.enableCustomField = true;
      this.setDiagnosisFilterValues();
    }else if(this.title==="Sugg. Plans"){
      this.setPlansFilterValues('$', 1);
    }else if(this.title==="Radiology" || this.title =="Pathology" || this.title === "Neurology"){
      this.setLabOrderFilterValues(this.title,1);
    }
    this.plannedCareItemId = this.navParams.get('plannedCareItemId');
    this.encounterId =  this.navParams.get('encounterId');
    this.uuid =  this.navParams.get('uuid');
    this.selectedId = this.navParams.get('selectedId');
  }

  setLabOrderFilterValues(type?:string,pageno?: number,searchValue?:any){
    let orderFilters = [];
    this.pageno = pageno;
    this.authService.getLabOrdersMasterData(type,pageno,searchValue).then((data)=>{
        let listFilters = data.testComponents;
        listFilters.forEach(ele => {
          
          orderFilters.push({
            lab_sub_category_id : ele.labSubCategoryId,
            order_id : ele.orderId, 
            lab_test_item_id : 0,
            lab_component_item_id : 0,
            isEditable : false,
            isChecked : false,
            label :  ele.orderDescription
          });
        });

        this.diagnosisList = orderFilters;
        if (this.diagnosisList && this.diagnosisList.length == 0) {
          this.isNoRecord = true;
        } else {
          this.isNoRecord = false;
        }

        if ((orderFilters.length === this.totalOrder) || orderFilters.length < 15) {
          this.infiniteScroll = false;
        } else {
          this.infiniteScroll = true;
        }
        this.contentLoading = false;
        this.totalOrder = orderFilters.length;
    });
  }
 
  setAllergiesFilterValues(){
    
    if(this.selectedValues != undefined){
      let selectArry: string[] = this.selectedValues.split(",");
      selectArry.forEach(ele =>{
          this.selectedFilters.push({
            value: ele,
            id: ele,
            isChecked: true,
            label: ele.trim()
          });
       
      });
    }
    this.getAllergiesMasterData("$");
  }
  getAllergiesMasterData(searchVal){
    let allergiesFilters = [];
    this.authService.getAllergiesMasterData(searchVal).then((data)=>{
      let listFilters = data.allergies;
       
         listFilters.forEach(ele => {
          allergiesFilters.push({
             value: ele.description,
             id: ele.description,
             isChecked: (this.selectedValues != undefined && this.selectedValues.indexOf(ele.description) !== -1),
             label: ele.description.trim()
           });
         });
         this.diagnosisList = allergiesFilters;
         this.contentLoading = false;

         if (this.diagnosisList && this.diagnosisList.length == 0) {
          this.isNoRecord = true;
        } else {
          this.isNoRecord = false;
        }
      });
  }
  setSuggSelValues(){
    if(this.selectedValues){
      this.selectedValues.forEach(ele =>{
        this.selectedFilters.push({
          value: {
            "id":ele.id,
            "value":ele.value
          },
          id:ele.id,
          isChecked: true,
          label: ele.value.trim()
        });
      });
    }
    
  }
  setPlansFilterValues(searchVal, pageno){
    let suggplansFilters = [];
    this.pageno = pageno;
    this.authService.getSuggestedPlansMasterData(searchVal, pageno).then((data)=>{
        let listFilters = data.suggestedPlans;
        let selectIds : string = "";
        if(this.selectedValues){
          this.selectedValues.forEach(ele =>{
            if(selectIds){
              selectIds += ",";
            }
            selectIds += ele["id"];
          });
        }
        listFilters.forEach(ele => {
            if(selectIds != undefined && selectIds.indexOf(ele.serviceId) !== -1){
              this.selectedFilters.push({
                value: {
                  "id":ele.serviceId,
                  "value":ele.serviceName
                },
                id:ele.serviceId,
                isChecked: true,
                label: ele.serviceName.trim()
              });
          }

          suggplansFilters.push({
            value: {
              "id":ele.serviceId,
              "value":ele.serviceName
            },
            id:ele.serviceId,
            isChecked: (selectIds != undefined && selectIds.indexOf(ele.serviceId) !== -1),
            label: ele.serviceName
          });         
        });
        this.diagnosisList = suggplansFilters;
        if ((suggplansFilters.length === this.totalSuggPlans) || suggplansFilters.length < 15) {
          this.infiniteScroll = false;
        } else {
          this.infiniteScroll = true;
        }
        this.contentLoading = false;
        this.totalSuggPlans = suggplansFilters.length;

        if (this.diagnosisList && this.diagnosisList.length == 0) {
          this.isNoRecord = true;
        } else {
          this.isNoRecord = false;
        }
      });
  }

  setDiagnosisFilterValues(){
    
    if(this.selectedValues != undefined){
      let selectArry: string[] = this.selectedValues.split(",");
      selectArry.forEach(ele =>{
          this.selectedFilters.push({
            value: ele,
            id: ele,
            isChecked: true,
            label: ele
          });
       
      })
    }
    this.getDiagnosisMasterDataWithLoader('all', 1);
  }

  getDiagnosisData(searchVal, pageno, loading?) {
    this.authService.getDiagnosisMasterData(searchVal, pageno).then((data)=>{
      if (data && data.diagnosis && data.diagnosis.length !== 0){
        for (let i = 0; i < data.diagnosis.length; i = i + 1) {
          data.diagnosis[i].value = data.diagnosis[i].description;
          data.diagnosis[i].id = data.diagnosis[i].description;
          data.diagnosis[i].isChecked = (this.selectedValues != undefined && this.selectedValues.indexOf(data.diagnosis[i].description) !== -1),
          data.diagnosis[i].label = data.diagnosis[i].description;
          this.diagnosisList.push(data.diagnosis[i]);
        }
        this.contentLoading = false;
        if (data.diagnosis.length < 15) {
          this.infiniteScroll = false;
        } else {
          this.infiniteScroll = true;
        }
      } else {
        this.infiniteScroll = false;
      }

      if (this.diagnosisList && this.diagnosisList.length == 0) {
        this.isNoRecord = true;
      } else {
        this.isNoRecord = false;
      }

      if (this.contentLoading === false && loading) {
        loading.dismiss();
      }
    });
  }

  async getDiagnosisMasterDataWithLoader(searchVal, pageno){
    if(searchVal == ""){
      searchVal = "all";
    }

    this.pageno = pageno;
    let loading = await this.loadingCtrl.create({
      message: 'loading...'
    });
    
    if (this.contentLoading === false) {
      await loading.present();
    }

    this.getDiagnosisData(searchVal, pageno, loading);
  }

  getDiagnosisMasterData(searchVal, pageno){
    if(searchVal == ""){
      searchVal = "all";
    }
    this.pageno = pageno;

    this.getDiagnosisData(searchVal, pageno);
  }
  
  doInfinite(event) {
    var clientHeight = event.target.clientHeight;
    var scrollUpto = event.target.scrollHeight - event.target.scrollTop;
    if (clientHeight === scrollUpto) {
      this.pageno = Number(this.pageno) + 1;
      setTimeout(() => {
        setTimeout(() => {
          if(this.title==="Radiology" || this.title ==="Pathology" || this.title === "Neurology"){
            this.setLabOrderFilterValues(this.title,this.pageno);
          } else if(this.title === "Sugg. Plans"){
            this.setPlansFilterValues('$', this.pageno);
          } else if (this.title === "Diagnosis"){
            this.getDiagnosisMasterData('all', this.pageno);
          } 
          
          event.target.complete();
        }, 500);
      });
    } 
  }
  
  resetFilters() {
   this.selectedFilters = [];
   this.showRecords = false;
   this.apptFilters = [];
   this.labApptFilters = [];
   this.masterFilters = [];
   
   this.onSearchInput();

  }
  pushToSelectList(item){
    if(item.value){
      this.selectedFilters.push(item);
    }
  }
  pushToDiagnosisiSelectList(item) {
    let isSelectedEle: boolean = false;
    let selectedEles = this.selectedFilters;
    if(this.title === "Radiology" || this.title ==="Pathology" || this.title === "Neurology"){
      for (let pathArray = 0; pathArray < selectedEles.length; pathArray++) {
        if(selectedEles[pathArray].order_id === item.order_id){
          isSelectedEle = true;
          break;
        }
      }
    } else if (this.title==="Diagnosis") {
      for (let pathArray = 0; pathArray < selectedEles.length; pathArray++) {
        if((selectedEles[pathArray].id).trim() === (item.id).trim()) {
          isSelectedEle = true;
          break;
        }
      }
    } else {
      for (let pathArray = 0; pathArray < selectedEles.length; pathArray++) {
        if(selectedEles[pathArray].id === item.id) {
          isSelectedEle = true;
          break;
        }
      }
    }
    
    if(!isSelectedEle){
      this.selectedFilters.push(item);
    }
  }
  
  unselectLabItem(orderId,testId,isEditable){
    if(isEditable){
      this.deleteOrderIds.push(testId);
      this.selectedFilters = this.selectedFilters.filter(item => item.lab_component_item_id !== testId);
    }else{
      this.selectedFilters = this.selectedFilters.filter(item => item.order_id !== orderId);
    }
    
  }
  unselectItem(id){
    this.selectedFilters = this.selectedFilters.filter(item => item.id !== id);
  }
  
  onSearchInput(){

    if(this.searchTerm){
      clearTimeout(this.timer);
      if (this.searchVal != this.searchTerm) {
        
        let temp = this;
        this.timer = setTimeout(function() {
          temp.searchVal = temp.searchTerm;
          temp.showRecords = true;
          temp.apptFilters = [];
          temp.labApptFilters = [];

          if(temp.title==="Radiology" || temp.title ==="Pathology" || temp.title === "Neurology"){
            temp.totalOrder = 0;
            temp.setLabOrderFilterValues(temp.title,1,temp.searchVal);
          }else{
            if(temp.title==="Sugg. Plans"){
              temp.totalSuggPlans = 0 ;
              temp.setPlansFilterValues(temp.searchVal, 1);
            }else if(temp.title==="Allergies"){
              temp.getAllergiesMasterData(temp.searchVal);
            }else if(temp.title==="Diagnosis"){
              temp.diagnosisList =[];
              temp.getDiagnosisMasterData(temp.searchVal, 1);
            }else{
              temp.masterFilters.filter((item) => {
                if(item.label.toLowerCase().indexOf(temp.searchTerm.toLowerCase()) > -1){
                  temp.apptFilters.push(item);
                }
              });
            }
          }
        },400);
      }
    }else{   
      this.apptFilters = [];
      this.labApptFilters = [];
      this.showRecords = false;
      if(this.title==="Radiology" || this.title ==="Pathology" || this.title === "Neurology"){
        this.totalOrder = 0;
        this.setLabOrderFilterValues(this.title,1);
      }else{
        if(this.title==="Sugg. Plans"){
          this.totalSuggPlans = 0;
          this.setPlansFilterValues('$', 1);
        }else if(this.title==="Allergies"){
          this.getAllergiesMasterData('$');
        }else if(this.title==="Diagnosis"){
          this.diagnosisList =[];
          this.getDiagnosisMasterData('all', 1);
        }
    }
  }
  }

  dismiss(isupdated,data?: any) {
    // using the injected ViewController this page
    // can "dismiss" itself and pass back data
    this.viewCtrl.dismiss(data,isupdated);
  }
  applyFilters() {
    // Pass back a new array of track names to exclude
    let selectedEleLabels:string[];
    
    let selectedEleValues : any;
    if(this.title==="Radiology" || this.title ==="Pathology" || this.title === "Neurology"){
      selectedEleValues = this.selectedFilters.map(c => c);
      selectedEleLabels = this.selectedFilters.map(c => c.label);
    }else{
      selectedEleValues = this.selectedFilters.map(c => c.value);
      selectedEleLabels = this.selectedFilters.map(c => c.label);
    }
    if(this.selectedValues != undefined && selectedEleValues != undefined && 
      (this.selectedValues.toString() === selectedEleValues.toString())){
      this.dismiss(false);
    }else{
      
      if(this.title==="Radiology"){
        this.saveRadiologyData(selectedEleValues,selectedEleLabels);
      }else if(this.title ==="Pathology"){
        this.savePathologyData(selectedEleValues,selectedEleLabels);
      }else if(this.title ==="Neurology"){
        this.saveNeurologyData(selectedEleValues,selectedEleLabels);
      }
      else if(this.title === "Allergies"){
        this.saveAllergies(selectedEleValues,selectedEleLabels);
      }else if(this.title==="Diagnosis"){
        this.saveDiagnosis(selectedEleValues,selectedEleLabels);
      }else if(this.title==="Sugg. Plans"){
        this.saveSuggestedPlan(selectedEleValues,selectedEleLabels);
      }
    }
    
  }
  saveAllergies(selectedEleValues,selectedEleLabels){
    let data ={
      chartingallergies : selectedEleValues.toString()
    }
    this.timeLineServ.saveAllergiesData(this.uuid,data).then(()=>{
      
        let data1 = {
          "labels" : selectedEleLabels,
          "values" : selectedEleValues
        }
        this.dismiss(true,data1);
    
    });
  }
  pushData(data,selectedEleLabels,selectedEleValues,type){

    this.timeLineServ.pushChartData(this.uuid,data,type).then((result)=>{
      
        let data1 = {
          "labels" : selectedEleLabels,
          "values" : selectedEleValues,
          "returnObj" : result
        }
        this.dismiss(true,data1);
    
    });

  }
  saveDiagnosis(selectedEleValues,selectedEleLabels){
    let data ={
      plannedId : this.plannedCareItemId,
      chartingDiagnosis : selectedEleValues.toString(),
      encounterId : this.encounterId
    }
    this.pushData(data,selectedEleLabels,selectedEleValues,"diagnosis");
  }
  

  saveSuggestedPlan(selectedEleValues,selectedEleLabels){
    let data ={
      plannedId : this.plannedCareItemId,
      chartingSPlan : selectedEleValues,
      hasNoteData : false,
      hasPrescriptionData : false,
      hasOrderData : false,
      encounterId : this.encounterId
    }
  
    this.pushData(data,selectedEleLabels,selectedEleValues,"suggPlan");
  }
  saveRadiologyData(selectedEleValues,selectedEleLabels){

    let orderList : Array<{}> = [];
    let radilogyOrders = selectedEleValues;
    let pathArrayList = new Array();
    for (let pathArray = 0; pathArray < radilogyOrders.length; pathArray++) {
      let pData = radilogyOrders[pathArray];
      let testTempId = pData["lab_sub_category_id"]; 
      let compTempId = pData["order_id"];                   
      let testId = pData["lab_test_item_id"];  
      let compId = pData["lab_component_item_id"];      
      pathArrayList[pathArray] = [testTempId,compTempId,testId,compId];
     
    }

    if(radilogyOrders.length > 0){
      let radiologyArray = [];
      
      radiologyArray.push("Radiology");
      radiologyArray.push(this.selectedId);
      radiologyArray.push(pathArrayList);

      orderList.push(radiologyArray);
    }

    let data ={
      plannedId : this.plannedCareItemId,
      hasNoteData : false,
      hasPrescriptionData : false,
      hasOrderData : true,
      orderList : orderList,
      encounterId : this.encounterId,
      deletedRadiologyData : this.deleteOrderIds,
      radiologyInvestigationId: this.selectedId
    }
   
    this.pushData(data,selectedEleLabels,selectedEleValues,"orders");
  }

  savePathologyData(selectedEleValues,selectedEleLabels){
    let orderList : Array<{}> = [];
    let pathologyOrders = selectedEleValues;
    let pathArrayList = new Array();
    for (let pathArray = 0; pathArray < pathologyOrders.length; pathArray++) {
      let pData = pathologyOrders[pathArray];
      let testTempId = pData["lab_sub_category_id"]; 
      let compTempId = pData["order_id"];                   
      let testId = pData["lab_test_item_id"];  
      let compId = pData["lab_component_item_id"];      
      pathArrayList[pathArray] = [testTempId,compTempId,testId,compId];
      
    }

    let pathologyArray = [];
    if(pathologyOrders.length > 0){
      pathologyArray.push("Pathology");
      pathologyArray.push(this.selectedId);
      pathologyArray.push(pathArrayList);
      orderList.push(pathologyArray);
    }

    let data ={
      plannedId : this.plannedCareItemId,
      hasNoteData : false,
      hasPrescriptionData : false,
      hasOrderData : true,
      orderList : orderList,
      encounterId : this.encounterId,
      deletedPathologyData : this.deleteOrderIds,
      pathologyInvestigationId: this.selectedId
    };
    this.pushData(data, selectedEleLabels, selectedEleValues,"orders");
  }
  saveNeurologyData(selectedEleValues,selectedEleLabels){
    let orderList : Array<{}> = [];
    let neurologyOrders = selectedEleValues;
    let pathArrayList = new Array();
    for (let pathArray = 0; pathArray < neurologyOrders.length; pathArray++) {
      let pData = neurologyOrders[pathArray];
      let testTempId = pData["lab_sub_category_id"]; 
      let compTempId = pData["order_id"];                   
      let testId = pData["lab_test_item_id"];  
      let compId = pData["lab_component_item_id"];      
      pathArrayList[pathArray] = [testTempId,compTempId,testId,compId];
      
    }

    let neurologyArray = [];
    if(neurologyOrders.length > 0){
      neurologyArray.push("Neurology");
      neurologyArray.push(this.selectedId);
      neurologyArray.push(pathArrayList);
      orderList.push(neurologyArray);
    }

    let data ={
      plannedId : this.plannedCareItemId,
      hasNoteData : false,
      hasPrescriptionData : false,
      hasOrderData : true,
      orderList : orderList,
      encounterId : this.encounterId,
      deletedPathologyData : this.deleteOrderIds,
      pathologyInvestigationId: this.selectedId
    };
    this.pushData(data, selectedEleLabels, selectedEleValues,"orders");
  }
  async addCustomInput(){
    if(this.title==="Allergies" || this.title === "Diagnosis"){
      let prompt = await this.alertCtrl.create({
        message: "Custom "+this.title,
        inputs: [
          {
            name: 'title',
            placeholder: ' '
          },
        ],
        buttons: [
          {
            text: 'Cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Save',
            handler: data => {
              if(data.title){
                let item = {value: data.title,id:0, isChecked: true,label:data.title}
                this.pushToSelectList(item);
              } else {
                this.tost.presentToast('Blank value cannot be  saved..!');
              }
            }
          }
        ]
      });
      await prompt.present();

    }
  }


}
