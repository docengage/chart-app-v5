import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectListPage } from './select-list.page';

describe('SelectListPage', () => {
  let component: SelectListPage;
  let fixture: ComponentFixture<SelectListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
