import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ClinicalNotePageRoutingModule } from './clinical-note-routing.module';

import { ClinicalNotePage } from './clinical-note.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClinicalNotePageRoutingModule
  ],
  declarations: [ClinicalNotePage]
})
export class ClinicalNotePageModule {}
