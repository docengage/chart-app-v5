import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClinicalNotePage } from './clinical-note.page';

const routes: Routes = [
  {
    path: '',
    component: ClinicalNotePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClinicalNotePageRoutingModule {}
