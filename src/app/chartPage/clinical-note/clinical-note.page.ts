import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { TimeLineDataPushService } from 'src/app/service/timeline-auth-service';
import { SelectNoteTemplateComponent } from 'src/app/select-note-template/select-note-template.component';
import { Handlebar } from 'src/app/components/handlebar';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-clinical-note',
  templateUrl: './clinical-note.page.html',
  styleUrls: ['./clinical-note.page.scss'],
})
export class ClinicalNotePage {

  clinicalNote: any;
  item: string;
  plannedCareItemId: string;
  encounterId: string;
  uuid: string;
  noteTemplates: any = [];
  searchTerm: any;
  searchVal: any;
  timer: any;
  searchBarShow = false;
  notetimer: any;
  note: any;
  constructor(public viewCtrl: ModalController,
              public navParam: NavParams,
              private timelineAuthService: TimeLineDataPushService,
              private modalCtrl: ModalController,
              public authService : AuthService,
              private handlebar:Handlebar
              ) {
    this.clinicalNote = this.navParam.get('ClinicalNote');
    this.plannedCareItemId = this.navParam.get('plannedCareItemId');
    this.encounterId =  this.navParam.get('encounterId');
    this.uuid =  this.navParam.get('uuid');
    if (this.clinicalNote && this.clinicalNote.clinical_notes){

      this.replaceItem(this.clinicalNote.clinical_notes);
    }
    this.searchNotesData('');
  }

  saveNote() {

    let progressNoteId = 0;
    let generalNotes = "";
    let noteTitle = "";
    if (this.clinicalNote){
      progressNoteId = this.clinicalNote["progress_note_id"];
      noteTitle = this.clinicalNote["note_title"];
    }
    generalNotes = this.item;

    let data = {
      plannedId : this.plannedCareItemId,
      hasNoteData : true,
      encounterId : this.encounterId,
      chartingProgressNoteId : progressNoteId,
      generalNotes : generalNotes,
      noteTitle : noteTitle
    }
    this.timelineAuthService.saveClinicalNoteData(this.uuid, data).then((retrunVal) => {
      let returnData = {
          "returnJson" : retrunVal,
          "note" : generalNotes
        };
      this.viewCtrl.dismiss(returnData);
      });

  }
  dismiss(){
    this.viewCtrl.dismiss();
  }
  ionViewWillLoad() {
    if (this.clinicalNote && this.clinicalNote.clinical_notes){

      this.replaceItem(this.clinicalNote.clinical_notes);
    }
  }

  async navigateToSelectModel(){
    let data0 = {}
    let modal = await this.modalCtrl.create({
        component: SelectNoteTemplateComponent,
        componentProps: data0
      });
    await modal.present();
    modal.onDidDismiss().then((data) => {
        if (data.data) {
          this.replaceItem(data.data.selectedValue);
        }
      });
  }

  replaceItem(item: String) {
    item = item ? String(item).replace(/<[^>]+>/gm, '') : '';
    item = item ? String(item).replace(/&nbsp;/g, ' ') : '';
    if (!this.item){
      this.item = "";
    }
    this.item += item;
  }

  clearNote() {
    this.item = "";
  }

  onSearchInput() {
    if (this.searchTerm) {
      if (this.searchVal != this.searchTerm) {
        clearTimeout(this.timer);
        let temp = this;
        this.timer = setTimeout(function() {
          temp.searchVal = temp.searchTerm;
          temp.searchNotesData(temp.searchTerm);
        }, 400);
      }
    } else {
      this.searchNotesData('$');
    }
  }
  
  searchNotesData(itemname){
    if (this.handlebar.isEmpty(itemname)) {
      itemname = "$";
    }

    this.authService.getNoteTemplateMasterData("Clinical Note",itemname).then((data) => {
      this.noteTemplates = data.templates;
      // this.contentLoading = false;
    });
  }

  onItemClick(noteData) {
    clearTimeout(this.notetimer);
    if (this.note != noteData) {
      let temp = this;
      this.notetimer = setTimeout(function () {
        if (noteData && noteData.template) {
          temp.replaceItem(noteData.template);
        } else {
          temp.item = "";
        }
      }, 400);
    }
    
}

}
