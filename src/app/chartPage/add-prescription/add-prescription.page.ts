import { Component, OnInit, ViewChild } from "@angular/core";
import { MasterDataService } from "src/app/service/master-data";
import {
  NavParams,
  ModalController,
  AlertController,
  IonDatetime,
} from "@ionic/angular";
import { AuthService } from "src/app/service/auth.service";
import { TimeLineDataPushService } from "src/app/service/timeline-auth-service";
import { ToastServiceData } from "src/app/service/toast-service";
import * as moment from "moment";
import { SelectMedicinePage } from "../select-medicine/select-medicine.page";
import { LocalStorageService } from "src/app/service/local-storage.service";
import { async } from "rxjs/internal/scheduler/async";
import { Handlebar } from "src/app/components/handlebar";

@Component({
  selector: "app-add-prescription",
  templateUrl: "./add-prescription.page.html",
  styleUrls: ["./add-prescription.page.scss"],
})
export class AddPrescriptionPage implements OnInit {
  medicationsMasterList: any;
  strength: string;
  strengthUnit: string = "mg";
  productId: number;
  productName: string;
  startDate: string = new Date().toISOString();
  duration: number = 1;
  dosage: string;
  medicationNote: string = "";
  dosageInstruction: string = "";
  suggestion: string = "AF";
  durationUnit: string = "d";
  dosageM: number;
  dosageN: number;
  dosageE: number;
  categoryType: number = 1;
  chartingPrescriptionId: string;
  plannedCareItemId: string;
  encounterId: string;
  uuid: string;
  isMCI: boolean = false;
  quantity: number;
  isCustomMedicine: boolean = false;
  minDate: string = new Date().toISOString();
  medicineListArray: any = [];

  dateValue = moment().format("DD-MM-YYYY");

  @ViewChild(IonDatetime) datetime: IonDatetime;
  showModal = true;
  patientName: any;

  constructor(
    public masterData: MasterDataService,
    public navParams: NavParams,
    public authService: AuthService,
    private timeLineDataPushService: TimeLineDataPushService,
    public toastServ: ToastServiceData,
    private modalCtrl: ModalController,
    private localstorage: LocalStorageService,
    private alertController: AlertController,
    private handlebar: Handlebar
  ) {
    this.chartingPrescriptionId = this.navParams.get("chartingPrescriptionId");
    this.plannedCareItemId = this.navParams.get("plannedCareItemId");
    this.encounterId = this.navParams.get("encounterId");
    this.uuid = this.navParams.get("uuid");
    this.patientName = this.navParams.get("patientName");
  }
  ngOnInit(): void {
    this.authService.isTokenValid().then((returnVal) => {
      this.authService.getOrgPreferences().then((pref) => {
        this.isMCI = pref["isMCIFormat"];
      });
      this.getPrescriptionMasterData();
    });
  }

  dateChange(value) {
    if (this.handlebar.isEmpty(value) || value === undefined) {
      value = moment().format();
    }
    this.startDate = value;
    this.dateValue = moment(value).format("DD-MM-YYYY");
    this.dateCancel();
  }

  dateCancel() {
    if (this.datetime) {
      this.datetime.cancel(true);
    }
  }

  getPrescriptionMasterData() {
    this.authService.getMedicationMasterData("$", 0, false).then((data) => {
      this.medicationsMasterList = data.medicines;
    });
  }

  onSelectMedicine(medicine) {
    if (medicine) {
      this.strength = medicine.strength;
      this.strengthUnit = medicine.strengthUnit;
      this.suggestion = medicine.suggestion;
      this.productName = medicine.productName;
      this.dosageM = medicine.morning;
      this.dosageN = medicine.noon;
      this.dosageE = medicine.evening;
      this.dosage = this.dosageM + "-" + this.dosageN + "-" + this.dosageE;
      this.categoryType = medicine.categoryType;
      if (medicine.description !== null && medicine.description !== undefined) {
        this.dosageInstruction = medicine.description;
      } else {
        this.dosageInstruction = "";
      }
    } else {
      this.dosageM = this.dosageN = this.dosageE = 0;
      this.dosage = "0-0-0";
      this.productName =
        this.suggestion =
        this.strengthUnit =
        this.strength =
        this.dosageInstruction =
          "";
    }
  }
  getMedicationList() {
    let medicineList = [];
    let chartMediStart = moment(this.startDate).format("DD-MM-YYYY");
    let chartMediEnd = moment().format("DD-MM-YYYY");

    let chartingSaveErr: boolean = false;
    if (!this.productName) {
      chartingSaveErr = true;
    } else {
      this.productName = this.productName.replace("'", "").replace("'", "");
    }
    if (this.isCustomMedicine && !chartingSaveErr && !this.strengthUnit) {
      chartingSaveErr = true;
    }
    if (!chartingSaveErr && !chartMediStart) {
      chartingSaveErr = true;
    }

    if (this.categoryType < 2) {
      if (!chartingSaveErr &&  Math.ceil(this.duration) > 0) {
        this.duration =
          Math.ceil(this.duration) > 0 ? Math.ceil(this.duration) : 1;
        let chartMediStartMoment = moment(chartMediStart, "DD-MM-YYYY");
        if (this.durationUnit === "w") {
          chartMediEnd = chartMediStartMoment
            .add(this.duration * 7, "day")
            .format("DD-MM-YYYY");
        } else if (this.durationUnit === "m") {
          chartMediEnd = chartMediStartMoment
            .add(this.duration, "month")
            .format("DD-MM-YYYY");
        } else if (this.durationUnit === "y") {
          chartMediEnd = chartMediStartMoment
            .add(this.duration, "year")
            .format("DD-MM-YYYY");
        } else {
          chartMediEnd = chartMediStartMoment
            .add(this.duration, "day")
            .format("DD-MM-YYYY");
        }
      } else {
        chartingSaveErr = true;
      }
    }
    
    if (!this.isMCI && this.categoryType < 2) {
      if (!this.dosage.split("-")[0]) {
        this.dosageM = 0;
      } else {
        this.dosageM = Number(this.dosage.split("-")[0]);
      }
      if (!this.dosage.split("-")[1]) {
        this.dosageN = 0;
      } else {
        this.dosageN = Number(this.dosage.split("-")[1]);
      }
      if (!this.dosage.split("-")[2]) {
        this.dosageE = 0;
      } else {
        this.dosageE = Number(this.dosage.split("-")[2]);
      }
      let tQ = this.dosageM + this.dosageN + this.dosageE;

      if (Number(tQ) > 0 && Number(this.duration) > 0) {
        let dU = 1;
        if (this.durationUnit === "w") {
          dU = this.duration * 7;
        } else if (this.durationUnit === "m") {
          dU = this.duration * 30;
        } else if (this.durationUnit === "y") {
          dU = this.duration * 365;
        } else {
          dU = this.duration;
        }
        this.quantity = tQ * dU;
      } else {
        this.quantity = 0;
      }
    } else {
      this.dosageM = 0;
      this.dosageN = 0;
      this.dosageE = 0;
      this.suggestion = "";
      if (!this.quantity) {
        chartingSaveErr = true;
      }
    }

    if (chartingSaveErr) {
      this.toastServ.presentToast("Please fill mandatory fields");
      return medicineList;
    } else {
      let isComposite = false;
      if (!isComposite) {
        isComposite = false;
      }

      if (this.productName != "" || this.productName != null) {
        if (this.categoryType < 2) {
          medicineList[0] = [
            this.productName,
            this.strength,
            this.strengthUnit,
            this.productId,
            this.quantity,
            this.dosageInstruction,
            chartMediStart,
            chartMediEnd,
            "Started",
            this.duration,
            this.durationUnit,
            0,
            this.dosageM,
            this.dosageN,
            this.dosageE,
            this.suggestion,
          ];
        } else {
          medicineList[0] = [
            this.productName,
            this.strength,
            this.strengthUnit,
            this.productId,
            this.quantity,
            this.dosageInstruction,
            chartMediStart,
            chartMediEnd,
            "Started",
            0,
            '',
            0,
            0,
            0,
            0,
            '',
          ];
        }
      }
      return medicineList;
    }
  }

  async navigateToSelectModel() {
    let data0 = {
      selectedId: this.productId,
    };
    let modal = await this.modalCtrl.create({
      component: SelectMedicinePage,
      componentProps: data0,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: "chartModelStyle",
    });
    await modal.present();

    var chartPage = document.getElementsByTagName("app-add-prescription");
    chartPage[0].classList.add("disableClick");

    modal.onDidDismiss().then((data) => {
      chartPage[0].classList.remove("disableClick");
      let medicine: any;
      if (data) {
        this.productId = Number(data.data["selectedId"]);
        medicine = data.data["medicine"];
      } else {
        this.productId = 0;
      }
      this.onSelectMedicine(medicine);
    });
  }

  saveChartAndAddMoreData(isAddCustomMedicine: boolean) {
    this.saveMedicine(true, isAddCustomMedicine);
  }

  validateMedicineData(): boolean {
    if (this.handlebar.isEmpty(this.productName)) {
      this.toastServ.presentToast("Medicine cannot be empty");
      return false;
    } else {
      if (this.productName.length > 130) {
        this.toastServ.presentToast("Medicine name is too long");
        return false;
      }
    }
    if (this.handlebar.isNotEmpty(this.strength)) {
      if (this.strength.length > 15) {
        this.toastServ.presentToast("Strength is too long");
        return false;
      }
    } else {
      this.toastServ.presentToast("Strength cannot be empty");
      return false;
    }

    if (this.handlebar.isEmpty(this.duration) && (this.categoryType < 2)) {
      this.toastServ.presentToast("Please add duration.");
      return false;
    }
    if (this.handlebar.isEmpty(this.startDate)) {
      this.toastServ.presentToast("Please add start date.");
      return false;
    }
    return true;
  }

  saveChartData() {
    const isValid = this.validateMedicineData();
    if (isValid) {
      this.saveMedicine(false, false);
    }
  }
  saveMedicine(isAddMore: boolean, isAddCustomMedicine: boolean) {
    let medications = this.getMedicationList();
    if (medications.length > 0) {
      let data = {
        plannedId: this.plannedCareItemId,
        medicineList: medications,
        chartingPrescriptionId: this.chartingPrescriptionId,
        encounterId: this.encounterId,
      };
      this.timeLineDataPushService
        .savePrescriptionData(this.uuid, data)
        .then((result) => {
          this.localstorage.getIsOnline().then(async (isOnline) => {
            if (isOnline) {
              this.toastServ.presentToast(this.productName + " Saved!" + " for " + this.patientName);
              this.saveToArray(result, isAddMore, isAddCustomMedicine);
            } else {
              const alert = await this.alertController.create({
                header: "Alert",
                subHeader: "You are doing a offline post",
                message:
                  "Your request is cached, once internet is back it will be processed automatically.",
                buttons: ["OK"],
              });

              await alert.present();
              this.dismiss(false);
            }
          });
        });
    }
  }
  clearData(isAddCustomMedicine: boolean, data1?: any) {
    this.chartingPrescriptionId = data1[0];
    this.plannedCareItemId = data1[1];
    this.encounterId = data1[2];

    this.strength = "";
    this.strengthUnit = "";
    this.productId = 0;
    this.productName = "";
    this.startDate = new Date().toISOString();
    this.dateValue = moment().format("DD-MM-YYYY");
    this.duration = 0;
    this.medicationNote = "";
    this.dosageInstruction = "";
    this.suggestion = "AF";
    this.durationUnit = "d";
    this.dosageM = 0;
    this.dosageN = 0;
    this.dosageE = 0;
    this.quantity = 0;
    this.dosage = "";
    this.dateValue = '';
    this.isCustomMedicine = isAddCustomMedicine;
  }
  saveToArray(data1: any, isAddMore: boolean, isAddCustomMedicine: boolean) {
    let medicineId = data1[1];
    let prescriptionId = data1[0];
    let encounterId = data1[2];
    if (this.productName) {
      let data = {
        strength: this.strength,
        strength_unit: this.strengthUnit,
        product_id: this.productId,
        medicine_name: this.productName,
        medication_start_date: moment(this.startDate),
        duration: this.duration,
        dosage_instruction: this.dosageInstruction,
        suggestion: this.suggestion,
        duration_unit: this.durationUnit,
        m: this.dosageM,
        n: this.dosageN,
        e: this.dosageE,
        medication_id: medicineId,
        prescriptionId: prescriptionId,
        encounterId: encounterId,
      };
      let medicine = { medication: data };
      this.medicineListArray.push(medicine);
    }
    if (!isAddMore) {
      this.saveMedicineData(data1);
    } else {
      this.clearData(isAddCustomMedicine, data1);
    }
  }
  saveMedicineData(data1?: any) {
    if (this.medicineListArray.length > 0) {
      let prescriptionId = this.medicineListArray[0].medication.prescriptionId;
      let encounterId = this.medicineListArray[0].medication.encounterId;

      let dataArray = [];

      dataArray.push(prescriptionId);
      dataArray.push(this.medicineListArray);
      dataArray.push(encounterId);
      this.dismiss(true, dataArray);
    } else {
      this.dismiss(false);
    }
  }

  dismiss(isupdated, data?: any) {
    // using the injected ViewController this page
    // can "dismiss" itself and pass back data
    this.modalCtrl.dismiss(data, isupdated);
  }
}
