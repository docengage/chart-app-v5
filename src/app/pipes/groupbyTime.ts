import { Pipe, PipeTransform } from '@angular/core';
import * as moment from "moment";

@Pipe({
  name: 'groupByTime',
  pure: false
})
export class GroupByTimePipe implements PipeTransform {
    transform(value: Array<{}>, field:string): Array<any> {
        if (value) {
          const groupedObj = value.reduce((prev, cur) =>{
            let keyEle  = moment(cur[field]).format('hh:mm A');
            if(!prev[keyEle]) {
              prev[keyEle] = [cur]
            } else{
              prev[keyEle].push(cur);
            }
    
            return prev;
          }, {});
    
          return Object.keys(groupedObj).map(key => ({ key, value: groupedObj[key] }));
        } else {
          return null;
        }
      }
}