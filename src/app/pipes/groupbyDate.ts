import { Pipe, PipeTransform } from '@angular/core';
import * as moment from "moment";

@Pipe({
  name: 'groupByDate',
  pure: false
})
export class GroupByDatePipe implements PipeTransform {
    transform(value: Array<{}>, field:string): Array<any> {
        if (value) {
          const groupedObj = value.reduce((prev, cur) =>{
            let keyEle  = moment(cur[field]).format('DD MMM YY');
            if(!prev[keyEle]) {
              prev[keyEle] = [cur]
            } else{
              prev[keyEle].push(cur);
            }
    
            return prev;
          }, {});
    
          return Object.keys(groupedObj).map(key => ({ key, value: groupedObj[key] }));
        } else {
          return null;
        }
      }
}