import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'datePlusAgo'
})
export class datePlusAgo implements PipeTransform {
  transform(x: string): string {
    return moment(x).calendar() + ' (' + moment(x).fromNow() + ')';
  }
}