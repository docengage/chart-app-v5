import { Directive, NgModule } from '@angular/core';
import { datePlusAgo } from './datePlusAgo';
import { GroupByPipe } from './groupby';
import { GroupByDatePipe } from './groupbyDate';
import { GroupByTimePipe } from './groupbyTime';


@NgModule({
	declarations: [
		datePlusAgo,
		GroupByPipe,
		GroupByDatePipe,
		GroupByTimePipe],
	imports: [],
	exports: [
		datePlusAgo,
		GroupByPipe,
		GroupByDatePipe,
		GroupByTimePipe]
})
export class PipesModule { }
