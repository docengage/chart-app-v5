import { Component, OnInit, ViewChild } from "@angular/core";
import {
  IonDatetime,
  LoadingController,
  ModalController,
} from "@ionic/angular";
import * as moment from "moment";
import { Handlebar } from "../components/handlebar";
import { AuthService } from "../service/auth.service";
import { ToastServiceData } from "../service/toast-service";

@Component({
  selector: "app-add-leave",
  templateUrl: "./add-leave.page.html",
  styleUrls: ["./add-leave.page.scss"],
})
export class AddLeavePage implements OnInit {
  leaveReason: any = "";
  leaveFrom: any = "";
  leaveFromTime: any = "";
  leaveTo: any = "";
  leaveToTime: any = "";
  minDateTime = new Date().toISOString();
  pageLoad: any;
  isValidReason = true;
  isValidStartDate = true;
  isValidStartTime = true;
  isValidEndDate = true;
  isValidEndTime = true;
  showModal = false;
  presentation: any;
  formatedDate: any;
  fieldName: any;

  @ViewChild(IonDatetime) datetime: IonDatetime;

  constructor(
    public handlebar: Handlebar,
    private toast: ToastServiceData,
    private loadingController: LoadingController,
    private authService: AuthService,
    private modalCtrl: ModalController
  ) {}

  ngOnInit() {
    this.leaveReason = "";
    this.leaveFrom = "";
    this.leaveFromTime = "";
    this.leaveTo = "";
    this.leaveToTime = "";
  }

  dismiss(isUpdated) {
    this.modalCtrl.dismiss(isUpdated);
  }

  checkdate(txtDate) {
    let currVal = txtDate;
    if (!currVal) return false;

    let rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    let dtArray = currVal.match(rxDatePattern);

    if (dtArray == null) return false;

    //Checks for mm/dd/yyyy format.
    const dtMonth = dtArray[3];
    const dtDay = dtArray[1];
    const dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12) return false;
    else if (dtDay < 1 || dtDay > 31) return false;
    else if (
      (dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) &&
      dtDay == 31
    )
      return false;
    else if (dtMonth == 2) {
      let isleap = dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0);
      if (dtDay > 29 || (dtDay == 29 && !isleap)) return false;
    }
    return true;
  }

  async presentLoading() {
    this.pageLoad = await this.loadingController.create({
      message: "Please wait...",
      duration: 2000,
    });
    await this.pageLoad.present();
  }

  setDateModal(value, type, fieldName) {
    this[fieldName] = this.formatedDate = value ? value : moment().format();
    this.presentation = type;
    this.fieldName = fieldName;
    this.showModal = true;
  }

  dateChange(value) {
    this[this.fieldName] = value ? moment(value).format() : moment().format();

    this.dateCancel();
  }

  dateCancel() {
    if (this.datetime) {
      this.showModal = false;
    }
  }

  doValidation() {
    this.isValidReason = true;
    this.isValidStartDate = true;
    this.isValidEndDate = true;

    let global = "";
    let error = "";
    let isDateValid;

    if (this.handlebar.isEmpty(this.leaveReason)) {
      global = "Please enter reason for leave.<br/>";
      this.isValidReason = false;
    }
    let isFromValid = false;
    let leaveFromValue;
    if (
      this.handlebar.isEmpty(this.leaveFrom) ||
      this.leaveFrom.includes("Invalid")
    ) {
      error = "Enter leave from date.<br>";
      global = global + error;
      this.isValidStartDate = false;
    } else {
      const leaveFromDate =
        typeof this.leaveFrom === "string"
          ? moment(this.leaveFrom, "DD-MM-YYYY hh:mm A").format("DD-MM-YYYY")
          : moment(this.leaveFrom).format("DD-MM-YYYY");

      isDateValid = this.checkdate(leaveFromDate);

      if (isDateValid) {
        if (typeof this.leaveFrom === "string") {
          leaveFromValue = moment(this.leaveFrom, "DD-MM-YYYY hh:mm A");
        } else {
          leaveFromValue = moment(this.leaveFrom).format("YYYY-MM-DD hh:mm A");
        }
        isFromValid = true;
      } else {
        error = "Entered leave from date is not valid.<br>";
        global = global + error;
        this.isValidStartDate = false;
      }
    }

    if (
      this.handlebar.isEmpty(this.leaveTo) ||
      this.leaveTo.includes("Invalid")
    ) {
      error = "Enter Leave To Date.<br>";
      global = global + error;
      this.isValidEndDate = false;
    } else {
      const leaveToDate =
        typeof this.leaveTo === "string"
          ? moment(this.leaveTo, "DD-MM-YYYY hh:mm A").format("DD-MM-YYYY")
          : moment(this.leaveTo).format("DD-MM-YYYY");

      isDateValid = this.checkdate(leaveToDate);
      if (isDateValid) {
        let leaveToValue;
        if (typeof this.leaveTo === "string") {
          leaveToValue = moment(this.leaveTo, "DD-MM-YYYY hh:mm A");
        } else {
          leaveToValue = moment(this.leaveTo).format("DD-MM-YYYY hh:mm A");
        }
        if (isFromValid) {
          const diff =
            moment(leaveFromValue).valueOf() - moment(leaveToValue).valueOf();
          const diffInHours = diff / 1000 / 60 / 60;
          if (diffInHours >= 0) {
            global =
              global +
              "Entered leave to date should be more than from date.<br>";
            this.isValidEndDate = false;
          }
        }
      } else {
        error = "Entered leave from date is not valid.<br>";
        global = global + error;
        this.isValidEndDate = false;
      }
    }
    return global;
  }

  saveLeaveInfo() {
    this.presentLoading().then(() => {
      this.pageLoad.message = "validating Data...";
      const isValid = this.doValidation();
      if (this.handlebar.isNotEmpty(isValid)) {
        this.toast.presentToast(isValid);
      } else {
        this.pageLoad.message = "Saving Data...";
        const leaveData = {
          leaveReason: this.leaveReason,
          leaveFrom: moment(this.leaveFrom).format("DD-MM-YYYY HH:mm"),
          leaveTo: moment(this.leaveTo).format("DD-MM-YYYY HH:mm"),
        };
        this.authService.saveLeaveInfo(leaveData).then((data) => {
          if (
            data &&
            (data.status === 400 || data.status === 401 || data.status === 500)
          ) {
            const data0 = JSON.parse(data._body);
            this.toast.presentToast(data0.description);
          } else if (data) {
            this.dismiss(true);
            this.toast.presentToast("Leave Applied Successfully.");
          }
          this.pageLoad.dismiss();
        });
      }
    });
  }
}
