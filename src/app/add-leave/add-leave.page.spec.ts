import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddLeavePage } from './add-leave.page';

describe('AddLeavePage', () => {
  let component: AddLeavePage;
  let fixture: ComponentFixture<AddLeavePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLeavePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddLeavePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
