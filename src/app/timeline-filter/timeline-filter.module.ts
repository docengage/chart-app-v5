import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TimelineFilterPageRoutingModule } from './timeline-filter-routing.module';

import { TimelineFilterPage } from './timeline-filter.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TimelineFilterPageRoutingModule
  ],
  declarations: [TimelineFilterPage]
})
export class TimelineFilterPageModule {}
