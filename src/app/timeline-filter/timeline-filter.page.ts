import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { MasterDataService } from '../service/master-data';
import { LocalStorageService } from '../service/local-storage.service';

@Component({
  selector: 'app-timeline-filter',
  templateUrl: './timeline-filter.page.html',
  styleUrls: ['./timeline-filter.page.scss'],
})
export class TimelineFilterPage implements OnInit {


  apptFilters: Array<{value: string, isChecked: boolean, label: string}> = [];
  includedTrackNames: any;
  constructor(
    public masterData: MasterDataService,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private localstorage: LocalStorageService
  ) {
    // passed in array of track names that should be excluded (unchecked)
  }
  ngOnInit(): void {
    this.includedTrackNames = this.navParams.data;

    this.masterData.getTimeLineFilters().then((listFilters) => {
      listFilters.forEach(ele => {
        this.apptFilters.push({
          value: ele.value,
          isChecked: ele.isChecked,
          label: ele.label
        });
      });
    });
  }
  resetFilters() {
    // reset all of the toggles to be checked
    this.apptFilters.forEach(filter => {
      filter.isChecked = true;
    });
  }

  applyFilters() {
    // Pass back a new array of track names to exclude
    const includedTrackNames = this.apptFilters.filter(c => c.isChecked).map(c => c.value);
    if (this.includedTrackNames.toString() === includedTrackNames.toString()) {
      this.dismiss(false);
    } else {
      this.dismiss(true, includedTrackNames);
    }

    const tracks = [];
    for (let i = 0; i < this.apptFilters.length; i++) {
      if (this.apptFilters[i].isChecked) {
        tracks.push({value: this.apptFilters[i].value, label: this.apptFilters[i].label, isChecked: true});
      } else {
        tracks.push({value: this.apptFilters[i].value, label: this.apptFilters[i].label, isChecked: false});
      }
    }
    this.localstorage.setTracks(tracks);

  }

  dismiss(isupdated, data?: any) {
    // using the injected ViewController this page
    // can "dismiss" itself and pass back data
    this.modalCtrl.dismiss(data, isupdated);
  }
}
