import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimelineFilterPage } from './timeline-filter.page';

const routes: Routes = [
  {
    path: '',
    component: TimelineFilterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TimelineFilterPageRoutingModule {}
