import { AfterViewChecked, Component, OnInit } from '@angular/core';
import { AlertController, ModalController, NavController } from '@ionic/angular';
import * as moment from 'moment';
import { AddLeavePage } from '../add-leave/add-leave.page';
import { AuthService } from '../service/auth.service';
import { LocalStorageService } from '../service/local-storage.service';
import { ToastServiceData } from '../service/toast-service';

@Component({
  selector: 'app-leave-list',
  templateUrl: './leave-list.page.html',
  styleUrls: ['./leave-list.page.scss'],
})
export class LeaveListPage implements OnInit, AfterViewChecked {
  providerInfo: any;
  accessControl: any;
  currentPage = 1;
  leaveInfo: any;
  totalPages: number = 0;

  constructor(
    private nav: NavController,
    public authService: AuthService,
    public localStorge: LocalStorageService,
    private toastServ: ToastServiceData,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController
  ) { }

  ngOnInit() {
    this.authService.isTokenValid().then(() => {
      this.providerInfo = [];
      this.authService.getProviderInfo().then((data) => {
        this.providerInfo = data;
        this.localStorge.getAccessControl().then((accessObj: any) => {
          if (accessObj) {
            this.accessControl = accessObj.permissions;
          }
          this.getLeaveInfo(this.providerInfo.providerId, true);
        });
      });
    });
  }

  ngAfterViewChecked() {
    if(document.getElementById('tab-button-leaveList')){
      document.getElementById('tab-button-leaveList').classList.add('tab-selected');
    }
    if(document.getElementById('tab-button-myCharts')){
      document.getElementById('tab-button-myCharts').classList.remove('tab-selected');
    }
    if(document.getElementById('tab-button-allPatient')){
      document.getElementById('tab-button-allPatient').classList.remove('tab-selected');
    }
    if(document.getElementById('tab-button-schedule')){
      document.getElementById('tab-button-schedule').classList.remove('tab-selected');
    }
  }

  getLeaveInfo(providerId, isEmpty) {
    if (isEmpty) {
      this.leaveInfo = [];
      this.totalPages = 0;
      this.currentPage = 1;
    }
    this.authService.getLeaveInfo(providerId, this.currentPage).then((data) => {
      if (data) {
        for (let i = 0; i < data.length; i = i + 1) {
          data[i].from = moment(data[i].leavefrom).format("DD/MM/YYYY");
          data[i].to = moment(data[i].leaveto).format("DD/MM/YYYY");
          this.leaveInfo.push(data[i]);
        }
        this.totalPages = data[0].total_page;
      }
    });
  }
  doRefresh(event) {
    this.currentPage = 1;
    this.getLeaveInfo(this.providerInfo.providerId, true);
    setTimeout(() => {
      event.target.complete();
      this.toastServ.presentToast('Leave List Refreshed');
    }, 1000);
  }

  doInfinite(infiniteScroll) {
    this.currentPage = Number(this.currentPage) + 1;
    setTimeout(() => {
      setTimeout(() => {
        if (this.currentPage <= this.totalPages) {
          this.getLeaveInfo(this.providerInfo.providerId, false);
        }
        infiniteScroll.target.complete();
      }, 500);
    });
  }

  goBack() {
    this.nav.navigateBack(['home']);
  }

  async presentAlert(reason, from, to) {
    const alert = await this.alertCtrl.create({
      subHeader: from+' - '+to,
      message: reason,
      buttons: ['OK']
    });

    await alert.present();
  }
  async addLeave() {
    let modal = await this.modalCtrl.create({
      component: AddLeavePage
    });
    modal.onDidDismiss().then((data) => {
      if (data.data === true) {
        this.getLeaveInfo(this.providerInfo.providerId, true);
      }
    });
    await modal.present();
  }

}
