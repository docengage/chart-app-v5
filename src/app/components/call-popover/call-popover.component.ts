import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController, NavParams, Platform, PopoverController } from '@ionic/angular';
import { AuthService } from 'src/app/service/auth.service';
import { LocalStorageService } from 'src/app/service/local-storage.service';
import { CallNumberService } from 'src/app/service/phone-call-service';
import { ToastServiceData } from 'src/app/service/toast-service';
import { JoinVcPage } from 'src/app/videoCall/join-vc/join-vc.page';
import { Handlebar } from '../handlebar';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

@Component({
  selector: 'app-call-popover',
  templateUrl: './call-popover.component.html',
  styleUrls: ['./call-popover.component.scss'],
})
export class CallPopoverComponent implements OnInit {
  list: any ;
  isIOSPlt: boolean;
  appointmentId: any;
  appointmentData: any;
  accessControl: any;
  patientInfo: any;
  constructor(
    public navParam: NavParams,
    private toastServ: ToastServiceData,
    private callNumberService: CallNumberService,
    private plt: Platform,
    private tost: ToastServiceData,
    private authServ: AuthService,
    private authService : AuthService,
    private handlebar: Handlebar,
    private modalCtrl: ModalController,
    private localstorage: LocalStorageService,
    private loadingCtrl: LoadingController,
    private androidPermissions: AndroidPermissions,
    public popoverCtrlr: PopoverController
  ) {
    if (this.plt.is('ios')) {
      this.isIOSPlt = true;
    }
  }

  ngOnInit() {
    this.appointmentId = this.navParam.get('appointmentId');
    this.patientInfo = this.navParam.get('patientInfo');
    
    this.localstorage.getAccessControl().then((accessObj) => {
      this.accessControl = accessObj;
      this.appointmentData = this.navParam.get('appointmentData');
      this.isAllowVideoCall(this.appointmentData);
    });
  }

  callPatient(item) {
    if (item === "Audio") {
      if (this.patientInfo && this.patientInfo.telephone){
        this.authService.callPatient(this.patientInfo.countryCode,this.patientInfo.telephone,this.patientInfo.localPatientId);
        this.toastServ.presentToast("Call Connecting Through IVR!");

        // this.callPatientMobile(this.patientInfo.telephone, '91',this.patientInfo.localPatientId)
      } else {
        this.tost.presentToast('Please add mobile number.');
      }
    } else {
      this.startVideoCall(this.appointmentData);
    }
    this.popoverCtrlr.dismiss();
  }
 

  async startVideoCall(item) {
    // const data = {
    //   name: item.participants[0].name,
    //   roomId: item.videoCallSid
    // };
    // const startVideoCallModal = await this.modalCtrl.create({
    //     component: JoinVcPage,
    //     componentProps: data,
    //     id: 'joinVcModal'
    // });
    // await startVideoCallModal.present();

    if (item) {
      if (item.videoVendor === 'enableX' && this.handlebar.isNotEmpty(item.videoCallSid)) {
        // const navigationExtras: NavigationExtras = {
        //   queryParams: {
        //     name: item.participants[0].name,
        //     roomId: item.videoCallSid
        //   }
        // };
        // this.router.navigate(['/join-vc'], navigationExtras);

        const data = {
          name: item.participants[0].name,
          roomId: item.videoCallSid,
          vendorName: item.videoVendor
        };
        const startVideoCallModal = await this.modalCtrl.create({
            component: JoinVcPage,
            componentProps: data,
            id: 'joinVcModal'
        });
        await startVideoCallModal.present();

        return true;
      }
      else if (item.videoVendor === 'dyte' && this.handlebar.isNotEmpty(item.videoCallSid)) {
        if (this.plt.is('android')) {
          const permissions = [this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.RECORD_AUDIO]
          this.androidPermissions.requestPermissions(permissions).then(
            (result) => {
              if (!result.hasPermission) {
                this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
                  (result) => {
                    if (!result.hasPermission) {
                      this.tost.presentToast('You camera permission denied, unable to start video consultation.');
                    } else {
                      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.RECORD_AUDIO).then(
                        (result) => {
                          if (!result.hasPermission) {
                            this.tost.presentToast('You microphone permission denied, unable to start video consultation.');
                            return false;
                          } else {
                            this.startDyteMeeting(item);
                          }
                        });
                    }
                  });
                return false;
              } else {
                this.startDyteMeeting(item);
              }

            });
        } else {
          this.startDyteMeeting(item);
        }

      } else {
        this.tost.presentToast('Invalid Vendor details, Please contact your admin.');
        return false;
      }
    }
    this.tost.presentToast('Something went wrong...');
    return false;
  }

  async startDyteMeeting(item) {
    const providerName = item.participants[1].name;
    const meetingNode = JSON.parse(item.meetingNode);
    let meetingUrl = meetingNode.join_url + `&providerName=${providerName}`;
    (this.plt.is('android')) ? meetingUrl +="&platform=android" : meetingUrl+="&platform=ios";
    const data = {
      name: item.participants[1].name,
      roomId: item.videoCallSid,
      vendorName: item.videoVendor,
      meetingUrl: meetingUrl
    };
    const startVideoCallModal = await this.modalCtrl.create({
      component: JoinVcPage,
      componentProps: data,
      id: 'joinVcModal'
    });
    await startVideoCallModal.present();
    return true;
  }

  isAllowVideoCall(item) {
    if (this.accessControl && this.accessControl.permissions && this.accessControl.permissions.Telehealth5674) {
      if (item.apptType === 'Video' && this.handlebar.isNotEmpty(item.videoCallSid)) {
        this.list = ['Audio', 'Video'];
        return true;
      }
      this.list = ['Audio'];
      // this.callPatient('Audio');
      return false;
    }
    this.list = ['Audio'];
    return false;
  }

}
