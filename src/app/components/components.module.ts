import { Component, NgModule } from '@angular/core';
import { Handlebar } from './handlebar';
import { DynamicFormCommon } from './dynamicform-common';
import { ColorgeneratorComponent } from 'src/app/components/colorgenerator/colorgenerator.component';
import { IonicModule } from '@ionic/angular';
import { NewTxtImgComponent } from '../components/new-txt-img/new-txt-img.component';
import { SelectNoteTemplateComponent } from '../select-note-template/select-note-template.component';


@NgModule({
declarations: [ColorgeneratorComponent, Handlebar, DynamicFormCommon, NewTxtImgComponent],
imports: [ IonicModule],
exports: [ColorgeneratorComponent, Handlebar, DynamicFormCommon, NewTxtImgComponent]
})
export class ComponentsModule {}
