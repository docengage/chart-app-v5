import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { ColorgeneratorComponent } from 'src/app/components/colorgenerator/colorgenerator.component';


@Component({
  selector: 'app-new-txt-img',
  templateUrl: './new-txt-img.component.html',
  styleUrls: ['./new-txt-img.component.scss'],
})
export class NewTxtImgComponent {
  value: string;
  colorVar: string;
  constructor(private element: ElementRef, public colorGenerator: ColorgeneratorComponent) { }

  @Input()
  set text(txt: string) {
      if (txt) {
          this.element.nativeElement.style.backgroundColor = this.colorGenerator.getColor(txt);
          this.element.nativeElement.style.width = '1.7em';
          this.element.nativeElement.style.height = '1.7em';
          this.element.nativeElement.style.borderRadius = '8px';
          this.element.nativeElement.style.display = 'table-cell';
          this.element.nativeElement.style.textAlign = 'center';
          this.element.nativeElement.style.color = '#fff';
          this.element.nativeElement.style.verticalAlign = 'middle';
          this.element.nativeElement.style.fontSize = '1.3em';
          this.element.nativeElement.style.textTransform = 'capitalize';
          this.element.nativeElement.setAttribute('value', txt.charAt(0));
          this.colorVar = this.colorGenerator.getColor(txt);
          this.value =  txt.charAt(0);
      }
  }
}
