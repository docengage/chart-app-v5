import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewTxtImgComponent } from './new-txt-img.component';
import { ColorgeneratorComponent } from 'src/app/components/colorgenerator/colorgenerator.component';

describe('NewTxtImgComponent', () => {
  let component: NewTxtImgComponent;
  let fixture: ComponentFixture<NewTxtImgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewTxtImgComponent, ColorgeneratorComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewTxtImgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
