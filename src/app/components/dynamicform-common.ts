import { Component } from '@angular/core';
import * as moment from 'moment';

@Component({
  template: 'dummy.html'

})


export class DynamicFormCommon {
   
    content:any;
    isEmpty(str) {
        return (!str || 0 === str.length || str==='null');
      }
      isNotEmpty(str) {
        return !this.isEmpty(str);
      }
  enableDependentField (secCounter,counter,fieldName,fieldType,content){
    this.content = content;
    
    let fieldValue = "";
    let fieldTypeVal = "";
    if (fieldType === "Checkbox") {
        let fieldValues = this.content[secCounter].sectionContent[counter].field_type_values;
        for(let k=0;k<fieldValues.length;k++){
         
          if(fieldValues[k].isChecked){
            if (fieldValue === "") {
              fieldValue = fieldValues[k].value;
            }else{
              fieldValue = fieldValue + "," +fieldValues[k].value;
            }
          }
        }

    } else {
      fieldValue = this.content[secCounter].sectionContent[counter].value;
    }
    if(this.content){
    for (let i = 0; i < this.content.length; i += 1) {
        for (let j = 0; j < this.content[i].sectionContent.length; j += 1) {
          if (this.isNotEmpty(this.content[i].sectionContent[j].depends_on) && this.isNotEmpty(fieldName)) {
                if (this.content[i].sectionContent[j].depends_on.toLowerCase() === fieldName.toLowerCase()) {
                  
                    if(this.isEmpty(fieldValue)){
                        fieldValue = "";
                    }
                    let dependsCond = this.content[i].sectionContent[j].depends_condition;
                    if(this.isEmpty(this.content[i].sectionContent[j].depends_condition)){
                        dependsCond = "";
                    }
                    let defaultval = this.content[i].sectionContent[j].defaultval;
                    if (dependsCond === "" || (fieldValue.toLowerCase().indexOf(dependsCond.toLowerCase()) >= 0)) {
                       this.content[i].sectionContent[j].visible = "Show";
                        
                        fieldTypeVal = this.content[i].sectionContent[j].field_type;
                        let fieldCapturedValue = this.content[i].sectionContent[j].value;
                        if(fieldTypeVal === "Checkbox"){
                          fieldCapturedValue = "";
                          for(let k=0;k<this.content[secCounter].sectionContent[counter].field_type_values.length;k++){
         
                            if(this.content[secCounter].sectionContent[counter].field_type_values[k].isChecked){
                              this.content[secCounter].sectionContent[counter].field_type_values[k].isChecked = true;
                              if (fieldValue === "") {
                                fieldCapturedValue = this.content[secCounter].sectionContent[counter].field_type_values[k].value;
                              }else{
                                fieldCapturedValue = fieldCapturedValue + "," +this.content[secCounter].sectionContent[counter].field_type_values[k].value;
                              }
                               
                            }
                          }
                          
                        } else {
                            if(this.isEmpty(fieldCapturedValue)){
                              fieldCapturedValue = defaultval;
                            }
                            this.content[i].sectionContent[j].value = fieldCapturedValue;
                        }

                        
                         this.enableSubGroup(this.content[i].sectionContent[j].field_name, fieldCapturedValue);
                        //  this.enableDependentField(i,j,this.content[i].sectionContent[j].field_name,this.content[i].sectionContent[j].field_type);
                    } else {
                        this.content[i].sectionContent[j].visible = "Hide";
                        if(fieldTypeVal === "Checkbox"){
                          for(let k=0;k<this.content[secCounter].sectionContent[counter].field_type_values.length;k++){
                            if(this.content[secCounter].sectionContent[counter].field_type_values[k].isChecked){
                              this.content[secCounter].sectionContent[counter].field_type_values[k].isChecked = false;
                            }
                          }
                        }else{
                          this.content[i].sectionContent[j].value = "";
                        }
                        this.enableSubGroup(this.content[i].sectionContent[j].field_name, "");
                      //  this.enableDependentField(i,j,this.content[i].sectionContent[j].field_name,this.content[i].sectionContent[j].field_type);
                        
                    }
                }
            }
        }
    }
}
return this.content;
  }

  enableSubGroup(fieldName, fieldValue) {
    if(this.isEmpty(fieldValue)){
      fieldValue = "";
    }
    let element1 = "";
    let fieldTypeVal ="";
    fieldName = fieldName.split(",,").join("~,");
    let filedValues = fieldName.split(",");
    for (let k = 0; k < filedValues.length; k += 1) {
        fieldName = filedValues[k];
        fieldName = fieldName.split("~").join(",");
        for (let i = 0; i < this.content.length; i += 1) {
            for (let j = 0; j < this.content[i].sectionContent.length; j += 1) {
                if (this.isNotEmpty(this.content[i].sectionContent[j].depends_on) && this.isNotEmpty(fieldName)) {
                    if (this.content[i].sectionContent[j].depends_on.toLowerCase() === fieldName.toLowerCase()) {
                     
                        let counterVal = this.content[i].sectionContent[j].counter;
                     
                        if(this.isEmpty(fieldValue)){
                          fieldValue = "";
                      }
                      let dependsCond = this.content[i].sectionContent[j].depends_condition;
                      if(this.isEmpty(this.content[i].sectionContent[j].depends_condition)){
                          dependsCond = "";
                      }
                      let defaultval = this.content[i].sectionContent[j].defaultval;

                        if (dependsCond === "" || (fieldValue.toLowerCase().indexOf(dependsCond.toLowerCase()) >= 0)) {
                          this.content[i].sectionContent[j].visible = "Show";
                          fieldTypeVal = this.content[i].sectionContent[j].field_type;
                          let fieldCapturedValue = this.content[i].sectionContent[j].value;
                          if(fieldTypeVal === "Checkbox"){

                            for(let k=0;k<this.content[i].sectionContent[j].field_type_values.length;k++){
          
                              if(this.content[i].sectionContent[j].field_type_values[k].isChecked){
                                this.content[i].sectionContent[j].field_type_values[k].isChecked = true;
                              }
                            }
                            
                          } else {
                              if(this.isEmpty(fieldCapturedValue)){
                                fieldCapturedValue = defaultval;
                              }
                              this.content[i].sectionContent[j].value = fieldCapturedValue;
                          }

                        } else {
                          this.content[i].sectionContent[j].visible = "Hide";
                         //   $("[id='" +randomNum+ formData[i].sectionContent[j].field_name + "']").addClass("display-none");
                            if (counterVal > 0) {
                                fieldTypeVal = this.content[i].sectionContent[j].field_type;
                                if (fieldTypeVal === "Checkbox") {
                                  for(let k=0;k<this.content[i].sectionContent[j].field_type_values.length;k++){
         
                                    if(this.content[i].sectionContent[j].field_type_values[k].isChecked){
                                      this.content[i].sectionContent[j].field_type_values[k].isChecked = true;
                                    }
                                  }
                                } else {
                                  this.content[i].sectionContent[j].value="";
                                }

                                if (element1 = "") {
                                    element1 = this.content[i].sectionContent[j].field_name;
                                } else {
                                    element1 += "," + this.content[i].sectionContent[j].field_name;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if (element1 !== "") {
      this.enableSubGroup(element1, "");
    }
    return this.content;
}



}