import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AllPatientPageRoutingModule } from './all-patient-routing.module';

import { AllPatientPage } from './all-patient.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AllPatientPageRoutingModule,
    ComponentsModule
  ],
  declarations: [AllPatientPage]
})
export class AllPatientPageModule {}
