import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AllPatientPage } from './all-patient.page';

describe('AllPatientPage', () => {
  let component: AllPatientPage;
  let fixture: ComponentFixture<AllPatientPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllPatientPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AllPatientPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
