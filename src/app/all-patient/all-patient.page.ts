import { AfterViewChecked, Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { NavParams, Platform, ActionSheetController, ModalController, NavController, IonModal } from '@ionic/angular';
import { ToastServiceData } from '../service/toast-service';
import { Handlebar } from '../components/handlebar';
import { CallNumberService } from '../service/phone-call-service';
import * as moment from "moment";
import { LocalStorageService } from '../service/local-storage.service';
import { Router } from '@angular/router';
import { TimeLineDataPushService } from '../service/timeline-auth-service';
import { AddPrescriptionPage } from '../chartPage/add-prescription/add-prescription.page';
import { ClinicalNotePage } from '../chartPage/clinical-note/clinical-note.page';
import { SelectListPage } from '../chartPage/select-list/select-list.page';

@Component({
  selector: 'app-all-patient',
  templateUrl: './all-patient.page.html',
  styleUrls: ['./all-patient.page.scss'],
})
export class AllPatientPage implements OnInit, AfterViewChecked {


  icons: string[];
  items: Array<{}>;
  isLoggedIn: boolean = false;
  noRecordFound: string;
  queryText: string;
  totalPages: number;
  currentPage: number = 1;
  enableToolbar: boolean = false;
  searchVal: string;
  timer: any;

  medications: any[];
  prescription: {};
  isIOSPlt: boolean = false;
  plannedCareItemId: number;
  encounterId: number;
  uuid: string;
  uhid: string;

  progressNote: Array<{}>;
  accessControl: {};
  customerContInfo: any;
  isModalOpen = false;

  @ViewChild(IonModal) fabModal: IonModal;

  constructor(private authService: AuthService, private app: Router,
    private timelineAuthServ: TimeLineDataPushService,
    public navParam: NavParams,
    private plt: Platform,
    private toastServ: ToastServiceData,
    private handlebar: Handlebar,
    private actionSheetCtrl: ActionSheetController,
    private callNumberService: CallNumberService,
    private modalCtrl: ModalController,
    private localstorage: LocalStorageService,
    private nav: NavController,) {

    if (this.plt.is('ios')) {
      this.isIOSPlt = true;
    }
    this.medications = this.navParam.get('medications');
    this.prescription = this.navParam.get('prescription');
    this.plannedCareItemId = this.navParam.get('plannedCareItemId');
    this.encounterId = this.navParam.get('encounterId');
    this.uuid = this.navParam.get('uuid');
  }
  ngOnInit(): void {
    this.authService.isTokenValid().then((returnVal) => {
      this.localstorage.getAccessControl().then((accessObj) => {
        this.accessControl = accessObj;
        this.getMyPatientsList(true);
        this.getCustomerContInfo();
      });
    });
  }

  ngAfterViewChecked() {
    if (document.getElementById('tab-button-allPatient')) {
      document.getElementById('tab-button-allPatient').classList.add('tab-selected');
    }
    if (document.getElementById('tab-button-myCharts')) {
      document.getElementById('tab-button-myCharts').classList.remove('tab-selected');
    }
    if (document.getElementById('tab-button-schedule')) {
      document.getElementById('tab-button-schedule').classList.remove('tab-selected');
    }
    if(document.getElementById('tab-button-leaveList')) {
      document.getElementById('tab-button-leaveList').classList.remove('tab-selected');
    }
  }

  getCustomerContInfo() {
    if (this.accessControl["permissions"] && this.accessControl["permissions"].CustomerContactInfo5674 && this.accessControl["permissions"].CustomerContactInfoview) {
      this.customerContInfo = true;
    } else {
      this.customerContInfo = false;
    }
  }

  doInfinite(event) {
    var clientHeight = event.target.clientHeight;
    var scrollUpto = event.target.scrollHeight - event.target.scrollTop;
    if (clientHeight === scrollUpto) {
      this.currentPage = Number(this.currentPage) + 1;
      setTimeout(() => {
        setTimeout(() => {
          if (this.totalPages >= 15) {
            this.getMyPatientsList(false);
          }
          event.target.complete();
        }, 500);
      });
    } 
  }

  viewPatientTimeLine(uhid) {
    this.timelineAuthServ.viewPatientTimeLine(uhid);
  }

  searchPatients() {
    clearTimeout(this.timer);
    if (this.searchVal != this.queryText) {
      let temp = this;
      this.timer = setTimeout(function () {
        temp.searchVal = temp.queryText;
        temp.getMyPatientsList(true);
      }, 400);
    }
  }

  getMyPatientsList(isEmpty) {
    if (isEmpty) {
      this.items = [];
      this.totalPages = 0;
      this.currentPage = 1;
    }
    this.authService.searchPatientsList(this.queryText, this.currentPage).then((data) => {
      if (data && data.patients) {
        
        for (let i = 0; i < data.patients.length; i++) {
          if(data.patients[i].lastVisit){
              data.patients[i].lastVisit = moment(data.patients[i].lastVisit).format('DD MMM YYYY hh:mm a');
          }

          if (this.accessControl['permissions'].allowToSeeContactInfo) {
            if (data.patients[i].telephone) {
              data.patients[i].checkedTelephoneNo = data.patients[i].telephone;
            }
          } else {
            if (data.patients[i].telephone) {
              data.patients[i].checkedTelephoneNo = "XXXXXXX"+ data.patients[i].telephone.substring(data.patients[i].telephone.length - 3)
            }
          }
          
          this.items.push(data.patients[i]);
        }
        this.totalPages = data.patients.length;
      }
      let recordName = "Patients"
      this.noRecordFound = this.toastServ.displayNoRecordsFound(this.items, recordName);
    });
  }

  doRefresh(event) {
    this.getMyPatientsList(true);
    setTimeout(() => {
      event.target.complete();
      this.toastServ.presentToast('Patients List Refreshed');
    }, 1000);
  }

  checkPatientHasImage(imagePath) {
    return this.handlebar.checkImagePath(imagePath);
  }
  callPatientMobile(mobileNo,countryCode,localPatientId) {
    this.callNumberService.callToNumber(mobileNo,countryCode,localPatientId);
  }

  async presentActionSheet(uuid, patientName) {
    let buttons = [];

    if (this.accessControl["permissions"] && this.accessControl["permissions"].Prescription5674
      && this.accessControl["permissions"].Prescriptioncreate) {

      buttons.push({
        text: 'Add Prescription',
        role: 'Prescription',
        handler: async () => {
          let data0 = {
            "uuid": uuid,
            "patientName": patientName
          };
          let modal = await this.modalCtrl.create({
            component: AddPrescriptionPage,
            componentProps: data0
          });
          await modal.present();
          modal.onDidDismiss().then((data) => {
            if (data.role) {
              let element = {
                medication: data.data[1]
              }
              if (!this.medications) {
                this.medications = [];
              }
              if (!this.prescription) {
                this.prescription = {};
              }
              this.prescription["prescription_id"] = data.data[0];
              this.encounterId = data.data[2];
              this.medications.push(element);
            }
          });
        }
      });
    }
    if (this.accessControl["permissions"] && this.accessControl["permissions"].GeneralNotes5674
      && this.accessControl["permissions"].GeneralNotescreate) {

      buttons.push({
        text: 'Add Note',
        role: 'Note',
        handler: async () => {
          let data0 = {
            // "ClinicalNote":this.progressNote,
            // "plannedCareItemId":this.plannedCareItemId,
            // "encounterId":this.encounterId,
            "uuid": uuid
          };
          let modal = await this.modalCtrl.create({
            component: ClinicalNotePage,
            componentProps: data0
          });
          await modal.present();
          modal.onDidDismiss().then((data) => {
            if (data) {
              if (this.progressNote && this.progressNote["clinical_notes"]) {
                this.progressNote["clinical_notes"] = data.data["note"];
              } else {
                let note = [];
                note["clinical_notes"] = data.data["note"];
                this.progressNote = note;
              }
              let returnJson = data.data["returnJson"];
              this.encounterId = returnJson[0];
              this.progressNote["progress_note_id"] = returnJson[1];
            }
          });

        }
      });
    }
    if (this.accessControl["permissions"] && this.accessControl["permissions"].Pathology5674
      && this.accessControl["permissions"].Pathologycreate) {

      buttons.push({
        text: 'Add Pathology',
        role: 'Pathology',
        handler: () => {
          this.selectValues("Pathology", uuid);
        }
      });
    }
    if (this.accessControl["permissions"] && this.accessControl["permissions"].Radiology5674
      && this.accessControl["permissions"].Radiologycreate) {

      buttons.push({
        text: 'Add Radiology',
        role: 'Radiology',
        handler: () => {
          this.selectValues("Radiology", uuid);
        }
      });
    }

    if (this.accessControl["permissions"] && this.accessControl["permissions"].Neurology5674
    && this.accessControl["permissions"].Neurologycreate) {

    buttons.push({
      text: 'Add Neurology',
      role: 'Neurology',
      handler: () => {
        this.selectValues("Neurology", uuid);
      }
    });

  }
    buttons.push({
      text: 'Cancel',
      role: 'cancel',
      handler: () => { }
    });

    let actionSheet = await this.actionSheetCtrl.create({
      header: patientName,
      buttons: buttons
    });
    if (document.getElementsByClassName('action-sheet-title')[0]) {
      document.getElementsByClassName('action-sheet-title')[0].classList.add('actionSheetStyle');
    }
    await actionSheet.present();
  }

  selectValues(key, uhid) {

    let data = {
      "key": key,
      "plannedCareItemId": 0,
      "uuid": uhid,
      "values": []
    };
    this.navigateToSelectModel(data, key);

  }

  async navigateToSelectModel(data0?: any, key?: string) {
    let modal = await this.modalCtrl.create({
      component: SelectListPage,
      componentProps: data0
    });
    await modal.present();
    modal.onDidDismiss().then((data) => {
    });
  }

  openModal() {
    this.isModalOpen = true;
  }

  onWillDismiss(event) {
    this.isModalOpen = false;
  }

  openAddPatientPage() {
    this.isModalOpen = false;
    this.fabModal.dismiss(null, 'cancel');
    this.nav.navigateForward(['/add-patient']);
  }
  openQuickSchedulePage() {
    this.isModalOpen = false;
    this.fabModal.dismiss(null, 'cancel');
    this.nav.navigateForward(['/quick-schedule']);
  }
  openAddActivityPage() {
    this.isModalOpen = false;
    this.fabModal.dismiss(null, 'cancel');
    this.nav.navigateForward(['/addactivity']);
  }
}
