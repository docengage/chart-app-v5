import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import {
  IonDatetime,
  LoadingController,
  ModalController,
} from "@ionic/angular";
import * as moment from "moment";
import { Handlebar } from "../components/handlebar";
import { AuthService } from "../service/auth.service";
import { LocalStorageService } from "../service/local-storage.service";
import { ToastServiceData } from "../service/toast-service";
import { Location } from "@angular/common";
import { SelectProviderPage } from "../select-provider/select-provider.page";

@Component({
  selector: "app-add-patient",
  templateUrl: "./add-patient.page.html",
  styleUrls: ["./add-patient.page.scss"],
})
export class AddPatientPage implements OnInit {
  notes: any = "";
  careCenters: any = "";
  selectedCareCenter: any = [];
  enquiryTypes: any = "";
  enquiryType: any = "";
  mobileNumber: any = "";
  email: any = "";
  dob: any;
  dobValid: boolean = true;
  gender: any = "";
  patientName: any = "";
  ReferralType: any = "";
  enquirySubTypes: any = "";
  enquirerName: any = "";
  enquirySubType: any = "";
  enquirySources: any = "";
  enquirySource: any = "";
  enquirerProviderId: any = 0;
  careCenterId: any = 0;
  consultants: any = "";
  exProviders: any = "";
  pageLoad: any;
  returnType: boolean = true;
  patientNameValid: boolean = true;
  referralTypeValid: boolean = true;
  enquiryTypeValid: boolean = true;
  careCenterValid: boolean = true;
  mobileNumberValid: boolean = true;
  maxData: any = moment().format("YYYY-MM-DD");
  genderValid: boolean = true;
  activities: any = "";
  campaignName: any = "";
  enquiryItem: any = "";
  orgId: any = "";
  orgPref: any;
  emailValid: boolean = true;
  // startDate: any;
  dateValue: any;

  @ViewChild(IonDatetime) datetime: IonDatetime;
  showModal = true;
  sourceMrn: any;
  mandatoryFields: any;
  sourceMrnValid = true;
  isMrnMandatory: any;

  constructor(
    public authService: AuthService,
    private handlebar: Handlebar,
    private modalCtrl: ModalController,
    private toast: ToastServiceData,
    private router: Router,
    private loadingController: LoadingController,
    public localstorage: LocalStorageService,
    private location: Location
  ) {}

  ngOnInit() {
    this.getOrgMasterData();
    this.getAllCareCenter();
    this.getEnquiryTypes();
    this.getExProviders();
    this.getSources();
    this.getActivities();
    this.isFieldMandatory();
  }

  getOrgMasterData() {
    this.authService.getProviderInfo().then((data) => {
      this.orgId = data.organizationId;
      this.getOrgPreferences();
    });
  }
  getOrgPreferences() {
    this.authService.getOrgPreferences().then((data) => {
      this.orgPref = data;
    });
  }
  getAllCareCenter() {
    this.authService.getAllCareCenter().then((data) => {
      this.careCenters = data;
    });
  }
  getEnquiryTypes() {
    this.authService.getEnquiryTypes().then((data) => {
      if (data.enquiryTypes) {
        this.enquiryTypes = data.enquiryTypes;
      } else {
        this.enquiryTypes = data;
      }
    });
  }

  getActivities() {
    this.authService.getActivities().then((data) => {
      if (data.activities) {
        this.activities = data.activities;
      } else {
        this.activities = data;
      }
    });
  }

  enquiryTypeOnChange(value) {
    this.enquirerProviderId = 0;
    this.enquirerName = "";
    if (this.handlebar.isNotEmpty(value)) {
      this.authService.getEnquirySubTypes(value).then((data) => {
        this.enquirySubTypes = data;
      });
    } else {
      this.enquirySubTypes = null;
    }
  }
  getSources() {
    this.authService.getSources().then((data) => {
      this.enquirySources = data;
    });
  }
  onSubTypeChangeMe() {
    let enquiryType = this.enquiryType.trim();
    let enquirySubType = this.enquirySubType.trim();
    this.enquirySources = [];
    this.enquirySource = "";

    if (
      enquirySubType &&
      (enquirySubType.toLowerCase() == "staff" ||
        enquirySubType.toLowerCase() == "consultant" ||
        enquirySubType.toLowerCase() == "associate network")
    ) {
      if (enquirySubType.toLowerCase() === "staff") {
        this.authService.getAllStaff().then((data) => {
          data.forEach((element) => {
            this.enquirySources.push({ reference: element.fullname });
          });
        });
      }
      if (enquirySubType.toLowerCase() === "consultant") {
        this.authService.getAllConsultants().then((data) => {
          data.forEach((element) => {
            this.enquirySources.push({ reference: element.fullname });
          });
        });
      }
      if (enquirySubType.toLowerCase() === "associate network") {
        this.authService.getExProviders().then((data) => {
          data.forEach((element) => {
            this.enquirySources.push({ reference: element.provider_name });
          });
        });
      }
    } else {
      if (
        enquiryType.toLowerCase() === "staff" ||
        enquiryType.toLowerCase() === "consultant" ||
        enquiryType.toLowerCase() === "associate network" ||
        enquiryType.toLowerCase() === "patient referral" ||
        enquiryType.toLowerCase() === "external physician" ||
        enquiryType.toLowerCase() === "hospital"
      ) {
        enquiryType = "all";
      }
      this.authService
        .getSourcesBySubType(enquiryType, enquirySubType)
        .then((data) => {
          if (data.enquiry_sub_type) {
            this.enquirySources = data.reference;
          } else {
            this.enquirySources = data;
          }
        });
    }
  }
  onSubTypeChangeTriggerMe(type) {
    if (type === "Consultant") {
      this.enquirerProviderId = this.enquiryItem.provider_id;
      this.enquirerName = this.enquiryItem.fullname;
    } else {
      this.enquirerProviderId = this.enquiryItem.external_provider_id;
      this.enquirerName = this.enquiryItem.provider_name;
    }
  }
  getAllConsultants() {
    this.authService.getAllConsultants().then((data) => {
      this.consultants = data;
    });
  }

  getExProviders() {
    this.authService.getExProviders().then((data) => {
      this.exProviders = data;
    });
  }

  async navigateToSelectModel(type) {
    const data0 = {
      ccid: this.careCenterId,
      type: type,
    };
    const modal = await this.modalCtrl.create({
      component: SelectProviderPage,
      componentProps: data0,
    });
    modal.onDidDismiss().then((data) => {
      const isUpdate = data.role; // Here's your selected user!
      if (isUpdate) {
        if (type === "Staff") {
          this.enquirerProviderId = data.data.selectedEmpId;
          this.enquirerName = data.data.selectedValue;
          this.enquirySubType = data.data.selectedValue;
        }
      }
    });
    return await modal.present();
  }

  onChangeCareCenter() {
    this.careCenterId = this.selectedCareCenter.care_center_id;
  }

  async presentLoading() {
    this.pageLoad = await this.loadingController.create({
      message: "Please wait...",
      duration: 2000,
    });
    await this.pageLoad.present();
  }

  dismiss() {
    this.modalCtrl.dismiss();
    this.location.back();
  }

  isFieldMandatory() {
    this.authService.getAllEMRCustomFields().then((data) => {
      this.mandatoryFields = data;
      this.isMrnMandatory =
        this.mandatoryFields["patient.sourceMrn5742"].mandetory;
    });
  }

  doValidation() {
    this.returnType = true;
    this.patientNameValid = true;
    this.referralTypeValid = true;
    this.mobileNumberValid = true;
    this.enquiryTypeValid = true;
    this.careCenterValid = true;
    this.genderValid = true;
    this.emailValid = true;
    this.sourceMrnValid = true;

    if (this.handlebar.isEmpty(this.patientName)) {
      this.returnType = false;
      this.patientNameValid = false;
    }
    if (
      this.handlebar.isEmpty(this.mobileNumber) &&
      this.handlebar.mobileValidator(this.mobileNumber) !== "valid"
    ) {
      this.returnType = false;
      this.mobileNumberValid = false;
    }
    if (
      this.handlebar.isNotEmpty(this.email) &&
      this.handlebar.emailValidator(this.email) !== "valid"
    ) {
      this.returnType = false;
      this.emailValid = false;
    }
    if (this.handlebar.isEmpty(this.selectedCareCenter)) {
      this.returnType = false;
      this.careCenterValid = false;
    }
    if (this.handlebar.isEmpty(this.gender)) {
      this.returnType = false;
      this.genderValid = false;
    }
    if (this.handlebar.isEmpty(this.enquiryType)) {
      this.returnType = false;
      this.enquiryTypeValid = false;
    }

    if (
      this.mandatoryFields &&
      this.mandatoryFields["patient.sourceMrn5742"] &&
      this.mandatoryFields["patient.sourceMrn5742"].mandetory &&
      this.handlebar.isEmpty(this.sourceMrn)
    ) {
      this.returnType = false;
      this.sourceMrnValid = false;
    } else if (
      this.handlebar.isNotEmpty(this.sourceMrn) &&
      (this.sourceMrn.length < 5 || this.sourceMrn.length > 31)
    ) {
      this.returnType = false;
      this.sourceMrnValid = false;
    }

    if (this.sourceMrnValid && this.handlebar.isNotEmpty(this.sourceMrn)) {
      this.authService
        .checkForUnique(this.sourceMrn, "source_mrn")
        .then((returnObj) => {
          if (returnObj && returnObj.isAvailable) {
            this.returnType = false;
            this.sourceMrnValid = false;
            this.toast.presentToast("The Source Mrn is already present.");
          }
        });
    }

    return this.returnType;
  }

  savePatientInfo() {
    this.presentLoading().then(() => {
      this.pageLoad.message = "validating Data...";
      const returnType = this.doValidation();
      if (returnType) {
        this.pageLoad.message = "Saving Patient...";
        // let defaultDateFormatter = 'DD-MM-YYYY';
        // if (this.orgPref) {
        //   defaultDateFormatter = this.orgPref.defaultDateFormatter.toUpperCase();
        // }
        if (this.dob) {
          this.dob = moment(this.dob).format("DD-MM-YYYY");
        }
        let patientData = {
          businessUnit: {
            name: this.selectedCareCenter.display_name,
          },
          referrerInfo: {
            name: this.enquirerName,
            source: this.enquirySource,
            type: this.enquiryType,
            subType: this.enquirySubType.trim(),
            campaignName: this.campaignName,
          },
          providerId: this.enquirerProviderId,
          email: this.email,
          name: {
            givenName: this.patientName,
          },
          telephone: [
            {
              use: "primary",
              number: this.mobileNumber,
              countryCode: 91,
            },
          ],
          gender: this.gender,
          privateNote: this.notes,
          birthDate: this.dob,
          sourceMrn: this.sourceMrn,
        };
        this.authService.savePatientInfo(patientData).then((data) => {
          if (
            data &&
            (data.status === 400 || data.status === 401 || data.status === 500)
          ) {
            const data0 = JSON.parse(data._body);
            this.toast.presentToast(data0.description);
          } else if (data) {
            this.dismiss();
            this.toast.presentToast("Patient Added Successfully.");
          }
          this.pageLoad.dismiss();
        });
      } else {
        this.pageLoad.dismiss();
        this.toast.presentToast(
          "Please Fill Mandatory Fields / Add Valid details."
        );
      }
    });
  }
  dateChange(value) {
    this.dob = value;
    this.dateValue = moment(value).format("DD-MM-YYYY");
    this.showModal = false;
  }
}
