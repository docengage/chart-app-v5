import { Component, OnInit, ViewChild } from "@angular/core";
import { Location } from "@angular/common";
import { Handlebar } from "../components/handlebar";
import { ToastServiceData } from "../service/toast-service";
import { AuthService } from "../service/auth.service";
import { IonDatetime, ModalController } from "@ionic/angular";
import { SelectPatientPage } from "../select-patient/select-patient.page";
import { RequestedServicesPage } from "../requested-services/requested-services.page";
import * as moment from "moment";
import { SelectProviderPage } from "../select-provider/select-provider.page";

@Component({
  selector: "app-quick-schedule",
  templateUrl: "./quick-schedule.page.html",
  styleUrls: ["./quick-schedule.page.scss"],
})
export class QuickSchedulePage implements OnInit {
  careCenterId = 0;
  selectedCareCenter: any = [];
  careCenters: any;
  patientUHID: any;
  patientInfo: any;
  visitType: any;
  provData: any;
  providerId: any;
  quickEnableVC = false;
  serviceName: any;
  serviceId: any;
  appointmentNote: any;
  notify: any;
  appointmentDuration: any;
  quickBookApptStatus: any;
  formatedDate: string;
  presentation: any;
  fieldName: any;
  showModal: boolean;
  appointmentTime = moment().format();
  appointmentDate = moment().format();
  checkedInOutTimeLabel = ''

  @ViewChild(IonDatetime) datetime: IonDatetime;
  restrictBackAppt: any;
  checkedInOutTime: any;
  providerName: any;
  
  constructor(private location: Location,
    private handlebar: Handlebar,
    private toastServ: ToastServiceData,
    private authService: AuthService,
    private modalCntrl: ModalController) {}

  ngOnInit() {
    this.getAllCareCenter();
    this.getAllProvByOrgId();
    this.getCalendarSettings();
  }

  getAllCareCenter() {
    this.authService.getAllCareCenter().then((data) => {
      this.careCenters = data;
    });
  }

  onChangeCareCenter() {
    this.careCenterId = this.selectedCareCenter.care_center_id;
    this.providerId = '';
    this.providerName = '';
  }

  async openPatientList(){
    const data = {
      pageType: 'quickSchedule'
    };

    const patientModal = await this.modalCntrl.create({
      component: SelectPatientPage,
      componentProps: data
    });
    await patientModal.present();
    patientModal.onDidDismiss().then((data0) => {
      if (data0 && data0.role){
        this.patientInfo = data0.data;
      }
    });
  }

  getAllProvByOrgId() {
    this.authService.getAllProvByOrgId().then((data) => {
      this.provData = JSON.parse(JSON.stringify(data));
    });
  }

  async navigateToSelectModel(type) {
    const data0 = {
      ccid: this.careCenterId,
      type: type
    };
    const modal = await this.modalCntrl.create({
      component: SelectProviderPage,
      componentProps: data0
    });
    modal.onDidDismiss().then((data) => {
      const isUpdate = data.role; // Here's your selected user!
      if (isUpdate) {
        if (type === 'Staff') {
          this.providerId = data.data.selectedId;
          this.providerName = data.data.selectedValue;
        }
      }
    });
    return await modal.present();
  }

  async openServiceList(){
    const data = {
      careCenterId : this.careCenterId
    };
    const serviceModal = await this.modalCntrl.create({
      component: RequestedServicesPage,
      componentProps: data
    });
    await serviceModal.present();
    serviceModal.onDidDismiss().then((data0) => {
      if (data0 && data0.role){
        this.serviceName = data0.data.service_name;
        this.serviceId = data0.data.health_service_template_id;
      }
    });
  }

  setDateModal(value, type, fieldName) {
    this.formatedDate = value
      ? moment(value).format()
      : moment().format();
    this[fieldName] = value ? value : moment().format("YYYY-MM-DD");
    this.presentation = type;
    this.fieldName = fieldName;
    this.showModal = true;
  }

  dateChange(value) {
    this[this.fieldName] = value ? moment(value).format() : moment().format();
    this.dateCancel();
  }

  dateCancel() {
    if (this.datetime) {
      this.showModal = false;
    }
  }

  getCalendarSettings() {
    this.authService.getCalendarSettings().then((data) => {
      this.restrictBackAppt = data.restrictBackDatedAppointment;
    });
  }

  checkBackDatedAppointment(startTime, global) {
    let scheduleDate = moment(startTime, "DD-MM-YYYY").format("YYYY-MM-DD");
    let today = moment().format("YYYY-MM-DD");
  
    if (this.restrictBackAppt && (scheduleDate < today)) {
      return global + " Back Dated Appointment not Allowed.<br/>";
    }
    return global;
  }

  onChangeStatus(event) {
    if(event === "CheckedIn"){
      this.checkedInOutTimeLabel = 'In Time';
    } else if (event === "Completed") {
      this.checkedInOutTimeLabel = 'Out Time';
    } else {
      this.checkedInOutTimeLabel = '';
    }
  }

  saveScheduleInfo() {
    let global = "";
    
    if (this.patientInfo){
      if(this.handlebar.isEmpty(this.patientInfo.localPatientId) || (Number(this.patientInfo.localPatientId) <= 0)) {
        global = global + " Select a Patient.<br/>";
      }
    } else {
      global = global + " Select a Patient.<br/>";
    }
    
    if(this.handlebar.isEmpty(this.quickBookApptStatus)){
      global = global + " Please select status.<br/>";
    }

    if(this.handlebar.isEmpty(this.careCenterId) || (Math.ceil(this.careCenterId) <= 0)){
      global = global + " Select a  Care Center .<br/>";
    }
    if(this.handlebar.isEmpty(this.visitType)){
      global = global + " Select a Visit Type.<br/>";
    }			
    if(this.handlebar.isEmpty(this.providerId)){
      global = global + " Select a Provider.<br/>";
    }	
    
    if(this.handlebar.isEmpty(this.appointmentDuration) || Number(this.appointmentDuration) <= 5){
      global = global + " Appointment Duration must be greater than 5 mins.<br/>";
    }	
    
    if(this.handlebar.isEmpty(this.serviceId)){
      global = global + " Select a Service.<br/>";
    }
    
    var appointmentDateTime = "";
    var isDateValid = this.handlebar.checkdate(moment(this.appointmentDate).format("DD-MM-YYYY"));
    if(isDateValid){
      global = this.checkBackDatedAppointment(moment(this.appointmentDate).format("DD-MM-YYYY"), global);
      var validTime = this.appointmentTime && moment(this.appointmentTime).format("hh:mm A").match(/^(0?[1-9]|1[012])(:[0-5]\d) [APap][mM]$/);
          if (!validTime) {
            var error="Enter valid time.<br>";
            global=global+error; 
          } else {
            appointmentDateTime = moment(this.appointmentDate).format("DD-MM-YYYY")+" "+moment(this.appointmentTime).format("hh:mm A");
          }
          appointmentDateTime = moment(appointmentDateTime, 'DD-MM-YYYY hh:mm A').format('DD-MM-YYYY HH:mm');
    }else{
      var error="Please select Date & Time<br>";
      global=global+error; 
    }
    
    
    var appointmentCheckedInOutDateTime = "";
    if(this.quickBookApptStatus==="CheckedIn" || this.quickBookApptStatus==="Engage" || this.quickBookApptStatus==="Completed"){
      var isDateValid = this.handlebar.checkdate(moment(this.appointmentDate).format("DD-MM-YYYY"));
      if(isDateValid){
        global = this.checkBackDatedAppointment(moment(this.appointmentDate).format("DD-MM-YYYY"),global);
        var validTime = this.checkedInOutTime && moment(this.checkedInOutTime).format("hh:mm A").match(/^(0?[1-9]|1[012])(:[0-5]\d) [APap][mM]$/);
            if (!validTime) {
                var error="Enter valid time.<br>";
                global=global+error; 
            } else {
                appointmentCheckedInOutDateTime = moment(this.appointmentDate).format("DD-MM-YYYY")+" "+moment(this.checkedInOutTime).format("hh:mm A");
            }
            appointmentCheckedInOutDateTime = moment(appointmentCheckedInOutDateTime, 'DD-MM-YYYY hh:mm A').format('DD-MM-YYYY HH:mm');
      }else{
        var error="Enter valid "+this.quickBookApptStatus+" time.<br>";
              global=global+error;
      }
      
      if(appointmentCheckedInOutDateTime < appointmentDateTime){
        var error=this.quickBookApptStatus+" time Should be greater than Start Time.<br>";
        global=global+error; 
      }
      
    }
    
    if (global){
      this.toastServ.presentToast(global);
      return false;
    }

    const apptData = {
      eventId: 0,
      careCenterId: this.careCenterId,
      plannedCareItemId: 0,
      carePlanItemId: 0,
      patientUHID: this.patientInfo.patientUhid,
      patientId: this.patientInfo.localPatientId,
      visitType: this.visitType,
      providerId: this.providerId,
      serviceId: this.serviceId,
      dateTime: appointmentDateTime,
      preConditions: '',
      appointmentNote: this.appointmentNote,
      notifyPatient: this.notify ? this.notify.includes('Patient') : false,
      notifyProvider: this.notify ? this.notify.includes('Provider') : false,
      duration: this.appointmentDuration,
      status: this.quickBookApptStatus,
      checkedInOutTime: appointmentCheckedInOutDateTime,
      enableVC: this.quickEnableVC,
      sourcePage: "SingleAppointment",
    };

    this.authService.saveApptInfo(apptData).then((data) => {
      if (
        data &&
        (data.status === 400 || data.status === 401 || data.status === 500)
      ) {
        const data0 = JSON.parse(data._body);
        this.toastServ.presentToast(data0.description);
      } else {
        this.dismiss();
        this.toastServ.presentToast("Appoinment added Successfully.");
      }
    });
  }

  dismiss() {
    this.location.back();
  }
}
