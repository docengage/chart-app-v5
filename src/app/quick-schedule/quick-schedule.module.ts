import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QuickSchedulePageRoutingModule } from './quick-schedule-routing.module';

import { QuickSchedulePage } from './quick-schedule.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QuickSchedulePageRoutingModule
  ],
  declarations: [QuickSchedulePage]
})
export class QuickSchedulePageModule {}
