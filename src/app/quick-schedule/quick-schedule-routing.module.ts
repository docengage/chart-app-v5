import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuickSchedulePage } from './quick-schedule.page';

const routes: Routes = [
  {
    path: '',
    component: QuickSchedulePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuickSchedulePageRoutingModule {}
