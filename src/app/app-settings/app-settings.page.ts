import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { LocalStorageService } from '../service/local-storage.service';
import { ToastServiceData } from '../service/toast-service';

@Component({
  selector: 'app-app-settings',
  templateUrl: './app-settings.page.html',
  styleUrls: ['./app-settings.page.scss'],
})
export class AppSettingsPage implements OnInit {
  constructor(
    public navParams: NavParams,
    public viewCtrl: ModalController,
    private localstorage: LocalStorageService, public toastService: ToastServiceData
  ) {
    if (this.navParams.get('env') && this.navParams.get('env') != null) {
      this.appBaseUrl = this.navParams.get('env');
      this.getCustomURLValue();
      this.onChangeENV();
    }
  }

  appBaseUrl = 'environment';
  isCustomUrl = false;
  customURL = '';


  ngOnInit() {
  }
  getCustomURLValue() {
    this.localstorage.getCustomURL().then((customURL) => {
      this.customURL = customURL;
    });
  }
  onChangeENV() {
    if (this.appBaseUrl === 'customEnv') {
        this.isCustomUrl = true;
    } else {
        this.isCustomUrl = false;
    }
  }
  isValidURL(customURL) {
    const pattern = new RegExp('^((http?:)?\\/\\/)?' + // protocol
        '([a-zA-Z+]+[0-9+:?]+)|([0-9+]+[a-zA-Z+]+)$');

    if (!pattern.test(customURL)) {
        return false;
    } else {
      if (customURL.substring(customURL.length-1) == "/"){
        return true;
      }else{
        return false;
      }
    }
  }
  saveEnvChanges() {
    if (this.isCustomUrl) {
      const returnVal = this.isValidURL(this.customURL);
      if (returnVal) {
        this.localstorage.setCustomURL(this.customURL);
        this.dismiss(true);
      } else {
        this.toastService.presentToast('Invalid Custom URL');
      }

    } else {
      this.localstorage.setCustomURL('');
      this.dismiss(true);
    }
  }


  dismiss(isupdated) {
    if (isupdated) {
      this.localstorage.setEnvironment(this.appBaseUrl);
    }
    // using the injected ViewController this page
    // can "dismiss" itself and pass back data
    this.viewCtrl.dismiss(isupdated);
  }
}
