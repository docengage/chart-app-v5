import { AfterViewChecked, Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { ToastServiceData } from '../service/toast-service';
import { Handlebar } from '../components/handlebar';
import { MasterDataService } from '../service/master-data';
import { ModalController } from '@ionic/angular';
import * as moment from 'moment';
import { TimeLineDataPushService } from '../service/timeline-auth-service';
import { TaskItemDetailsPage } from '../task-item-details/task-item-details.page';
import { ListFilterPage } from '../list-filter/list-filter.page';
import { LocalStorageService } from '../service/local-storage.service';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.page.html',
  styleUrls: ['./schedule.page.scss'],
})
export class SchedulePage implements OnInit, AfterViewChecked {

  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;

  totalPages: number;
  currentPage = 1;

  apptSec = 'today';
  fromDate: string;
  toDate: string;
  noRecordFound: string;
  queryText: string;
  isShowSearchBar = false;
  searchVal: string;
  timer: any;
  statusValue: any;
  constructor(
              private authService: AuthService,
              private toastServ: ToastServiceData,
              private handlebar: Handlebar,
              private timelineAuthServ: TimeLineDataPushService,
              private masterDataServ: MasterDataService,
              private modalCtrl: ModalController,
              private localstorage: LocalStorageService) {}
  ngOnInit(): void {
    this.items = [];
    this.totalPages = 0;
    this.currentPage = 1;
    this.authService.isTokenValid().then((returnVal) => {
      this.getFilterData();
      // this.getTodayApptList();
    });
  }

  ngAfterViewChecked() {
    if(document.getElementById('tab-button-schedule')){
      document.getElementById('tab-button-schedule').classList.add('tab-selected');
    }
    if(document.getElementById('tab-button-myCharts')){
      document.getElementById('tab-button-myCharts').classList.remove('tab-selected');
    }
    if(document.getElementById('tab-button-allPatient')){
      document.getElementById('tab-button-allPatient').classList.remove('tab-selected');
    }
    if(document.getElementById('tab-button-leaveList')){
      document.getElementById('tab-button-leaveList').classList.remove('tab-selected');
    }
  }

  searchData() {
      clearTimeout(this.timer);
      if (this.searchVal !== this.queryText) {

        const temp = this;
        this.timer = setTimeout(function() {
          temp.searchVal = temp.queryText;
          temp.updateSchedule();
        }, 400);
      }
  }
  getFilterData(){
    this.localstorage.getActivityFilter().then((filterdata) => {
      if (filterdata){
        this.statusValue = filterdata.statusValue;
        this.apptSec = (filterdata.apptSec === 'all' || this.handlebar.isEmpty(filterdata.apptSec)) ? 'today' : filterdata.apptSec;
        if (this.apptSec === 'customDate') {
          this.fromDate = filterdata.fromDate;
          this.toDate = filterdata.toDate;
        }
        this.updateSchedule();
      } else {
        this.getTodayApptList();
      }
      
    });
  }

  async filter() {
    let data0 = {};
    
    if (this.apptSec === 'customDate') {
      data0 = {
        pageType: 'Activity',
        statusData: this.statusValue,
        dateRange: this.apptSec,
        fromDate: this.fromDate,
        toDate: this.toDate
      };
    } else {
      data0 = {
        pageType: 'Activity',
        statusData: this.statusValue,
        dateRange: this.apptSec,
      };
    }
    const modal = await this.modalCtrl.create({
      component: ListFilterPage,
      componentProps: data0
    });
    
    await modal.present();
    modal.onDidDismiss().then((data) => {
      const isUpdate = data.role;
      if (isUpdate) {
        this.statusValue = data.data.statusData;
        this.apptSec = data.data.dateRange;
        let filterdata = {};

        if (this.apptSec === 'customDate') {
          this.fromDate = data.data.fromDate;
          this.toDate = data.data.toDate;
          filterdata = {
            statusValue: data.data.statusData,
            apptSec: data.data.dateRange,
            fromDate: this.fromDate,
            toDate: this.toDate
          };
        } else {
          filterdata = {
            statusValue: data.data.statusData,
            apptSec: data.data.dateRange,
          };
        }
        
        this.localstorage.setActivityFilter(filterdata);
        this.updateSchedule();
      }
    });
  }

  updateSchedule() {
    if (this.apptSec === 'recent') {
      this.getRecentApptList();
    } else if (this.apptSec === 'today') {
      this.getTodayApptList();
    } else if (this.apptSec === 'upcoming'){
      this.getAllApptList();
    } else {
      this.getCustomApptList();
    }
  }
  getTodayApptList() {
    this.fromDate =  moment().format('YYYY-MM-DD');
    this.toDate =  moment().format('YYYY-MM-DD');
    this.totalPages = 0;
    this.currentPage = 1;
    this.getAppointments(true);

  }
  getRecentApptList() {
    this.fromDate = moment().subtract(7, 'day').format('YYYY-MM-DD');
    this.toDate =  moment().subtract(1, 'day').format('YYYY-MM-DD');
    this.totalPages = 0;
    this.currentPage = 1;

    this.getAppointments(true);
  }

  getAllApptList() {
    this.fromDate =   moment().add(1, 'day').format('YYYY-MM-DD');
    this.toDate =  moment().add(7, 'day').format('YYYY-MM-DD');
    this.currentPage = 1;
    this.totalPages = 0;
    this.getAppointments(true);
  }

  getCustomApptList() {
    this.currentPage = 1;
    this.totalPages = 0;
    this.getAppointments(true);
  }

  getAppointments(isEmpty) {
    if (this.statusValue) {
    } else {
      this.statusValue = 'all';
    }

    this.authService.getMyTaskListService(this.fromDate, this.toDate, this.queryText, this.currentPage, this.statusValue).then((data) => {
      if (isEmpty) {
        this.items = [];
        this.totalPages = 0;
        this.currentPage = 1;
      }
      if (data && data.appointments) {
        if (data.appointments) {
          for (let i = 0; i < data.appointments.length; i++) {
            this.items.push(data.appointments[i]);
          }
        }
        this.totalPages = data.total_page;
      }
      let recordName = "Activity"
      this.noRecordFound = this.toastServ.displayNoRecordsFound(this.items,recordName);
    });

  }


  viewPatientTimeLine(uhid) {
    if (uhid) {
      this.timelineAuthServ.viewPatientTimeLine(uhid);
    }
  }


  async taskItemSelected(apptId) {
    let data0 = {
      "apptId" : apptId
    };
    let modal = await this.modalCtrl.create({
      component: TaskItemDetailsPage,
      componentProps: data0
    });
    await modal.present();

    modal.onDidDismiss().then((data) => {

      if(data.role){
        this.updateSchedule();
      }
    });
  }

  doInfinite(infiniteScroll) {
    var clientHeight = infiniteScroll.target.clientHeight;
    var scrollUpto = infiniteScroll.target.scrollHeight - infiniteScroll.target.scrollTop;
    if (clientHeight === scrollUpto) {
      setTimeout(() =>  {
        setTimeout(() => {
          this.currentPage = Number(this.currentPage) + 1;
          if (this.currentPage <= this.totalPages) {
            this.getAppointments(false);
          }
          infiniteScroll.target.complete();
        }, 500);
      });
    }
  }

  checkPatientHasImage(imagePath) {
    return this.handlebar.checkImagePath(imagePath);
  }
  doRefresh(event) {
    this.updateSchedule();
    setTimeout(() => {
      event.target.complete();
      this.toastServ.presentToast('Tasks List Refreshed');
    }, 1000);
  }

  updateStatus(status, plannedCareItemId) {
    this.authService.updateApptStatus(status, plannedCareItemId, 'event').then(() => {
      this.getAppointments(true);

    });
  }
}
