import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JoinVcPage } from './join-vc.page';

describe('JoinVcPage', () => {
  let component: JoinVcPage;
  let fixture: ComponentFixture<JoinVcPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinVcPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JoinVcPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
