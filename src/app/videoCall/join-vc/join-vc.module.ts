import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JoinVcPageRoutingModule } from './join-vc-routing.module';

import { JoinVcPage } from './join-vc.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JoinVcPageRoutingModule
  ],
  declarations: [JoinVcPage]
})
export class JoinVcPageModule {}
