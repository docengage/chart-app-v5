import { Component, OnInit, ViewChild } from '@angular/core';
import { NavigationExtras, ActivatedRoute } from '@angular/router';
import { from } from 'rxjs';
import { GestureController, LoadingController, ModalController, NavController, NavParams } from '@ionic/angular';
import { HttpClient } from 'src/app/service/HttpClient';
import { ToastServiceData } from 'src/app/service/toast-service';
import { AuthService } from 'src/app/service/auth.service';
import { VideoCallPage } from '../video-call/video-call.page';
import { Handlebar } from 'src/app/components/handlebar';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { DomSanitizer } from '@angular/platform-browser';


declare var window;
/* To try the app with Enablex hosted service you need to set the kTry = true */
const kTry = true;
/*Your webservice host URL, Keet the defined host when kTry = true */
const kBasedURL = 'https://demo.enablex.io/';
/*The following information required, Only when kTry = true, When you hosted your own webservice remove these fileds*/

/*Use enablec portal to create your app and get these following credentials*/
const kAppId = '5fc9bf1e29c90260b85d50b2';
const kAppkey = 'umyhaWyha6eGeRuXeuugaBa9ysemaNe5eqyT';
@Component({
  selector: 'app-join-vc',
  templateUrl: './join-vc.page.html',
  styleUrls: ['./join-vc.page.scss'],
})
export class JoinVcPage implements OnInit {

  userName: string = '';
  roomID: string = '';
  kAppId = '';
  kAppkey = '';
  errorMsg = '';
  vendorName: string = '';
  meetingUrl = '';
  sanitizedURL: any = '';
  constructor(public navCont: NavController,
              public httpClient: HttpClient,
              public toastServ: ToastServiceData,
              private route: ActivatedRoute,
              private navParam: NavParams,
              private modalCtrl: ModalController,
              private auth: AuthService,
              private handlebar: Handlebar,
              private inAppbwsr: InAppBrowser,
              private gestureCtrl: GestureController,
              private loadingCtrl: LoadingController,
              private domSantizer: DomSanitizer,) { }

  ngOnInit() {
    this.roomID = this.navParam.get('roomId');
    this.userName = this.navParam.get('name');
    this.vendorName = this.navParam.get('vendorName');
    this.meetingUrl = this.navParam.get('meetingUrl');
    this.sanitizedURL = this.domSantizer.bypassSecurityTrustResourceUrl(this.meetingUrl);
    // this.route.queryParams.subscribe(params => {
    //   this.roomID = params.roomId;
    //   this.userName = params.name;
    // });
    this.auth.getVideoCommunicationConfig().then((data) => {
      if (data && data !== 'false') {
        this.kAppId = data[0].clientid;
        this.kAppkey = data[0].clientsecret;
      } else {
        this.toastServ.presentToast('Invalid Video credentials... contact your service provider');
      }
    });
    window.joinVC  = this;
  }

  getSanitizedURL() {
    return this.sanitizedURL;
  }

  dragGesture() {
    const vcNode = document.getElementById('videoCallModal');
    const joinVCMOdal = document.getElementById('joinVcModal');
    const vcDrag = this.gestureCtrl.create({
      el: vcNode,
      gestureName: 'vcDrag',
      onStart: ev => {
        vcNode.style.opacity = 0.8 + '';
      },
      onMove: ev => {
        const positionData = this.isInBound(ev.deltaX, ev.deltaY, joinVCMOdal, vcNode);
        vcNode.style.transform = `translate(${positionData.x}px, ${positionData.y}px)`;
        vcNode.style.opacity = 1 + '';
      }
    });
    vcDrag.enable();
  }

  isInBound(x, y, modalPage, drageedDiv) {
    const pageBoundaries = modalPage.getBoundingClientRect();
    let data = {x: '0', y: '0'};
    if (x < pageBoundaries.left || x >= pageBoundaries.right) {
      data.x = (pageBoundaries.right - drageedDiv.offsetWidth) + '';
    } else {
      data.x = x;
    }
    if (y < pageBoundaries.top || y >= pageBoundaries.bottom) {
      data.y = (pageBoundaries.bottom - drageedDiv.offsetHeight) + '';
    } else {
      data.y = y;
    }
    return data;
  }

  closeModal() {
    let iframeWin = window.document.getElementById("dyteVC").contentWindow;
    iframeWin.postMessage("endMeeting", "*");
    this.modalCtrl.dismiss();
  }

  async joinCall() {
    this.modalCtrl.dismiss();
    const data = {
        token: "dsafasdf",
        name: this.userName,
        role: 'moderator',
        user_ref: 'user_ref',
        roomId: this.roomID
    };
    const startVideoCallModal = await this.modalCtrl.create({
        component: VideoCallPage,
        componentProps: data,
        id: 'videoCallModal',
        cssClass: 'videoModal'

    });
    await startVideoCallModal.present();
  }
  async joinRoom() {
    if (this.roomID.length === 0) {
      this.toastServ.presentToast('Kindly Enter Room ID');
      return;
    }
    // const browser = this.inAppbwsr.create('https://dev.docengage.in/join/meeting?meetingId=' + this.roomID, '_blank');
    // browser.show();
    // return;
    const loading = await this.loadingCtrl.create({
      message: 'Initating video call...',
      cssClass: 'loadingContent'
    });
    loading.present();
    
    const loginString = {
      name: this.userName,
      role: 'moderator',
      user_ref: '2236',
      roomId: this.roomID
    };
    const hedare = (kTry) ?
                    {
                      'x-app-id': this.kAppId,
                      'x-app-key': this.kAppkey,
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                    } :
                    { 'Accept': 'application/json',
                      'Content-Type': 'application/json'
                    };

    let url = kBasedURL + 'createToken';
    console.log('Url Faired' + url);
    this.httpClient.postEnablex(url, loginString, { headers: hedare }).subscribe(async data => {
      loading.dismiss();
      const resltData = JSON.parse(data['_body']);
      console.log(resltData);
      if (this.handlebar.isEmpty(resltData.token) || resltData.token === undefined) {
        this.errorMsg = resltData.error;
        return false;
      } else {
        this.errorMsg = '';
      }
      // const navigationExtras: NavigationExtras = {
      //   queryParams: {
      //     token: resltData.token,
      //     name: this.userName,
      //     role: 'participant',
      //     user_ref: 'user_ref'
      //   }
      // };
      // this.navCont.navigateForward(['video-call'], navigationExtras);
      const data0 = {
        token: resltData.token,
        name: this.userName,
        role: 'moderator',
        user_ref: 'user_ref'
      };
      const startVideoCallModal = await this.modalCtrl.create({
          component: VideoCallPage,
          componentProps: data0,
          id: 'videoCallModal',
          cssClass: 'videoModal'
      });
      await startVideoCallModal.present();
    }, error => {
      loading.dismiss();
      alert(error);
      console.log('Hello service failed');
      console.log(error);
    });
    console.log('ClickEvent Join event');
  }

  testMicroPhone() {

  }

  testCemara() {

  }

  switchCemera() {

  }

  testSpeaker() {

  }

}
