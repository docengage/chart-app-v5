import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JoinVcPage } from './join-vc.page';

const routes: Routes = [
  {
    path: '',
    component: JoinVcPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JoinVcPageRoutingModule {}
