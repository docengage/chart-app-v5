import { Location } from '@angular/common';
import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, LoadingController, ModalController, NavController, NavParams, Platform } from '@ionic/angular';
import { interval } from 'rxjs';
import { Handlebar } from 'src/app/components/handlebar';
import { AuthService } from 'src/app/service/auth.service';
import { ToastServiceData } from 'src/app/service/toast-service';
import { VideoChatPage } from '../video-chat/video-chat.page';

declare var window: any;
export interface TimeSpan {
  hours: number;
  minutes: number;
  seconds: number;
}
export interface Entry {
  created: Date;
  id: string;
}
@Component({
  selector: 'app-video-call',
  templateUrl: './video-call.page.html',
  styleUrls: ['./video-call.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class VideoCallPage implements OnInit {

  audioimgSrc = '../../../assets/icon/unmute_audio.png';
  videoimgSrc = '../../../assets/icon/unmute_video.png';
  speakerImgSrc = '../../../assets/icon/mute_speaker.png';
  speakerMute: boolean = false;
  audioMute = false;
  videoMute = false;
  videoModalHeight: any;
  shortWindowHeight: any;
  roomId: any;
  conf_duration: any = 0;
  aboutToEnd: any;
  timer: any;
  entries: Entry[] = [];
  messagesData: any;
  timeAlert: boolean;
  name: any;
  role: string;

  constructor(private route: ActivatedRoute, private navCtrl: NavController,
              private authService: AuthService,
              private modalCtrl: ModalController,
              private alertCtrl: AlertController,
              private toast: ToastServiceData,
              private loaderCtrl: LoadingController,
              private navParam: NavParams,
              private changeDetector: ChangeDetectorRef,
              public platform: Platform,
              private router: Router,
              private location: Location,
              public handlebar: Handlebar,
    ) { }
  ionViewDidEnter() {
    this.videoModalHeight = document.getElementById('videoCallModal').offsetHeight;
    const height = (this.videoModalHeight * 0.4) + '';
    this.shortWindowHeight = parseInt(height, 0);
    window.pChart.dragGesture();
  }
  ngOnInit() {
    this.timeAlert = false;
    this.messagesData = [];
    this.entries.push({
      created: new Date(),
      id: ''
    });
    interval(1000).subscribe(() => {
      if (!this.changeDetector['destroyed']) {
        this.changeDetector.detectChanges();
      }
    });

    this.clickBackBtn();


    this.changeDetector.detectChanges();
    this.aboutToEnd = true;
    this.roomId = this.navParam.get('roomId');
    this.name = this.navParam.get('name');
    this.role = 'moderator';
    this.timer = '00:00';
    // this.startDuration();
    // this.route.queryParams.subscribe(params => {

      var videoSize = {
        minWidth: 320,
        minHeight: 180,
        maxWidth: 1280,
        maxHeight: 720,
      };
      var streamOpt = {
        audio: true,
        video: true,
        data: true,
        audioOnlyMode: false,
        framerate: 30,
        maxVideoBW: 1500,
        minVideoBW: 150,
        videoSize: videoSize,
        audioMuted: false,
        videoMuted: false,
        maxVideoLayers: 1,
        name: this.navParam.get('name')
      };
      var playerConfiguration = {
        audiomute: true,
        videomute: true,
        bandwidth: true,
        screenshot: true,
        avatar: true,
        iconHeight: 30,
        iconWidth: 30,
        avatarHeight: 200,
        avatarWidth: 200,
      };
      var roomOpt = {
        activeviews: "list",
        allow_reconnect: true,
        number_of_attempts: 3,
        timeout_interval: 15,
        playerConfiguration: playerConfiguration,
        chat_only: false,
      };
    
    window.EnxRtc.joinRoom(this.navParam.get('token'), streamOpt, roomOpt);
    this.addEnxListner();
    // });

  }

  // Init LocalView
  initLocalView() {
    var initLocalViewOptions = {
      height: 130,
      width: 100,
      margin_top: 60,
      margin_left: 0,
      margin_right: 15,
      margin_bottom: 10,
      position: "top"
    };
    window.EnxRtc.initLocalView(initLocalViewOptions, function(data) {
    }, function(err) {
    });
  }
  initRemoteView() {
    const _this = this;
    var initRemoteViewOptions = {
      margin_bottom: 140
    };
    window.EnxRtc.initRemoteView(initRemoteViewOptions, function(data) {
      try {
        const newData = document.getElementById('selected_video_container_div');
        window.EnxRtc.switchMediaDevice('SPEAKER_PHONE', function(data) {
          _this.speakerImgSrc = '../../../assets/icon/unmute_speaker.png';
        }, function(err) {
        });
      } catch (err) {
       // alert(JSON.stringify(err));
      }

      try {
        const dsafData = document.getElementsByClassName('live_stream_div');
      } catch (err) {
       // alert(JSON.stringify(err));
      }
    }, function(err) {
    });
  }
  startCameraBelow() {
    //start small vide0
  }
  clickBackBtn() {
    this.platform.backButton.subscribeWithPriority(10, () => {
      if (this.router.url.toString().includes('video-call')) {
        this.disconnect();
      } else {
        this.location.back();
      }
    });
  }
  pushlogs(desc, type, self, userName, userRole) {
    const pushName = self.handlebar.isNotEmpty(userName) ? userName : self.name;
    const pushRole = self.handlebar.isNotEmpty(userRole) ? userRole : self.role;
    const data = {
      name: pushName,
      role: pushRole,
      description: desc,
      evenType: type
    };
    self.authService.pushLogDetails(self.roomId, data);
  }
  // Add listoner
  addEnxListner() {
    const _this = this;
    window.EnxRtc.addEventListner('onRoomConnected', function(data) {
      _this.initLocalView();
      _this.initRemoteView();
      // this.authService.pushLogDetails(this.roomId, data);
      _this.pushlogs('Room Connected', data.eventType, _this, '' , '');
      const data0 = {role: 'moderator', name: self.name};
      this.authService.pushParicipant(this.roomId, data0);
    });

    window.EnxRtc.addEventListner('onUserConnected', function(data) {
      _this.pushlogs('User Connected', data.eventType, _this, data.data.name, data.data.role);
    });

    window.EnxRtc.addEventListner('onUserAwaited', function(data) {
    //  this.authService.pushLogDetails(this.roomId, data);
    });

    window.EnxRtc.addEventListner('onConnectionLost', async function(data) {
      const alertMsg = await this.alertCtrl.create({
        message: 'Your call was disconnected due to internet issue',
        mode: 'ios'
      });
      await alertMsg.present();
      _this.pushlogs('Your call was disconnected due to internet issue', 'Connection Lost', _this, '', '');
    });

    window.EnxRtc.addEventListner('onRoomDisConnected', function(data) {
      _this.goBackToPriviousPage();
      _this.pushlogs('Your call was disconnected due to internet issue', data.eventType, _this, '', '');
    });

    window.EnxRtc.addEventListner('onEventError', function(data) {
      _this.pushlogs(data.data.msg, data.eventType, _this, '', '');
    });

    window.EnxRtc.addEventListner('onAudioEvent', function(data) {
      const response = data.data;
      if (response.msg === 'Audio Off') {
        _this.audioMute = true;
      } else {
        _this.audioMute = false;
      }
    });

    window.EnxRtc.addEventListner('onVideoEvent', function(data) {
      const response = data.data;
      if (response.msg === 'Video Off') {
        _this.videoMute = true;
        _this.videoimgSrc = '../../../assets/icon/unmute_video.png';
      } else {
        _this.videoMute = false;
        _this.videoimgSrc = '../../../assets/icon/mute_video.png';
      }

    });

    window.EnxRtc.addEventListner('onEventError', function(data) {
    });
    window.EnxRtc.addEventListner('onConferencessExtended', function(data) {
      console.log('onConferencessExtended   ' + JSON.stringify(data));
      alert('Extended for 30min');
     });

    window.EnxRtc.addEventListner('onNotifyDeviceUpdate', function (data) {
      const deviceName = data.data;
      if (deviceName === 'EARPIECE') {
        this.speakerMute = false;
      } else {
        this.speakerMute = true;
      }
    });

    window.EnxRtc.addEventListner('onConferenceRemainingDuration',async function(data) {
      const timeLeft = data.data.msg.timeLeft;
      if (timeLeft <= 10 ) {
        this.aboutToEnd = true;
      }
    });
    window.EnxCordovaPlugin.addEventListner('onMessageReceived', function(data) {
      this.messagesData.push(data);
      this.msgRecieved(data);
    });
    window.EnxRtc.addEventListner('onNotifyDeviceUpdate', function(data) {
     // this.authService.pushLogDetails(this.roomId, data);
      console.log('onNotifyDeviceUpdate', data);
      // const deviceName = data.data;
      // if (deviceName === 'EARPIECE') {
      //   // _this.speakerImgSrc = '../../../assets/icon/mute_speaker.png';
      // } else {
      //   // _this.speakerImgSrc = '../../../assets/icon/unmute_speaker.png';
      // }
    });

    window.EnxRtc.switchMediaDevice('SPEAKER_PHONE', function(data) {
      _this.speakerImgSrc = '../../../assets/icon/unmute_speaker.png';
    }, function(err) {
    });
  }

  startDuration() {
    let conf_duration = 0;
    setInterval(function () {
      conf_duration++;
      let minute, second, hour;
      let min = Number(conf_duration / 60);
      let sec = conf_duration % 60;
      let hr = 0;
      if (min > 59) {
        min = min % 60;
        hr = Number(conf_duration / 3600);
      }
      (sec < 10) ? (second = '0' + sec) : (second = '' + sec);
      (min < 10) ? (minute = '0' + min) : (minute = '' + min);
      (hr < 10) ? (hour = '0' + hr) : (hour = '' + hr);
      this.timer = (hr > 0) ? hour + ' : ' : '' + minute + ' : ' + second;
    }, 1000);
  }
  goBackToPriviousPage() {
   this.modalCtrl.dismiss();
  }
  muteUnMuteAudio() {
    if (!this.audioMute) {
      window.EnxRtc.muteSelfAudio(true);
      this.audioimgSrc = '../../../assets/icon/mute_audio.png';
    } else {
      window.EnxRtc.muteSelfAudio(false);
      this.audioimgSrc = '../../../assets/icon/unmute_audio.png';
    }
  }
  muteUnMuteVideo() {
    if (!this.videoMute) {
      window.EnxRtc.muteSelfVideo(true);
    } else {
      window.EnxRtc.muteSelfVideo(false);
    }
  }
  disconnect() {
    if (confirm('Are you want to disconnect ?')) {
      this.goBackToPriviousPage();
      window.EnxRtc.disconnect(false, function(data) {
      }, function(err) {
      });
    } else {
    }
  }
  switchCamera() {
    window.EnxRtc.switchCamera(false, function(data) {
    }, function(err) {
    });
  }
  msgRecieved(data) {

  }
  async sendMsg() {
    const chatUi = document.getElementById('chatUi');
    const messageModal = this.modalCtrl.create({
      component: VideoChatPage,
      cssClass: 'videoChatUI',
      componentProps: this.messagesData
    });
    (await messageModal).present();
    // const data = {
    //   'sender': 'Sender\'s Name',
    //   'message': 'Message body',
    //   'custom_key': 'Data'
    //  }
    const message = 'Message body';
    // window.EnxCordovaPlugin.sendMessage( message, true, []);
  }
  getElapsedTime(entry: Entry): TimeSpan {
    let totalSeconds = Math.floor((new Date().getTime() - entry.created.getTime()) / 1000);
    let hours = 0;
    let minutes = 0;
    let seconds = 0;

    if (totalSeconds >= 3600) {
      hours = Math.floor(totalSeconds / 3600);
      totalSeconds -= 3600 * hours;
    }

    if (totalSeconds >= 60) {
      minutes = Math.floor(totalSeconds / 60);
      totalSeconds -= 60 * minutes;
    }

    seconds = totalSeconds;
    return {
      hours: hours,
      minutes: minutes,
      seconds: seconds
    };
  }
  donotExtend() {
    this.aboutToEnd = true;
    this.timeAlert = false;
    //this.setStandradRemoteView();
  }

  setStandradRemoteView() {
    //var divhiegth = document.getElementById('bottom_link');
    //divhiegth.style.height = '60px';
    const options = {
      margin_top: 60,
      margin_bottom: 120,
      width: 380,
    };
    window.EnxRtc.resizeRemoteView(options, function (data) {
      console.log('extension succuss! ' + JSON.stringify(data.data));
    },function(err) {
      //alert("111 " +JSON.stringify(err));
    });
  }
  forceExtend() {
    this.aboutToEnd = false;
    this.extendMeeting();
  }
  extendMeeting() {
    this.aboutToEnd = false;
    window.EnxRtc.extendConferenceDuration().then(async data => {
     alert('Extended for 30min');
    });
    //this.setStandradRemoteView();
   

  }
  cheangAudioMedia() {
    this.speakerMute = !this.speakerMute;
    window.EnxRtc.getSelectedDevice(function(data) {
      const currentDevice = data.data;
      window.EnxRtc.getDevices(function(data) {
        const connectedMedia = data.data;
        let meida;
        for (meida of connectedMedia) {
          if (meida === currentDevice) {
          } else {
            window.EnxRtc.switchMediaDevice(meida, function(data) {
            }, function(err) {
            });
            break;
          }
        }
      });
    });
  }
}



