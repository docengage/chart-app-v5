import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-video-chat',
  templateUrl: './video-chat.page.html',
  styleUrls: ['./video-chat.page.scss'],
})
export class VideoChatPage implements OnInit {

  message: any;
  constructor(
    private mdlCtrl: ModalController
  ) { }

  ngOnInit() {
  }

  dismiss() {
    this.mdlCtrl.dismiss();
  }

  sendMsg() {

  }
}
