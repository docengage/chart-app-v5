import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-table-columns',
  templateUrl: './table-columns.page.html',
  styleUrls: ['./table-columns.page.scss'],
})
export class TableColumnsPage {

  uhid:string;
  rowElem: any;
  colElem: any;
  sectionName:string;
  fieldLabel:string;
  constructor(public navCtrl: NavController, public viewCtrl: ModalController, public navParams: NavParams) {

    this.uhid = this.navParams.get('uuid');
    this.rowElem = this.navParams.get('rowElem');
    this.colElem = this.navParams.get('colElem');
    this.sectionName = this.navParams.get('sectionName');
    this.fieldLabel = this.navParams.get('fieldLabel');
  }

  isReadonly() {
    return this.isReadonly;   //return true/false
  }

  dismiss(isupdated) {
    const data = {
      rowElem : this.rowElem,
      secCounter: this.navParams.get('secCounter'),
      fieldCounter: this.navParams.get('fieldCounter'),
      rowCounter: this.navParams.get('rowCounter')
    };
    this.viewCtrl.dismiss(data, isupdated);
  }

}
