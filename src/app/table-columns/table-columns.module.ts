import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TableColumnsPageRoutingModule } from './table-columns-routing.module';

import { TableColumnsPage } from './table-columns.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TableColumnsPageRoutingModule
  ],
  declarations: [TableColumnsPage]
})
export class TableColumnsPageModule {}
