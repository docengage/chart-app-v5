import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TableColumnsPage } from './table-columns.page';

const routes: Routes = [
  {
    path: '',
    component: TableColumnsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TableColumnsPageRoutingModule {}
