import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TableColumnsPage } from './table-columns.page';

describe('TableColumnsPage', () => {
  let component: TableColumnsPage;
  let fixture: ComponentFixture<TableColumnsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableColumnsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TableColumnsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
