import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'my-charts',
    loadChildren: () => import('./my-charts/my-charts.module').then( m => m.MyChartsPageModule)
  },
  {
    path: 'schedule',
    loadChildren: () => import('./schedule/schedule.module').then( m => m.SchedulePageModule)
  },
  {
    path: 'all-patient',
    loadChildren: () => import('./all-patient/all-patient.module').then( m => m.AllPatientPageModule)
  },
  {
    path: 'about-provider',
    loadChildren: () => import('./about-provider/about-provider.module').then( m => m.AboutProviderPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'app-settings',
    loadChildren: () => import('./app-settings/app-settings.module').then( m => m.AppSettingsPageModule)
  },
  {
    path: 'patient-time-line',
    loadChildren: () => import('./patient-time-line/patient-time-line.module').then( m => m.PatientTimeLinePageModule)
  },
  {
    path: 'timeline-filter',
    loadChildren: () => import('./timeline-filter/timeline-filter.module').then( m => m.TimelineFilterPageModule)
  },
  {
    path: 'time-line-item-details',
    loadChildren: () => import('./time-line-item-details/time-line-item-details.module').then( m => m.TimeLineItemDetailsPageModule)
  },
  {
    path: 'table-columns',
    loadChildren: () => import('./table-columns/table-columns.module').then( m => m.TableColumnsPageModule)
  },
  {
    path: 'task-item-details',
    loadChildren: () => import('./task-item-details/task-item-details.module').then( m => m.TaskItemDetailsPageModule)
  },
  {
    path: 'patient-charting',
    loadChildren: () => import('./patient-charting/patient-charting.module').then( m => m.PatientChartingPageModule)
  },
  {
    path: 'support',
    loadChildren: () => import('./support/support.module').then( m => m.SupportPageModule)
  },
  {
    path: 'clinical-note',
    loadChildren: () => import('./chartPage/clinical-note/clinical-note.module').then( m => m.ClinicalNotePageModule)
  },
  {
    path: 'add-vital-partial',
    loadChildren: () => import('./chartPage/add-vital-partial/add-vital-partial.module').then( m => m.AddVitalPartialPageModule)
  },
  {
    path: 'unit-select',
    loadChildren: () => import('./chartPage/unit-select/unit-select.module').then( m => m.UnitSelectPageModule)
  },
  {
    path: 'add-work-list',
    loadChildren: () => import('./chartPage/add-work-list/add-work-list.module').then( m => m.AddWorkListPageModule)
  },
  {
    path: 'select-list',
    loadChildren: () => import('./chartPage/select-list/select-list.module').then( m => m.SelectListPageModule)
  },
  {
    path: 'prescription-list',
    loadChildren: () => import('./chartPage/prescription-list/prescription-list.module').then( m => m.PrescriptionListPageModule)
  },
  {
    path: 'add-document',
    loadChildren: () => import('./chartPage/add-document/add-document.module').then( m => m.AddDocumentPageModule)
  },
  {
    path: 'multi-form',
    loadChildren: () => import('./chartPage/multi-form/multi-form.module').then( m => m.MultiFormPageModule)
  },
  {
    path: 'dynamic-form-section',
    loadChildren: () => import('./chartPage/dynamic-form-section/dynamic-form-section.module').then( m => m.DynamicFormSectionPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'tutorial',
    loadChildren: () => import('./tutorial/tutorial.module').then( m => m.TutorialPageModule)
  },
  {
    path: 'chart-page-config',
    loadChildren: () => import('./chartPage/chart-page-config/chart-page-config.module').then( m => m.ChartPageConfigPageModule)
  },
  {
    path: 'join-vc',
    loadChildren: () => import('./videoCall/join-vc/join-vc.module').then( m => m.JoinVcPageModule)
  },
  {
    path: 'video-call',
    loadChildren: () => import('./videoCall/video-call/video-call.module').then( m => m.VideoCallPageModule)
  },
  {
    path: 'leave-list',
    loadChildren: () => import('./leave-list/leave-list.module').then( m => m.LeaveListPageModule)
  },
  {
    path: 'add-leave',
    loadChildren: () => import('./add-leave/add-leave.module').then( m => m.AddLeavePageModule)
  },
  {
    path: 'certificate-list',
    loadChildren: () => import('./medicalCertificate/certificate-list/certificate-list.module').then( m => m.CertificateListPageModule)
  },
  {
    path: 'add-certificate',
    loadChildren: () => import('./medicalCertificate/add-certificate/add-certificate.module').then( m => m.AddCertificatePageModule)
  },
  {
    path: 'video-chat',
    loadChildren: () => import('./videoCall/video-chat/video-chat.module').then( m => m.VideoChatPageModule)
  },
  {
    path: 'prescription-template',
    loadChildren: () => import('./chartPage/prescription-template/prescription-template.module').then( m => m.PrescriptionTemplatePageModule)
  },
  {
    path: 'select-organization',
    loadChildren: () => import('./select-organization/select-organization.module').then( m => m.SelectOrganizationPageModule)
  },
  {
    path: 'add-patient',
    loadChildren: () => import('./add-patient/add-patient.module').then( m => m.AddPatientPageModule)
  },
  {
    path: 'select-provider',
    loadChildren: () => import('./select-provider/select-provider.module').then( m => m.SelectProviderPageModule)
  },
  {
    path: 'list-filter',
    loadChildren: () => import('./list-filter/list-filter.module').then( m => m.ListFilterPageModule)
  },
  {
    path: 'addactivity',
    loadChildren: () => import('./addactivity/addactivity.module').then( m => m.AddactivityPageModule)
  },
  {
    path: 'quick-schedule',
    loadChildren: () => import('./quick-schedule/quick-schedule.module').then( m => m.QuickSchedulePageModule)
  },
  {
    path: 'requested-services',
    loadChildren: () => import('./requested-services/requested-services.module').then( m => m.RequestedServicesPageModule)
  },
  {
    path: 'select-patient',
    loadChildren: () => import('./select-patient/select-patient.module').then( m => m.SelectPatientPageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
