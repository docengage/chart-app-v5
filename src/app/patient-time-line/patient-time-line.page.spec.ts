import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PatientTimeLinePage } from './patient-time-line.page';

describe('PatientTimeLinePage', () => {
  let component: PatientTimeLinePage;
  let fixture: ComponentFixture<PatientTimeLinePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientTimeLinePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PatientTimeLinePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
