import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, ModalController, ActionSheetController, Platform } from '@ionic/angular';

import { AuthService } from '../service/auth.service';
import { Handlebar } from '../components/handlebar';
import { MasterDataService } from '../service/master-data';
import { ToastServiceData } from '../service/toast-service';
import { CameraService } from '../service/camera-service';
import { TimelineFilterPage } from '../timeline-filter/timeline-filter.page';
import { TimeLineItemDetailsPage } from '../time-line-item-details/time-line-item-details.page';
import { LocalStorageService } from '../service/local-storage.service';
import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { TableColumnsPage } from '../table-columns/table-columns.page';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-patient-time-line',
  templateUrl: './patient-time-line.page.html',
  styleUrls: ['./patient-time-line.page.scss'],
})
export class PatientTimeLinePage implements OnInit {

  visibility: string = 'shown';
  patientId: any;
  page: any;
  icons: string[];
  items: Array<any>;
  item1: any[];
  totalItems: String;
  uhid: String;
  apptSec: string = "today";
  patientInfo: any;
  noRecordFound: string;
  totalPages: any;
  currentPage: any;
  includeFilters: any = [];
  apptContent: boolean = false;
  dynFormContent: boolean = false;
  noteContent: boolean = false;
  actvtyContent: boolean = false;
  laborderContent: boolean = false;
  estContent: boolean = false;
  medicationContent: boolean = false;
  vitalContent: boolean = false;
  careplanContent: boolean = false;
  pymtContent: boolean = false;
  invContent: boolean = false;
  fileContent: boolean = false;
  listNo: any;
  cardIndex: number;
  content: any;
  recordName: string = "Patient's data ";
  data0:any = [];
  medData:any = [];
  accessControl: string;
  constructor(
    private router: Router,
    private Actvroute: ActivatedRoute,
    private navCtrl: NavController,
    private authService: AuthService,
    private handlebar: Handlebar,
    private modalCtrl: ModalController,
    private masterData: MasterDataService,
    private cameraService: CameraService,
    private actionSheetCtrl: ActionSheetController,
    private toastServ: ToastServiceData,
    public localStorage: LocalStorageService,
    private file: File,
    private fileOpener: FileOpener,
    private documentViewer: DocumentViewer,
    private platform: Platform,
    private transfer: FileTransfer,
    private photoViewer: PhotoViewer,
    private inAppBrowser: InAppBrowser
  ) { }

  ngOnInit() {
    this.Actvroute.queryParams.subscribe(params => {
      if (params) {
        this.uhid = params.id;
        this.page = params.page;
      }
    });
  }
  
  goBack() {
    this.navCtrl.navigateBack(this.page);
  }
  ionViewDidEnter() {
    this.authService.isTokenValid().then((returnVal) => {
      this.getPatientInfo(this.uhid);

      this.masterData.getTimeLineFilters().then((listFilters) => {
        listFilters.forEach(ele => {
          if (ele.isChecked) {
            this.includeFilters.push(ele.value);
          }
        });
        this.getTimeLineData();
      });
    });

  }

  getTimeLineData() {
    this.items = [];
    this.totalItems = "0";
    this.totalPages = 0;
    this.currentPage = 1;
    this.getPatientTimeLineData(true);
  }
  getPatientInfo(uhid) {
    this.authService.getPatientInfoLight(uhid).then((data) => {
      this.patientInfo = data;
      this.getAccessControl();
    });
  }

  getAccessControl()  {
    this.localStorage.getAccessControl().then((accessObj) => {
      this.accessControl = accessObj;
      this.allowToSeeContactInfo();
    });
  }

  allowToSeeContactInfo() {
    if (this.accessControl['permissions'].allowToSeeContactInfo) {
    } else {
      if (this.patientInfo.telephone) {
        this.patientInfo.telephone = "XXXXXXX"+ this.patientInfo.telephone.substring(this.patientInfo.telephone.length - 3);
      }
      if (this.patientInfo.alternatePhone) {
        this.patientInfo.alternatePhone = "XXXXXXX"+ this.patientInfo.alternatePhone.substring(this.patientInfo.alternatePhone.length - 3);
      }
    }
  }

  async presentFilter() {
    const modal = await this.modalCtrl.create({
      component: TimelineFilterPage,
      componentProps: this.includeFilters
    });
    await modal.present();

    modal.onDidDismiss().then((data) => {
      const isUpdated = data.role;
      if (isUpdated && isUpdated !== 'backdrop') {
        this.includeFilters = data.data;
        this.getTimeLineData();
      }
    });

  }
  checkPatientHasImage(imagePath) {
    return this.handlebar.checkImagePath(imagePath);
  }
  doInfinite(infiniteScroll) {
    var clientHeight = infiniteScroll.target.clientHeight;
    var scrollUpto = infiniteScroll.target.scrollHeight - infiniteScroll.target.scrollTop;
    if (clientHeight === scrollUpto) {
      this.currentPage = Number(this.currentPage) + 1;
      setTimeout(() => {
        setTimeout(() => {
          if (this.currentPage <= this.totalPages) {
            this.getPatientTimeLineData(false);
          }
          infiniteScroll.target.complete();
        }, 500);
      });
    }
  }

  getPatientTimeLineData(checkItemData) {
    if (this.includeFilters && this.includeFilters.length > 0) {
      this.authService.getPatientTimeLineData(this.uhid, this.currentPage, this.includeFilters).then((data) => {
        if(data){
          
          if(checkItemData){
            this.data0 = [];
            this. medData = [];
          }
       
        for (let i = 0; i < data.items.length; i++) {
          let contains = false;

          if (data.items[i].referenceType === "vitals") {
            this.data0.push(data.items[i].itemDetails);
            for (let j = 0; j < this.items.length; j++) {
              if (this.items[j].referenceType === "vitals" && this.items[j].performDateTimeLong === data.items[i].performDateTimeLong) {
                this.items[j].referenceId = this.items[j].referenceId + "," + data.items[i].referenceId;
                contains = true;
              }
            }
            if (this.data0.length > 0) {
              data.items[i].itemDetails = this.data0
            }
          } else if (data.items[i].referenceType === "medication") {
            data.items[i].itemDetails.performDateTimeLong = data.items[i].performDateTimeLong;

            this.medData.push(data.items[i].itemDetails);
            for (let k = 0; k < this.items.length; k++) {
              if (this.items[k].referenceType === "medication" && this.items[k].performDateTimeLong === data.items[i].performDateTimeLong) {
                this.items[k].referenceId = this.items[k].referenceId + "," + data.items[i].referenceId;
                contains = true;
              }
            }
          } else if (data.items[i].referenceType.indexOf("dynamicform-") >= 0) {
            data.items[i].referenceType = "DynamicForm";
            let itemDetails = JSON.parse(data.items[i].itemDetails.FormData);
            if (itemDetails.content != null) {
              itemDetails.FormDataContent = JSON.parse(itemDetails.content);
              itemDetails.content = itemDetails.FormDataContent;
            }
            data.items[i].itemDetails = itemDetails;
          } else if (data.items[i].referenceType === "estimated") {
            this.estContent = !this.estContent;
            if (data.items[i].itemDetails.estimationLineitemData != null) {
              let x = data.items[i].itemDetails.estimationLineitemData.value;
              data.items[i].itemDetails.lineItems = JSON.parse(x);
            }
          } else if (data.items[i].referenceType === "laborder") {

            this.laborderContent = !this.laborderContent;
            if (data.items[i].itemDetails.orderLineitemData != null) {
              data.items[i].itemDetails.lineItems = JSON.parse(data.items[i].itemDetails.orderLineitemData.value);
            }
          }

          if (!contains) {
            this.items.push(data.items[i]);
          }
        }
        for (let j = 0; j < this.items.length; j++) {
          this.items[j].cardId = 'sc_' + this.items[j].referenceId;
          this.items[j]['sc_' + this.items[j].referenceId] = false;

          if (this.items[j].referenceType === 'vitals') {
            var vitalData = [];
            for (let k = 0; k < this.data0.length; k++) {
              if (this.data0.length > 0 && (this.items[j].performDateTime === this.data0[k].effectiveDateTime)) {
                vitalData.push(this.data0[k]);
              }
            }
            this.items[j].itemDetails = vitalData;
          }
          else if (this.items[j].referenceType === "medication" && this.medData.length > 0) {
            var itemData = [];
            for (let k = 0; k < this.medData.length; k++) {
              if (this.items[j].performDateTimeLong === this.medData[k].performDateTimeLong) {
                itemData.push(this.medData[k]);
              }
            }
            this.items[j].itemDetails = itemData;
          }
          else if (this.items[j].referenceType === "invoiced") {
            this.invContent = !this.invContent;
            this.items[j].itemDetails.lineItems = JSON.parse(this.items[j].itemDetails.invoiceLineitemData);
          }
        }
        this.totalPages = data.totalPages;
        this.noRecordFound = this.toastServ.displayNoRecordsFound(data.items, this.recordName);
        if (this.items.length < 15 && this.totalPages > this.currentPage) {
          this.loadMoreData();
        }
       }else {
        this.items = [];
        this.totalPages = 0;
        this.noRecordFound = this.toastServ.displayNoRecordsFound(this.items, this.recordName);
  
       }
      });
    } else {
      this.items = [];
      this.totalPages = 0;
      this.noRecordFound = this.toastServ.displayNoRecordsFound(this.items, this.recordName);
    }


  }

  loadMoreData() {
    this.currentPage = Number(this.currentPage) + 1;
    if (this.currentPage <= this.totalPages) {
      this.getPatientTimeLineData(false);
    }
  }

  getAppointmentData(referenceId, referenceName, item1, index, cardIdx) {
    this.apptContent = !this.apptContent;
    this.item1 = [];
    this.listNo = index;
    this.cardIndex = cardIdx;
    this.authService.getPatientTimeLineItemDetails(referenceId, referenceName).then((data) => {
      this.item1 = data.itemDetails;
    });

  }

  getSelectedData(referenceId, referenceName) {
    this.authService.getPatientTimeLineItemDetails(referenceId, referenceName).then((data) => {
      return data;
    });
  }


  itemSelected(referenceId, referenceName, item1, cardIdx) {
    if (referenceName === "medication") {
      this.medicationContent = !this.medicationContent;
    } else if (referenceName === "vitals") {
      this.vitalContent = !this.vitalContent;
    } else if (referenceName == "activity") {
      this.actvtyContent = !this.actvtyContent;
    } else if (referenceName === "planneditem") {
      this.apptContent = !this.apptContent;
    } else if (referenceName === 'payments') {
      this.pymtContent = !this.pymtContent;
    } else if (referenceName === 'files') {
      this.fileContent = !this.fileContent;
    }

    const selectedId = 'sc_' + referenceId;

    if (selectedId !== this.listNo) {
      const objIndex = this.items.findIndex((obj => obj.cardId == selectedId));
      if (objIndex > -1) {
        this.items[objIndex][selectedId] = true;
      }
      this.item1 = [];
      const oldIndex = this.items.findIndex((obj => obj.cardId == this.listNo));
      if (oldIndex > -1) {
        this.items[oldIndex][this.listNo] = false;
      }
      const data = this.items[objIndex];
      this.listNo = selectedId;
      if (referenceName === "DynamicForm") {
        this.dynFormContent = !this.dynFormContent;
        const itemDetails = data.itemDetails;
        if (itemDetails.content != null) {
          this.content = itemDetails.content;
          this.item1 = itemDetails;
        }
      }
    } else {
      const objIndex = this.items.findIndex((obj => obj.cardId == this.listNo));
      if (objIndex > -1) {
        this.items[objIndex][this.listNo] = !this.items[objIndex][this.listNo];
      }
    }
  }
  openIosFile(url, filename, mimeType) {
    if (mimeType.includes('image')) {
      try {
        this.platform.ready().then((data) => {
          const target = '_blank';
          this.inAppBrowser.create(url, target, 'location=no').show();
          return true;
        }).catch((err) => {
        });
      } catch (error) {
      }
    }

    const path = this.file.dataDirectory + filename;
    const fileTransfer: FileTransferObject = this.transfer.create();

    fileTransfer.download(url, path).then((entry) => {
      const finalUrl = entry.toURL();
      const options: DocumentViewerOptions = {
        title: filename
      };
      this.documentViewer.viewDocument(finalUrl, mimeType, options);
    }).catch((error) => {
      this.toastServ.presentToast('Error view file');
    });
  }
  openFile(url,mimeType,base64,filename){
    const ios = this.platform.is('ios');
    if (ios) {
      this.openIosFile(url, filename, mimeType);
    } else {
      const writeDirectory = this.file.externalDataDirectory;
      this.file.writeFile(writeDirectory, filename, this.convertBaseb64ToBlob(base64, 'data:'+mimeType+';base64'), {replace: true})
        .then(() => {
            this.fileOpener.open(writeDirectory + filename, mimeType)
                .catch((err) => {
                    this.toastServ.presentToast('Error opening file');
                });
        })
        .catch((err) => {
          this.toastServ.presentToast('Error writing file');
      });
    }
}
  convertBaseb64ToBlob(b64Data, contentType): Blob {
    contentType = contentType || '';
    const sliceSize = 512;
    b64Data = b64Data.replace(/^[^,]+,/, '');
    b64Data = b64Data.replace(/\s/g, '');
    const byteCharacters = window.atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    return new Blob(byteArrays, { type: contentType });
  }

  async openColumns(secCounter, fieldCounter, rowCounter) {
    let row = this.content[secCounter].sectionContent[fieldCounter].rowContentValuesList[rowCounter];
    let fieldLabel = this.content[secCounter].sectionContent[fieldCounter].field_label;
    let sectionName = this.content[secCounter].sectionName;
    let colElem = this.content[secCounter].sectionContent[fieldCounter].columnValues;
    let data = {
      "rowElem": row,
      "sectionName": sectionName,
      "fieldLabel": fieldLabel,
      "colElem": colElem,
      "secCounter": secCounter,
      "fieldCounter": fieldCounter,
      "rowCounter": rowCounter,
    }

    let modal = await this.modalCtrl.create({
      component: TableColumnsPage,
      componentProps: data
    });
    await modal.present();
    modal.onDidDismiss().then((data) => {
      if (data) {
        let secCounter = Number(data.data["secCounter"]);
        let fieldCounter = Number(data.data["fieldCounter"]);
        let rowCounter = Number(data.data["rowCounter"]);
        this.content[secCounter].sectionContent[fieldCounter].rowContentValuesList[rowCounter] = data["rowElem"];
      }
    });

  }

}
