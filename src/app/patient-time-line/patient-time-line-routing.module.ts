import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PatientTimeLinePage } from './patient-time-line.page';

const routes: Routes = [
  {
    path: '',
    component: PatientTimeLinePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PatientTimeLinePageRoutingModule {}
