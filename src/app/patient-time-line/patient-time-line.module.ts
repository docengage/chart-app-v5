import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PatientTimeLinePageRoutingModule } from './patient-time-line-routing.module';

import { PatientTimeLinePage } from './patient-time-line.page';
import { PipesModule } from '../pipes/pipes.module';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PatientTimeLinePageRoutingModule,
    PipesModule,
    ComponentsModule
  ],
  declarations: [PatientTimeLinePage]
})
export class PatientTimeLinePageModule {}
