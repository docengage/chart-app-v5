export const stageEnv = {
    production: true,  // this value can be anything - will be replaced during build
    BASE_URL: 'https://stage.docengage.in/', // this value can be anything - will be replaced during build
};